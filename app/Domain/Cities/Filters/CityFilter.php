<?php

namespace App\Domain\Cities\Filters;

use App\Domain\Cities\Structures\CityFilterData;
use App\Services\FilterService\Filter;

class CityFilter extends Filter
{
    /**
     * Доступные фильтры.
     *
     * @var array
     */
    protected $available = [
         'id', 'name', 'sort', 'perPage'
    ];

    protected $translationTable = 'city_translations';

    /**
     * Фильтры по умолчанию.
     *
     * @var array
     */
    protected $defaults = [
        'sort' => '-id'
    ];

    public function __construct(CityFilterData $data)
    {
        $this->input = $this->prepareInput($data->toArray());
    }

    /**
     * Инициализация фильтра.
     */
    protected function init()
    {
        //Добавляем поля для сортировки
        $this->addSortable('id');
        $this->addSortable('name', $this->translationTable);

        $this->addJoin($this->translationTable, function () {
            $this->builder->leftJoin($this->translationTable, function ($join) {
                /**
                 * @var $join \Illuminate\Database\Query\JoinClause
                 */
                $join->on($this->table . '.id', $this->translationTable . '.city_id')->where('locale', \App::getLocale());
            })->select($this->table . '.*');
        });
    }

    /**
     * Поиск по id.
     *
     * @param $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function id($value)
    {
        return $this->builder->where($this->column('id'), $value);
    }

    /**
     * Поиск по имени.
     *
     * @param $value
     * @return \Illuminate\Database\Eloquent\Builder
     */

    public function name($value)
    {
        return $this->builder->whereHas('translations', function ($query) use ($value) {
            /**
             * @var $query \Illuminate\Database\Eloquent\Builder
             */
            $query->where('name', 'like', '%' . $value . '%')
                ->where('locale', \App::getLocale());
        });
    }
}
