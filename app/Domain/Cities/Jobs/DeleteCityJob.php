<?php

namespace App\Domain\Cities\Jobs;

use App\Domain\Cities\Models\City;
use App\Domain\Taverns\Jobs\DeleteTavernJob;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class DeleteCityJob
{
    use Dispatchable, Queueable;

    /**
     * @var City
     */
    private $city;

    /**
     * Create a new job instance.
     *
     * DeleteUserJob constructor.
     * @param City $city
     */
    public function __construct(City $city)
    {
        $this->city = $city;
    }

    /**
     * Execute the job.
     *
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $this->city->delete();
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();
    }
}
