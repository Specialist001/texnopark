<?php

namespace App\Domain\Cities\Jobs;

use App\Domain\Cities\Models\City;
use App\Domain\Cities\Requests\CityRequest;
use App\Domain\Cities\Structures\CityData;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class StoreCityJob
{
    use Dispatchable, Queueable;

    /**
     * @var CityData
     */
    private $data;

    /**
     * @var CityRequest
     */
    private $request;

    /**
     * StoreCityJob constructor.
     * @param CityData $data
     * @param CityRequest $request
     */
    public function __construct(CityData $data, CityRequest $request)
    {
        $this->data = $data;
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return City
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $city = new City();
            $city->save();

            $city->translations()->saveMany($this->data->getTranslations());
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $city;
    }
}
