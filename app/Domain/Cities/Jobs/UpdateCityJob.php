<?php

namespace App\Domain\Cities\Jobs;

use App\Domain\Cities\Models\City;
use App\Domain\Cities\Requests\CityRequest;
use App\Domain\Cities\Structures\CityData;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateCityJob
{
    use Dispatchable, Queueable;

    /**
     * @var CityData
     */
    private $data;

    /**
     * @var City
     */
    private $city;

    /**
     * @var CityRequest
     */
    private $request;

    /**
     * UpdateCityJob constructor.
     * @param City $city
     * @param CityData $data
     * @param CityRequest $request
     */
    public function __construct(City $city, CityData $data, CityRequest $request)
    {
        $this->data = $data;
        $this->city = $city;
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return City
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $city = $this->city;
            $city->save();

            $city->translations()->delete();
            $city->translations()->saveMany($this->data->getTranslations());

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $city;
    }
}
