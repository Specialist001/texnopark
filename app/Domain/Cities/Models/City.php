<?php
namespace App\Domain\Cities\Models;

use App\Domain\Taverns\Models\Tavern;
use App\Services\FilterService\Filterable;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\UploadedFile;

/**
 * App\Domain\Cities\Models\City
 *
 * @property int $id
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Cities\Models\CityTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\City filter(\App\Services\FilterService\Filter $filters)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\City listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\City newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\City newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\City notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\Cities\Models\City onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\City orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\City orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\City orderByTranslation($key, $sortmethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\City paginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\City query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\City simplePaginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\City translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\City translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\City whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\City whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\City whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\City whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\City withTranslation()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\Cities\Models\City withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\Cities\Models\City withoutTrashed()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Taverns\Models\Tavern[] $taverns
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\City withTaverns()
 * @property string|null $image
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\City whereImage($value)
 */
class City extends Model
{
    use Filterable, SoftDeletes, Translatable;

    public $translatedAttributes = ['name'];

    public $timestamps = false;
    protected $guarded = ['id'];
}
