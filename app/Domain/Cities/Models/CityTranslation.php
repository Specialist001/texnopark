<?php
namespace App\Domain\Cities\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\Cities\Models\CityTranslation
 *
 * @property int $id
 * @property int $city_id
 * @property string|null $name
 * @property string $locale
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\CityTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\CityTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\CityTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\CityTranslation whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\CityTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\CityTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Cities\Models\CityTranslation whereName($value)
 * @mixin \Eloquent
 */
class CityTranslation extends Model
{
    public $timestamps = false;
    protected $guarded = ['id'];
}
