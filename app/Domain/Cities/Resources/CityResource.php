<?php

namespace App\Domain\Cities\Resources;

use App\Domain\Cities\Models\City;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class UserResource
 * @mixin City
 */
class CityResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }
}
