<?php

namespace App\Domain\Cities\Structures;

use App\Domain\Cities\Models\CityTranslation;

/**
 * Class CityData
 */
class CityData
{
    /**
     * @var array
     */
    private $translations;

    /**
     * CityData constructor.
     * @param array $translations
     */
    public function __construct(array $translations)
    {
        $this->setTranslations($translations);
    }

    /**
     * @param array $input
     * @return CityData
     */
    public static function fill(array $input): CityData
    {
        return new self(
            $input['translations']
        );
    }

    /**
     * @return array
     */
    public function getTranslations(): array
    {
        return $this->translations;
    }

    /**
     * @param array $translations
     */
    public function setTranslations(array $translations): void
    {
        foreach ($translations as $translate) {
            if($translate['name'] == '') continue;
            $this->translations[] = new CityTranslation($translate);
        }
    }
}
