<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 11.03.2019
 * Time: 15:53
 */

namespace App\Domain\Forum\Filters;


use App\Services\FilterService\Filter;
use Illuminate\Http\Request;

class ForumThemeFilter extends Filter
{
    protected $available = [
        'title',
        'user_id',
        'updated_at',
        'status',
        'sort', 'perPage'
    ];

    protected $defaults = [
        'sort' => '-updated_at',
    ];

    public function __construct(Request $request)
    {
        $this->input = $this->prepareInput($request->all());
    }

    protected function init()
    {
        //Добавляем поля для сортировки
        $this->addSortable('id');
        $this->addSortable('updated_at');
    }

    public function title($value)
    {
        return $this->builder->where($this->column('title'), 'like', '%'.$value.'%');
    }

    public function user_id($value)
    {
        return $this->builder->where($this->column('user_id'), $value);
    }

    public function status($value)
    {
        return $this->builder->where($this->column('status'), $value);
    }
}
