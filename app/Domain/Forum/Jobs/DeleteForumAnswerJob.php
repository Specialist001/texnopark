<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 11.03.2019
 * Time: 17:17
 */

namespace App\Domain\Forum\Jobs;


use App\Domain\Forum\Models\ForumAnswer;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class DeleteForumAnswerJob
{
    use Queueable, Dispatchable;


    /**
     * @var ForumAnswer
     */
    public $forumAnswer;

    /**
     * DeleteForumAnswerJob constructor.
     * @param ForumAnswer $forumAnswer
     */
    public function __construct(ForumAnswer $forumAnswer)
    {
        $this->forumAnswer = $forumAnswer;
    }

    /**
     * Execute the job.
     *
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $this->forumAnswer->deleteFile();
            $this->forumAnswer->delete();
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();
    }
}
