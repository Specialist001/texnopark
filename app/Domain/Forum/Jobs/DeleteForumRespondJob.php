<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 11.03.2019
 * Time: 17:17
 */

namespace App\Domain\Forum\Jobs;


use App\Domain\Forum\Models\ForumRespond;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class DeleteForumRespondJob
{
    use Queueable, Dispatchable;


    /**
     * @var ForumRespond
     */
    public $respond;

    /**
     * DeleteForumAnswerJob constructor.
     * @param ForumRespond $respond
     */
    public function __construct(ForumRespond $respond)
    {
        $this->respond = $respond;
    }

    /**
     * Execute the job.
     *
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $this->respond->delete();
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();
    }
}
