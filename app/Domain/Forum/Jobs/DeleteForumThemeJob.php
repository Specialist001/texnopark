<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 11.03.2019
 * Time: 17:17
 */

namespace App\Domain\Forum\Jobs;


use App\Domain\Forum\Models\ForumTheme;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class DeleteForumThemeJob
{
    use Queueable, Dispatchable;


    /**
     * @var ForumTheme
     */
    public $forumTheme;

    /**
     * DeleteForumAnswerJob constructor.
     * @param ForumTheme $forumTheme
     */
    public function __construct(ForumTheme $forumTheme)
    {
        $this->forumTheme = $forumTheme;
    }

    /**
     * Execute the job.
     *
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            foreach ($this->forumTheme->answers as $answer) {
                dispatch_now(new DeleteForumAnswerJob($answer));
            }
            foreach ($this->forumTheme->responds as $respond) {
                dispatch_now(new DeleteForumRespondJob($respond));
            }
            $this->forumTheme->deleteFile();
            $this->forumTheme->delete();
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();
    }
}
