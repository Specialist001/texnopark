<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 11.03.2019
 * Time: 16:20
 */

namespace App\Domain\Forum\Jobs;


use App\Domain\Forum\Models\ForumAnswer;
use App\Domain\Forum\Models\ForumTheme;
use App\Domain\Forum\Requests\ForumAnswerRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class StoreForumAnswerJob
{
    use Queueable, Dispatchable;

    /**
     * @var ForumAnswerRequest
     */
    private $request;

    /**
     * @var ForumTheme
     */
    private $theme;

    /**
     * StoreForumAnswerJob constructor.
     * @param ForumTheme $theme
     * @param ForumAnswerRequest $request
     */
    public function __construct(ForumTheme $theme, ForumAnswerRequest $request)
    {
        $this->request = $request;
        $this->theme = $theme;
    }

    /**
     * Execute the job.
     *
     * @return ForumAnswer
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $forumAnswer = new ForumAnswer();

            if (!\Auth::check()) {
                $forumAnswer->user_name = $this->request->input('user_name');
            } else {
                if ($this->request->user()->isInvestor() || $this->request->user()->isStartUp()) {
                    $forumAnswer->is_moderated = 0;
                } else {
                    $forumAnswer->is_moderated = 1;
                }
                $forumAnswer->user_id = $this->request->user()->id;
            }
            $forumAnswer->text = $this->request->input('text');
            $forumAnswer->file = null;

            $this->theme->answers()->save($forumAnswer);


            if ($this->request->hasFile('file')) {
                $forumAnswer->file = $forumAnswer->uploadFile($this->request->file('file'));
            }

            $forumAnswer->save();

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $forumAnswer;
    }
}
