<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 11.03.2019
 * Time: 16:20
 */

namespace App\Domain\Forum\Jobs;


use App\Domain\Forum\Models\ForumRespond;
use App\Domain\Forum\Models\ForumTheme;
use App\Domain\Forum\Requests\ForumRespondRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class StoreForumRespondJob
{
    use Queueable, Dispatchable;

    /**
     * @var ForumRespondRequest
     */
    private $request;

    /**
     * @var ForumTheme
     */
    private $theme;

    /**
     * StoreForumAnswerJob constructor.
     * @param ForumTheme $theme
     * @param ForumRespondRequest $request
     */
    public function __construct(ForumTheme $theme, ForumRespondRequest $request)
    {
        $this->request = $request;
        $this->theme = $theme;
    }

    /**
     * Execute the job.
     *
     * @return ForumRespond
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $respond = new ForumRespond();

            $respond->name = $this->request->input('name');
            $respond->phone = $this->request->input('phone', null);
            $respond->email = $this->request->input('email', null);
            $respond->comment = $this->request->input('comment', null);
            $respond->is_moderated = 0;

            $this->theme->responds()->save($respond);

            $respond->save();

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $respond;
    }
}
