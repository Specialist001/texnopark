<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 11.03.2019
 * Time: 16:20
 */

namespace App\Domain\Forum\Jobs;


use App\Domain\Forum\Models\ForumTheme;
use App\Domain\Forum\Requests\ForumThemeRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class StoreForumThemeJob
{
    use Queueable, Dispatchable;

    /**
     * @var ForumThemeRequest
     */
    private $request;

    /**
     * StoreForumTheme constructor.
     * @param ForumThemeRequest $request
     */
    public function __construct(ForumThemeRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return ForumTheme
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $forumTheme = new ForumTheme();

            $forumTheme->user_id = $this->request->user()->id;
            $forumTheme->title = $this->request->input('title');
            $forumTheme->text = $this->request->input('text');

            $forumTheme->organization_title = $this->request->input('organization_title', null);
            $forumTheme->category = $this->request->input('category', null);
            $forumTheme->required_solution = $this->request->input('required_solution', null);
            $forumTheme->expected_effect = $this->request->input('expected_effect', null);
            $forumTheme->required_organization = $this->request->input('required_organization', null);
            $forumTheme->contacts = $this->request->input('contacts', null);

            $forumTheme->file = null;

            if ($this->request->user()->isInvestor() || $this->request->user()->isStartUp()) {
                $forumTheme->status = ForumTheme::STATUS_MODERATING;
                $forumTheme->is_moderated = 0;
            } else {
                $forumTheme->status = $this->request->input('status');
                $forumTheme->is_moderated = 1;
            }

            $forumTheme->view = 0;

            $forumTheme->save();


            if ($this->request->hasFile('file')) {
                $forumTheme->file = $forumTheme->uploadFile($this->request->file('file'));
            }

            $forumTheme->save();

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $forumTheme;
    }
}
