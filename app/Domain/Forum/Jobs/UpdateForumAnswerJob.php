<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 11.03.2019
 * Time: 17:12
 */

namespace App\Domain\Forum\Jobs;

use App\Domain\Forum\Models\ForumAnswer;
use App\Domain\Forum\Requests\ForumAnswerRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateForumAnswerJob
{
    use Queueable, Dispatchable;

    /**
     * @var ForumAnswerRequest
     */
    private $request;

    /**
     * @var ForumAnswer
     */
    private $forumAnswer;

    /**
     * UpdateForumAnswerJob constructor.
     * @param ForumAnswer $forumTheme
     * @param ForumAnswerRequest $request
     */
    public function __construct(ForumAnswer $forumTheme, ForumAnswerRequest $request)
    {
        $this->forumAnswer = $forumTheme;
        $this->request = $request;
    }


    /**
     * @return ForumAnswer
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $forumAnswer = $this->forumAnswer;
            $forumAnswer->text = $this->request->input('text');

            if (!\Auth::check()) {
                $forumAnswer->user_name = $this->request->input('user_name');
            } else {
                if ($this->request->user()->isInvestor() || $this->request->user()->isStartUp()) {
                    $forumAnswer->is_moderated = 0;
                }
            }

            $forumAnswer->save();

            if ($this->request->hasFile('file')) {
                $forumAnswer->deleteFile();
                $forumAnswer->file = $forumAnswer->uploadFile($this->request->file('file'));
            }

            $forumAnswer->save();

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $forumAnswer;
    }
}
