<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 11.03.2019
 * Time: 17:12
 */

namespace App\Domain\Forum\Jobs;

use App\Domain\Forum\Models\ForumTheme;
use App\Domain\Forum\Requests\ForumThemeRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateForumThemeJob
{
    use Queueable, Dispatchable;

    private $request;

    private $forumTheme;

    public function __construct(ForumTheme $forumTheme, ForumThemeRequest $request)
    {
        $this->forumTheme = $forumTheme;
        $this->request = $request;
    }


    /**
     * @return ForumTheme
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $forumTheme = $this->forumTheme;
            $forumTheme->title = $this->request->input('title');
            $forumTheme->text = $this->request->input('text');

            $forumTheme->organization_title = $this->request->input('organization_title', null);
            $forumTheme->category = $this->request->input('category', null);
            $forumTheme->required_solution = $this->request->input('required_solution', null);
            $forumTheme->expected_effect = $this->request->input('expected_effect', null);
            $forumTheme->required_organization = $this->request->input('required_organization', null);
            $forumTheme->contacts = $this->request->input('contacts', null);

            if ($this->request->user()->isInvestor() || $this->request->user()->isStartUp()) {
                $forumTheme->status = ForumTheme::STATUS_MODERATING;
                $forumTheme->is_moderated = 1;
            } else {
                $forumTheme->status = $this->request->input('status');
            }

            $forumTheme->save();

            if ($this->request->hasFile('file')) {
                $forumTheme->deleteFile();
                $forumTheme->file = $forumTheme->uploadFile($this->request->file('file'));
            }

            $forumTheme->save();

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $forumTheme;
    }
}
