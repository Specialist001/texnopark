<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 11.03.2019
 * Time: 15:52
 */

namespace App\Domain\Forum\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\UploadedFile;

/**
 * App\Domain\Forum\Models\ForumAnswer
 *
 * @property int $id
 * @property int $forum_theme_id
 * @property int|null $user_id
 * @property string|null $user_name
 * @property string $text
 * @property string|null $file
 * @property int|null $is_moderated
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumAnswer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumAnswer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumAnswer query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumAnswer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumAnswer whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumAnswer whereForumThemeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumAnswer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumAnswer whereIsModerated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumAnswer whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumAnswer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumAnswer whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumAnswer whereUserName($value)
 * @mixin \Eloquent
 * @property string|null $deleted_at
 * @property-read \Illuminate\Foundation\Auth\User|null $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\Forum\Models\ForumAnswer onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumAnswer whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\Forum\Models\ForumAnswer withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\Forum\Models\ForumAnswer withoutTrashed()
 */
class ForumAnswer extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    protected static $filePath = 'uploads/forum/';

    public static function getFilePath()
    {
        return self::$filePath;
    }

    public function fileUrl()
    {
        if($this->file) {
            return asset(self::getFilePath().$this->file);
        }
        return null;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function uploadFile(UploadedFile $image)
    {
        $extension = $image->getClientOriginalExtension();
        $filename = $this->id.'_'.uniqid().'.'.$extension;
        $image->move(public_path(self::getFilePath()), $filename);
        return $filename;
    }

    public function deleteFile()
    {
        $imagePath = public_path(self::getFilePath().$this->file);
        if ($this->file != '' && file_exists($imagePath)) {
            unlink($imagePath);
        }
        $this->file = null;
    }
}
