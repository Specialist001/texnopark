<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 14.03.2019
 * Time: 13:15
 */

namespace App\Domain\Forum\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Domain\Forum\Models\ForumRespond
 *
 * @property int $id
 * @property int $forum_theme_id
 * @property string $name
 * @property string|null $phone
 * @property string|null $email
 * @property string|null $comment
 * @property int|null $is_moderated
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumRespond filter(\App\Services\FilterService\Filter $filters)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumRespond newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumRespond newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\Forum\Models\ForumRespond onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumRespond paginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumRespond query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumRespond simplePaginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumRespond whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumRespond whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumRespond whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumRespond whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumRespond whereForumThemeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumRespond whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumRespond whereIsModerated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumRespond whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumRespond wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumRespond whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\Forum\Models\ForumRespond withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\Forum\Models\ForumRespond withoutTrashed()
 * @mixin \Eloquent
 * @property-read \App\Domain\Forum\Models\ForumTheme $theme
 */
class ForumRespond extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    public function theme()
    {
        return $this->belongsTo(ForumTheme::class, 'forum_theme_id');
    }


}
