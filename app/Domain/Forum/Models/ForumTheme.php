<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 11.03.2019
 * Time: 15:51
 */

namespace App\Domain\Forum\Models;


use App\Domain\Users\Models\User;
use App\Services\FilterService\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\UploadedFile;

/**
 * App\Domain\Forum\Models\ForumTheme
 *
 * @property int $id
 * @property int $user_id
 * @property string $text
 * @property string|null $file
 * @property string $status
 * @property int $view
 * @property int|null $is_moderated
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Forum\Models\ForumAnswer[] $answers
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumTheme filter(\App\Services\FilterService\Filter $filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumTheme newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumTheme newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumTheme paginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumTheme query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumTheme simplePaginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumTheme whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumTheme whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumTheme whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumTheme whereIsModerated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumTheme whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumTheme whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumTheme whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumTheme whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumTheme whereView($value)
 * @mixin \Eloquent
 * @property string $title
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumTheme whereTitle($value)
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Forum\Models\ForumAnswer[] $newAnswers
 * @property-read \App\Domain\Users\Models\User $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\Forum\Models\ForumTheme onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumTheme whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\Forum\Models\ForumTheme withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\Forum\Models\ForumTheme withoutTrashed()
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Forum\Models\ForumRespond[] $responds
 * @property string|null $organization_title
 * @property string|null $category
 * @property string|null $required_solution
 * @property string|null $expected_effect
 * @property string|null $required_organization
 * @property string|null $contacts
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumTheme whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumTheme whereContacts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumTheme whereExpectedEffect($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumTheme whereOrganizationTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumTheme whereRequiredOrganization($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Forum\Models\ForumTheme whereRequiredSolution($value)
 */
class ForumTheme extends Model
{
    use Filterable, SoftDeletes;

    const STATUS_MODERATING = 'moderating';
    const STATUS_PUBLISHED = 'published';
    const STATUS_BLOCKED = 'blocked';

    protected $guarded = ['id'];

    protected static $filePath = 'uploads/forum/';

    public static function statuses()
    {
        return [
            static::STATUS_MODERATING,
            static::STATUS_PUBLISHED,
            static::STATUS_BLOCKED,
        ];
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function answers()
    {
        return $this->hasMany(ForumAnswer::class);
    }

    public function responds()
    {
        return $this->hasMany(ForumRespond::class);
    }

    public function newAnswers()
    {
        return $this->hasMany(ForumAnswer::class)->where('is_moderated', 0);
    }


    public static function getFilePath()
    {
        return self::$filePath;
    }

    public function fileUrl()
    {
        if($this->file) {
            return asset(self::getFilePath().$this->file);
        }
        return null;
    }

    public function uploadFile(UploadedFile $image)
    {
        $extension = $image->getClientOriginalExtension();
        $filename = $this->id.'_'.uniqid().'.'.$extension;
        $image->move(public_path(self::getFilePath()), $filename);
        return $filename;
    }

    public function deleteFile()
    {
        $imagePath = public_path(self::getFilePath().$this->file);
        if ($this->file != '' && file_exists($imagePath)) {
            unlink($imagePath);
        }
        $this->file = null;
    }
}
