<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 11.03.2019
 * Time: 15:56
 */

namespace App\Domain\Forum\Requests;


use App\Domain\Forum\Models\ForumTheme;
use Illuminate\Foundation\Http\FormRequest;

class ForumAnswerRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $data = [
            'text' => 'required|string',
            'file' => 'nullable|file|max:10240',
        ];

        if(!\Auth::check()) {
            $data['user_name'] = 'required|string|max:255';
        }

        return $data;
    }
}
