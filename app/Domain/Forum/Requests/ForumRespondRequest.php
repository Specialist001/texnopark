<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 11.03.2019
 * Time: 15:56
 */

namespace App\Domain\Forum\Requests;


use App\Domain\Forum\Models\ForumTheme;
use Illuminate\Foundation\Http\FormRequest;

class ForumRespondRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $data = [
            'name' => 'required|string|max:255',
            'phone' => 'required_without:email|string|max:255',
            'email' => 'required_without:phone|string|max:255',
            'comment' => 'nullable|string',
        ];

        return $data;
    }
}
