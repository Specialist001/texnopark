<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 11.03.2019
 * Time: 15:56
 */

namespace App\Domain\Forum\Requests;


use App\Domain\Forum\Models\ForumTheme;
use Illuminate\Foundation\Http\FormRequest;

class ForumThemeRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $data = [
            'title' => 'required|string|max:255',
            'text' => 'required|string',

            'organization_title' => 'nullable|string',
            'category' => 'nullable|string',
            'required_solution' => 'nullable|string',
            'expected_effect' => 'nullable|string',
            'required_organization' => 'nullable|string',
            'contacts' => 'nullable|string',

            'file' => 'nullable|file|max:10240',
        ];

        if(!$this->user()->isInvestor() && !$this->user()->isStartUp()) {
            $data['status'] = 'required|in:'.implode(',', ForumTheme::statuses());
        }

        return $data;
    }
}
