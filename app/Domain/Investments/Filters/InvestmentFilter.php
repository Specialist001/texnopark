<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 09.03.2019
 * Time: 13:06
 */

namespace App\Domain\Investments\Filters;


use App\Services\FilterService\Filter;
use Illuminate\Http\Request;

class InvestmentFilter extends Filter
{
    protected $available = [
        'id',
        'sum',
        'project_id',
        'user_id',
        'updated_at',
        'status',
        'sort', 'perPage'
    ];

    protected $defaults = [
        'sort' => '-updated_at',
    ];

    public function __construct(Request $request)
    {
        $this->input = $this->prepareInput($request->all());
    }

    protected function init()
    {
        //Добавляем поля для сортировки
        $this->addSortable('id');
        $this->addSortable('project_id');
        $this->addSortable('sum');
        $this->addSortable('updated_at');
    }

    public function id($value)
    {
        return $this->builder->where($this->column('id'), $value);
    }

    public function project_id($value)
    {
        return $this->builder->where($this->column('project_id'), $value);
    }

    public function user_id($value)
    {
        return $this->builder->where($this->column('user_id'), $value);
    }

    public function sum($value)
    {
        return $this->builder->where($this->column('sum'), $value);
    }

    public function status($value)
    {
        return $this->builder->where($this->column('status'), $value);
    }
}
