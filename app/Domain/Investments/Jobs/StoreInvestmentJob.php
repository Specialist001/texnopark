<?php

namespace App\Domain\Investments\Jobs;

use App\Domain\Investments\Models\Investment;
use App\Domain\Investments\Requests\InvestmentRequest;
use App\Domain\Users\Models\UserNotifications;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class StoreInvestmentJob
{
    use Dispatchable, Queueable;

    /**
     * @var InvestmentRequest
     */
    private $request;

    /**
     * StoreProjectCategoryJob constructor.
     * @param InvestmentRequest $request
     */
    public function __construct(InvestmentRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return Investment
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $investment = new Investment();

            $investment->project_id = $this->request->input('project_id');

            if ($this->request->user()->isInvestor()) {
                $investment->user_id = $this->request->user()->id;
                $investment->is_moderated = 0;
                $investment->status = Investment::STATUS_MODERATING;
            } else {
                $investment->user_id = $this->request->input('user_id');
                $investment->is_moderated = 1;
                $investment->status = $this->request->input('status');
            }

            $investment->sum = $this->request->input('sum');

            $investment->save();

            if($investment->status == Investment::STATUS_OK) {
                UserNotifications::create([
                    'user_id' => $investment->user_id,
                    'project_id' => $investment->id,
                    'action' => 'portal.notifications_type.investment.ok',
                    'viewed' => 0,
                ]);
                UserNotifications::create([
                    'user_id' => $investment->project->user_id,
                    'project_id' => $investment->project->id,
                    'action' => 'portal.notifications_type.project.invested',
                    'viewed' => 0,
                ]);
            }
            if($investment->status == Investment::STATUS_CANCEL) {
                UserNotifications::create([
                    'user_id' => $investment->user_id,
                    'project_id' => $investment->id,
                    'action' => 'portal.notifications_type.investment.cancel',
                    'viewed' => 0,
                ]);
            }

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $investment;
    }
}
