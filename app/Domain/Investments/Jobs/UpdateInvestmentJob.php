<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 01.03.2019
 * Time: 12:39
 */

namespace App\Domain\Investments\Jobs;

use App\Domain\Investments\Models\Investment;
use App\Domain\Investments\Requests\InvestmentRequest;
use App\Domain\Users\Models\UserNotifications;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateInvestmentJob
{
    use Queueable, Dispatchable;

    /**
     * @var InvestmentRequest
     */
    private $request;

    /**
     * @var Investment
     */
    private $investment;

    /**
     * UpdateProjectJob constructor.
     * @param Investment $investment
     * @param InvestmentRequest $request
     */
    public function __construct(Investment $investment, InvestmentRequest $request)
    {
        $this->investment = $investment;
        $this->request = $request;
    }

    /**
     * @return Investment
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $investment = $this->investment;
            $old_status = $this->investment->status;
            if (!$this->request->user()->isInvestor()) {
                $investment->project_id = $this->request->input('project_id');
                $investment->user_id = $this->request->input('user_id');
                $investment->status = $this->request->input('status');
            }
            if ($this->request->user()->isInvestor()) {
                $investment->is_moderated = 0;
            }

            $investment->sum = $this->request->input('sum');
            $investment->save();

            if ($old_status != $investment->status) {
                if($investment->status == Investment::STATUS_OK) {
                    UserNotifications::create([
                        'user_id' => $investment->user_id,
                        'project_id' => $investment->id,
                        'action' => 'portal.notifications_type.investment.ok',
                        'viewed' => 0,
                    ]);
                    UserNotifications::create([
                        'user_id' => $investment->project->user_id,
                        'project_id' => $investment->project->id,
                        'action' => 'portal.notifications_type.project.invested',
                        'viewed' => 0,
                    ]);
                }

                if($investment->status == Investment::STATUS_CANCEL) {
                    UserNotifications::create([
                        'user_id' => $investment->user_id,
                        'project_id' => $investment->id,
                        'action' => 'portal.notifications_type.investment.cancel',
                        'viewed' => 0,
                    ]);
                }
            }

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $investment;
    }
}
