<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 09.03.2019
 * Time: 12:28
 */

namespace App\Domain\Investments\Models;


use App\Domain\Projects\Models\Project;
use App\Domain\Users\Models\User;
use App\Services\FilterService\Filterable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Domain\Investments\Models\Investment
 *
 * @property int $id
 * @property int $project_id
 * @property string $sum
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Domain\Projects\Models\Project $project
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Investments\Models\Investment filter(\App\Services\FilterService\Filter $filters)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Investments\Models\Investment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Investments\Models\Investment newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\Investments\Models\Investment onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Investments\Models\Investment paginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Investments\Models\Investment query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Investments\Models\Investment simplePaginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Investments\Models\Investment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Investments\Models\Investment whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Investments\Models\Investment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Investments\Models\Investment whereProjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Investments\Models\Investment whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Investments\Models\Investment whereSum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Investments\Models\Investment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Investments\Models\Investment withProject()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\Investments\Models\Investment withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\Investments\Models\Investment withoutTrashed()
 * @mixin \Eloquent
 * @property int $user_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Investments\Models\Investment whereUserId($value)
 * @property-read \App\Domain\Users\Models\User $user
 * @property int|null $is_moderated
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Investments\Models\Investment whereIsModerated($value)
 */
class Investment extends Model
{
    use SoftDeletes, Filterable;

    const STATUS_MODERATING = 'moderating';
    const STATUS_OK = 'ok';
    const STATUS_CANCEL = 'cancel';

    protected $guarded = ['id'];

    public static function statuses()
    {
        return [
            static::STATUS_MODERATING,
            static::STATUS_OK,
            static::STATUS_CANCEL,
        ];
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param $query Builder
     * @return Builder
     */
    public function scopeWithProject($query)
    {
        return $query->with(['project' => function ($query){
            /**
             * @var $query Builder|Project
             */
            $query->withTranslation();
            $query->withCategory();
            $query->withType();
        }]);
    }

    public function canUpdate()
    {
        return $this->status == static::STATUS_MODERATING;
    }

    public function canDelete()
    {
        return $this->status == static::STATUS_MODERATING;
    }
}
