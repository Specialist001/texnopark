<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 09.03.2019
 * Time: 13:09
 */

namespace App\Domain\Investments\Requests;


use App\Domain\Investments\Models\Investment;
use App\Domain\Users\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class InvestmentRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $data = [
            'sum' => 'required|integer|min:1',
        ];

        if(!$this->user()->isInvestor()) {
            $data['user_id'] = 'required|exists:users,id,role,'.User::ROLE_INVESTOR;
            $data['project_id'] = 'required|exists:projects,id';
            $data['status'] = 'required|in:'.implode(',', Investment::statuses());
        }

        return $data;
    }
}
