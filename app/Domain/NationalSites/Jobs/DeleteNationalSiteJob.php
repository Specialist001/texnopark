<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 25.03.2019
 * Time: 12:27
 */

namespace App\Domain\NationalSites\Jobs;


use App\Domain\NationalSites\Models\NationalSite;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class DeleteNationalSiteJob
{
    use Queueable, Dispatchable;

    /**
     * @var NationalSite
     */
    protected $nationalSite;

    /**
     * DeleteNationalSiteJob constructor.
     * @param NationalSite $nationalSite
     */
    public function __construct(NationalSite $nationalSite)
    {
        $this->nationalSite = $nationalSite;
    }


    /**
     * Execute the job.
     *
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $this->nationalSite->translations()->delete();
            $this->nationalSite->deleteIcon();
            $this->nationalSite->delete();
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();
    }
}
