<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 25.03.2019
 * Time: 12:21
 */

namespace App\Domain\NationalSites\Jobs;


use App\Domain\NationalSites\Models\NationalSite;
use App\Domain\NationalSites\Models\NationalSiteTranslation;
use App\Domain\NationalSites\Requests\NationalSitesRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class StoreNationalSiteJob
{
    use Queueable, Dispatchable;

    /**
     * @var NationalSitesRequest
     */
    protected $request;

    /**
     * StoreNationalSiteJob constructor.
     * @param NationalSitesRequest $request
     */
    public function __construct(NationalSitesRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return NationalSite
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $nationalSite = new NationalSite();
            $nationalSite->icon = null;
            $nationalSite->order = $this->request->input('order', 0);
            $nationalSite->link = $this->request->input('link', null);
            $nationalSite->type = $this->request->input('type');

            $nationalSite->save();

            $translations = [];
            foreach ($this->request->input('translations', []) as $translate) {
                if ($translate['name'] == '') {
                    continue;
                }
                $translations[] = new NationalSiteTranslation($translate);
            }

            if (!empty($translations)) {
                $nationalSite->translations()->saveMany($translations);
            }

            if ($this->request->hasFile('icon')) {
                $nationalSite->icon = $nationalSite->uploadIcon($this->request->file('icon'));
            } else {
                $nationalSite->icon = null;
            }
            $nationalSite->save();

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $nationalSite;
    }
}
