<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 25.03.2019
 * Time: 12:24
 */

namespace App\Domain\NationalSites\Jobs;


use App\Domain\NationalSites\Models\NationalSite;
use App\Domain\NationalSites\Models\NationalSiteTranslation;
use App\Domain\NationalSites\Requests\NationalSitesRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateNationalSiteJob
{
    use Queueable, Dispatchable;

    /**
     * @var NationalSitesRequest
     */
    protected $request;

    /**
     * @var NationalSite
     */
    protected $nationalSite;

    /**
     * UpdateNationalSiteJob constructor.
     * @param NationalSite $nationalSite
     * @param NationalSitesRequest $request
     */
    public function __construct(NationalSite $nationalSite, NationalSitesRequest $request)
    {
        $this->nationalSite = $nationalSite;
        $this->request = $request;
    }

    /**
     * @return NationalSite
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $nationalSite = $this->nationalSite;

            $nationalSite->order = $this->request->input('order', 0);
            $nationalSite->link = $this->request->input('link', null);

            $nationalSite->save();

            $nationalSite->translations()->delete();
            $translations = [];
            foreach ($this->request->input('translations', []) as $translate) {
                if ($translate['name'] == '') {
                    continue;
                }
                $translations[] = new NationalSiteTranslation($translate);
            }

            if (!empty($translations)) {
                $nationalSite->translations()->saveMany($translations);
            }

            if ($this->request->hasFile('icon')) {
                $nationalSite->deleteIcon();
                $nationalSite->icon = $nationalSite->uploadIcon($this->request->file('icon'));
            }

            $nationalSite->save();

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $nationalSite;
    }
}
