<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 25.03.2019
 * Time: 12:03
 */

namespace App\Domain\NationalSites\Models;


use App\Services\FilterService\Filterable;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

/**
 * App\Domain\NationalSites\Models\NationalSite
 *
 * @property int $id
 * @property string|null $icon
 * @property int|null $order
 * @property string|null $link
 * @property string|null $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\NationalSites\Models\NationalSiteTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite filter(\App\Services\FilterService\Filter $filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite orderByTranslation($key, $sortmethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite paginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite partners()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite simplePaginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite sites()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSite withTranslation()
 * @mixin \Eloquent
 */
class NationalSite extends Model
{
    use Translatable, Filterable;

    const TYPE_SITE = 'site';
    const TYPE_PARTNER = 'partner';

    protected $guarded = ['id'];

    public $translatedAttributes = ['name'];

    protected static $iconPath = 'uploads/sites/';


    public static function types()
    {
        return [
            static::TYPE_SITE,
            static::TYPE_PARTNER,
        ];
    }

    public function isSite()
    {
        return $this->type === static::TYPE_SITE;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSites($query)
    {
        return $query->where('type', static::TYPE_SITE);
    }

    public function isPartner()
    {
        return $this->type === static::TYPE_PARTNER;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePartners($query)
    {
        return $query->where('type', static::TYPE_PARTNER);
    }


    public static function getIconPath()
    {
        return self::$iconPath;
    }

    public function iconUrl()
    {
        if (!$this->icon) {
            if ($this->isPartner()) {
                return asset(config('upload.projectLogo.default'));
            }
            return asset(config('upload.sites.default'));
        }
        return asset(self::getIconPath() . $this->icon);
    }

    public function uploadIcon(UploadedFile $image)
    {
        $extension = $image->getClientOriginalExtension();
        $filename = $this->id . '_' . uniqid() . '.' . $extension;
        \Image::make($image)->resize(config('upload.sites.width'), config('upload.sites.height'), function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path(self::getIconPath().$filename));


        return $filename;
    }

    public function deleteIcon()
    {
        $imagePath = public_path(self::getIconPath() . $this->icon);
        if ($this->icon != '' && file_exists($imagePath)) {
            unlink($imagePath);
        }
        $this->icon = null;
    }
}
