<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 25.03.2019
 * Time: 12:10
 */

namespace App\Domain\NationalSites\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\NationalSites\Models\NationalSiteTranslation
 *
 * @property int $id
 * @property int $national_site_id
 * @property string $name
 * @property string $locale
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSiteTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSiteTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSiteTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSiteTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSiteTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSiteTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\NationalSites\Models\NationalSiteTranslation whereNationalSiteId($value)
 * @mixin \Eloquent
 */
class NationalSiteTranslation extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];
}
