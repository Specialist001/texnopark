<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 25.03.2019
 * Time: 12:15
 */

namespace App\Domain\NationalSites\Requests;


use App\Services\TranslationService\TranslationsRule;
use Illuminate\Foundation\Http\FormRequest;

class NationalSitesRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'order' => 'nullable|integer',
            'link' => 'nullable|string|max:255',

            'icon' => 'nullable|image|mimes:jpeg,bmp,png',
            'translations' => ['required', new TranslationsRule()],
            'translations.'.\LaravelLocalization::getDefaultLocale().'.name' => 'required|max:255',
            'translations.*.name' => 'max:255',
        ];
    }
}
