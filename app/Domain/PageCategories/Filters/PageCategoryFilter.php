<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 19.03.2019
 * Time: 6:41
 */

namespace App\Domain\PageCategories\Filters;


use App\Services\FilterService\Filter;
use Illuminate\Http\Request;

class PageCategoryFilter extends Filter
{
    protected $available = [
        'id',
        'name',
        'sort', 'perPage'
    ];

    protected $translationTable = 'page_category_translations';

    protected $defaults = [
        'sort' => '-id'
    ];

    public function __construct(Request $request)
    {
        $this->input = $this->prepareInput($request->all());
    }

    protected function init()
    {
        //Добавляем поля для сортировки
        $this->addSortable('id');
        $this->addSortable('name', $this->translationTable);

        $this->addJoin($this->translationTable, function () {
            $this->builder->leftJoin($this->translationTable, function ($join) {
                /**
                 * @var $join \Illuminate\Database\Query\JoinClause
                 */
                $join->on($this->table . '.id', $this->translationTable . '.page_category_id')->where('locale', \App::getLocale());
            })->select($this->table . '.*');
        });
    }

    public function id($value)
    {
        return $this->builder->where($this->column('id'), $value);
    }

    public function name($value)
    {
        return $this->builder->whereHas('translations', function ($query) use ($value) {
            /**
             * @var $query \Illuminate\Database\Eloquent\Builder
             */
            $query->where('name', 'like', '%' . $value . '%')
                ->where('locale', \App::getLocale());
        });
    }
}
