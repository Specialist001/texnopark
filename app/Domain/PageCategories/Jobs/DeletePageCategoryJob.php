<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 19.03.2019
 * Time: 6:52
 */

namespace App\Domain\PageCategories\Jobs;


use App\Domain\PageCategories\Models\PageCategory;
use App\Domain\Pages\Jobs\DeletePageJob;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class DeletePageCategoryJob
{
    use Dispatchable, Queueable;

    /**
     * @var PageCategory
     */
    private $pageCategory;

    /**
     * DeletePageCategoryJob constructor.
     * @param PageCategory $pageCategory
     */
    public function __construct(PageCategory $pageCategory)
    {
        $this->pageCategory = $pageCategory;
    }

    /**
     * Execute the job.
     *
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            foreach ($this->pageCategory->pages as $page) {
                dispatch_now(new DeletePageJob($page));
            }
            $this->pageCategory->translations()->delete();
            $this->pageCategory->delete();
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();
    }
}
