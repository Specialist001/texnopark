<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 19.03.2019
 * Time: 6:49
 */

namespace App\Domain\PageCategories\Jobs;


use App\Domain\PageCategories\Models\PageCategory;
use App\Domain\PageCategories\Models\PageCategoryTranslation;
use App\Domain\PageCategories\Requests\PageCategoryRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdatePageCategoryJob
{
    use Queueable, Dispatchable;


    /**
     * @var PageCategoryRequest
     */
    private $request;

    /**
     * @var PageCategory
     */
    private $pageCategory;

    /**
     * UpdatePageCategoryJob constructor.
     * @param PageCategory $pageCategory
     * @param PageCategoryRequest $request
     */
    public function __construct(PageCategory $pageCategory, PageCategoryRequest $request)
    {
        $this->pageCategory = $pageCategory;
        $this->request = $request;
    }

    /**
     * @return PageCategory
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $pageCategory = $this->pageCategory;

            $pageCategory->active = $this->request->input('active', 0);
            if ($pageCategory->active == 0) {
                $pageCategory->pages()->update(['active' => 0]);
            }
            $pageCategory->order = $this->request->input('order', 0);
            $pageCategory->top = $this->request->input('top', 0);
            $pageCategory->bottom = $this->request->input('bottom', 0);

            $pageCategory->save();

            $pageCategory->translations()->delete();
            $translations = [];
            foreach ($this->request->input('translations', []) as $translate) {
                if($translate['name'] == '') continue;
                $translations[] = new PageCategoryTranslation($translate);
            }

            if (!empty($translations)) {
                $pageCategory->translations()->saveMany($translations);
            }

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $pageCategory;
    }
}
