<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 19.03.2019
 * Time: 6:34
 */

namespace App\Domain\PageCategories\Models;


use App\Domain\Pages\Models\Page;
use App\Services\FilterService\Filterable;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\PageCategories\Models\PageCategory
 *
 * @property int $id
 * @property int $active
 * @property int $order
 * @property int $top
 * @property int $bottom
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\PageCategories\Models\PageCategoryTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory filter(\App\Services\FilterService\Filter $filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory orderByTranslation($key, $sortmethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory paginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory simplePaginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory whereBottom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory whereTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory withTranslation()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Pages\Models\Page[] $pages
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory activePages()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\PageCategories\Models\PageCategory withActivePages()
 */
class PageCategory extends Model
{
    use Translatable, Filterable;

    protected $guarded = ['id'];

    public $translatedAttributes = ['name'];

    public function pages()
    {
        return $this->hasMany(Page::class);
    }


    /**
     * @param $query Builder
     * @return Builder
     */
    public function scopeWithActivePages($query)
    {
        return $query->with(['pages' => function ($query){
            /**
             * @var $query Builder|Page
             */
            $query->actives()->withTranslation()->orderBy('order');
        }]);
    }
}
