<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 19.03.2019
 * Time: 6:43
 */

namespace App\Domain\PageCategories\Requests;


use App\Services\TranslationService\TranslationsRule;
use Illuminate\Foundation\Http\FormRequest;

class PageCategoryRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'translations' => ['required', new TranslationsRule()],
            'translations.'.\LaravelLocalization::getDefaultLocale().'.name' => 'required|max:255',
            'translations.*.name' => 'max:255',

            'active' => 'nullable|boolean',
            'bottom' => 'nullable|boolean',
            'top' => 'nullable|boolean',
            'order' => 'nullable|integer',
        ];
    }
}
