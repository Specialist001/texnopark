<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 20.03.2019
 * Time: 13:27
 */

namespace App\Domain\Pages\Jobs;


use App\Domain\Pages\Models\Page;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class DeletePageJob
{
    use Queueable, Dispatchable;

    protected $page;

    public function __construct(Page $page)
    {
        $this->page = $page;
    }


    /**
     * Execute the job.
     *
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $this->page->translations()->delete();
            $this->page->deletePoster();
            $this->page->deleteIcon();
            $this->page->delete();
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();
    }
}
