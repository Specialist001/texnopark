<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 20.03.2019
 * Time: 13:22
 */

namespace App\Domain\Pages\Jobs;

use App\Domain\Pages\Models\Page;
use App\Domain\Pages\Models\PageTranslation;
use App\Domain\Pages\Requests\PageRequest;
use App\Services\TranslitService;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdatePageJob
{
    use Queueable, Dispatchable;


    private $request;

    private $page;


    public function __construct(Page $page, PageRequest $request)
    {
        $this->page = $page;
        $this->request = $request;
    }

    /**
     * @return Page
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $page = $this->page;

            $page->page_category_id = $this->request->input('page_category_id', null);
//            $page->type = $this->request->input('type');
//            $page->poster = null;
//            $page->thumb = null;
            if (!$page->system) {
                $page->active = $this->request->input('active', 0);
            }
            if ($page->type != Page::TYPE_NEWS) {
                $page->order = $this->request->input('order', 0);
            } else {
                $page->order = 0;
            }
            $page->created_at = $this->request->input('created_at');
            $page->top = $this->request->input('top', 0);
//            $page->system = 0;

            $page->save();

            $page->translations()->delete();
            $translations = [];
            foreach ($this->request->input('translations', []) as $translate) {
                if ($translate['name'] == '') {
                    continue;
                }
                // if ($translate['full'] == '') {
                //     continue;
                // }
                if (!isset($translate['short']) || $translate['short'] == '') {
                    $temp = strip_tags($translate['full']);
                    if(mb_strlen($temp) > 500) {
                        $translate['short'] = mb_substr($temp, 0, 500);
                    } else {
                        $translate['short'] = $temp;
                    }
                    $translate['short'] = str_replace('&nbsp;', '', $translate['short']);
                }
                
                $translations[] = new PageTranslation($translate);
            }

            if (!empty($translations)) {
                $page->translations()->saveMany($translations);
            }
            if ($page->type == Page::TYPE_NEWS) {
                $page->uri = TranslitService::makeLatin($page->name);
                $page->save();
            }

            if ($this->request->hasFile('poster')) {
                $page->deletePoster();
                $page->poster = $page->uploadPoster($this->request->file('poster'));
                $page->thumb = 'th_'.$page->poster;
            }
			if ($this->request->hasFile('icon')) {
                $page->deleteIcon();
                $page->icon = $page->uploadIcon($this->request->file('icon'));
            }

            $page->save();

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $page;
    }

}
