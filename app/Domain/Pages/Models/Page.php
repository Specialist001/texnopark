<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 20.03.2019
 * Time: 11:23
 */

namespace App\Domain\Pages\Models;


use App\Domain\PageCategories\Models\PageCategory;
use App\Domain\VacancyRespond\Models\VacancyRespond;
use App\Services\FilterService\Filterable;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

/**
 * App\Domain\Pages\Models\Page
 *
 * @property int $id
 * @property int|null $page_category_id
 * @property string $type
 * @property string|null $poster
 * @property string|null $thumb
 * @property int $active
 * @property int $order
 * @property int $top
 * @property int $system
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Domain\PageCategories\Models\PageCategory|null $pageCategory
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Pages\Models\PageTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page actives()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page filter(\App\Services\FilterService\Filter $filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page news()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page orderByTranslation($key, $sortmethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page pages()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page allPages()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page about()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page contact()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page paginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page simplePaginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page wherePageCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page wherePoster($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page whereSystem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page whereThumb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page whereTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page withTranslation()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page withCategory()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page labs()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page vacs()
 * @property int|null $views
 * @property string|null $uri
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page whereUri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page whereViews($value)
 * @property string|null $icon
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\VacancyRespond\Models\VacancyRespond[] $vacancyResponds
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\Page whereIcon($value)
 */
class Page extends Model
{
    use Translatable, Filterable;

    const TYPE_NEWS = 'news';
    const TYPE_PAGE = 'page';
    const TYPE_ABOUT = 'about';
    const TYPE_CONTACT = 'contact';
    const TYPE_LAB = 'lab';
    const TYPE_VAC = 'vacancy';

    protected $guarded = ['id'];

    public $translatedAttributes = ['name', 'short', 'full'];

    protected static $posterPath = 'uploads/pages/';

    protected static $iconPath = 'uploads/page-icons/';

    public function pageCategory()
    {
        return $this->belongsTo(PageCategory::class);
    }

    /**
     * @param $query Builder
     * @return Builder
     */
    public function scopeWithCategory($query)
    {
        return $query->with(['pageCategory' => function ($query){
            /**
             * @var $query Builder|PageCategory
             */
            $query->withTranslation();
        }]);
    }

    public function isActive()
    {
        return $this->active === 1;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActives($query)
    {
        return $query->where('active', 1);
    }

    public function isNews()
    {
        return $this->type === static::TYPE_NEWS;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNews($query)
    {
        return $query->where('type', static::TYPE_NEWS);
    }

    public function isLab()
    {
        return $this->type === static::TYPE_LAB;
    }

    public function isVac()
    {
        return $this->type === static::TYPE_VAC;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLabs($query)
    {
        return $query->where('type', static::TYPE_LAB);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVacs($query)
    {
        return $query->where('type', static::TYPE_VAC);
    }

    public function isPage()
    {
        return $this->type === static::TYPE_PAGE;
    }

    public function isAbout()
    {
        return $this->type === static::TYPE_ABOUT;
    }

    public function isContact()
    {
        return $this->type === static::TYPE_CONTACT;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePages($query)
    {
        return $query->where('type', static::TYPE_PAGE);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAbout($query)
    {
        return $query->where('type', static::TYPE_ABOUT);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeContact($query)
    {
        return $query->where('type', static::TYPE_CONTACT);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAllPages($query)
    {
        return $query->whereIn('type', [static::TYPE_PAGE, static::TYPE_ABOUT, static::TYPE_CONTACT]);
    }

    public static function types()
    {
        return [
            static::TYPE_NEWS,
            static::TYPE_PAGE,
            static::TYPE_ABOUT,
            static::TYPE_CONTACT,
        ];
    }
    public function vacancyResponds()
    {
        return $this->hasMany(VacancyRespond::class);
    }

    public static function getPosterPath()
    {
        return self::$posterPath;
    }

    public function posterUrl()
    {
        if(!$this->poster) {
            return asset(config('upload.poster.default'));
        }
        return asset(self::getPosterPath().$this->poster);
    }


    public function thumbUrl()
    {
        if(!$this->thumb) {
            return asset(config('upload.poster_thumb.default'));
        }
        return asset(self::getPosterPath().$this->thumb);
    }

    public function uploadPoster(UploadedFile $image)
    {
        $extension = $image->getClientOriginalExtension();
        $filename = $this->id.'_'.uniqid().'.'.$extension;
        \Image::make($image)->fit(config('upload.poster.width'), config('upload.poster.height'))->save(public_path(self::getPosterPath().$filename));
        \Image::make($image)->fit(config('upload.poster_thumb.width'), config('upload.poster_thumb.height'))->save(public_path(self::getPosterPath().'th_'.$filename));
        return $filename;
    }

    public function deletePoster()
    {
        $imagePath = public_path(self::getPosterPath().$this->poster);
        $thumbPath = public_path(self::getPosterPath().$this->thumb);
        if ($this->poster != '' && file_exists($imagePath)) {
            unlink($imagePath);
        }
        if ($this->thumb != '' && file_exists($thumbPath)) {
            unlink($thumbPath);
        }
        $this->poster = null;
        $this->thumb = null;
    }

	public static function getIconPath()
    {
        return self::$iconPath;
    }

	public function iconUrl()
    {
        if(!$this->icon) {
            return asset(config('upload.icon.default'));
        }
        return asset(self::getIconPath().$this->icon);
    }

	public function uploadIcon(UploadedFile $image)
    {
        $extension = $image->getClientOriginalExtension();
        $filename = $this->id.'_'.uniqid().'.'.$extension;
        \Image::make($image)->fit(config('upload.icon.width'), config('upload.icon.height'))->save(public_path(self::getIconPath().$filename));

        return $filename;
    }

	public function deleteIcon()
    {
        $iconPath = public_path(self::getIconPath().$this->icon);
        if ($this->icon != '' && file_exists($iconPath)) {
            unlink($iconPath);
        }

        $this->icon = null;
    }
}
