<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 20.03.2019
 * Time: 11:43
 */

namespace App\Domain\Pages\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\Pages\Models\PageTranslation
 *
 * @property int $id
 * @property int $page_id
 * @property string|null $name
 * @property string $short
 * @property string $full
 * @property string $locale
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\PageTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\PageTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\PageTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\PageTranslation whereFull($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\PageTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\PageTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\PageTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\PageTranslation wherePageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Pages\Models\PageTranslation whereShort($value)
 * @mixin \Eloquent
 */
class PageTranslation extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];
}
