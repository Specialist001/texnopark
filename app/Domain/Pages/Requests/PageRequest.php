<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 20.03.2019
 * Time: 11:46
 */

namespace App\Domain\Pages\Requests;


use App\Services\TranslationService\TranslationsRule;
use Illuminate\Foundation\Http\FormRequest;

class PageRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'page_category_id' => 'nullable|exists:page_categories,id',

            'active' => 'nullable|boolean',
            'top' => 'nullable|boolean',
            'order' => 'nullable|integer',

            'poster' => 'nullable|image|mimes:jpeg,bmp,png',
            'icon' => 'nullable|image|mimes:jpeg,bmp,png',
            'translations' => ['required', new TranslationsRule()],
            'translations.'.\LaravelLocalization::getDefaultLocale().'.name' => 'required|max:255',
            'translations.'.\LaravelLocalization::getDefaultLocale().'.full' => 'required|string',
            'translations.*.name' => 'max:255',
            'translations.*.short' => 'nullable|string',
            'translations.*.full' => 'nullable|string',
        ];
    }
}
