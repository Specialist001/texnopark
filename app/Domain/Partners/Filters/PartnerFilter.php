<?php


namespace App\Domain\Partners\Filters;


use App\Services\FilterService\Filter;
use Illuminate\Http\Request;

class PartnerFilter extends Filter
{
    protected $available = [
        'id',
        'order',
        'name',
    ];

    protected $defaults = [
        'sort' => '-id'
    ];

    public function __construct(Request $request)
    {
        $this->input = $this->prepareInput($request->all());
    }

    protected function init()
    {
        //Добавляем поля для сортировки
        $this->addSortable('id');
        $this->addSortable('order');
        $this->addSortable('name');
    }

    public function id($value)
    {
        return $this->builder->where($this->column('id'), $value);
    }

    public function name($value)
    {
        return $this->builder->where($this->column('name'), $value);
    }

    public function order($value)
    {
        return $this->builder->where($this->column('order'), $value);
    }
}
