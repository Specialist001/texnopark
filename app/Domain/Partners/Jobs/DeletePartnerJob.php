<?php

namespace App\Domain\Partners\Jobs;


use App\Domain\Partners\Models\Partner;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class DeletePartnerJob
{
    use Queueable, Dispatchable;

    /**
     * @var Partner
     */
    protected $partner;

    /**
     * DeleteNationalSiteJob constructor.
     * @param Partner $partner
     */
    public function __construct(Partner $partner)
    {
        $this->partner = $partner;
    }

    /**
     * Execute the job.
     *
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $this->partner->deleteLogo();
            $this->partner->delete();
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();
    }
}
