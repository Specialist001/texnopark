<?php


namespace App\Domain\Partners\Jobs;


use App\Domain\Partners\Models\Partner;
use App\Domain\Partners\Requests\PartnerRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class StorePartnerJob
{
    use Queueable, Dispatchable;

    public $request;

    public function __construct(PartnerRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return Partner
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $partner = new Partner();
            $partner->name = $this->request->input('name');
            $partner->order = $this->request->input('order', 0);
            if ($this->request->hasFile('logo')) {
                $partner->logo = $partner->uploadLogo($this->request->file('logo'));
            }
            $partner->save();
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $partner;
    }
}
