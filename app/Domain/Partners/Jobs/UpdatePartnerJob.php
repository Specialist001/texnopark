<?php


namespace App\Domain\Partners\Jobs;


use App\Domain\Partners\Models\Partner;
use App\Domain\Partners\Requests\PartnerRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdatePartnerJob
{
    use Queueable, Dispatchable;

    public $request;
    public $partner;

    public function __construct(PartnerRequest $request, Partner $partner)
    {
        $this->request = $request;
        $this->partner = $partner;
    }

    /**
     * Execute the job.
     *
     * @return Partner
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $partner = $this->partner;
            $partner->name = $this->request->input('name');
            $partner->order = $this->request->input('order');
            if ($this->request->hasFile('logo')) {
                $partner->deleteLogo();
                $partner->logo = $partner->uploadLogo($this->request->file('logo'));
            }
            $partner->save();
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $partner;
    }
}
