<?php


namespace App\Domain\Partners\Models;


use App\Services\FilterService\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

/**
 * App\Domain\Partners\Models\Partner
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $logo
 * @property int|null $order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Partners\Models\Partner filter(\App\Services\FilterService\Filter $filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Partners\Models\Partner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Partners\Models\Partner newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Partners\Models\Partner paginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Partners\Models\Partner query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Partners\Models\Partner simplePaginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Partners\Models\Partner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Partners\Models\Partner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Partners\Models\Partner whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Partners\Models\Partner whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Partners\Models\Partner whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Partner extends Model
{
    use Filterable;

    protected $guarded = ['id'];

    protected static $logoPath = 'uploads/partners/';

    public static function getLogoPath()
    {
        return self::$logoPath;
    }

    public function logoUrl()
    {
        if(!$this->logo) {
            return asset(config('upload.icon.default'));
        }
        return asset(self::getLogoPath().$this->logo);
    }

    public function uploadLogo(UploadedFile $image)
    {
        $extension = $image->getClientOriginalExtension();
        $filename = $this->id.'_'.uniqid().'.'.$extension;
        \Image::make($image)->fit(config('upload.logo.width'), config('upload.logo.height'))->save(public_path(self::getLogoPath().$filename));

        return $filename;
    }

    public function deleteLogo()
    {
        $logoPath = public_path(self::getLogoPath().$this->logo);
        if ($this->logo != '' && file_exists($logoPath)) {
            unlink($logoPath);
        }

        $this->logo = null;
    }
}
