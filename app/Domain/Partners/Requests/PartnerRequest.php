<?php


namespace App\Domain\Partners\Requests;


use Illuminate\Foundation\Http\FormRequest;

class PartnerRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'logo' => 'nullable|image|mimes:jpeg,bmp,png',
            'order' => 'nullable|integer|min:0',
        ];
    }

}
