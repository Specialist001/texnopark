<?php

namespace App\Domain\ProjectCategories\Jobs;

use App\Domain\ProjectCategories\Models\ProjectCategory;
use App\Domain\Projects\Jobs\DeleteProjectJob;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class DeleteProjectCategoryJob
{
    use Dispatchable, Queueable;

    /**
     * @var ProjectCategory
     */
    private $category;

    /**
     * Create a new job instance.
     *
     * DeleteUserJob constructor.
     * @param ProjectCategory $category
     */
    public function __construct(ProjectCategory $category)
    {
        $this->category = $category;
    }

    /**
     * Execute the job.
     *
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            foreach ($this->category->projects as $project) {
                dispatch_now(new DeleteProjectJob($project));
            }
            $this->category->deleteIcon();
            $this->category->delete();
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();
    }
}
