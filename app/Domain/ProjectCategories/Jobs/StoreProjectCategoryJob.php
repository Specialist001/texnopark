<?php

namespace App\Domain\ProjectCategories\Jobs;

use App\Domain\ProjectCategories\Models\ProjectCategory;
use App\Domain\ProjectCategories\Models\ProjectCategoryTranslation;
use App\Domain\ProjectCategories\Requests\ProjectCategoryRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class StoreProjectCategoryJob
{
    use Dispatchable, Queueable;

    /**
     * @var ProjectCategoryRequest
     */
    private $request;

    /**
     * StoreCityJob constructor.
     * @param ProjectCategoryRequest $request
     */
    public function __construct(ProjectCategoryRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return ProjectCategory
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $category = new ProjectCategory();
            $category->icon = null;
            $category->order = $this->request->input('order', 0);
            $category->save();

            $translations = [];
            foreach ($this->request->input('translations', []) as $translate) {
                if($translate['name'] == '') continue;
                $translations[] = new ProjectCategoryTranslation($translate);
            }

            if (!empty($translations)) {
                $category->translations()->saveMany($translations);
            }

            if ($this->request->hasFile('icon')) {
                $category->icon = $category->uploadIcon($this->request->file('icon'));
            } else {
                $category->icon = null;
            }
            $category->save();

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $category;
    }
}
