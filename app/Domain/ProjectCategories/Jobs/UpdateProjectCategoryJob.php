<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 01.03.2019
 * Time: 12:39
 */

namespace App\Domain\ProjectCategories\Jobs;

use App\Domain\ProjectCategories\Models\ProjectCategory;
use App\Domain\ProjectCategories\Models\ProjectCategoryTranslation;
use App\Domain\ProjectCategories\Requests\ProjectCategoryRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateProjectCategoryJob
{
    use Queueable, Dispatchable;

    /**
     * @var ProjectCategoryRequest
     */
    private $request;

    /**
     * @var ProjectCategory
     */
    private $category;

    /**
     * UpdateProjectCategoryJob constructor.
     * @param ProjectCategory $category
     * @param ProjectCategoryRequest $request
     */
    public function __construct(ProjectCategory $category, ProjectCategoryRequest $request)
    {
        $this->category = $category;
        $this->request = $request;
    }

    /**
     * @return ProjectCategory
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $category = $this->category;
            $category->order = $this->request->input('order', 0);
            $category->save();

            $category->translations()->delete();
            $translations = [];
            foreach ($this->request->input('translations', []) as $translate) {
                if($translate['name'] == '') continue;
                $translations[] = new ProjectCategoryTranslation($translate);
            }

            if (!empty($translations)) {
                $category->translations()->saveMany($translations);
            }
            if ($this->request->hasFile('icon')) {
                $category->deleteIcon();
                $category->icon = $category->uploadIcon($this->request->file('icon'));
            }

            $category->save();

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $category;
    }
}
