<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 28.02.2019
 * Time: 14:13
 */

namespace App\Domain\ProjectCategories\Models;


use App\Domain\Projects\Models\Project;
use App\Services\FilterService\Filterable;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\UploadedFile;

/**
 * App\Domain\ProjectCategories\Models\ProjectCategory
 *
 * @property int $id
 * @property string|null $icon
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Projects\Models\Project[] $projects
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategory whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategory whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategory whereId($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation[] $translations
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategory listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategory notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\ProjectCategories\Models\ProjectCategory onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategory orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategory orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategory orderByTranslation($key, $sortmethod = 'asc')
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategory translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategory translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategory whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategory whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategory withTranslation()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\ProjectCategories\Models\ProjectCategory withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\ProjectCategories\Models\ProjectCategory withoutTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategory filter(\App\Services\FilterService\Filter $filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategory paginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategory simplePaginateFilter($perPage = 20)
 * @property int|null $order
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategory whereOrder($value)
 */
class ProjectCategory extends Model
{
    use SoftDeletes, Translatable, Filterable;

    public $timestamps = false;

    protected $guarded = ['id'];

    public $translatedAttributes = ['name'];

    protected static $iconPath = 'uploads/categories/';

    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    public static function getIconPath()
    {
        return self::$iconPath;
    }

    public function iconUrl()
    {
        if(!$this->icon) {
            return asset(config('upload.category.default'));
        }
        return asset(self::getIconPath().$this->icon);
    }

    public function uploadIcon(UploadedFile $image)
    {
        $extension = $image->getClientOriginalExtension();
        $filename = $this->id.'_'.uniqid().'.'.$extension;
        \Image::make($image)->fit(config('upload.category.width'), config('upload.category.height'))->save(public_path(self::getIconPath().$filename));
        return $filename;
    }

    public function deleteIcon()
    {
        $imagePath = public_path(self::getIconPath().$this->icon);
        if ($this->icon != '' && file_exists($imagePath)) {
            unlink($imagePath);
        }
        $this->icon = null;
    }
}
