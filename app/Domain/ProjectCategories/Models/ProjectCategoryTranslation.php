<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 28.02.2019
 * Time: 14:15
 */

namespace App\Domain\ProjectCategories\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\ProjectCategories\Models\ProjectCategoryTranslation
 *
 * @property int $id
 * @property int $project_category_id
 * @property string $name
 * @property string $locale
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation whereProjectCategoryId($value)
 * @mixin \Eloquent
 */
class ProjectCategoryTranslation extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];
}
