<?php

namespace App\Domain\ProjectTypes\Jobs;

use App\Domain\Projects\Jobs\DeleteProjectJob;
use App\Domain\ProjectTypes\Models\ProjectType;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class DeleteProjectTypeJob
{
    use Dispatchable, Queueable;

    /**
     * @var ProjectType
     */
    private $type;

    /**
     * Create a new job instance.
     *
     * DeleteUserJob constructor.
     * @param ProjectType $type
     */
    public function __construct(ProjectType $type)
    {
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            foreach ($this->type->projects as $project) {
                dispatch_now(new DeleteProjectJob($project));
            }
            $this->type->delete();
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();
    }
}
