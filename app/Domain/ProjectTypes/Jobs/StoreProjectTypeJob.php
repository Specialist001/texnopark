<?php

namespace App\Domain\ProjectTypes\Jobs;

use App\Domain\ProjectTypes\Requests\ProjectTypeRequest;
use App\Domain\ProjectTypes\Models\ProjectType;
use App\Domain\ProjectTypes\Models\ProjectTypeTranslation;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class StoreProjectTypeJob
{
    use Dispatchable, Queueable;

    /**
     * @var ProjectTypeRequest
     */
    private $request;

    /**
     * StoreCityJob constructor.
     * @param ProjectTypeRequest $request
     */
    public function __construct(ProjectTypeRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return ProjectType
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $type = new ProjectType();
            $type->save();

            $translations = [];
            foreach ($this->request->input('translations', []) as $translate) {
                if($translate['name'] == '') continue;
                $translations[] = new ProjectTypeTranslation($translate);
            }

            if (!empty($translations)) {
                $type->translations()->saveMany($translations);
            }

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $type;
    }
}
