<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 01.03.2019
 * Time: 12:39
 */

namespace App\Domain\ProjectTypes\Jobs;

use App\Domain\ProjectTypes\Models\ProjectType;
use App\Domain\ProjectTypes\Models\ProjectTypeTranslation;
use App\Domain\ProjectTypes\Requests\ProjectTypeRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateProjectTypeJob
{
    use Queueable, Dispatchable;

    /**
     * @var ProjectTypeRequest
     */
    private $request;

    /**
     * @var ProjectType
     */
    private $type;

    /**
     * UpdateProjectCategoryJob constructor.
     * @param ProjectType $type
     * @param ProjectTypeRequest $request
     */
    public function __construct(ProjectType $type, ProjectTypeRequest $request)
    {
        $this->type = $type;
        $this->request = $request;
    }

    /**
     * @return ProjectType
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $type = $this->type;
            $type->save();

            $type->translations()->delete();
            $translations = [];
            foreach ($this->request->input('translations', []) as $translate) {
                if($translate['name'] == '') continue;
                $translations[] = new ProjectTypeTranslation($translate);
            }

            if (!empty($translations)) {
                $type->translations()->saveMany($translations);
            }

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $type;
    }
}
