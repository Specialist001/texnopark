<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 28.02.2019
 * Time: 14:06
 */

namespace App\Domain\ProjectTypes\Models;


use App\Domain\Projects\Models\Project;
use App\Services\FilterService\Filterable;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Domain\ProjectTypes\Models\ProjectType
 *
 * @property int $id
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Projects\Models\Project[] $projects
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\ProjectTypes\Models\ProjectTypeTranslation[] $translations
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectType listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectType notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\ProjectTypes\Models\ProjectType onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectType orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectType orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectType orderByTranslation($key, $sortmethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectType query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectType translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectType translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectType whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectType whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectType whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectType withTranslation()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\ProjectTypes\Models\ProjectType withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\ProjectTypes\Models\ProjectType withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectType filter(\App\Services\FilterService\Filter $filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectType paginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectType simplePaginateFilter($perPage = 20)
 */
class ProjectType extends Model
{
    use SoftDeletes, Translatable, Filterable;

    const TYPE_PATENTED = 3;

    public $timestamps = false;

    protected $guarded = ['id'];

    public $translatedAttributes = ['name'];

    public function projects()
    {
        return $this->hasMany(Project::class);
    }
}
