<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 28.02.2019
 * Time: 14:07
 */

namespace App\Domain\ProjectTypes\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\ProjectTypes\Models\ProjectTypeTranslation
 *
 * @property int $id
 * @property int $project_type_id
 * @property string $name
 * @property string $locale
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectTypeTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectTypeTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectTypeTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectTypeTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectTypeTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectTypeTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProjectTypes\Models\ProjectTypeTranslation whereProjectTypeId($value)
 * @mixin \Eloquent
 */
class ProjectTypeTranslation extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];
}
