<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 01.03.2019
 * Time: 11:54
 */

namespace App\Domain\ProjectTypes\Requests;

use App\Services\TranslationService\TranslationsRule;
use Illuminate\Foundation\Http\FormRequest;

class ProjectTypeRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'translations' => ['required', new TranslationsRule()],
            'translations.'.\LaravelLocalization::getDefaultLocale().'.name' => 'required|max:255',
            'translations.*.name' => 'max:255',
        ];
    }
}
