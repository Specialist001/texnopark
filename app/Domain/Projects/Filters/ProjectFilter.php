<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 01.03.2019
 * Time: 16:09
 */

namespace App\Domain\Projects\Filters;


use App\Services\FilterService\Filter;
use Illuminate\Http\Request;

class ProjectFilter extends Filter
{
    protected $available = [
        'id',
        'title',
        'status',
        'project_category_id',
        'project_type_id',
        'user_id',
        'patent',
        'hasPatent',

        'readiness',
        'payback_period',
        'updated_at',
        'sum_cost',
        'sum_need',
//        'usd_cost',
//        'usd_need',

        'sort', 'perPage'
    ];

    protected $defaults = [
        'sort' => '-updated_at',
    ];

//    protected $translationTable = 'project_translations';

    public function __construct(Request $request)
    {
        $this->input = $this->prepareInput($request->all());
    }

    protected function init()
    {
        //Добавляем поля для сортировки
        $this->addSortable('id');
        $this->addSortable('title');
        $this->addSortable('updated_at');
    }

    public function id($value)
    {
        return $this->builder->where($this->column('id'), $value);
    }

    public function user_id($value)
    {
        return $this->builder->where($this->column('user_id'), $value);
    }

    public function title($value)
    {
        return $this->builder->where($this->column('title'), 'like', '%' . $value . '%');
    }

    public function status($value)
    {
        return $this->builder->where($this->column('status'), $value);
    }

    public function project_category_id($value)
    {
        if(is_array($value)) {
            return $this->builder->whereIn($this->column('project_category_id'), $value);
        }
        return $this->builder->where($this->column('project_category_id'), $value);
    }

    public function project_type_id($value)
    {
        if(is_array($value)) {
            return $this->builder->whereIn($this->column('project_type_id'), $value);
        }
        return $this->builder->where($this->column('project_type_id'), $value);
    }

    public function patent($value)
    {
        return $this->builder->where($this->column('patent'), $value);
    }

    public function hasPatent($value)
    {
        if ($value == 'yes') {
            return $this->builder->whereNotNull($this->column('patent'));
        } else if ($value == 'no') {
            return $this->builder->whereNull($this->column('patent'));
        }

        return $this->builder;
    }

    public function readiness($value)
    {
        if(is_array($value)) {
            $query = $this->builder;
            if (isset($value[0]) && $value[0] != '') {
                $query->where($this->column('readiness'), '>=', $value[0]);
            }
            if (isset($value[1]) && $value[1] != '') {
                $query->where($this->column('readiness'), '<=', $value[1]);
            }
            return $query;
        }
        return $this->builder->where($this->column('readiness'), $value);
    }

    public function payback_period($value)
    {
        if(is_array($value)) {
            $query = $this->builder;
            if (isset($value[0]) && $value[0] != '') {
                $query->where($this->column('payback_period'), '>=', $value[0]);
            }
            if (isset($value[1]) && $value[1] != '') {
                $query->where($this->column('payback_period'), '<=', $value[1]);
            }
            return $query;
        }
        return $this->builder->where($this->column('payback_period'), $value);
    }

    public function sum_cost($value)
    {
        if(is_array($value)) {
            $query = $this->builder;
            if (isset($value[0]) && $value[0] != '') {
                $query->where($this->column('sum_cost'), '>=', $value[0]);
            }
            if (isset($value[1]) && $value[1] != '') {
                $query->where($this->column('sum_cost'), '<=', $value[1]);
            }
            return $query;
        }
        return $this->builder->where($this->column('sum_cost'), $value);
    }

    public function sum_need($value)
    {
        if(is_array($value)) {
            $query = $this->builder;
            if (isset($value[0]) && $value[0] != '') {
                $query->where($this->column('sum_need'), '>=', $value[0]);
            }
            if (isset($value[1]) && $value[1] != '') {
                $query->where($this->column('sum_need'), '<=', $value[1]);
            }
            return $query;
        }
        return $this->builder->where($this->column('sum_need'), $value);
    }
}
