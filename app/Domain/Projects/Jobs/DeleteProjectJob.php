<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 01.03.2019
 * Time: 11:59
 */

namespace App\Domain\Projects\Jobs;

use App\Domain\Investments\Jobs\DeleteInvestmentJob;
use App\Domain\Projects\Models\Project;
use App\Domain\Users\Models\UserFavourites;
use App\Domain\Users\Models\UserNotifications;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class DeleteProjectJob
{
    use Dispatchable, Queueable;

    /**
     * @var Project
     */
    public $project;

    /**
     * DeleteProjectJob constructor.
     * @param Project $project
     */
    public function __construct(Project $project)
    {
        $this->project = $project;
    }

    /**
     * Execute the job.
     *
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            //TODO delete project relations
            foreach ($this->project->investments as $investment) {
                dispatch_now(new DeleteInvestmentJob($investment));
            }

            UserFavourites::where('project_id', $this->project->id)->delete();
            UserNotifications::where('project_id', $this->project->id)->delete();

            $this->project->deleteLogo();
            $this->project->deleteImage();
            $this->project->deletePresentation();

            $this->project->delete();
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();
    }
}
