<?php

namespace App\Domain\Projects\Jobs;

use App\Domain\Projects\Models\Project;
use App\Domain\Projects\Models\ProjectTranslation;
use App\Domain\Projects\Requests\ProjectRequest;
use App\Domain\ProjectTypes\Models\ProjectType;
use App\Domain\Users\Models\UserNotifications;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class StoreProjectJob
{
    use Dispatchable, Queueable;

    /**
     * @var ProjectRequest
     */
    private $request;

    /**
     * StoreProjectCategoryJob constructor.
     * @param ProjectRequest $request
     */
    public function __construct(ProjectRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return Project
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $project = new Project();

            if ($this->request->user()->isStartUp()) {
                $project->user_id = $this->request->user()->id;
                $project->is_moderated = 0;
                $project->status = Project::STATUS_MODERATING;
            } else {
                $project->user_id = $this->request->input('user_id') ? $this->request->input('user_id') : 1;
                $project->is_moderated = 1;
                $project->status = $this->request->input('status');
            }

            $project->project_category_id = $this->request->input('project_category_id');
            $project->project_type_id = $this->request->input('project_type_id') ? $this->request->input('project_type_id') : ProjectType::TYPE_PATENTED;

            $project->logo = null;
            $project->image = null;

            $project->title = $this->request->input('title');
            $project->stage = $this->request->input('stage') ? $this->request->input('stage') : Project::STAGE_BUSINESS;
            $project->developer = $this->request->input('developer') ? $this->request->input('developer') : Project::CURRENT_DEVELOPER;
            $project->foreign = $this->request->input('foreign') ? $this->request->input('foreign') : 0;
            $project->importable = $this->request->input('importable') ? $this->request->input('importable') : 0;
            $project->readiness = $this->request->input('readiness') ? $this->request->input('readiness') : 100;
            $project->patent = $this->request->filled('patent') ? $this->request->input('patent', null): null;

            $project->implementation_period = $this->request->input('implementation_period');
            $project->payback_period = $this->request->input('payback_period');
            $project->sum_cost = $this->request->input('sum_cost', null);
            $project->usd_cost = $this->request->input('usd_cost', null);
            $project->sum_need = $this->request->input('sum_need', null);
            $project->usd_need = $this->request->input('usd_need', null);
            $project->sum_current = $this->request->input('sum_current', null);
            $project->usd_current = $this->request->input('usd_current', null);
            $project->prod_capacity = $this->request->input('prod_capacity', null);
            $project->prod_capacity_unit = $this->request->input('prod_capacity_unit', null);
            $project->metal_structures = $this->request->input('metal_structures', null);
            $project->sandwich_panels = $this->request->input('sandwich_panels', null);
            $project->indoor_area = $this->request->input('indoor_area', null);

            $project->workers = $this->request->input('workers');
            $project->presentation = null;
            $project->view = 0;
            $project->save();

            $translations = [];
            foreach ($this->request->input('translations', []) as $translate) {
                $translate = array_filter($translate);
                if(empty($translate)) {
                    continue;
                }
                $translations[] = new ProjectTranslation($translate);
            }

            if (!empty($translations)) {
                $project->translations()->saveMany($translations);
            }

            if ($this->request->hasFile('logo')) {
                $project->logo = $project->uploadLogo($this->request->file('logo'));
            }

            if ($this->request->hasFile('image')) {
                $project->image = $project->uploadImage($this->request->file('image'));
            }

            if ($this->request->hasFile('presentation')) {
                $project->presentation = $project->uploadPresentation($this->request->file('presentation'));
            }

            $project->save();

            if($project->status == Project::STATUS_PUBLISHED) {
                UserNotifications::create([
                    'user_id' => $project->user_id,
                    'project_id' => $project->id,
                    'action' => 'portal.notifications_type.project.published',
                    'viewed' => 0,
                ]);
            }
            if($project->status == Project::STATUS_BLOCKED) {
                UserNotifications::create([
                    'user_id' => $project->user_id,
                    'project_id' => $project->id,
                    'action' => 'portal.notifications_type.project.blocked',
                    'viewed' => 0,
                ]);
            }

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $project;
    }
}
