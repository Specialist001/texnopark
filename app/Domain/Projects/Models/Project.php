<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 28.02.2019
 * Time: 14:08
 */

namespace App\Domain\Projects\Models;


use App\Domain\Investments\Models\Investment;
use App\Domain\ProjectCategories\Models\ProjectCategory;
use App\Domain\ProjectTypes\Models\ProjectType;
use App\Domain\Units\Models\Unit;
use App\Services\FilterService\Filterable;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\UploadedFile;

/**
 * App\Domain\Projects\Models\Project
 *
 * @property int $id
 * @property int $user_id
 * @property int $project_category_id
 * @property int $project_type_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Domain\ProjectCategories\Models\ProjectCategory $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Projects\Models\ProjectTranslation[] $translations
 * @property-read \App\Domain\ProjectTypes\Models\ProjectType $type
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\Projects\Models\Project onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project orderByTranslation($key, $sortmethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereProjectCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereProjectTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project withCategory()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project withTranslation()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\Projects\Models\Project withTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project withType()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\Projects\Models\Project withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project filter(\App\Services\FilterService\Filter $filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project paginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project simplePaginateFilter($perPage = 20)
 * @property string|null $logo
 * @property string|null $image
 * @property string $title
 * @property string $stage
 * @property string $developer
 * @property int $foreign
 * @property int $readiness
 * @property string|null $patent
 * @property int|null $implementation_period
 * @property int|null $payback_period
 * @property string|null $sum_cost
 * @property string|null $usd_cost
 * @property string|null $sum_need
 * @property string|null $usd_need
 * @property string|null $sum_current
 * @property string|null $usd_current
 * @property int|null $workers
 * @property int|null $prod_capacity
 * @property int|null $prod_capacity_unit
 * @property int|null $metal_structures
 * @property int|null $sandwich_panels
 * @property int|null $indoor_area
 * @property string|null $presentation
 * @property string $status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereDeveloper($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereForeign($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereImplementationPeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project wherePatent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project wherePaybackPeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project wherePresentation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereReadiness($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereStage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereSumCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereSumCurrent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereSumNeed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereUsdCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereUsdCurrent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereUsdNeed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereWorkers($value)
 * @property int $view
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project published()
 * @property int $importable
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereImportable($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Investments\Models\Investment[] $investments
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Investments\Models\Investment[] $activeInvestments
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Units\Models\Unit[] $unit
 * @property int|null $is_moderated
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereIsModerated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereIndoorArea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereMetalStructures($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereProdCapacity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereProdCapacityUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\Project whereSandwichPanels($value)
 */
class Project extends Model
{
    use SoftDeletes, Translatable, Filterable;

    const STAGE_IDEA = 'idea';
    const STAGE_MODEL = 'model';
    const STAGE_BUSINESS = 'business';

    const STATUS_MODERATING = 'moderating';
    const STATUS_PUBLISHED = 'published';
    const STATUS_BLOCKED = 'blocked';

    const CURRENT_DEVELOPER = 'TEXNOPARK';

    protected static $logoPath = 'uploads/logos/';
    protected static $imagePath = 'uploads/images/';
    protected static $presentationPath = 'uploads/images/';

    public $translatedAttributes = [
        'about_developer',
        'place',
        'short_description',
        'description',
        'target',
        'finally',
        'state',
        'about_presentation',
        'additional_info',
    ];

    public static function stages()
    {
        return [
            static::STAGE_IDEA,
            static::STAGE_MODEL,
            static::STAGE_BUSINESS,
        ];
    }

    public static function statuses()
    {
        return [
            static::STATUS_MODERATING,
            static::STATUS_PUBLISHED,
            static::STATUS_BLOCKED,
        ];
    }

    public function calcInvested()
    {
        $invested = $this->activeInvestments->sum('sum');
        if ($this->sum_need <= 0) return 100;
        if ($this->sum_need <= $invested) return 100;
        return round($invested * 100 / $this->sum_need);
    }

    public function canUpdate()
    {
        return $this->activeInvestments()->count() == 0;
    }

    public function canDelete()
    {
        return $this->activeInvestments()->count() == 0;
    }

    public function canTurnOn()
    {
        return $this->status == static::STATUS_PUBLISHED;
    }

    public function category()
    {
        return $this->belongsTo(ProjectCategory::class, 'project_category_id');
    }

    public function investments()
    {
        return $this->hasMany(Investment::class);
    }

    public function activeInvestments()
    {
        return $this->hasMany(Investment::class)->where('status', Investment::STATUS_OK);
    }

    public function unit()
    {
        return $this->hasOne(Unit::class);
    }

    /**
     * @param $query Builder
     * @return Builder
     */
    public function scopeWithCategory($query)
    {
        return $query->with(['category' => function ($query){
            /**
             * @var $query Builder|ProjectCategory
             */
            $query->withTranslation();
        }]);
    }

    public function type()
    {
        return $this->belongsTo(ProjectType::class,  'project_type_id');
    }

    /**
     * @param $query Builder
     * @return Builder
     */
    public function scopeWithType($query)
    {
        return $query->with(['type' => function ($query){
            /**
             * @var $query Builder|ProjectType
             */
            $query->withTranslation();
        }]);
    }


    public static function getLogoPath()
    {
        return self::$logoPath;
    }

    public function logoUrl()
    {
        if(!$this->logo) {
            return asset(config('upload.projectLogo.default'));
        }
        return asset(self::getLogoPath().$this->logo);
    }

    public function uploadLogo(UploadedFile $image)
    {
        $extension = $image->getClientOriginalExtension();
        $filename = $this->id.'_'.uniqid().'.'.$extension;
        \Image::make($image)->resize(config('upload.projectLogo.width'), config('upload.projectLogo.height'), function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path(self::getLogoPath().$filename));
        return $filename;
    }

    public function deleteLogo()
    {
        $imagePath = public_path(self::getLogoPath().$this->logo);
        if ($this->logo != '' && file_exists($imagePath)) {
            unlink($imagePath);
        }
        $this->logo = null;
    }

    public static function getImagePath()
    {
        return self::$imagePath;
    }

    public function imageUrl()
    {
        if(!$this->image) {
            return asset(config('upload.projectImage.default'));
        }
        return asset(self::getImagePath().$this->image);
    }

    public function uploadImage(UploadedFile $image)
    {
        $extension = $image->getClientOriginalExtension();
        $filename = $this->id.'_'.uniqid().'.'.$extension;
        \Image::make($image)->resize(config('upload.projectImage.width'), config('upload.projectImage.height'), function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path(self::getImagePath().$filename));
        return $filename;
    }

    public function deleteImage()
    {
        $imagePath = public_path(self::getImagePath().$this->image);
        if ($this->image != '' && file_exists($imagePath)) {
            unlink($imagePath);
        }
        $this->image = null;
    }

    public static function getPresentationPath()
    {
        return self::$presentationPath;
    }

    public function presentationUrl()
    {
        if($this->presentation) {
            return asset(self::getPresentationPath().$this->presentation);
        }
        return null;
    }

    public function uploadPresentation(UploadedFile $image)
    {
        $extension = $image->getClientOriginalExtension();
        $filename = $this->id.'_'.uniqid().'.'.$extension;
        $image->move(public_path(self::getPresentationPath()), $filename);
        return $filename;
    }

    public function deletePresentation()
    {
        $imagePath = public_path(self::getPresentationPath().$this->presentation);
        if ($this->presentation != '' && file_exists($imagePath)) {
            unlink($imagePath);
        }
        $this->presentation = null;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished($query)
    {
        return $query->where('status', static::STATUS_PUBLISHED);
    }
}
