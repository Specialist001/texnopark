<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 28.02.2019
 * Time: 14:12
 */

namespace App\Domain\Projects\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\Projects\Models\ProjectTranslation
 *
 * @property int $id
 * @property string|null $created_at
 * @property string|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\ProjectTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\ProjectTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\ProjectTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\ProjectTranslation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\ProjectTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\ProjectTranslation whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $project_id
 * @property string|null $about_developer
 * @property string|null $place
 * @property string|null $short_description
 * @property string|null $description
 * @property string|null $target
 * @property string|null $finally
 * @property string|null $state
 * @property string|null $about_presentation
 * @property string|null $additional_info
 * @property string $locale
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\ProjectTranslation whereAboutDeveloper($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\ProjectTranslation whereAboutPresentation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\ProjectTranslation whereAdditionalInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\ProjectTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\ProjectTranslation whereFinally($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\ProjectTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\ProjectTranslation wherePlace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\ProjectTranslation whereProjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\ProjectTranslation whereShortDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\ProjectTranslation whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Projects\Models\ProjectTranslation whereTarget($value)
 */
class ProjectTranslation extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];
}
