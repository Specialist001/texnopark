<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 04.03.2019
 * Time: 13:50
 */

namespace App\Domain\Projects\Requests;


use App\Domain\Projects\Models\Project;
use App\Domain\Users\Models\User;
use App\Services\TranslationService\TranslationsRule;
use Illuminate\Foundation\Http\FormRequest;

class ProjectRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $data = [
            'project_category_id' => 'required|exists:project_categories,id',
            'project_type_id' => 'required|exists:project_types,id',
            'logo' => 'nullable|image|mimes:jpeg,bmp,png',
            'image' => 'nullable|image|mimes:jpeg,bmp,png',
            'title' => 'required|string|max:255',
            'stage' => 'required|in:'.implode(',', Project::stages()),
            'developer' => 'required|string|max:255',
            'foreign' => 'required|in:0,1',
            'importable' => 'required|in:0,1',
            'readiness' => 'required|integer|min:0|max:100',
            'patent' => 'nullable|string|max:255',
            'implementation_period' => 'required|integer|min:1',
            'payback_period' => 'required|integer|min:1',
            'sum_cost' => 'nullable|integer|min:0',
            'usd_cost' => 'nullable|integer|min:0',
            'sum_need' => 'required|integer|min:1',
            'usd_need' => 'nullable|integer|min:0',
            'sum_current' => 'nullable|integer|min:0',
            'usd_current' => 'nullable|integer|min:0',
            'workers' => 'nullable|integer|min:0',
            'prod_capacity' => 'nullable|integer|min:0',
            'prod_capacity_unit' => 'nullable|integer|min:0',
            'metal_structures' => 'nullable|integer|min:0',
            'sandwich_panels' => 'nullable|integer|min:0',
            'indoor_area' => 'nullable|integer|min:0',
            'presentation' => 'nullable|file',

            'translations' => ['required', new TranslationsRule()],
            'translations.'.\LaravelLocalization::getDefaultLocale().'.place' => 'required|string',
            'translations.'.\LaravelLocalization::getDefaultLocale().'.short_description' => 'required|string|max:255',
            'translations.'.\LaravelLocalization::getDefaultLocale().'.description' => 'required|string',
            'translations.'.\LaravelLocalization::getDefaultLocale().'.target' => 'required|string',
//            'translations.'.\LaravelLocalization::getDefaultLocale().'.finally' => 'required|string',
//            'translations.'.\LaravelLocalization::getDefaultLocale().'.state' => 'required|string',

            'translations.*.place' => 'nullable|string',
            'translations.*.short_description' => 'nullable|string',
            'translations.*.description' => 'nullable|string',
            'translations.*.target' => 'nullable|string',
            'translations.*.finally' => 'nullable|string',
            'translations.*.state' => 'nullable|string',
            'translations.*.about_developer' => 'nullable|string',
            'translations.*.about_presentation' => 'nullable|string',
            'translations.*.additional_info' => 'nullable|string',
        ];

        if(!$this->user()->isStartUp()) {
            $data['user_id'] = 'required|exists:users,id,role,'.User::ROLE_STARTUP;
            $data['status'] = 'required|in:'.implode(',', Project::statuses());
        }

        return [];
    }
}
