<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 02.04.2019
 * Time: 16:18
 */

namespace App\Domain\ProofReadings\Filters;


use App\Services\FilterService\Filter;
use Illuminate\Http\Request;

class ProofReadingFilter extends Filter
{

    /**
     * Доступные фильтры.
     *
     * @var array
     */
    protected $available = [
        'attached_user',
        'sort', 'perPage'
    ];

    /**
     * Фильтры по умолчанию.
     *
     * @var array
     */
    protected $defaults = [
        'sort' => '-id'
    ];

    public function __construct(Request $request)
    {
        $this->input = $this->prepareInput($request->all());
    }

    /**
     * Инициализация фильтра.
     */
    protected function init()
    {
        //Добавляем поля для сортировки
        $this->addSortable('id');
//        $this->addSortable('verified');
//        $this->addSortable('active');
    }
    public function attached_user($value)
    {
        return $this->builder->where($this->column('attached_user'), $value);
    }
}
