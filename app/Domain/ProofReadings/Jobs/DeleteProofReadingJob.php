<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 02.04.2019
 * Time: 16:33
 */

namespace App\Domain\ProofReadings\Jobs;


use App\Domain\ProofReadings\Models\ProofReading;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class DeleteProofReadingJob
{
    use Dispatchable, Queueable;

    public $proof;

    public function __construct(ProofReading $proof)
    {
        $this->proof = $proof;
    }


    /**
     * Execute the job.
     *
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $this->proof->deleteFile();
            $this->proof->delete();
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();
    }
}
