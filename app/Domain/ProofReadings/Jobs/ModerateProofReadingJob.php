<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 02.04.2019
 * Time: 16:35
 */

namespace App\Domain\ProofReadings\Jobs;


use App\Domain\ProofReadings\Models\ProofReading;
use App\Domain\ProofReadings\Requests\ModerateProofReadingRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class ModerateProofReadingJob
{
    use Dispatchable, Queueable;

    public $request;

    public $proof;

    public function __construct(ProofReading $proof, ModerateProofReadingRequest $request)
    {
        $this->proof = $proof;
        $this->request = $request;
    }


    /**
     * Execute the job.
     *
     * @return ProofReading
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $proof = $this->proof;
            if($this->request->user()->isAdmin()) {
                $proof->attached_user = $this->request->input('attached_user', null);
            }

            $proof->status = $this->request->input('status');
            $proof->save();

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $proof;
    }
}
