<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 02.04.2019
 * Time: 16:20
 */

namespace App\Domain\ProofReadings\Jobs;


use App\Domain\ProofReadings\Models\ProofReading;
use App\Domain\ProofReadings\Requests\ProofReadingRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class StoreProofReadingJob
{
    use Dispatchable, Queueable;

    public $request;

    public function __construct(ProofReadingRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return ProofReading
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $proofReading = new ProofReading();

            $proofReading->attached_user = null;

            $proofReading->name = $this->request->input('name');
            $proofReading->email = $this->request->input('email');
            $proofReading->phone = $this->request->input('phone', '');
            $proofReading->type = $this->request->input('type');
            $proofReading->wallpaper = $this->request->input('wallpaper', '');

            $proofReading->hash = uniqid();
            $proofReading->file = null;
            $proofReading->is_moderated = 0;
            $proofReading->status = ProofReading::STATUS_MODERATING;

            $proofReading->save();

            if ($this->request->hasFile('file')) {
                $proofReading->file = $proofReading->uploadFile($this->request->file('file'));
            }
            $proofReading->save();

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $proofReading;
    }
}
