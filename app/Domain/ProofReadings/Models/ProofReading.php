<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 02.04.2019
 * Time: 14:48
 */

namespace App\Domain\ProofReadings\Models;


use App\Domain\Users\Models\User;
use App\Services\FilterService\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

/**
 * App\Domain\ProofReadings\Models\ProofReading
 *
 * @property int $id
 * @property int $attached_user
 * @property string $email
 * @property string|null $wallpaper
 * @property string|null $phone
 * @property string|null $hash
 * @property string $type
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProofReadings\Models\ProofReading filter(\App\Services\FilterService\Filter $filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProofReadings\Models\ProofReading newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProofReadings\Models\ProofReading newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProofReadings\Models\ProofReading paginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProofReadings\Models\ProofReading query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProofReadings\Models\ProofReading simplePaginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProofReadings\Models\ProofReading whereAttachedUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProofReadings\Models\ProofReading whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProofReadings\Models\ProofReading whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProofReadings\Models\ProofReading whereHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProofReadings\Models\ProofReading whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProofReadings\Models\ProofReading wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProofReadings\Models\ProofReading whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProofReadings\Models\ProofReading whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProofReadings\Models\ProofReading whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProofReadings\Models\ProofReading whereWallpaper($value)
 * @mixin \Eloquent
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProofReadings\Models\ProofReading whereName($value)
 * @property string|null $file
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProofReadings\Models\ProofReading whereFile($value)
 * @property int|null $is_moderated
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\ProofReadings\Models\ProofReading whereIsModerated($value)
 * @property-read \App\Domain\Users\Models\User|null $user
 */
class ProofReading extends Model
{
    use Filterable;

    const TYPE_TRANSLATE = 'translate';
    const TYPE_CHECK = 'check';
    const TYPE_PUBLISH = 'publish';

    const STATUS_MODERATING = 'moderating';
    const STATUS_PROGRESS = 'progress';
    const STATUS_DONE = 'done';

    protected static $filePath = 'uploads/proof/';

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'attached_user');
    }

    public function getRouteKeyName()
    {
        return 'hash';
    }

    public static function types()
    {
        return [
            static::TYPE_TRANSLATE,
            static::TYPE_CHECK,
            static::TYPE_PUBLISH,
        ];
    }

    public static function statuses()
    {
        return [
            static::STATUS_MODERATING,
            static::STATUS_PROGRESS,
            static::STATUS_DONE,
        ];
    }

    public static function getFilePath()
    {
        return self::$filePath;
    }

    public function fileUrl()
    {
        if($this->file) {
            return asset(self::getFilePath().$this->file);
        }
        return null;
    }

    public function uploadFile(UploadedFile $image)
    {
        $extension = $image->getClientOriginalExtension();
        $filename = $this->id.'_'.uniqid().'.'.$extension;
        $image->move(public_path(self::getFilePath()), $filename);
        return $filename;
    }

    public function deleteFile()
    {
        $imagePath = public_path(self::getFilePath().$this->file);
        if ($this->file != '' && file_exists($imagePath)) {
            unlink($imagePath);
        }
        $this->file = null;
    }

}
