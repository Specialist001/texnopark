<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 02.04.2019
 * Time: 16:30
 */

namespace App\Domain\ProofReadings\Requests;


use App\Domain\ProofReadings\Models\ProofReading;
use App\Domain\Users\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class ModerateProofReadingRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'attached_user' => 'nullable|exists:users,id,role,'.User::ROLE_PROOF,
            'status' => 'required|in:'.implode(',', ProofReading::statuses()),
        ];
    }
}
