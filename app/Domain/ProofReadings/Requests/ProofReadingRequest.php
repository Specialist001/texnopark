<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 02.04.2019
 * Time: 16:14
 */

namespace App\Domain\ProofReadings\Requests;


use App\Domain\ProofReadings\Models\ProofReading;
use Illuminate\Foundation\Http\FormRequest;

class ProofReadingRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|email',
            'phone' => 'nullable|string|max:255',
            'type' => 'required|in:'.implode(',', ProofReading::types()),
            'wallpaper' => 'required_if:type,publish',
            'file' => 'required|file',
        ];
    }
}
