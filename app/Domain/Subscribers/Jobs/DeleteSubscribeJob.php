<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 03.04.2019
 * Time: 12:58
 */

namespace App\Domain\Subscribers\Jobs;


use App\Domain\Subscribers\Models\Subscriber;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class DeleteSubscribeJob
{
    use Dispatchable, Queueable;

    public $subscriber;

    public function __construct(Subscriber $subscriber)
    {
        $this->subscriber = $subscriber;
    }

    /**
     * Execute the job.
     *
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $this->subscriber->delete();
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();
    }
}
