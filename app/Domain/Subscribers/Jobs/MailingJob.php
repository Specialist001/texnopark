<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 03.04.2019
 * Time: 13:11
 */

namespace App\Domain\Subscribers\Jobs;


use App\Domain\Subscribers\Models\Subscriber;
use App\Domain\Subscribers\Requests\MailingRequest;
use App\Domain\Users\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class MailingJob implements ShouldQueue
{
    use Dispatchable, Queueable, InteractsWithQueue, SerializesModels;

    public $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function handle()
    {
        $is_subscribers = $this->request['subscribers'] ?? 0;
        $is_users = $this->request['users'] ?? 0;

        $send = [];

        if ($is_subscribers) {
            $subscribers = Subscriber::where('status', 1)->get();
            foreach ($subscribers as $subscriber) {
                $subscriber->sendEmailNotification($this->request['subject'], $this->request['content']);
                $send[] = $subscriber->email;
            }
        }
        if ($is_users) {
            $users = User::where('active', 1)->whereNotNull('email_verified')->whereIn('role', [User::ROLE_INVESTOR, User::ROLE_STARTUP]);
            foreach ($users as $user) {
                if ($user->email && !in_array($user->email, $send)) {
                    $subscriber = new Subscriber();
                    $subscriber->email = $user->email;
                    $subscriber->sendEmailNotification($this->request['subject'], $this->request['content']);
                    $send[] = $subscriber->email;
                }
            }
        }
    }
}
