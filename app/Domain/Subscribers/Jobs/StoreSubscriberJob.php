<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 03.04.2019
 * Time: 12:52
 */

namespace App\Domain\Subscribers\Jobs;


use App\Domain\Subscribers\Models\Subscriber;
use App\Domain\Subscribers\Requests\AddSubscriberRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class StoreSubscriberJob
{
    use Dispatchable, Queueable;

    public $request;

    public function __construct(AddSubscriberRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return Subscriber
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $subscriber = new Subscriber();
            $subscriber->name = $this->request->input('name');
            $subscriber->email = $this->request->input('email');
            $subscriber->status = $this->request->input('status');
            $subscriber->save();

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $subscriber;
    }
}
