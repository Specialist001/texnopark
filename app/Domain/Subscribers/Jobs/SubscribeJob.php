<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 03.04.2019
 * Time: 12:05
 */

namespace App\Domain\Subscribers\Jobs;


use App\Domain\Subscribers\Models\Subscriber;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;

class SubscribeJob
{
    use Dispatchable, Queueable;

    public $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }


    /**
     * Execute the job.
     *
     * @return Subscriber
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $subscriber = new Subscriber();
            $subscriber->name = $this->request->input('name');
            $subscriber->email = $this->request->input('email');
            $subscriber->status = 1;
            $subscriber->save();
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $subscriber;
    }
}
