<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 03.04.2019
 * Time: 12:56
 */

namespace App\Domain\Subscribers\Jobs;


use App\Domain\Subscribers\Models\Subscriber;
use App\Domain\Subscribers\Requests\UpdateSubscriberRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateSubscriberJob
{
    use Dispatchable, Queueable;

    public $subscriber;

    public $request;

    public function __construct(Subscriber $subscriber, UpdateSubscriberRequest $request)
    {
        $this->subscriber = $subscriber;
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return Subscriber
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $subscriber = $this->subscriber;
            $subscriber->name = $this->request->input('name');
            $subscriber->email = $this->request->input('email');
            $subscriber->status = $this->request->input('status');
            $subscriber->save();

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $subscriber;
    }
}
