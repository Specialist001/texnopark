<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 03.04.2019
 * Time: 11:51
 */

namespace App\Domain\Subscribers\Models;


use App\Domain\Subscribers\Notifications\MailingNotification;
use App\Services\FilterService\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * App\Domain\Subscribers\Models\Subscriber
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string|null $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Subscribers\Models\Subscriber newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Subscribers\Models\Subscriber newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Subscribers\Models\Subscriber query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Subscribers\Models\Subscriber whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Subscribers\Models\Subscriber whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Subscribers\Models\Subscriber whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Subscribers\Models\Subscriber whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Subscribers\Models\Subscriber whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Subscribers\Models\Subscriber whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Subscribers\Models\Subscriber filter(\App\Services\FilterService\Filter $filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Subscribers\Models\Subscriber paginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Subscribers\Models\Subscriber simplePaginateFilter($perPage = 20)
 */
class Subscriber extends Model
{
    use Notifiable, Filterable;

    protected $guarded = ['id'];

    public function sendEmailNotification($subject, $content)
    {
        $this->notify(new MailingNotification($subject, $content));
    }
}
