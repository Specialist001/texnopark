<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 03.04.2019
 * Time: 13:09
 */

namespace App\Domain\Subscribers\Requests;


use Illuminate\Foundation\Http\FormRequest;

class MailingRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject' => 'required|string|max:255',
            'content' => 'required|string',
            'subscribers' => 'required_without:users|in:0,1',
            'users' => 'required_without:subscribers|in:0,1',
        ];
    }
}
