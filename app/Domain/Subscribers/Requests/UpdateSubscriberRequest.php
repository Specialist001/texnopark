<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 03.04.2019
 * Time: 12:02
 */

namespace App\Domain\Subscribers\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateSubscriberRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => [
                'nullable',
                'string',
                'max:255',
                Rule::unique('subscribers')->ignore($this->route('subscriber'))
            ],
            'status' => 'required|in:0,1',
        ];
    }
}
