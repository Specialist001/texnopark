<?php

namespace App\Domain\Units\Jobs;

use App\Domain\ProjectCategories\Models\ProjectCategory;
use App\Domain\Projects\Jobs\DeleteProjectJob;
use App\Domain\Units\Models\Unit;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class DeleteUnitJob
{
    use Dispatchable, Queueable;

    /**
     * @var Unit
     */
    private $unit;

    /**
     * Create a new job instance.
     *
     * DeleteUserJob constructor.
     * @param Unit $unit
     */
    public function __construct(Unit $unit)
    {
        $this->unit = $unit;
    }

    /**
     * Execute the job.
     *
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
//            foreach ($this->unit->projects as $project) {
//                dispatch_now(new DeleteProjectJob($project));
//            }
//            $this->unit->deleteIcon();
            $this->unit->delete();
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();
    }
}
