<?php

namespace App\Domain\Units\Jobs;

use App\Domain\Units\Models\Unit;
use App\Domain\Units\Models\UnitTranslation;
use App\Domain\Units\Requests\UnitRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateUnitJob
{
    use Queueable, Dispatchable;

    /**
     * @var Unit
     */
    private $request;

    /**
     * @var Unit
     */
    private $unit;

    /**
     * UpdateUnitJob constructor.
     * @param Unit $category
     * @param UnitRequest $request
     */
    public function __construct(Unit $unit, UnitRequest $request)
    {
        $this->unit = $unit;
        $this->request = $request;
    }

    /**
     * @return Unit
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $unit = $this->unit;
            $unit->active = $this->request->input('active', 1);
            $unit->save();

            $unit->translations()->delete();
            $translations = [];
            foreach ($this->request->input('translations', []) as $translate) {
                if($translate['name'] == '') continue;
                $translations[] = new UnitTranslation($translate);
            }

            if (!empty($translations)) {
                $unit->translations()->saveMany($translations);
            }

            $unit->save();

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $unit;
    }
}
