<?php


namespace App\Domain\Units\Models;


use App\Services\FilterService\Filterable;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Domain\Units\Models\Unit
 *
 * @property int $id
 * @property int|null $active
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Units\Models\UnitTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Units\Models\Unit withTranslation()
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Units\Models\Unit filter(\App\Services\FilterService\Filter $filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Units\Models\Unit listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Units\Models\Unit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Units\Models\Unit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Units\Models\Unit notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Units\Models\Unit orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Units\Models\Unit orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Units\Models\Unit orderByTranslation($key, $sortmethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Units\Models\Unit paginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Units\Models\Unit query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Units\Models\Unit simplePaginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Units\Models\Unit translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Units\Models\Unit translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Units\Models\Unit whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Units\Models\Unit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Units\Models\Unit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Units\Models\Unit whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Units\Models\Unit whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Units\Models\Unit whereUpdatedAt($value)
 * @mixin \Eloquent
 */

class Unit extends Model
{
    use Translatable, Filterable;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public $translatedAttributes = [
        'name',
    ];

    public static function actives()
    {
        return [
            static::STATUS_ACTIVE,
            static::STATUS_INACTIVE,
        ];
    }
}
