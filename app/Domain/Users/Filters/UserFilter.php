<?php

namespace App\Domain\Users\Filters;

use App\Services\FilterService\Filter;
use Illuminate\Http\Request;

class UserFilter extends Filter
{
    /**
     * Доступные фильтры.
     *
     * @var array
     */
    protected $available = [
        'id',
        'username',
        'email',
        'phone',
        'name',
        'active',
        'sort', 'perPage'
    ];

    /**
     * Фильтры по умолчанию.
     *
     * @var array
     */
    protected $defaults = [
        'sort' => '-id'
    ];

    public function __construct(Request $request)
    {
        $this->input = $this->prepareInput($request->all());
    }

    /**
     * Инициализация фильтра.
     */
    protected function init()
    {
        //Добавляем поля для сортировки
        $this->addSortable('id');
        $this->addSortable('username');
        $this->addSortable('name');
//        $this->addSortable('verified');
//        $this->addSortable('active');
    }

    /**
     * Поиск по id.
     *
     * @param $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function id($value)
    {
        return $this->builder->where($this->column('id'), $value);
    }

    /**
     * Поиск по имени.
     *
     * @param $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function name($value)
    {
        return $this->builder->where($this->column('name'), 'like', '%'.$value.'%');
    }

    public function username($value)
    {
        return $this->builder->where($this->column('username'), 'like', '%'.$value.'%');
    }

    public function email($value)
    {
        return $this->builder->where($this->column('email'), 'like', '%'.$value.'%');
    }

    public function phone($value)
    {
        return $this->builder->where($this->column('phone'), 'like', '%'.$value.'%');
    }

    public function role($value)
    {
        return $this->builder->where($this->column('role'), $value);
    }

    public function active($value)
    {
        return $this->builder->where($this->column('active'), $value);
    }
}
