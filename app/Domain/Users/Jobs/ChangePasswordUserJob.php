<?php

namespace App\Domain\Users\Jobs;

use App\Domain\Users\Models\User;
use App\Domain\Users\Requests\UpdateProfileUserRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Hash;

class ChangePasswordUserJob
{
    use Dispatchable, Queueable;

    /**
     * @var UpdateProfileUserRequest
     */
    private $request;

    public function __construct(UpdateProfileUserRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return User
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $user = $this->request->user();
            if (!Hash::check($this->request->input('current_password'), $user->password)) {
                throw new \Exception();
            }
            $user->password = Hash::make($this->request->input('password'));
            $user->save();
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $user;
    }
}
