<?php

namespace App\Domain\Users\Jobs;

use App\Domain\Forum\Jobs\DeleteForumAnswerJob;
use App\Domain\Forum\Jobs\DeleteForumThemeJob;
use App\Domain\Investments\Jobs\DeleteInvestmentJob;
use App\Domain\Projects\Jobs\DeleteProjectJob;
use App\Domain\Users\Models\User;
use App\Domain\Users\Models\UserFavourites;
use App\Domain\Users\Models\UserNotifications;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class DeleteUserJob
{
    use Dispatchable, Queueable;

    /**
     * @var User
     */
    private $user;

    /**
     * Create a new job instance.
     *
     * DeleteUserJob constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {

            foreach ($this->user->projects as $project) {
                dispatch_now(new DeleteProjectJob($project));
            }
            foreach ($this->user->investments as $investment) {
                dispatch_now(new DeleteInvestmentJob($investment));
            }
            foreach ($this->user->forumThemes as $theme) {
                dispatch_now(new DeleteForumThemeJob($theme));
            }
            foreach ($this->user->forumAnswers as $answer) {
                dispatch_now(new DeleteForumAnswerJob($answer));
            }

            UserFavourites::where('user_id', $this->user->id)->delete();
            UserNotifications::where('user_id', $this->user->id)->delete();

            $this->user->delete();
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();
    }
}
