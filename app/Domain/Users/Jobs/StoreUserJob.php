<?php

namespace App\Domain\Users\Jobs;

use App\Domain\Users\Models\User;
use App\Domain\Users\Requests\StoreUserRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Hash;

class StoreUserJob
{
    use Dispatchable, Queueable;

    /**
     * @var StoreUserRequest
     */
    private $request;

    /**
     * StoreUserJob constructor.
     * @param StoreUserRequest $request
     */
    public function __construct(StoreUserRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return User
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $user = new User();
            $user->username = $this->request->input('username');
            $user->role = $this->request->input('role');
            $user->name = $this->request->input('name');
            $user->password = Hash::make($this->request->input('password'));
            $user->remember_token = str_random(10);
            $user->active = $this->request->input('active', 1);

            if($this->request->input('role') == User::ROLE_INVESTOR || $this->request->input('role') == User::ROLE_STARTUP) {
                $user->email = $this->request->input('email', null);
                if ($this->request->input('verified', 0) == 1) {
                    $user->email_verified_at = now();
                } else {
                    $user->email_verified_at = null;
                }
                $user->city_id = $this->request->input('city_id', null);
                $user->phone = $this->request->input('phone', null);
                $user->personality = $this->request->input('personality', null);
                $user->site = $this->request->filled('site') ? $this->request->input('site', null): null;
                $user->locale = $this->request->input('locale', \LaravelLocalization::getDefaultLocale());
            }
            else {
                $user->email = $this->request->input('username').'@'.config('app.domain');
                $user->email_verified_at = now();
            }

            $user->save();
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $user;
    }
}
