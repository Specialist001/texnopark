<?php

namespace App\Domain\Users\Jobs;

use App\Domain\Users\Models\User;
use App\Domain\Users\Requests\UpdateProfileUserRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateProfileUserJob
{
    use Dispatchable, Queueable;

    /**
     * @var UpdateProfileUserRequest
     */
    private $request;

    /**
     * UpdateProfileUserJob constructor.
     * @param UpdateProfileUserRequest $request
     */
    public function __construct(UpdateProfileUserRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return User
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $user = $this->request->user();
            $user->name = $this->request->input('name');

            if ($user->isStartUp() || $user->isInvestor()) {
                $user->city_id = $this->request->input('city_id');
                $user->phone = $this->request->input('phone');
                $user->personality = $this->request->input('personality');
                $user->site = $this->request->filled('site') ? $this->request->input('site', null): null;
                $user->locale = $this->request->input('locale');
            }

            $user->save();

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $user;
    }
}
