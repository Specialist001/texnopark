<?php

namespace App\Domain\Users\Jobs;

use App\Domain\Users\Models\User;
use App\Domain\Users\Requests\UpdateUserRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Hash;

class UpdateUserJob
{
    use Dispatchable, Queueable;

    /**
     * @var UpdateUserRequest
     */
    private $request;

    /**
     * @var User
     */
    private $user;

    /**
     * Create a new job instance.
     *
     * UpdateUserJob constructor.
     * @param User $user
     * @param UpdateUserRequest $request
     */
    public function __construct(User $user, UpdateUserRequest $request)
    {
        $this->request = $request;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return User
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $user = $this->user;

            $user->username = $this->request->input('username');
            $user->name = $this->request->input('name');
            $user->active = $this->request->input('active', 1);

            if($this->request->filled('password')) {
                $user->password = Hash::make($this->request->input('password'));
            }

            if($this->request->input('role') == User::ROLE_INVESTOR || $this->request->input('role') == User::ROLE_STARTUP) {
                $user->email = $this->request->input('email', null);
                if ($this->request->input('verified', 0) == 1) {
                    $user->email_verified_at = now();
                } else {
                    $user->email_verified_at = null;
                }
                $user->city_id = $this->request->input('city_id', null);
                $user->phone = $this->request->input('phone', null);
                $user->personality = $this->request->input('personality', null);
                $user->site = $this->request->filled('site') ? $this->request->input('site', null): null;
                $user->locale = $this->request->input('locale', \LaravelLocalization::getDefaultLocale());
            }

            $user->save();
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $user;
    }
}
