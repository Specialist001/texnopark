<?php

namespace App\Domain\Users\Models;

use App\Domain\Forum\Models\ForumAnswer;
use App\Domain\Forum\Models\ForumTheme;
use App\Domain\Investments\Models\Investment;
use App\Domain\Projects\Models\Project;
use App\Domain\Users\Notifications\ResetPassword;
use App\Domain\Users\Notifications\VerifyEmail;
use App\Services\FilterService\Filterable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\Domain\Users\Models\User
 *
 * @property int $id
 * @property string $username
 * @property string $role
 * @property string $name
 * @property string $email
 * @property string|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User filter(\App\Services\FilterService\Filter $filters)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\Users\Models\User onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User paginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User simplePaginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\Users\Models\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\Users\Models\User withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User admins()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User managers()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User proofs()
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Projects\Models\Project[] $projects
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User project()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User startUp()
 * @property int|null $city_id
 * @property string|null $phone
 * @property string|null $site
 * @property string|null $personality
 * @property string|null $locale
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User investors()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User startUps()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User wherePersonality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\User whereSite($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Investments\Models\Investment[] $investments
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Users\Models\UserNotifications[] $customNotifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Forum\Models\ForumAnswer[] $forumAnswers
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Forum\Models\ForumTheme[] $forumThemes
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, SoftDeletes, Filterable;

    const ROLE_ADMIN = 'admin';
    const ROLE_MANAGER = 'manager';
    const ROLE_PROOF = 'proof';
    const ROLE_INVESTOR = 'investor';
    const ROLE_STARTUP = 'startup';

    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 0;

    const PERSONALITY_LEGAL = 'legal';
    const PERSONALITY_INDIVIDUAL = 'individual';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function preferredLocale()
    {
        return $this->locale;
    }

    public static function roles()
    {
        return [
            static::ROLE_ADMIN,
            static::ROLE_MANAGER,
            static::ROLE_PROOF,
            static::ROLE_INVESTOR,
            static::ROLE_STARTUP,
        ];
    }

    public static function statuses()
    {
        return [
            static::STATUS_ACTIVE,
            static::STATUS_NOT_ACTIVE,
        ];
    }

    public static function personalities()
    {
        return [
            static::PERSONALITY_INDIVIDUAL,
            static::PERSONALITY_LEGAL,
        ];
    }

    public function isAdmin()
    {
        return $this->role === static::ROLE_ADMIN;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAdmins($query)
    {
        return $query->where('role', static::ROLE_ADMIN);
    }

    public function isManager()
    {
        return $this->role === static::ROLE_MANAGER;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeManagers($query)
    {
        return $query->where('role', static::ROLE_MANAGER);
    }

    public function isProof()
    {
        return $this->role === static::ROLE_PROOF;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeProofs($query)
    {
        return $query->where('role', static::ROLE_PROOF);
    }

    public function isStartUp()
    {
        return $this->role === static::ROLE_STARTUP;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStartUps($query)
    {
        return $query->where('role', static::ROLE_STARTUP);
    }

    public function isInvestor()
    {
        return $this->role === static::ROLE_INVESTOR;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeInvestors($query)
    {
        return $query->where('role', static::ROLE_INVESTOR);
    }

    /**
     * Check role
     *
     * @param array|string $roles
     * @return bool
     */
    public function hasRole($roles)
    {
        $myRoles = self::roles();

        if (is_array($roles)) {
            foreach ($roles as $role) {
                if (in_array($role, $myRoles) && $this->role == $role) {
                    return true;
                }
            }

            return false;
        }

        return in_array($roles, $myRoles) && $this->role == $roles;
    }

    public function investments()
    {
        return $this->hasMany(Investment::class);
    }

    public function customNotifications()
    {
        return $this->hasMany(UserNotifications::class);
    }

    public function projects()
    {
        return $this->hasMany(Project::class);
    }


    /**
     * @param $query Builder
     * @return Builder
     */
    public function scopeProject($query)
    {
        return $query->with(['projects' => function ($query){
            /**
             * @var $query Builder|Project
             */
            $query->withTranslation()->withType()->withCategory();
        }]);
    }


    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
//        $this->notify(new VerifyEmail);
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
//        $this->notify(new ResetPassword($token));
    }

    public function forumThemes()
    {
        return $this->hasMany(ForumTheme::class);
    }

    public function forumAnswers()
    {
        return $this->hasMany(ForumAnswer::class);
    }
}
