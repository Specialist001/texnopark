<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 11.03.2019
 * Time: 5:47
 */

namespace App\Domain\Users\Models;


use App\Domain\Projects\Models\Project;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\Users\Models\UserFavourites
 *
 * @property int $id
 * @property int $user_id
 * @property int $project_id
 * @property-read \App\Domain\Projects\Models\Project $project
 * @property-read \App\Domain\Users\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\UserFavourites newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\UserFavourites newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\UserFavourites query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\UserFavourites whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\UserFavourites whereProjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\UserFavourites whereUserId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\UserFavourites withProject()
 */
class UserFavourites extends Model
{
    protected $guarded = ['id'];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * @param $query Builder
     * @return Builder
     */
    public function scopeWithProject($query)
    {
        return $query->with(['project' => function ($query){
            /**
             * @var $query Builder|Project
             */
            $query->withTranslation();
            $query->withCategory();
            $query->withType();
        }]);
    }
}
