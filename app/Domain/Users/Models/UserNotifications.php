<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 11.03.2019
 * Time: 12:54
 */

namespace App\Domain\Users\Models;


use App\Domain\Projects\Models\Project;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\Users\Models\UserNotifications
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $action
 * @property int|null $target_id
 * @property int|null $viewed
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\UserNotifications newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\UserNotifications newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\UserNotifications query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\UserNotifications whereAction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\UserNotifications whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\UserNotifications whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\UserNotifications whereTargetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\UserNotifications whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\UserNotifications whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\UserNotifications whereViewed($value)
 * @mixin \Eloquent
 * @property int $project_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Users\Models\UserNotifications whereProjectId($value)
 * @property-read \App\Domain\Projects\Models\Project $project
 */
class UserNotifications extends Model
{
    protected $guarded = ['id'];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
