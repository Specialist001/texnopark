<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 08.03.2019
 * Time: 21:25
 */

namespace App\Domain\Users\Notifications;


use Illuminate\Support\Facades\Lang;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPassword extends Notification
{
    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * The callback that should be used to build the mail message.
     *
     * @var \Closure|null
     */
    public static $toMailCallback;

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $this->token);
        }

        return (new MailMessage)
            ->subject(trans('mail.reset.subject'))
            ->line(trans('mail.reset.top_text'))
//            ->action(trans('mail.reset.button'), url(config('app.url').route('portal.auth.resetForm', $this->token, false)))
            ->action(trans('mail.reset.button'), route('portal.auth.resetForm', ['token' => $this->token, 'email' => $notifiable->email]))
            ->line(trans('mail.reset.bottom_text', ['count' => config('auth.passwords.users.expire')]))
            ->line(trans('mail.reset.no_further'));
    }

    /**
     * Set a callback that should be used when building the notification mail message.
     *
     * @param  \Closure  $callback
     * @return void
     */
    public static function toMailUsing($callback)
    {
        static::$toMailCallback = $callback;
    }
}
