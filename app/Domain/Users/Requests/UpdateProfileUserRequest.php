<?php

namespace App\Domain\Users\Requests;

use App\Domain\Users\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = [
            'name' => 'required|string|max:255',
            'password' => 'nullable|min:4|confirmed',
            'current_password' => 'required_with:password',
        ];

        if ($this->user()->isStartUp() || $this->user()->isInvestor()) {
            $data['city_id'] = ['required', 'exists:cities,id'];
            $data['phone'] = ['required', 'string', 'max:255'];
            $data['personality'] = 'required|in:'.implode(',', User::personalities());
            $data['site'] = ['nullable', 'string', 'max:255'];
            $data['locale'] = ['required', 'in:'.implode(',', array_keys(\LaravelLocalization::getSupportedLocales()))];
        }

        return $data;
    }
}
