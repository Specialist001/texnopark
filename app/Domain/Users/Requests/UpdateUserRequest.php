<?php

namespace App\Domain\Users\Requests;

use App\Domain\Users\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => [
                'required',
                'string',
                'max:255',
                Rule::unique('users')->ignore($this->route('user'))
            ],
//            'role' => 'required|in:'.implode(',', User::roles()),
            'name' => 'required|string|max:255',
            'password' => 'nullable|string|max:255|min:4',
            'active' => 'boolean',

            'email' => [
                'nullable',
                'string',
                'max:255',
                Rule::unique('users')->ignore($this->route('user'))
            ],
            'city_id' => 'nullable|exists:cities,id',
            'verified' => 'nullable|in:0,1',
            'phone' => 'nullable|string|max:255',
            'site' => 'nullable|string|max:255',
            'personality' => 'nullable|in:'.implode(',', User::personalities()),
            'locale' => 'nullable|in:'.implode(',', array_keys(\LaravelLocalization::getSupportedLocales())),
        ];
    }
}
