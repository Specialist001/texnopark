<?php

namespace App\Domain\VacancyRespond\Filters;

use App\Services\FilterService\Filter;
use Illuminate\Http\Request;

class VacancyRespondFilter extends Filter
{
    protected $available = [
        'vacancy_id',
        'first_name',
        'email',
    ];

    protected $defaults = [
        'sort' => '-created_at',
    ];

    public function __construct(Request $request)
    {
        $this->input = $this->prepareInput($request->all());
    }

    protected function init()
    {
        //Добавляем поля для сортировки
        $this->addSortable('id');
        $this->addSortable('crated_at');
    }

    public function vacancy_id($value)
    {
        return $this->builder->where($this->column('vacancy_id'), $value);
    }
}
