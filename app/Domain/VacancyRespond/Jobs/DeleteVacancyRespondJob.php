<?php


namespace App\Domain\VacancyRespond\Jobs;


use App\Domain\Pages\Models\Page;
use App\Domain\VacancyRespond\Models\VacancyRespond;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class DeleteVacancyRespondJob
{
    use Queueable, Dispatchable;

    protected $vacancyRespond;

    public function __construct(VacancyRespond $vacancyRespond)
    {
        $this->vacancyRespond = $vacancyRespond;
    }

    /**
     * Execute the job.
     *
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $this->vacancyRespond->deleteFile();
            $this->vacancyRespond->delete();
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();
    }
}
