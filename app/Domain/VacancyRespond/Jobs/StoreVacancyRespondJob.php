<?php


namespace App\Domain\VacancyRespond\Jobs;


use App\Domain\Pages\Models\Page;
use App\Domain\VacancyRespond\Models\VacancyRespond;
use App\Domain\VacancyRespond\Requests\VacancyRespondRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class StoreVacancyRespondJob
{
    use Queueable, Dispatchable;

    /**
     * @var VacancyRespondRequest
     */
    private $request;

    /**
     * StoreForumAnswerJob constructor.
     * @param Page $vacancy
     * @param VacancyRespondRequest $request
     */
    public function __construct(VacancyRespondRequest $request)
    {
        $this->request = $request;
//        $this->vacancy = $vacancy;
    }

    /**
     * Execute the job.
     *
     * @return VacancyRespond
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $respond = new VacancyRespond();
            $respond->vacancy_id = $this->request->input('vacancy_id');
            $respond->name = $this->request->input('first_name');
            $respond->phone = $this->request->input('phone');
            $respond->resume_file = null;

            $respond->save();

            if ($this->request->hasFile('resume_file')) {
                $respond->resume_file = $respond->uploadFile($this->request->file('resume_file'));
            }

            $respond->save();

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $respond;
    }
}
