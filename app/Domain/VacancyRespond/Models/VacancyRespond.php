<?php


namespace App\Domain\VacancyRespond\Models;


use App\Domain\Pages\Models\Page;
use App\Services\FilterService\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\UploadedFile;

/**
 * App\Domain\VacancyRespond\Models\VacancyRespond
 *
 * @property int $id
 * @property int $vacancy_id
 * @property string|null $name
 * @property string|null $phone
 * @property string|null $resume_file
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Domain\Pages\Models\Page $vacancy
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\VacancyRespond\Models\VacancyRespond newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\VacancyRespond\Models\VacancyRespond newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\VacancyRespond\Models\VacancyRespond onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\VacancyRespond\Models\VacancyRespond query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\VacancyRespond\Models\VacancyRespond whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\VacancyRespond\Models\VacancyRespond whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\VacancyRespond\Models\VacancyRespond whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\VacancyRespond\Models\VacancyRespond whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\VacancyRespond\Models\VacancyRespond wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\VacancyRespond\Models\VacancyRespond whereResumeFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\VacancyRespond\Models\VacancyRespond whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\VacancyRespond\Models\VacancyRespond whereVacancyId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\VacancyRespond\Models\VacancyRespond withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\VacancyRespond\Models\VacancyRespond withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\VacancyRespond\Models\VacancyRespond filter(\App\Services\FilterService\Filter $filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\VacancyRespond\Models\VacancyRespond paginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\VacancyRespond\Models\VacancyRespond simplePaginateFilter($perPage = 20)
 */
class VacancyRespond extends Model
{
    use Filterable, SoftDeletes;

    protected $guarded = ['id'];

    protected static $filePath = 'uploads/vacancy_respond/';


    public function vacancy()
    {
        return $this->belongsTo(Page::class);
    }

    public static function getFilePath()
    {
        return self::$filePath;
    }

    public function fileUrl()
    {
        if($this->resume_file) {
            return asset(self::getFilePath().$this->resume_file);
        }
        return null;
    }

    public function uploadFile(UploadedFile $file)
    {
        $extension = $file->getClientOriginalExtension();
        $filename = $this->id.'_'.uniqid().'.'.$extension;
        $file->move(public_path(self::getFilePath()), $filename);
        return $filename;
    }

    public function deleteFile()
    {
        $filePath = public_path(self::getFilePath().$this->resume_file);
        if ($this->resume_file != '' && file_exists($filePath)) {
            unlink($filePath);
        }
        $this->resume_file = null;
    }
}
