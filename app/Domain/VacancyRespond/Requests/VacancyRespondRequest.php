<?php


namespace App\Domain\VacancyRespond\Requests;


use Illuminate\Foundation\Http\FormRequest;

class VacancyRespondRequest extends FormRequest
{
    public function rules()
    {
        $data = [
            'vacancy_id' => 'required|integer',
            'first_name' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'resume_file' => 'nullable|file|max:10240',
        ];

        return $data;
    }
}
