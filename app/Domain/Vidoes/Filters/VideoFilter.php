<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 02.04.2019
 * Time: 13:08
 */

namespace App\Domain\Vidoes\Filters;


use App\Services\FilterService\Filter;
use Illuminate\Http\Request;

class VideoFilter extends Filter
{

    /**
     * Доступные фильтры.
     *
     * @var array
     */
    protected $available = [
        'id',
        'title',
        'sort', 'perPage'
    ];

    /**
     * Фильтры по умолчанию.
     *
     * @var array
     */
    protected $defaults = [
        'sort' => '-id'
    ];

    public function __construct(Request $request)
    {
        $this->input = $this->prepareInput($request->all());
    }

    /**
     * Инициализация фильтра.
     */
    protected function init()
    {
        //Добавляем поля для сортировки
        $this->addSortable('id');
//        $this->addSortable('verified');
//        $this->addSortable('active');
    }

    /**
     * Поиск по id.
     *
     * @param $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function id($value)
    {
        return $this->builder->where($this->column('id'), $value);
    }

    /**
     * Поиск по имени.
     *
     * @param $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function title($value)
    {
        return $this->builder->where($this->column('title'), 'like', '%'.$value.'%');
    }
}
