<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 02.04.2019
 * Time: 13:11
 */

namespace App\Domain\Vidoes\Jobs;


use App\Domain\Vidoes\Models\Video;
use App\Domain\Vidoes\Requests\VideoRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class StoreVideoJob
{
    use Dispatchable, Queueable;

    public $request;

    public function __construct(VideoRequest $request)
    {
        $this->request = $request;
    }


    /**
     * Execute the job.
     *
     * @return Video
     * @throws \Exception
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $video = new Video();
            $fillLink = $this->request->input('link');
            $explodedLink = explode('/', $fillLink);
            $link = end($explodedLink);

            $video->title = $this->request->input('title');
            $video->link = $link;
            $video->order = $this->request->input('order', 0);

            $video->save();

        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
        \DB::commit();

        return $video;
    }
}
