<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 02.04.2019
 * Time: 13:07
 */

namespace App\Domain\Vidoes\Models;


use App\Services\FilterService\Filterable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\Vidoes\Models\Video
 *
 * @property int $id
 * @property string $title
 * @property string $link
 * @property int|null $order
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Vidoes\Models\Video newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Vidoes\Models\Video newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Vidoes\Models\Video query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Vidoes\Models\Video whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Vidoes\Models\Video whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Vidoes\Models\Video whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Vidoes\Models\Video whereTitle($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Vidoes\Models\Video filter(\App\Services\FilterService\Filter $filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Vidoes\Models\Video paginateFilter($perPage = 20)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Vidoes\Models\Video simplePaginateFilter($perPage = 20)
 */
class Video extends Model
{
    use Filterable;

    protected $guarded = ['id'];
    public $timestamps = false;
}
