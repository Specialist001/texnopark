<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {

        if($exception instanceof ModelNotFoundException) {
            $subDomain = explode('.', $request->route()->getDomain());
            if(isset($subDomain[0]) && $subDomain[0] == 'admin') {
                return \Route::respondWithRoute('admin.404');
            }
            else if(isset($subDomain[0]) && $subDomain[0] == 'portal') {
                return \Route::respondWithRoute('portal.404');
            }
        }

        return parent::render($request, $exception);
    }

//    protected function unauthenticated($request, AuthenticationException $exception)
//    {
//
//        if (! $request->expectsJson()) {
//            $subDomain = explode('.', $request->route()->getDomain());
//            if(isset($subDomain[0]) && $subDomain[0] == 'admin') {
//                return redirect()->route('admin.auth.loginForm');
//            }
//            else if(isset($subDomain[0]) && $subDomain[0] == 'portal') {
//                return redirect()->route('portal.auth.loginForm');
//            }
//        }
//
//        return response()->json(['message' => $exception->getMessage()], 401);
//    }
}
