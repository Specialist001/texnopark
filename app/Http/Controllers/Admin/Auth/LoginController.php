<?php

namespace App\Http\Controllers\Admin\Auth;
use App\Domain\Users\Models\User;
use App\Http\Controllers\Auth\CommonLoginController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

/**
 * Created by PhpStorm.
 * User: lexcorp
 * Date: 07.06.2018
 * Time: 14:05
 */

class LoginController extends CommonLoginController
{
//    protected $role = User::ROLE_ADMIN;
    protected $formBlade = 'admin.auth.login';

    public function authenticated(Request $request, $user)
    {
        if (!$user->isAdmin() && !$user->isProof() && !$user->isManager()) {
            Auth::guard()->logout();
            $request->session()->invalidate();
            throw ValidationException::withMessages([
                $this->username() => [trans('auth.failed')],
            ]);
        }
    }
}
