<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Cities\Filters\CityFilter;
use App\Domain\Cities\Jobs\DeleteCityJob;
use App\Domain\Cities\Jobs\StoreCityJob;
use App\Domain\Cities\Jobs\UpdateCityJob;
use App\Domain\Cities\Models\City;
use App\Domain\Cities\Requests\CityRequest;
use App\Domain\Cities\Structures\CityData;
use App\Domain\Cities\Structures\CityFilterData;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CitiesController extends Controller
{
    public function index(Request $request)
    {
        $filter = new CityFilter(CityFilterData::fill($request->all()));
        $cities = City::withTranslation()->filter($filter)->paginateFilter();

        return view('admin.cities.index', [
            'cities' => $cities,
            'filters' => $filter->filters(),
        ]);
    }

    public function create()
    {
        return view('admin.cities.create');
    }

    public function store(CityRequest $request)
    {
        try {
            $this->dispatchNow(new StoreCityJob(CityData::fill($request->all()), $request));
            return redirect()->route('admin.cities.index')->with('success', trans('admin.created'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.cities.index')->with('danger', trans('admin.not_created'));
        }

    }

    public function edit(City $city)
    {
        return view('admin.cities.edit', [
            'city' => $city
        ]);
    }

    public function update(City $city, CityRequest $request)
    {
        try {
            $this->dispatchNow(new UpdateCityJob($city, CityData::fill($request->all()), $request));
            return redirect()->route('admin.cities.index')->with('success', trans('admin.edited'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.cities.index')->with('danger', trans('admin.not_edited'));
        }
    }

    public function destroy(City $city)
    {
        try {
            $this->dispatchNow(new DeleteCityJob($city));
            return redirect()->route('admin.cities.index')->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.cities.index')->with('danger', trans('admin.not_destroyed'));
        }
    }

    public function deleteImage(City $city)
    {
        try {
            $city->deleteImage();
            $city->save();
            return response()->json(['result' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['result' => 'error'], 200);
        }
    }
}
