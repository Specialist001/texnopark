<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ErrorController extends Controller
{
    public function notFound()
    {
        if(!Auth::check()) {
            return redirect()->route('admin.auth.login');
        }
        return view('admin.errors.404');
    }
}
