<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 12.03.2019
 * Time: 14:50
 */

namespace App\Http\Controllers\Admin;


use App\Domain\Forum\Jobs\DeleteForumAnswerJob;
use App\Domain\Forum\Jobs\StoreForumAnswerJob;
use App\Domain\Forum\Jobs\UpdateForumAnswerJob;
use App\Domain\Forum\Models\ForumAnswer;
use App\Domain\Forum\Models\ForumTheme;
use App\Domain\Forum\Requests\ForumAnswerRequest;
use App\Http\Controllers\Controller;

class ForumAnswersController extends Controller
{

    public function store(ForumTheme $theme, ForumAnswerRequest $request)
    {
        try {
            $this->dispatchNow(new StoreForumAnswerJob($theme, $request));
            return redirect()->route('admin.forum.edit', $theme)->with('success', trans('admin.created'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.forum.edit', $theme)->with('danger', trans('admin.not_created'));
        }

    }

    public function edit(ForumAnswer $answer)
    {

        $answer->is_moderated = 1;
        $answer->save();

        return view('admin.forum.edit_answer', [
            'answer' => $answer
        ]);
    }

    public function update(ForumAnswer $answer, ForumAnswerRequest $request)
    {
        try {
            $this->dispatchNow(new UpdateForumAnswerJob($answer, $request));
            return redirect()->route('admin.forum.edit', $answer->forum_theme_id)->with('success', trans('admin.edited'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.forum.edit', $answer->forum_theme_id)->with('danger', trans('admin.not_edited'));
        }
    }
    public function destroy(ForumAnswer $answer)
    {
        $themeId = $answer->forum_theme_id;
        try {
            $this->dispatchNow(new DeleteForumAnswerJob($answer));
            return redirect()->route('admin.forum.edit', $themeId)->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.forum.edit', $themeId)->with('danger', trans('admin.not_destroyed'));
        }
    }

    public function deleteFile(ForumAnswer $answer)
    {
        try {
            $answer->deleteFile();
            $answer->save();
            return response()->json(['result' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['result' => 'error'], 200);
        }
    }
}
