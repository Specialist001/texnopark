<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 14.03.2019
 * Time: 14:03
 */

namespace App\Http\Controllers\Admin;


use App\Domain\Forum\Jobs\DeleteForumRespondJob;
use App\Domain\Forum\Models\ForumRespond;
use App\Http\Controllers\Controller;

class ForumRespondsController extends Controller
{
    public function index()
    {
        return view('admin.responds.index', [
            'responds' => ForumRespond::orderBy('id', 'desc')->paginate()
        ]);
    }
    public function show(ForumRespond $respond)
    {
        $respond->is_moderated = 1;
        $respond->save();

        return view('admin.responds.show', [
            'respond' => $respond
        ]);
    }

    public function destroy(ForumRespond $respond)
    {
        try {
            $this->dispatchNow(new DeleteForumRespondJob($respond));
            return redirect()->back()->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('danger', trans('admin.not_destroyed'));
        }
    }
}
