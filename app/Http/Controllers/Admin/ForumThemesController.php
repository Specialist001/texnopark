<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 12.03.2019
 * Time: 14:50
 */

namespace App\Http\Controllers\Admin;


use App\Domain\Forum\Filters\ForumThemeFilter;
use App\Domain\Forum\Jobs\DeleteForumThemeJob;
use App\Domain\Forum\Jobs\StoreForumThemeJob;
use App\Domain\Forum\Jobs\UpdateForumThemeJob;
use App\Domain\Forum\Models\ForumAnswer;
use App\Domain\Forum\Models\ForumTheme;
use App\Domain\Forum\Requests\ForumThemeRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ForumThemesController extends Controller
{
    public function index(Request $request)
    {
        $filter = new ForumThemeFilter($request);
        $themes = ForumTheme::filter($filter)->withCount('answers')->withCount('newAnswers')->paginateFilter();

        return view('admin.forum.index', [
            'themes' => $themes,
            'filters' => $filter->filters(),
        ]);
    }

    public function create()
    {
        return view('admin.forum.create');
    }

    public function store(ForumThemeRequest $request)
    {
        try {
            $this->dispatchNow(new StoreForumThemeJob($request));
            return redirect()->route('admin.forum.index')->with('success', trans('admin.created'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.forum.index')->with('danger', trans('admin.not_created'));
        }

    }

    public function edit(ForumTheme $theme)
    {

        $theme->is_moderated = 1;
        $theme->save();

        $answers = ForumAnswer::where('forum_theme_id', $theme->id)->orderBy('id', 'desc')->paginate();

        foreach ($answers as $answer) {
            ForumAnswer::where('id', $answer->id)->update(['is_moderated' => 1]);
        }

        return view('admin.forum.edit', [
            'theme' => $theme,
            'answers' => $answers
        ]);
    }

    public function update(ForumTheme $theme, ForumThemeRequest $request)
    {
        try {
            $this->dispatchNow(new UpdateForumThemeJob($theme, $request));
            return redirect()->route('admin.forum.index')->with('success', trans('admin.edited'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.forum.index')->with('danger', trans('admin.not_edited'));
        }
    }
    public function destroy(ForumTheme $theme)
    {
        try {
            $this->dispatchNow(new DeleteForumThemeJob($theme));
            return redirect()->route('admin.forum.index')->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.forum.index')->with('danger', trans('admin.not_destroyed'));
        }
    }

    public function deleteFile(ForumTheme $theme)
    {
        try {
            $theme->deleteFile();
            $theme->save();
            return response()->json(['result' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['result' => 'error'], 200);
        }
    }
}
