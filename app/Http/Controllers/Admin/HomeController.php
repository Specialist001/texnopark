<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Users\Jobs\ChangePasswordUserJob;
use App\Domain\Users\Jobs\UpdateProfileUserJob;
use App\Domain\Users\Requests\UpdateProfileUserRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('admin.welcome');
    }

    public function profileForm(Request $request)
    {
        return view('admin.profile.edit', ['user' => $request->user()]);
    }

    public function profileUpdate(UpdateProfileUserRequest $request)
    {
        $redirect = redirect()->route('admin.profile');
        try{
            $this->dispatchNow(new UpdateProfileUserJob($request));
            $redirect->with('success', trans('admin.profile').' '.mb_strtolower(trans('admin.edited')));
        } catch (\Exception $exception) {
            $redirect->with('danger', trans('admin.not_edited'));
        }

        if($request->filled('password')) {
            try{
                $this->dispatchNow(new ChangePasswordUserJob($request));
                $redirect->with('success', trans('admin.profile').' '.mb_strtolower(trans('admin.edited')));
            } catch (\Exception $exception) {
                $redirect->with('success', trans('admin.name').' '.mb_strtolower(trans('admin.edited')));
                $redirect->with('danger', trans('auth.password_not_changed').': '.trans('auth.password_wrong_current'));
            }
        }
        return $redirect;
    }
}
