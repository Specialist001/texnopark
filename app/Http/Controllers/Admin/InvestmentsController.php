<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 09.03.2019
 * Time: 13:18
 */

namespace App\Http\Controllers\Admin;


use App\Domain\Investments\Filters\InvestmentFilter;
use App\Domain\Investments\Jobs\DeleteInvestmentJob;
use App\Domain\Investments\Jobs\StoreInvestmentJob;
use App\Domain\Investments\Jobs\UpdateInvestmentJob;
use App\Domain\Investments\Models\Investment;
use App\Domain\Investments\Requests\InvestmentRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InvestmentsController extends Controller
{
    public function index(Request $request)
    {
        $filter = new InvestmentFilter($request);
        $investments = Investment::withProject()->with('user')->filter($filter)->paginateFilter();

        return view('admin.investments.index', [
            'investments' => $investments,
            'filters' => $filter->filters(),
        ]);
    }

    public function create()
    {
        return view('admin.investments.create');
    }

    public function store(InvestmentRequest $request)
    {
        try {
            $this->dispatchNow(new StoreInvestmentJob($request));
            return redirect()->route('admin.investments.index')->with('success', trans('admin.created'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.investments.index')->with('danger', trans('admin.not_created'));
        }
    }

    public function edit(Investment $investment)
    {
        $investment->is_moderated = 1;
        $investment->save();
        return view('admin.investments.edit', [
            'investment' => $investment
        ]);
    }

    public function update(Investment $investment, InvestmentRequest $request)
    {
        try {
            $this->dispatchNow(new UpdateInvestmentJob($investment, $request));
            return redirect()->route('admin.investments.index')->with('success', trans('admin.edited'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.investments.index')->with('danger', trans('admin.not_edited'));
        }
    }

    public function destroy(Investment $investment)
    {
        try {
            $this->dispatchNow(new DeleteInvestmentJob($investment));
            return redirect()->route('admin.investments.index')->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.investments.index')->with('danger', trans('admin.not_destroyed'));
        }
    }
}
