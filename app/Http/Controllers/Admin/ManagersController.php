<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Users\Filters\UserFilter;
use App\Domain\Users\Jobs\DeleteUserJob;
use App\Domain\Users\Jobs\StoreUserJob;
use App\Domain\Users\Jobs\UpdateUserJob;
use App\Domain\Users\Models\User;
use App\Domain\Users\Requests\StoreUserRequest;
use App\Domain\Users\Requests\UpdateUserRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ManagersController extends Controller
{
    public function index(Request $request)
    {
        $filter = new UserFilter($request);
        $users = User::managers()->filter($filter)->paginateFilter();

        return view('admin.managers.index', [
            'users' => $users,
            'filters' => $filter->filters(),
        ]);
    }

    public function create()
    {
        return view('admin.managers.create');
    }

    public function store(StoreUserRequest $request)
    {
        $request->merge(['role' => User::ROLE_MANAGER]);

        try {
            $this->dispatchNow(new StoreUserJob($request));
            return redirect()->route('admin.managers.index')->with('success', trans('admin.created'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.managers.index')->with('danger', trans('admin.not_created'));
        }

    }

    public function edit(User $user)
    {
        if(!$user->isManager()) {
            return redirect()->route('admin.managers.index');
        }
        return view('admin.managers.edit', [
            'user' => $user
        ]);
    }

    public function update(User $user, UpdateUserRequest $request)
    {
        if(!$user->isManager()) {
            return redirect()->route('admin.managers.index');
        }

        try {
            $this->dispatchNow(new UpdateUserJob($user, $request));
            return redirect()->route('admin.managers.index')->with('success', trans('admin.edited'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.managers.index')->with('danger', trans('admin.not_edited'));
        }
    }

    public function destroy(User $user)
    {
        if(!$user->isManager()) {
            return redirect()->route('admin.managers.index');
        }

        try {
            $this->dispatchNow(new DeleteUserJob($user));
            return redirect()->route('admin.managers.index')->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.managers.index')->with('danger', trans('admin.not_destroyed'));
        }
    }
}
