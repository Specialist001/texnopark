<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 01.03.2019
 * Time: 11:30
 */

namespace App\Http\Controllers\Admin;

use App\Domain\NationalSites\Filters\NationalSitesFilter;
use App\Domain\NationalSites\Jobs\DeletePartnerJob;
use App\Domain\NationalSites\Jobs\StoreNationalSiteJob;
use App\Domain\NationalSites\Jobs\UpdateNationalSiteJob;
use App\Domain\NationalSites\Models\NationalSite;
use App\Domain\NationalSites\Requests\NationalSitesRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NationalSitesController extends Controller
{
    public function index(Request $request)
    {
        $filter = new NationalSitesFilter($request);
        $nationalSites = NationalSite::sites()->withTranslation()->filter($filter)->paginateFilter();

        return view('admin.national-sites.index', [
            'nationalSites' => $nationalSites,
            'filters' => $filter->filters(),
        ]);
    }

    public function create()
    {
        return view('admin.national-sites.create');
    }

    public function store(NationalSitesRequest $request)
    {

        $request->merge(['type' => NationalSite::TYPE_SITE]);

        try {
            $this->dispatchNow(new StoreNationalSiteJob($request));
            return redirect()->route('admin.national-sites.index')->with('success', trans('admin.created'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.national-sites.index')->with('danger', trans('admin.not_created'));
        }
    }

    public function edit(NationalSite $nationalSite)
    {

        if(!$nationalSite->isSite()) {
            return redirect()->route('admin.national-sites.index');
        }

        return view('admin.national-sites.edit', [
            'nationalSite' => $nationalSite
        ]);
    }

    public function update(NationalSite $nationalSite, NationalSitesRequest $request)
    {
        if(!$nationalSite->isSite()) {
            return redirect()->route('admin.national-sites.index');
        }

        try {
            $this->dispatchNow(new UpdateNationalSiteJob($nationalSite, $request));
            return redirect()->route('admin.national-sites.index')->with('success', trans('admin.edited'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.national-sites.index')->with('danger', trans('admin.not_edited'));
        }
    }

    public function destroy(NationalSite $nationalSite)
    {

        if(!$nationalSite->isSite()) {
            return redirect()->route('admin.national-sites.index');
        }

        try {
            $this->dispatchNow(new DeletePartnerJob($nationalSite));
            return redirect()->route('admin.national-sites.index')->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.national-sites.index')->with('danger', trans('admin.not_destroyed'));
        }
    }

    public function deleteImage(NationalSite $nationalSite)
    {

        if(!$nationalSite->isSite()) {
            return response()->json(['result' => 'error'], 200);
        }

        try {
            $nationalSite->deleteIcon();
            $nationalSite->save();
            return response()->json(['result' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['result' => 'error'], 200);
        }
    }
}
