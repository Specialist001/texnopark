<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 01.03.2019
 * Time: 11:30
 */

namespace App\Http\Controllers\Admin;

use App\Domain\PageCategories\Filters\PageCategoryFilter;
use App\Domain\PageCategories\Jobs\DeletePageCategoryJob;
use App\Domain\PageCategories\Jobs\StorePageCategoryJob;
use App\Domain\PageCategories\Jobs\UpdatePageCategoryJob;
use App\Domain\PageCategories\Models\PageCategory;
use App\Domain\PageCategories\Requests\PageCategoryRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PageCategoriesController extends Controller
{
    public function index(Request $request)
    {
        $filter = new PageCategoryFilter($request);
        $pageCategories = PageCategory::withTranslation()->filter($filter)->paginateFilter();

        return view('admin.page-categories.index', [
            'pageCategories' => $pageCategories,
            'filters' => $filter->filters(),
        ]);
    }

    public function create()
    {
        return view('admin.page-categories.create');
    }

    public function store(PageCategoryRequest $request)
    {
        try {
            $this->dispatchNow(new StorePageCategoryJob($request));
            return redirect()->route('admin.page-categories.index')->with('success', trans('admin.created'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.page-categories.index')->with('danger', trans('admin.not_created'));
        }
    }

    public function edit(PageCategory $category)
    {
        return view('admin.page-categories.edit', [
            'category' => $category
        ]);
    }

    public function update(PageCategory $category, PageCategoryRequest $request)
    {
        try {
            $this->dispatchNow(new UpdatePageCategoryJob($category, $request));
            return redirect()->route('admin.page-categories.index')->with('success', trans('admin.edited'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.page-categories.index')->with('danger', trans('admin.not_edited'));
        }
    }

    public function destroy(PageCategory $category)
    {
        try {
            $this->dispatchNow(new DeletePageCategoryJob($category));
            return redirect()->route('admin.page-categories.index')->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.page-categories.index')->with('danger', trans('admin.not_destroyed'));
        }
    }
}
