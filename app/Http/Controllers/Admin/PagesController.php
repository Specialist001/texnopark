<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 01.03.2019
 * Time: 11:30
 */

namespace App\Http\Controllers\Admin;

use App\Domain\Pages\Filters\PageFilter;
use App\Domain\Pages\Jobs\DeletePageJob;
use App\Domain\Pages\Jobs\StorePageJob;
use App\Domain\Pages\Jobs\UpdatePageJob;
use App\Domain\Pages\Models\Page;
use App\Domain\Pages\Requests\PageRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index(Request $request)
    {
        $filter = new PageFilter($request);
        $pages = Page::allPages()->withTranslation()->filter($filter)->paginateFilter();

        return view('admin.pages.index', [
            'pages' => $pages,
            'filters' => $filter->filters(),
        ]);
    }

    public function create()
    {
        return view('admin.pages.create');
    }

    public function store(PageRequest $request)
    {

        $request->merge(['type' => Page::TYPE_PAGE]);

        try {
            $this->dispatchNow(new StorePageJob($request));
            return redirect()->route('admin.pages.index')->with('success', trans('admin.created'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.pages.index')->with('danger', trans('admin.not_created'));
        }
    }

    public function edit(Page $page)
    {
        if($page->isPage() || $page->isAbout() || $page->isContact()) {
            return view('admin.pages.edit', [
                'page' => $page
            ]);
        } else {
            return redirect()->route('admin.pages.index');
        }
    }

    public function update(Page $page, PageRequest $request)
    {
        if($page->isPage() || $page->isAbout() || $page->isContact()) {

            try {
                $this->dispatchNow(new UpdatePageJob($page, $request));
                return redirect()->route('admin.pages.index')->with('success', trans('admin.edited'));
            } catch (\Exception $exception) {
                return redirect()->route('admin.pages.index')->with('danger', trans('admin.not_edited'));
            }
        } else {
            return redirect()->route('admin.pages.index');
        }

        

    }

    public function destroy(Page $page)
    {

        if(!$page->isPage() || !$page->isAbout() || !$page->isContact()) {
            return redirect()->route('admin.pages.index');
        }

        if($page->system == 1) {
            return redirect()->route('admin.pages.index')->with('danger', trans('admin.not_destroyed'));
        }

        try {
            $this->dispatchNow(new DeletePageJob($page));
            return redirect()->route('admin.pages.index')->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.pages.index')->with('danger', trans('admin.not_destroyed'));
        }
    }

    public function deleteImage(Page $page)
    {

        if(!$page->isPage() || !$page->isAbout() || !$page->isContact()) {
            return response()->json(['result' => 'error'], 200);
        }

        try {
            $page->deletePoster();
            $page->save();
            return response()->json(['result' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['result' => 'error'], 200);
        }
    }
}
