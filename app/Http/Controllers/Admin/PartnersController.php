<?php

namespace App\Http\Controllers\Admin;

use App\Domain\NationalSites\Filters\NationalSitesFilter;

use App\Domain\NationalSites\Jobs\StoreNationalSiteJob;
use App\Domain\NationalSites\Jobs\UpdateNationalSiteJob;
use App\Domain\NationalSites\Models\NationalSite;
use App\Domain\NationalSites\Requests\NationalSitesRequest;
use App\Domain\Partners\Filters\PartnerFilter;
use App\Domain\Partners\Jobs\StorePartnerJob;
use App\Domain\Partners\Jobs\UpdatePartnerJob;
use App\Domain\Partners\Jobs\DeletePartnerJob;
use App\Domain\Partners\Models\Partner;
use App\Domain\Partners\Requests\PartnerRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PartnersController extends Controller
{
    public function index(Request $request)
    {
        $filter = new PartnerFilter($request);
        $partners = Partner::filter($filter)->paginateFilter();

        return view('admin.partners.index', [
            'partners' => $partners,
            'filters' => $filter->filters(),
        ]);
    }

    public function create()
    {
        return view('admin.partners.create');
    }

    public function store(PartnerRequest $request)
    {
        try {
            $this->dispatchNow(new StorePartnerJob($request));
            return redirect()->route('admin.partners.index')->with('success', trans('admin.created'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.partners.index')->with('danger', trans('admin.not_created'));
        }
    }

    public function edit(Partner $partner)
    {
        return view('admin.partners.edit', [
            'partner' => $partner
        ]);
    }

    public function update(PartnerRequest $request, Partner $partner)
    {
        try {
            $this->dispatchNow(new UpdatePartnerJob($request, $partner));
            return redirect()->route('admin.partners.index')->with('success', trans('admin.edited'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.partners.index')->with('danger', trans('admin.not_edited'));
        }
    }

    public function destroy(Partner $partner)
    {
        try {
            $this->dispatchNow(new DeletePartnerJob($partner));
            return redirect()->route('admin.partners.index')->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.partners.index')->with('danger', trans('admin.not_destroyed'));
        }
    }

    public function deleteImage(Partner $partner)
    {
        try {
            $partner->deleteLogo();
            $partner->save();
            return response()->json(['result' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['result' => 'error'], 200);
        }
    }
}
