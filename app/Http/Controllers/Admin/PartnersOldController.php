<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 01.03.2019
 * Time: 11:30
 */

namespace App\Http\Controllers\Admin;

use App\Domain\NationalSites\Filters\NationalSitesFilter;
use App\Domain\NationalSites\Jobs\DeletePartnerJob;
use App\Domain\NationalSites\Jobs\StoreNationalSiteJob;
use App\Domain\NationalSites\Jobs\UpdateNationalSiteJob;
use App\Domain\NationalSites\Models\NationalSite;
use App\Domain\NationalSites\Requests\NationalSitesRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PartnersOldController extends Controller
{
    public function index(Request $request)
    {
        $filter = new NationalSitesFilter($request);
        $partners = NationalSite::partners()->withTranslation()->filter($filter)->paginateFilter();

        return view('admin.partners.index', [
            'partners' => $partners,
            'filters' => $filter->filters(),
        ]);
    }

    public function create()
    {
        return view('admin.partners.create');
    }

    public function store(NationalSitesRequest $request)
    {

        $request->merge(['type' => NationalSite::TYPE_PARTNER]);

        try {
            $this->dispatchNow(new StoreNationalSiteJob($request));
            return redirect()->route('admin.partners.index')->with('success', trans('admin.created'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.partners.index')->with('danger', trans('admin.not_created'));
        }
    }

    public function edit(NationalSite $partner)
    {

        if(!$partner->isPartner()) {
            return redirect()->route('admin.partners.index');
        }

        return view('admin.partners.edit', [
            'partner' => $partner
        ]);
    }

    public function update(NationalSite $partner, NationalSitesRequest $request)
    {
        if(!$partner->isPartner()) {
            return redirect()->route('admin.partners.index');
        }

        try {
            $this->dispatchNow(new UpdateNationalSiteJob($partner, $request));
            return redirect()->route('admin.partners.index')->with('success', trans('admin.edited'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.partners.index')->with('danger', trans('admin.not_edited'));
        }
    }

    public function destroy(NationalSite $partner)
    {

        if(!$partner->isPartner()) {
            return redirect()->route('admin.partners.index');
        }

        try {
            $this->dispatchNow(new DeletePartnerJob($partner));
            return redirect()->route('admin.partners.index')->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.partners.index')->with('danger', trans('admin.not_destroyed'));
        }
    }

    public function deleteImage(NationalSite $partner)
    {

        if(!$partner->isPartner()) {
            return response()->json(['result' => 'error'], 200);
        }

        try {
            $partner->deleteIcon();
            $partner->save();
            return response()->json(['result' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['result' => 'error'], 200);
        }
    }
}
