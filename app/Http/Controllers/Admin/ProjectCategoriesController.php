<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 01.03.2019
 * Time: 11:30
 */

namespace App\Http\Controllers\Admin;

use App\Domain\ProjectCategories\Filters\ProjectCategoryFilter;
use App\Domain\ProjectCategories\Jobs\DeleteProjectCategoryJob;
use App\Domain\ProjectCategories\Jobs\StoreProjectCategoryJob;
use App\Domain\ProjectCategories\Jobs\UpdateProjectCategoryJob;
use App\Domain\ProjectCategories\Models\ProjectCategory;
use App\Domain\ProjectCategories\Requests\ProjectCategoryRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProjectCategoriesController extends Controller
{
    public function index(Request $request)
    {
        $filter = new ProjectCategoryFilter($request);
        $categories = ProjectCategory::withTranslation()->filter($filter)->paginateFilter();

        return view('admin.project-categories.index', [
            'categories' => $categories,
            'filters' => $filter->filters(),
        ]);
    }

    public function create()
    {
        return view('admin.project-categories.create');
    }

    public function store(ProjectCategoryRequest $request)
    {
        try {
            $this->dispatchNow(new StoreProjectCategoryJob($request));
            return redirect()->route('admin.project-categories.index')->with('success', trans('admin.created'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.project-categories.index')->with('danger', trans('admin.not_created'));
        }
    }

    public function edit(ProjectCategory $projectCategory)
    {
        return view('admin.project-categories.edit', [
            'category' => $projectCategory
        ]);
    }

    public function update(ProjectCategory $projectCategory, ProjectCategoryRequest $request)
    {
        try {
            $this->dispatchNow(new UpdateProjectCategoryJob($projectCategory, $request));
            return redirect()->route('admin.project-categories.index')->with('success', trans('admin.edited'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.project-categories.index')->with('danger', trans('admin.not_edited'));
        }
    }

    public function destroy(ProjectCategory $projectCategory)
    {
        try {
            $this->dispatchNow(new DeleteProjectCategoryJob($projectCategory));
            return redirect()->route('admin.project-categories.index')->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.project-categories.index')->with('danger', trans('admin.not_destroyed'));
        }
    }

    public function deleteImage(ProjectCategory $projectCategory)
    {
        try {
            $projectCategory->deleteIcon();
            $projectCategory->save();
            return response()->json(['result' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['result' => 'error'], 200);
        }
    }
}
