<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 01.03.2019
 * Time: 11:30
 */

namespace App\Http\Controllers\Admin;

use App\Domain\ProjectTypes\Jobs\UpdateProjectTypeJob;
use App\Domain\ProjectTypes\Filters\ProjectTypeFilter;
use App\Domain\ProjectTypes\Jobs\DeleteProjectTypeJob;
use App\Domain\ProjectTypes\Jobs\StoreProjectTypeJob;
use App\Domain\ProjectTypes\Models\ProjectType;
use App\Domain\ProjectTypes\Requests\ProjectTypeRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProjectTypesController extends Controller
{
    public function index(Request $request)
    {
        $filter = new ProjectTypeFilter($request);
        $types = ProjectType::withTranslation()->filter($filter)->paginateFilter();

        return view('admin.project-types.index', [
            'types' => $types,
            'filters' => $filter->filters(),
        ]);
    }

    public function create()
    {
        return view('admin.project-types.create');
    }

    public function store(ProjectTypeRequest $request)
    {
        try {
            $this->dispatchNow(new StoreProjectTypeJob($request));
            return redirect()->route('admin.project-types.index')->with('success', trans('admin.created'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.project-types.index')->with('danger', trans('admin.not_created'));
        }
    }

    public function edit(ProjectType $projectType)
    {
        return view('admin.project-types.edit', [
            'type' => $projectType
        ]);
    }

    public function update(ProjectType $projectType, ProjectTypeRequest $request)
    {
        try {
            $this->dispatchNow(new UpdateProjectTypeJob($projectType, $request));
            return redirect()->route('admin.project-types.index')->with('success', trans('admin.edited'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.project-types.index')->with('danger', trans('admin.not_edited'));
        }
    }

    public function destroy(ProjectType $projectType)
    {
        try {
            $this->dispatchNow(new DeleteProjectTypeJob($projectType));
            return redirect()->route('admin.project-types.index')->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.project-types.index')->with('danger', trans('admin.not_destroyed'));
        }
    }
}
