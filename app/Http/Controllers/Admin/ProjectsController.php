<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 02.03.2019
 * Time: 13:04
 */

namespace App\Http\Controllers\Admin;


use App\Domain\Projects\Filters\ProjectFilter;
use App\Domain\Projects\Jobs\DeleteProjectJob;
use App\Domain\Projects\Jobs\StoreProjectJob;
use App\Domain\Projects\Jobs\UpdateProjectJob;
use App\Domain\Projects\Models\Project;
use App\Domain\Projects\Requests\ProjectRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    public function index(Request $request)
    {
        $filter = new ProjectFilter($request);
        $projects = Project::withTranslation()->withCategory()->withType()->filter($filter)->paginateFilter();

        return view('admin.projects.index', [
            'projects' => $projects,
            'filters' => $filter->filters(),
        ]);
    }

    public function create()
    {
        return view('admin.projects.create');
    }

    public function store(ProjectRequest $request)
    {
        try {
            $this->dispatchNow(new StoreProjectJob($request));
            return redirect()->route('admin.projects.index')->with('success', trans('admin.created'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.projects.index')->with('danger', trans('admin.not_created'));
        }
    }

    public function edit(Project $project)
    {
        $project->is_moderated = 1;
        $project->save();
        return view('admin.projects.edit', [
            'project' => $project
        ]);
    }

    public function update(Project $project, ProjectRequest $request)
    {
        try {
            $this->dispatchNow(new UpdateProjectJob($project, $request));
            return redirect()->route('admin.projects.index')->with('success', trans('admin.edited'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.projects.index')->with('danger', trans('admin.not_edited'));
        }
    }

    public function destroy(Project $project)
    {
        try {
            $this->dispatchNow(new DeleteProjectJob($project));
            return redirect()->route('admin.projects.index')->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.projects.index')->with('danger', trans('admin.not_destroyed'));
        }
    }

    public function deleteImage(Project $project)
    {
        try {
            $project->deleteImage();
            $project->save();
            return response()->json(['result' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['result' => 'error'], 200);
        }
    }

    public function deleteLogo(Project $project)
    {
        try {
            $project->deleteLogo();
            $project->save();
            return response()->json(['result' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['result' => 'error'], 200);
        }
    }

    public function deletePresentation(Project $project)
    {
        try {
            $project->deletePresentation();
            $project->save();
            return response()->json(['result' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['result' => 'error'], 200);
        }
    }

}
