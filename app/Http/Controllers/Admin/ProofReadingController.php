<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 02.04.2019
 * Time: 17:14
 */

namespace App\Http\Controllers\Admin;


use App\Domain\ProofReadings\Filters\ProofReadingFilter;
use App\Domain\ProofReadings\Jobs\DeleteProofReadingJob;
use App\Domain\ProofReadings\Jobs\ModerateProofReadingJob;
use App\Domain\ProofReadings\Models\ProofReading;
use App\Domain\ProofReadings\Requests\ModerateProofReadingRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProofReadingController extends Controller
{
    public function index(Request $request)
    {
        if(!$request->user()->isAdmin()) {
            $request->merge(['attached_user' => $request->user()->id]);
        }
        $filter = new ProofReadingFilter($request);
        $readings = ProofReading::filter($filter)->paginateFilter();

        return view('admin.readings.index', [
            'readings' => $readings,
            'filters' => $filter->filters(),
        ]);
    }

    public function edit(ProofReading $reading)
    {
        $reading->is_moderated = 1;
        $reading->save();
        return view('admin.readings.edit', [
            'reading' => $reading,
        ]);
    }


    public function update(ProofReading $reading, ModerateProofReadingRequest $request)
    {
        try {
            $this->dispatchNow(new ModerateProofReadingJob($reading, $request));
            return redirect()->route('admin.proof-readings.index')->with('success', trans('admin.edited'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.proof-readings.index')->with('danger', trans('admin.not_edited'));
        }
    }

    public function destroy(ProofReading $reading)
    {
        try {
            $this->dispatchNow(new DeleteProofReadingJob($reading));
            return redirect()->route('admin.proof-readings.index')->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.proof-readings.index')->with('danger', trans('admin.not_destroyed'));
        }
    }
}
