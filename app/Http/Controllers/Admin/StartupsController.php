<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Users\Filters\UserFilter;
use App\Domain\Users\Jobs\DeleteUserJob;
use App\Domain\Users\Jobs\StoreUserJob;
use App\Domain\Users\Jobs\UpdateUserJob;
use App\Domain\Users\Models\User;
use App\Domain\Users\Requests\StoreUserRequest;
use App\Domain\Users\Requests\UpdateUserRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StartupsController extends Controller
{
    public function index(Request $request)
    {
        $filter = new UserFilter($request);
        $users = User::startUps()->filter($filter)->paginateFilter();

        return view('admin.startups.index', [
            'users' => $users,
            'filters' => $filter->filters(),
        ]);
    }

    public function create()
    {
        return view('admin.startups.create');
    }

    public function store(StoreUserRequest $request)
    {
        $request->merge(['role' => User::ROLE_STARTUP]);

        try {
            $this->dispatchNow(new StoreUserJob($request));
            return redirect()->route('admin.startups.index')->with('success', trans('admin.created'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.startups.index')->with('danger', trans('admin.not_created'));
        }

    }

    public function edit(User $user)
    {
        if($user->role != User::ROLE_STARTUP) {
            return redirect()->route('admin.startups.index');
        }
        return view('admin.startups.edit', [
            'user' => $user
        ]);
    }

    public function update(User $user, UpdateUserRequest $request)
    {
        if($user->role != User::ROLE_STARTUP) {
            return redirect()->route('admin.startups.index');
        }
        $request->merge(['role' => User::ROLE_STARTUP]);

        try {
            $this->dispatchNow(new UpdateUserJob($user, $request));
            return redirect()->route('admin.startups.index')->with('success', trans('admin.edited'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.startups.index')->with('danger', trans('admin.not_edited'));
        }
    }

    public function destroy(User $user)
    {
        if($user->role != User::ROLE_STARTUP) {
            return redirect()->route('admin.startups.index');
        }

        try {
            $this->dispatchNow(new DeleteUserJob($user));
            return redirect()->route('admin.startups.index')->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.startups.index')->with('danger', trans('admin.not_destroyed'));
        }
    }
}
