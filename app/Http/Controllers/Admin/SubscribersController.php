<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 03.04.2019
 * Time: 12:26
 */

namespace App\Http\Controllers\Admin;


use App\Domain\Subscribers\Filters\SubscribersFilter;
use App\Domain\Subscribers\Jobs\DeleteSubscribeJob;
use App\Domain\Subscribers\Jobs\MailingJob;
use App\Domain\Subscribers\Jobs\StoreSubscriberJob;
use App\Domain\Subscribers\Jobs\UpdateSubscriberJob;
use App\Domain\Subscribers\Models\Subscriber;
use App\Domain\Subscribers\Requests\AddSubscriberRequest;
use App\Domain\Subscribers\Requests\MailingRequest;
use App\Domain\Subscribers\Requests\UpdateSubscriberRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SubscribersController extends Controller
{
    public function index(Request $request)
    {
        $filter = new SubscribersFilter($request);
        $subscribers = Subscriber::filter($filter)->paginateFilter();

        return view('admin.subscribers.index', [
            'subscribers' => $subscribers,
            'filters' => $filter->filters(),
        ]);
    }

    public function create()
    {
        return view('admin.subscribers.create');
    }

    public function store(AddSubscriberRequest $request)
    {
        try {
            $this->dispatchNow(new StoreSubscriberJob($request));
            return redirect()->route('admin.subscribers.index')->with('success', trans('admin.created'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.subscribers.index')->with('danger', trans('admin.not_created'));
        }
    }

    public function edit(Subscriber $subscriber)
    {
        return view('admin.subscribers.edit', [
            'subscriber' => $subscriber
        ]);
    }

    public function update(Subscriber $subscriber, UpdateSubscriberRequest $request)
    {
        try {
            $this->dispatchNow(new UpdateSubscriberJob($subscriber, $request));
            return redirect()->route('admin.subscribers.index')->with('success', trans('admin.edited'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.subscribers.index')->with('danger', trans('admin.not_edited'));
        }
    }

    public function destroy(Subscriber $subscriber)
    {
        try {
            $this->dispatchNow(new DeleteSubscribeJob($subscriber));
            return redirect()->route('admin.subscribers.index')->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.subscribers.index')->with('danger', trans('admin.not_destroyed'));
        }
    }

    public function mailingForm()
    {
        return view('admin.subscribers.mailing');
    }

    public function mailing(MailingRequest $request)
    {
        MailingJob::dispatch($request->all())->onQueue('subscriberMails');
        return redirect()->route('admin.subscribers.index')->with('success', trans('admin.mailed'));
    }
}
