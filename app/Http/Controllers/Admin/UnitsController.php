<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Units\Filters\UnitFilter;
use App\Domain\Units\Jobs\DeleteUnitJob;
use App\Domain\Units\Jobs\StoreUnitJob;
use App\Domain\Units\Jobs\UpdateUnitJob;
use App\Domain\Units\Models\Unit;
use App\Domain\Units\Requests\UnitRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UnitsController extends Controller
{
    public function index(Request $request)
    {
        $filter = new UnitFilter($request);
        $units = Unit::withTranslation()->filter($filter)->paginateFilter();

        return view('admin.units.index', [
            'units' => $units,
            'filters' => $filter->filters(),
        ]);
    }

    public function create()
    {
        return view('admin.units.create');
    }

    public function store(UnitRequest $request)
    {
        try {
            $this->dispatchNow(new StoreUnitJob($request));
            return redirect()->route('admin.units.index')->with('success', trans('admin.created'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.units.index')->with('danger', trans('admin.not_created'));
        }
    }

    public function edit(Unit $unit)
    {
        return view('admin.units.edit', [
            'unit' => $unit
        ]);
    }

    public function update(Unit $unit, UnitRequest $request)
    {
        try {
            $this->dispatchNow(new UpdateUnitJob($unit, $request));
            return redirect()->route('admin.units.index')->with('success', trans('admin.edited'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.units.index')->with('danger', trans('admin.not_edited'));
        }
    }

    public function destroy(Unit $unit)
    {
        try {
            $this->dispatchNow(new DeleteUnitJob($unit));
            return redirect()->route('admin.units.index')->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.units.index')->with('danger', trans('admin.not_destroyed'));
        }
    }

    public function deleteImage(Unit $unit)
    {
        try {
            $unit->save();
            return response()->json(['result' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['result' => 'error'], 200);
        }
    }
}
