<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 01.03.2019
 * Time: 11:30
 */

namespace App\Http\Controllers\Admin;

use App\Domain\Pages\Filters\PageFilter;
use App\Domain\Pages\Jobs\DeletePageJob;
use App\Domain\Pages\Jobs\StorePageJob;
use App\Domain\Pages\Jobs\UpdatePageJob;
use App\Domain\Pages\Models\Page;
use App\Domain\Pages\Requests\PageRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VacancyController extends Controller
{
    public function index(Request $request)
    {
        $filter = new PageFilter($request);
        $vacancy = Page::vacs()->withTranslation()->filter($filter)->paginateFilter();

        return view('admin.vacancy.index', [
            'vacancy' => $vacancy,
            'filters' => $filter->filters(),
        ]);
    }

    public function create()
    {
        return view('admin.vacancy.create');
    }

    public function store(PageRequest $request)
    {

        $request->merge(['type' => Page::TYPE_VAC]);
        $this->dispatchNow(new StorePageJob($request));

        try {
            return redirect()->route('admin.vacancy.index')->with('success', trans('admin.created'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.vacancy.index')->with('danger', trans('admin.not_created'));
        }
    }

    public function edit(Page $page)
    {

        if(!$page->isVac()) {
            return redirect()->route('admin.vacancy.index');
        }

        return view('admin.vacancy.edit', [
            'page' => $page
        ]);
    }

    public function update(Page $page, PageRequest $request)
    {
        if(!$page->isVac()) {
            return redirect()->route('admin.vacancy.index');
        }
$this->dispatchNow(new UpdatePageJob($page, $request));
        try {
            $this->dispatchNow(new UpdatePageJob($page, $request));
            return redirect()->route('admin.vacancy.index')->with('success', trans('admin.edited'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.vacancy.index')->with('danger', trans('admin.not_edited'));
        }
    }

    public function destroy(Page $page)
    {

        if(!$page->isVac()) {
            return redirect()->route('admin.vacancy.index');
        }

        if($page->system == 1) {
            return redirect()->route('admin.vacancy.index')->with('danger', trans('admin.not_destroyed'));
        }

        try {
            $this->dispatchNow(new DeletePageJob($page));
            return redirect()->route('admin.vacancy.index')->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.vacancy.index')->with('danger', trans('admin.not_destroyed'));
        }
    }

    public function deleteImage(Page $page)
    {

        if(!$page->isVac()) {
            return response()->json(['result' => 'error'], 200);
        }

        try {
            $page->deletePoster();
            $page->save();
            return response()->json(['result' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['result' => 'error'], 200);
        }
    }
	
	public function deleteIcon(Page $page)
    {

        if(!$page->isVac()) {
            return response()->json(['result' => 'error'], 200);
        }

        try {
            $page->deleteIcon();
            $page->save();
            return response()->json(['result' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['result' => 'error'], 500);
        }
    }
}
