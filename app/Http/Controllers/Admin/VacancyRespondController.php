<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Pages\Filters\PageFilter;
use App\Domain\Pages\Jobs\DeletePageJob;
use App\Domain\Pages\Jobs\StorePageJob;
use App\Domain\Pages\Jobs\UpdatePageJob;
use App\Domain\Pages\Models\Page;
use App\Domain\Pages\Requests\PageRequest;
use App\Domain\VacancyRespond\Filters\VacancyRespondFilter;
use App\Domain\VacancyRespond\Jobs\DeleteVacancyRespondJob;
use App\Domain\VacancyRespond\Models\VacancyRespond;
use App\Domain\VacancyRespond\Requests\VacancyRespondRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VacancyRespondController extends Controller
{
    public function index(Request $request)
    {
        $filter = new VacancyRespondFilter($request);
        $vacancyRespond = VacancyRespond::filter($filter)->paginateFilter();

        return view('admin.vacancy-respond.index', [
            'vacancyRespond' => $vacancyRespond,
            'filters' => $filter->filters(),
        ]);
    }

    public function store(VacancyRespondRequest $request)
    {

        $request->merge(['type' => Page::TYPE_VAC]);
        $this->dispatchNow(new StorePageJob($request));

        try {
            return redirect()->route('admin.vacancy.index')->with('success', trans('admin.created'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.vacancy.index')->with('danger', trans('admin.not_created'));
        }
    }


    public function destroy(VacancyRespond $vacancyRespond)
    {

        try {
            $this->dispatchNow(new DeleteVacancyRespondJob($vacancyRespond));
            return redirect()->route('admin.vacancy-responds.index')->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.vacancy-responds.index')->with('danger', trans('admin.not_destroyed'));
        }
    }

    public function deleteImage(Page $page)
    {

        if(!$page->isVac()) {
            return response()->json(['result' => 'error'], 200);
        }

        try {
            $page->deletePoster();
            $page->save();
            return response()->json(['result' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['result' => 'error'], 200);
        }
    }

	public function deleteIcon(Page $page)
    {

        if(!$page->isVac()) {
            return response()->json(['result' => 'error'], 200);
        }

        try {
            $page->deleteIcon();
            $page->save();
            return response()->json(['result' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['result' => 'error'], 500);
        }
    }
}
