<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Vidoes\Filters\VideoFilter;
use App\Domain\Vidoes\Jobs\DeleteVideoJob;
use App\Domain\Vidoes\Jobs\StoreVideoJob;
use App\Domain\Vidoes\Jobs\UpdateVideoJob;
use App\Domain\Vidoes\Models\Video;
use App\Domain\Vidoes\Requests\VideoRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VideosController extends Controller
{
    public function index(Request $request)
    {
        $filter = new VideoFilter($request);
        $videos = Video::filter($filter)->paginateFilter();

        return view('admin.videos.index', [
            'videos' => $videos,
            'filters' => $filter->filters(),
        ]);
    }

    public function create()
    {
        return view('admin.videos.create');
    }

    public function store(VideoRequest $request)
    {
        try {
            $this->dispatchNow(new StoreVideoJob($request));
            return redirect()->route('admin.videos.index')->with('success', trans('admin.created'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.videos.index')->with('danger', trans('admin.not_created'));
        }

    }

    public function edit(Video $video)
    {
        return view('admin.videos.edit', [
            'video' => $video
        ]);
    }

    public function update(Video $video, VideoRequest $request)
    {
        try {
            $this->dispatchNow(new UpdateVideoJob($video, $request));
            return redirect()->route('admin.videos.index')->with('success', trans('admin.edited'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.videos.index')->with('danger', trans('admin.not_edited'));
        }
    }

    public function destroy(Video $video)
    {
        try {
            $this->dispatchNow(new DeleteVideoJob($video));
            return redirect()->route('admin.videos.index')->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->route('admin.videos.index')->with('danger', trans('admin.not_destroyed'));
        }
    }
}
