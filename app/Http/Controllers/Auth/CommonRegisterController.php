<?php

namespace App\Http\Controllers\Auth;

use App\Domain\Users\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Validation\Rule;

class CommonRegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

//    protected $role;
    protected $formBlade;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->redirectTo = '/'.\LaravelLocalization::getCurrentLocale();
        $this->middleware('guest');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view($this->formBlade);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => [
                'required',
                'string',
                'max:255',
                Rule::unique('users')
            ],
            'role' => 'required|in:'.implode(',', [User::ROLE_STARTUP, User::ROLE_INVESTOR]),

            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:4', 'confirmed'],

            'city_id' => ['required', 'exists:cities,id'],
            'phone' => ['required', 'string', 'max:255'],
            'personality' => 'required|in:'.implode(',', User::personalities()),
            'site' => ['nullable', 'string', 'max:255'],
            'accept' => ['required', 'in:yes'],

        ]);
    }

    /**
     *
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return User|\Illuminate\Database\Eloquent\Model
     */
    protected function create(array $data)
    {
        return User::create([
            'username' => $data['username'],
            'role' => $data['role'],
            'name' => $data['name'],
            'email' => $data['email'],
            'email_verified_at' => date('Y-m-d H:i:s'),
            'password' => Hash::make($data['password']),
            'active' => 1,
            'city_id' => $data['city_id'],
            'phone' => $data['phone'],
            'site' => $data['site'] ?? null,
            'personality' => $data['personality'],
            'locale' => \LaravelLocalization::getCurrentLocale(),
        ]);
    }
}
