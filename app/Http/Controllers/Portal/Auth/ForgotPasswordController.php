<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 08.03.2019
 * Time: 21:26
 */

namespace App\Http\Controllers\Portal\Auth;


use App\Http\Controllers\Auth\CommonForgotPasswordController;
use Illuminate\Http\Request;

class ForgotPasswordController extends CommonForgotPasswordController
{
    protected $formBlade = 'portal.auth.forgot';

    protected function sendResetLinkResponse(Request $request, $response)
    {
        return redirect()->route('portal.auth.login')->with('success', trans($response));
    }
}
