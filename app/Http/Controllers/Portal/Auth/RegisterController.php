<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 07.03.2019
 * Time: 14:13
 */

namespace App\Http\Controllers\Portal\Auth;


use App\Http\Controllers\Auth\CommonRegisterController;
use Illuminate\Http\Request;

class RegisterController extends CommonRegisterController
{
    protected $formBlade = 'portal.auth.register';

    protected function registered(Request $request, $user)
    {
        return redirect()->route('portal.profile');
    }
}
