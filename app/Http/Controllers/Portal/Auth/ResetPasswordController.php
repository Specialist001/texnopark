<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 09.03.2019
 * Time: 0:15
 */

namespace App\Http\Controllers\Portal\Auth;


use App\Http\Controllers\Auth\CommonResetPasswordController;
use Illuminate\Http\Request;

class ResetPasswordController extends CommonResetPasswordController
{
    protected $formBlade = 'portal.auth.reset';

    protected function sendResetResponse(Request $request, $response)
    {
        return redirect()->route('portal.profile')->with('success', trans($response));
    }
}
