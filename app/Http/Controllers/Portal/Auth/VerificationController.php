<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 07.03.2019
 * Time: 21:53
 */

namespace App\Http\Controllers\Portal\Auth;


use App\Domain\Users\Models\User;
use App\Http\Controllers\Auth\CommonVerificationController;

class VerificationController extends CommonVerificationController
{
    protected $role = User::ROLE_STARTUP;
    protected $formBlade = 'portal.auth.verify';
}
