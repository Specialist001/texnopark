<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ErrorController extends Controller
{
    public function notFound()
    {
        return view('portal.errors.404');
    }
}
