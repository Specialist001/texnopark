<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 11.03.2019
 * Time: 5:50
 */

namespace App\Http\Controllers\Portal;


use App\Domain\Projects\Models\Project;
use App\Domain\Users\Models\UserFavourites;
use App\Http\Controllers\Controller;

class FavouritesController extends Controller
{
    public function add(Project $project)
    {
        if ($project->status == Project::STATUS_PUBLISHED) {
            $favourite = UserFavourites::where('user_id', \Auth::user()->id)->where('project_id', $project->id)->first();

            if ($favourite) {
                $favourite->delete();
            } else {
                UserFavourites::create([
                    'user_id' => \Auth::user()->id,
                    'project_id' => $project->id,
                ]);
            }
        }
        return response()->json(['result' => 'success'], 200);
    }

    public function index()
    {
        return view('portal.favorites', [
            'favorites_list' => UserFavourites::where('user_id', \Auth::user()->id)->withProject()->paginate()
        ]);
    }
}
