<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 12.03.2019
 * Time: 14:50
 */

namespace App\Http\Controllers\Portal;


use App\Domain\Forum\Jobs\DeleteForumAnswerJob;
use App\Domain\Forum\Jobs\StoreForumAnswerJob;
use App\Domain\Forum\Jobs\UpdateForumAnswerJob;
use App\Domain\Forum\Models\ForumAnswer;
use App\Domain\Forum\Models\ForumTheme;
use App\Domain\Forum\Requests\ForumAnswerRequest;
use App\Http\Controllers\Controller;

class ForumAnswersController extends Controller
{

    public function store(ForumTheme $theme, ForumAnswerRequest $request)
    {
        try {
            $this->dispatchNow(new StoreForumAnswerJob($theme, $request));
            return redirect()->back()->with('success', trans('admin.created'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('danger', trans('admin.not_created'));
        }

    }

    public function destroy(ForumAnswer $answer)
    {

        if ($answer->user_id != \Request::user()->id) {
            return redirect()->back();
        }

        try {
            $this->dispatchNow(new DeleteForumAnswerJob($answer));
            return redirect()->back()->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('danger', trans('admin.not_destroyed'));
        }
    }

    public function deleteFile(ForumAnswer $answer)
    {
        if ($answer->user_id != \Request::user()->id) {
            return redirect()->back();
        }

        try {
            $answer->deleteFile();
            $answer->save();
            return redirect()->back()->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('danger', trans('admin.not_destroyed'));
        }
    }
}
