<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 14.03.2019
 * Time: 13:34
 */

namespace App\Http\Controllers\Portal;


use App\Domain\Forum\Jobs\StoreForumRespondJob;
use App\Domain\Forum\Models\ForumTheme;
use App\Domain\Forum\Requests\ForumRespondRequest;
use App\Http\Controllers\Controller;

class ForumRespondController extends Controller
{
    public function store(ForumTheme $theme, ForumRespondRequest $request)
    {
        try {
            $this->dispatchNow(new StoreForumRespondJob($theme, $request));
            return redirect()->back()->with('success', trans('portal.send'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('danger', trans('portal.not_send'));
        }

    }
}
