<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 12.03.2019
 * Time: 14:50
 */

namespace App\Http\Controllers\Portal;


use App\Domain\Forum\Filters\ForumThemeFilter;
use App\Domain\Forum\Jobs\DeleteForumThemeJob;
use App\Domain\Forum\Jobs\StoreForumThemeJob;
use App\Domain\Forum\Jobs\UpdateForumThemeJob;
use App\Domain\Forum\Models\ForumAnswer;
use App\Domain\Forum\Models\ForumTheme;
use App\Domain\Forum\Requests\ForumThemeRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ForumThemesController extends Controller
{
    public function index(Request $request)
    {
        $filter = new ForumThemeFilter($request);
        $themes = ForumTheme::where('status', ForumTheme::STATUS_PUBLISHED)->filter($filter)->withCount('answers');

        $my_themes = null;
        if (\Auth::check()) {
            $my_themes = ForumTheme::where('user_id', $request->user()->id)->withCount('answers')->orderBy('id', 'desc')->get();
            if ($my_themes->isEmpty()) {
                $my_themes = null;
            }
            $themes->where('user_id', '!=', $request->user()->id);
        }

        return view('portal.forum.index', [
            'themes' => $themes->paginateFilter(),
            'my_themes' => $my_themes,
            'filters' => $filter->filters(),
        ]);
    }

    public function store(ForumThemeRequest $request)
    {
        try {
            $this->dispatchNow(new StoreForumThemeJob($request));
            return redirect()->route('portal.forum.index')->with('success', trans('admin.created'));
        } catch (\Exception $exception) {
            return redirect()->route('portal.forum.index')->with('danger', trans('admin.not_created'));
        }

    }

    public function show(ForumTheme $theme)
    {
        if ($theme->status != ForumTheme::STATUS_PUBLISHED) {
            return redirect()->route('portal.forum.index');
        }

        $viewed = session()->get('forum_viewed', []);

        if (!in_array($theme->id, $viewed)) {
            $theme->increment('view');
            $viewed[] = $theme->id;
            session()->put('forum_viewed', $viewed);
        }

        $answers = ForumAnswer::where('forum_theme_id', $theme->id)->orderBy('id', 'asc');

        return view('portal.forum.show', [
            'theme' => $theme,
            'answers_count' => $answers->count(),
            'answers' => $answers->paginate()
        ]);
    }

    public function edit(ForumTheme $theme)
    {
        if ($theme->user_id != \Request::user()->id) {
            return redirect()->route('portal.forum.index');
        }

        return view('portal.forum.edit', [
            'forumTheme' => $theme
        ]);
    }

    public function update(ForumTheme $theme, ForumThemeRequest $request)
    {
        if ($theme->user_id != \Request::user()->id) {
            return redirect()->route('portal.forum.index');
        }

        try {
            $this->dispatchNow(new UpdateForumThemeJob($theme, $request));
            return redirect()->route('portal.forum.index')->with('success', trans('admin.edited'));
        } catch (\Exception $exception) {
            return redirect()->route('portal.forum.index')->with('danger', trans('admin.not_edited'));
        }
    }

    public function destroy(ForumTheme $theme)
    {
        if ($theme->user_id != \Request::user()->id) {
            return redirect()->route('portal.forum.index');
        }

        try {
            $this->dispatchNow(new DeleteForumThemeJob($theme));
            return redirect()->route('portal.forum.index')->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->route('portal.forum.index')->with('danger', trans('admin.not_destroyed'));
        }
    }

    public function deleteFile(ForumTheme $theme)
    {
        if ($theme->user_id != \Request::user()->id) {
            return redirect()->route('portal.forum.index');
        }

        try {
            $theme->deleteFile();
            $theme->save();
            return response()->json(['result' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['result' => 'error'], 200);
        }
    }
}
