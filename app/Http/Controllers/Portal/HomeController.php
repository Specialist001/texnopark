<?php

namespace App\Http\Controllers\Portal;

use App\Domain\NationalSites\Models\NationalSite;
use App\Domain\Pages\Models\Page;
use App\Domain\ProjectCategories\Models\ProjectCategory;
use App\Domain\Projects\Models\Project;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        $projects = Project::published()->withTranslation()->orderBy('id', 'desc')->limit(6)->get();
        $categories = ProjectCategory::withTranslation()->withCount(['projects' => function ($query) {
            $query->where('status', Project::STATUS_PUBLISHED);
        }])->whereHas('projects', function ($query) {
            $query->where('status', Project::STATUS_PUBLISHED);
        })->orderBy('order')->get();

        $partners = NationalSite::partners()
//            ->where('active', 1)
            ->withTranslation()
            ->orderBy('order')
            ->limit(8)
            ->get();

        $news = Page::news()
            ->where('active', 1)
            ->withTranslation()
            ->orderBy('created_at', 'desc')
            ->limit(3)
            ->get();

        return view('portal.welcome', [
            'projects' => $projects,
            'categories' => $categories,
            'partners' => $partners,
            'news' => $news,
        ]);
    }
}
