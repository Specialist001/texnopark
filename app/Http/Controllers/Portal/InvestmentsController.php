<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 11.03.2019
 * Time: 2:39
 */

namespace App\Http\Controllers\Portal;


use App\Domain\Investments\Filters\InvestmentFilter;
use App\Domain\Investments\Jobs\DeleteInvestmentJob;
use App\Domain\Investments\Jobs\StoreInvestmentJob;
use App\Domain\Investments\Jobs\UpdateInvestmentJob;
use App\Domain\Investments\Models\Investment;
use App\Domain\Investments\Requests\InvestmentRequest;
use App\Domain\Projects\Models\Project;
use App\Domain\Users\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InvestmentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('relogin:investor');
    }

    public function index(Request $request)
    {
        $filter = new InvestmentFilter($request);
        $investments = $request->user()->investments()->withProject()->filter($filter)->paginateFilter();

        return view('portal.investments.index', [
            'investments' => $investments,
            'filters' => $filter->filters(),
        ]);
    }

    public function invest(Project $project, InvestmentRequest $request)
    {
        if($project->status != Project::STATUS_PUBLISHED) {
            return redirect()->route('portal.investments.index');
        }

        $hasInvested = Investment::where('project_id', $project->id)->where('user_id', $request->user()->id)->first();

        if($hasInvested) {
            return redirect()->route('portal.investments.index');
        }

        $request->merge(['project_id' => $project->id]);
        try {
            $investment = $this->dispatchNow(new StoreInvestmentJob($request));
            return redirect()->route('portal.investments.show', $investment)->with('success', trans('portal.invested_send'));
        } catch (\Exception $exception) {
            return redirect()->route('portal.investments.index')->with('danger', trans('portal.invested_not_send'));
        }
    }

    public function show(Investment $investment)
    {

        if($investment->user_id != \Auth::user()->id) {
            return redirect()->route('portal.investments.index');
        }

        return view('portal.investments.show', [
            'investment' => $investment
        ]);
    }

    public function edit(Investment $investment)
    {

        if(!$investment->canUpdate() || $investment->user_id != \Auth::user()->id) {
            return redirect()->route('portal.investments.index');
        }
        return view('portal.investments.edit', [
            'investment' => $investment
        ]);
    }

    public function update(Investment $investment, InvestmentRequest $request)
    {

        if(!$investment->canUpdate() || $investment->user_id != \Auth::user()->id) {
            return redirect()->route('portal.investments.index');
        }
        try {
            $this->dispatchNow(new UpdateInvestmentJob($investment, $request));
            return redirect()->route('portal.investments.index')->with('success', trans('admin.edited'));
        } catch (\Exception $exception) {
            return redirect()->route('portal.investments.index')->with('danger', trans('admin.not_edited'));
        }
    }

    public function destroy(Investment $investment)
    {

        if(!$investment->canDelete() || $investment->user_id != \Auth::user()->id) {
            return redirect()->route('portal.investments.index');
        }

        try {
            $this->dispatchNow(new DeleteInvestmentJob($investment));
            return redirect()->route('portal.investments.index')->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->route('portal.investments.index')->with('danger', trans('admin.not_destroyed'));
        }
    }
}
