<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 11.03.2019
 * Time: 13:01
 */

namespace App\Http\Controllers\Portal;


use App\Domain\Users\Models\UserNotifications;
use App\Http\Controllers\Controller;

class NotificationsController extends Controller
{
    public function index()
    {
        $notifications =  \Auth::user()->customNotifications()->with('project')->orderBy('id', 'desc')->paginate();

        foreach ($notifications as $notification) {
            UserNotifications::where('id', $notification->id)->update(['viewed' => 1]);
        }

        return view('portal.notifications', [
            'notifications' => $notifications
        ]);
    }
}
