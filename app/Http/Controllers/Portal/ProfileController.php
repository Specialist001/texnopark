<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 07.03.2019
 * Time: 23:38
 */

namespace App\Http\Controllers\Portal;


use App\Domain\Users\Jobs\ChangePasswordUserJob;
use App\Domain\Users\Jobs\UpdateProfileUserJob;
use App\Domain\Users\Requests\UpdateProfileUserRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{

    public function edit(Request $request)
    {
        return view('portal.profile.edit', ['user' => $request->user()]);
    }

    public function update(UpdateProfileUserRequest $request)
    {
        $redirect = redirect()->route('portal.profile');
        try{
            $this->dispatchNow(new UpdateProfileUserJob($request));
            $redirect->with('success', trans('admin.profile').' '.mb_strtolower(trans('admin.edited')));
        } catch (\Exception $exception) {
            $redirect->with('danger', trans('admin.not_edited'));
        }

        if($request->filled('password')) {
            try{
                $this->dispatchNow(new ChangePasswordUserJob($request));
                $redirect->with('success', trans('admin.profile').' '.mb_strtolower(trans('admin.edited')));
            } catch (\Exception $exception) {
                $redirect->with('success', trans('admin.profile').' '.mb_strtolower(trans('admin.edited')));
                $redirect->with('danger', trans('auth.password_not_changed').': '.trans('auth.password_wrong_current'));
            }
        }
        return $redirect;
    }
}
