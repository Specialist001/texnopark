<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 05.03.2019
 * Time: 17:24
 */

namespace App\Http\Controllers\Portal;


use App\Domain\Investments\Models\Investment;
use App\Domain\Projects\Models\Project;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
    public function index(Project $project)
    {
        $viewed = session()->get('viewed', []);

        if (!in_array($project->id, $viewed)) {
            $project->increment('view');
            $viewed[] = $project->id;
            session()->put('viewed', $viewed);
        }
        $hasInvested = false;

        if (\Auth::check()) {
            $hasInvested = Investment::where('project_id', $project->id)->where('user_id', \Auth::user()->id)->first();
        }

        return view('portal.project', [
            'project' => $project,
            'hasInvested' => $hasInvested
        ]);
    }
}
