<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 09.03.2019
 * Time: 14:15
 */

namespace App\Http\Controllers\Portal;


use App\Domain\Projects\Filters\ProjectFilter;
use App\Domain\Projects\Jobs\DeleteProjectJob;
use App\Domain\Projects\Jobs\StoreProjectJob;
use App\Domain\Projects\Jobs\UpdateProjectJob;
use App\Domain\Projects\Models\Project;
use App\Domain\Projects\Requests\ProjectRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{

    public function __construct()
    {
        $this->middleware('relogin:startup');
    }

    public function index(Request $request)
    {
        $filter = new ProjectFilter($request);
        $projects = $request->user()->projects()->withTranslation()->withCategory()->with('activeInvestments')->withType()->filter($filter)->paginateFilter();

        return view('portal.projects.index', [
            'projects' => $projects,
            'filters' => $filter->filters(),
        ]);
    }

    public function create()
    {
        return view('portal.projects.create');
    }

    public function store(ProjectRequest $request)
    {
        try {
            $this->dispatchNow(new StoreProjectJob($request));
            return redirect()->route('portal.projects.index')->with('success', trans('admin.created'));
        } catch (\Exception $exception) {
            return redirect()->route('portal.projects.index')->with('danger', trans('admin.not_created'));
        }
    }

    public function edit(Project $project)
    {

        if(!$project->canUpdate() || $project->user_id != \Auth::user()->id) {
            return redirect()->route('portal.projects.index');
        }

        return view('portal.projects.edit', [
            'project' => $project
        ]);
    }

    public function show(Project $project)
    {

        if($project->user_id != \Auth::user()->id) {
            return redirect()->route('portal.projects.index');
        }

        return view('portal.projects.show', [
            'project' => $project
        ]);
    }

    public function update(Project $project, ProjectRequest $request)
    {
        if(!$project->canUpdate() || $project->user_id != \Auth::user()->id) {
            return redirect()->route('portal.projects.index');
        }

        try {
            $this->dispatchNow(new UpdateProjectJob($project, $request));
            return redirect()->route('portal.projects.index')->with('success', trans('admin.edited'));
        } catch (\Exception $exception) {
            return redirect()->route('portal.projects.index')->with('danger', trans('admin.not_edited'));
        }
    }

    public function destroy(Project $project)
    {

        if(!$project->canDelete() || $project->user_id != \Auth::user()->id) {
            return redirect()->route('portal.projects.index');
        }

        try {
            $this->dispatchNow(new DeleteProjectJob($project));
            return redirect()->route('portal.projects.index')->with('success', trans('admin.destroyed'));
        } catch (\Exception $exception) {
            return redirect()->route('portal.projects.index')->with('danger', trans('admin.not_destroyed'));
        }
    }


    public function deleteImage(Project $project)
    {
        if(!$project->canUpdate() || $project->user_id != \Auth::user()->id) {
            return response()->json(['result' => 'error'], 200);
        }

        try {
            $project->deleteImage();
            $project->save();
            return response()->json(['result' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['result' => 'error'], 200);
        }
    }

    public function deleteLogo(Project $project)
    {

        if(!$project->canUpdate() || $project->user_id != \Auth::user()->id) {
            return response()->json(['result' => 'error'], 200);
        }
        try {
            $project->deleteLogo();
            $project->save();
            return response()->json(['result' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['result' => 'error'], 200);
        }
    }

    public function deletePresentation(Project $project)
    {
        if(!$project->canUpdate() || $project->user_id != \Auth::user()->id) {
            return response()->json(['result' => 'error'], 200);
        }

        try {
            $project->deletePresentation();
            $project->save();
            return response()->json(['result' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['result' => 'error'], 200);
        }
    }
}
