<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 09.03.2019
 * Time: 14:20
 */

namespace App\Http\Controllers\Portal;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReloginController extends Controller
{
    public function index(Request $request)
    {
        return view('portal.profile.relogin', [
            'user' => $request->user()
        ]);
    }
}
