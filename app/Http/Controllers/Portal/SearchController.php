<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 05.03.2019
 * Time: 13:32
 */

namespace App\Http\Controllers\Portal;


use App\Domain\Projects\Filters\ProjectFilter;
use App\Domain\Projects\Models\Project;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $filter = new ProjectFilter($request);
        $query = Project::published()->withTranslation()->withCategory()->with('activeInvestments')->filter($filter);
        $total_count = $query->count();
        $projects = $query->paginateFilter();

        return view('portal.search', [
                'projects' => $projects,
                'total_count' => $total_count,
                'filters' => $filter->filters(),
            ]);
    }

}
