<?php

namespace App\Http\Controllers\Site;

use App\Domain\NationalSites\Models\NationalSite;
use App\Domain\Pages\Models\Page;
use App\Domain\ProjectCategories\Models\ProjectCategory;
use App\Domain\Projects\Models\Project;
use App\Domain\Partners\Models\Partner;
use App\Domain\Subscribers\Jobs\SubscribeJob;
use App\Domain\Subscribers\Models\Subscriber;
use App\Domain\Users\Models\User;
use App\Domain\Vidoes\Models\Video;
use App\Http\Controllers\Controller;
use App\Notifications\ContactUs;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $banner = Page::news()
            ->where('active', 1)
            ->where('top', 1)
            ->withTranslation()
            ->orderBy('created_at', 'desc')
            ->first();

        $news = Page::news()
            ->where('active', 1)
            ->withTranslation()
            ->orderBy('created_at', 'desc')
            ->limit(10)
            ->get();

        $labs = Page::labs()
            ->where('active', 1)
            ->withTranslation()
            ->orderBy('order')
            ->limit(6)
            ->get();

        $vacancies = Page::vacs()
            ->where('active', 1)
            ->withTranslation()
            ->orderBy('order')
            ->limit(24)
            ->get();

        $nationalSites = NationalSite::sites()
//            ->where('active', 1)
            ->withTranslation()
            ->orderBy('order')
//            ->limit(6)
            ->get();

        $video = Video::orderBy('order')
//            ->limit(6)
            ->first();

		$partners = Partner::orderBy('order')->get();

        $system_page = Page::where('system', 1)->first();

        $projectCategories = ProjectCategory::where('deleted_at', '=',null)->withTranslation()->orderBy('order')->get();
        $projects = collect();
        foreach ($projectCategories as $projectCategory) {
            $projects[$projectCategory->id] = Project::where('project_category_id', $projectCategory->id)
                ->where('status', '=',Project::STATUS_PUBLISHED)
                ->withTranslation()->limit(4)->get();
        }

        return view('site.welcome', [
            'banner' => $banner,
            'news' => $news,
            'labs' => $labs,
            'vacancies' => $vacancies,
            'system_page' => $system_page,
            'video' => $video,
            'nationalSites' => $nationalSites,
            'partners' => $partners,
            'projectCategories' => $projectCategories,
            'projects' => $projects,
        ]);
    }

    public function contactUs(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'comment' => 'required|string',
            'email' => 'nullable|email',
            'phone' => 'nullable|string',
        ]);

        try {
            $admin = new User();
            $admin->email = 'info@texnopark.uz';
            $admin->notify(new ContactUs($request));
            return response()->json(['result' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['result' => 'error'], 200);
        }
    }

    public function subscribe(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|email',
        ]);

        $exists = Subscriber::where('email', $request->input('email'))->first();

        if ($exists) {
            return response()->json(['result' => trans('site.already_subscribed')], 200);
        }
        try {
            $this->dispatch(new SubscribeJob($request));
            return response()->json(['result' => trans('site.success_subscribe')], 200);
        } catch (\Exception $exception) {
            return response()->json(['result' => trans('site.failure_subscribe')], 200);
        }
    }

    public function search(Request $request)
    {
        $news = collect([]);
        $pages = collect([]);
        $labs = collect([]);
        $videos = collect([]);

        if ($request->filled('q')) {
            $q = $request->input('q');
            $news = Page::news()
                ->where('active', 1)
                ->withTranslation()
                ->whereHas('translations', function ($query) use ($q) {
                    /**
                     * @var $query \Illuminate\Database\Eloquent\Builder
                     */
                    $query->where('page_translations.' . 'name', 'like', '%' . $q . '%')
                        ->where('locale', \App::getLocale());
                })
                ->orderBy('created_at', 'desc')
                ->get();

            $labs = Page::labs()
                ->where('active', 1)
                ->withTranslation()
                ->whereHas('translations', function ($query) use ($q) {
                    /**
                     * @var $query \Illuminate\Database\Eloquent\Builder
                     */
                    $query->where('page_translations.' . 'name', 'like', '%' . $q . '%')
                        ->where('locale', \App::getLocale());
                })
                ->orderBy('order')
                ->get();

            $pages = Page::pages()
                ->where('active', 1)
                ->withTranslation()
                ->whereHas('translations', function ($query) use ($q) {
                    /**
                     * @var $query \Illuminate\Database\Eloquent\Builder
                     */
                    $query->where('page_translations.' . 'name', 'like', '%' . $q . '%')
                        ->where('locale', \App::getLocale());
                })
                ->orderBy('order')
                ->get();

            $videos = Video::orderBy('order')
                ->where('title', 'like', '%' . $q . '%')
//            ->limit(6)
                ->get();
        }

        return view('site.search', [
            'news' => $news,
            'pages' => $pages,
            'labs' => $labs,
            'videos' => $videos,
        ]);
    }
}
