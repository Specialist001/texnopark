<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 20.03.2019
 * Time: 16:41
 */

namespace App\Http\Controllers\Site;


use App\Domain\Pages\Models\Page;
use App\Http\Controllers\Controller;

class LabsController extends Controller
{
//    public function index()
//    {
//        $news = Page::news()
//            ->where('active', 1)
//            ->withTranslation()
//            ->orderBy('created_at', 'desc')
//            ->paginate();
//
//        return view('site.news', [
//            'news' => $news
//        ]);
//    }

    public function show(Page $page)
    {
        if(!$page->isLab() || $page->active != 1) {
            return redirect('404');
        }

        return view('site.page', [
            'page' => $page
        ]);
    }
}
