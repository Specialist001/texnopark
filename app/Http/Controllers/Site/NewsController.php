<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 20.03.2019
 * Time: 16:41
 */

namespace App\Http\Controllers\Site;


use App\Domain\Pages\Models\Page;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    public function index()
    {

        $news = Page::news()
            ->where('active', 1)
            ->withTranslation()
            ->orderBy('created_at', 'desc')
            ->paginate();
        return view('site.news', [
            'news' => $news
        ]);
    }

    public function show($page)
    {
        $all_news = Page::news()
            ->where('active', 1)
            ->withTranslation()
            ->orderBy('created_at', 'desc')
            ->limit(10)
            ->get();

        $page = Page::where('uri', $page)->first();
        if(!$page || !$page->isNews() || $page->active != 1) {
            return redirect('404');
        }

        $newsViewed = session('newsViewed', []);
        if(!in_array($page->id, $newsViewed)) {
            $page->increment('views');
            $newsViewed[] = $page->id;
            session()->put('newsViewed', $newsViewed);
        }

        return view('site.page', [
            'page' => $page,
            'news' => $all_news
        ]);
    }
}
