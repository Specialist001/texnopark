<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 20.03.2019
 * Time: 16:41
 */

namespace App\Http\Controllers\Site;


use App\Domain\Pages\Models\Page;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function index(Page $page)
    {
        if($page->isPage() && $page->active == 1 || $page->isAbout() && $page->active == 1 || $page->isContact() && $page->active == 1) {
            return view('site.page', [
                'page' => $page
            ]);
        } else {
            return redirect('404');
        }
    }

}
