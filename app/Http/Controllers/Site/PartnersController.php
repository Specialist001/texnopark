<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 25.03.2019
 * Time: 13:15
 */

namespace App\Http\Controllers\Site;


use App\Domain\NationalSites\Models\NationalSite;
use App\Http\Controllers\Controller;

class PartnersController extends Controller
{
    public function index()
    {
        $partners = NationalSite::partners()
//            ->where('active', 1)
            ->withTranslation()
            ->orderBy('order')
//            ->limit(6)
            ->get();
        return view('site.partners', [
            'partners' => $partners
        ]);
    }
}
