<?php


namespace App\Http\Controllers\Site;


use App\Domain\Pages\Models\Page;
use App\Domain\ProjectCategories\Models\ProjectCategory;
use App\Domain\Projects\Models\Project;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
    public function index()
    {
        $projects = Project::where('status', '=',Project::STATUS_PUBLISHED)->withTranslation()->withCategory()->get();

        $news = Page::news()
            ->where('active', 1)
            ->withTranslation()
            ->orderBy('created_at', 'desc')
            ->limit(10)
            ->get();

        return view('site.project', [
            'projects' => $projects,
            'news' => $news,
            'showAll' => true,
        ]);
    }

    public function show(Project $project)
    {
        $news = Page::news()
            ->where('active', 1)
            ->withTranslation()
            ->orderBy('created_at', 'desc')
            ->limit(10)
            ->get();

        return view('site.project-show', [
            'project' => $project,
            'news' => $news,
            'showAll' => true,
        ]);
    }
}
