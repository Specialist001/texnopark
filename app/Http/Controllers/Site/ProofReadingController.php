<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 02.04.2019
 * Time: 16:39
 */

namespace App\Http\Controllers\Site;


use App\Domain\ProofReadings\Jobs\StoreProofReadingJob;
use App\Domain\ProofReadings\Models\ProofReading;
use App\Domain\ProofReadings\Requests\ProofReadingRequest;
use App\Http\Controllers\Controller;

class ProofReadingController extends Controller
{
    public function store(ProofReadingRequest $request)
    {
        try {
            $reading = $this->dispatchNow(new StoreProofReadingJob($request));
            return redirect()->route('site.proof.show', $reading)->with('success', trans('site.contact_success'));
        } catch (\Exception $exception) {
            return redirect()->back();
        }
    }

    public function show(ProofReading $reading)
    {
        if(!$reading) {
            return redirect('404');
        }
        return view('site.proof', [
            'reading' => $reading
        ]);
    }
}
