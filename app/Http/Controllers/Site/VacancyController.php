<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 20.03.2019
 * Time: 16:41
 */

namespace App\Http\Controllers\Site;


use App\Domain\Pages\Models\Page;
use App\Domain\VacancyRespond\Jobs\StoreVacancyRespondJob;
use App\Domain\VacancyRespond\Models\VacancyRespond;
use App\Domain\VacancyRespond\Requests\VacancyRespondRequest;
use App\Http\Controllers\Controller;

class VacancyController extends Controller
{
    public function index()
    {
        $news = Page::news()
            ->where('active', 1)
            ->withTranslation()
            ->orderBy('created_at', 'desc')
            ->limit(10)
            ->get();

        $vacs = Page::vacs()
            ->where('active', 1)
            ->withTranslation()
            ->orderBy('created_at', 'desc')
            ->paginate();

        return view('site.vacancy', [
            'vacs' => $vacs,
            'news' => $news
        ]);
    }

    public function show(Page $page)
    {
        if(!$page->isVac() || $page->active != 1) {
            return redirect('404');
        }

        return view('site.page', [
            'page' => $page
        ]);
    }

    public function respond(VacancyRespondRequest $request)
    {
        //$request->merge(['type' => Page::TYPE_VAC]);
        $exists = VacancyRespond::where('phone', $request->input('phone'))->where('vacancy_id',$request->input('vacancy_id'))->first();
        if ($exists) {
            return response()->json(['result' => trans('site.already_responded')], 200);
        }
//        dd($request->file('resume_file'));

        try {
            $this->dispatchNow(new StoreVacancyRespondJob($request));
            return response()->json(['result' => trans('site.success_respond')], 200);
        } catch (\Exception $exception) {
            return response()->json(['result' => trans('site.failure_respond')], 200);
        }
    }
}
