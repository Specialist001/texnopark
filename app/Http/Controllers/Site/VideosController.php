<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 20.03.2019
 * Time: 16:41
 */

namespace App\Http\Controllers\Site;


use App\Domain\Pages\Models\Page;
use App\Domain\Vidoes\Models\Video;
use App\Http\Controllers\Controller;

class VideosController extends Controller
{
    public function index()
    {
        $video = Video::orderBy('order')
            ->paginate();

        return view('site.videos', [
            'videos' => $video
        ]);
    }
}
