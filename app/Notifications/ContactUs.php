<?php

namespace App\Notifications;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class ContactUs extends Notification
{

    protected $request;

    /**
     * The callback that should be used to build the mail message.
     *
     * @var \Closure|null
     */
    public static $toMailCallback;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable);
        }

        $mail = new MailMessage;

        $mail->subject(trans('mail.contact.subject'));
        $mail->line(trans('site.name').': '.$this->request->input('name'));
        if($this->request->filled('phone')) {
            $mail->line(trans('site.phone').': '.$this->request->input('phone'));
        }
        if($this->request->filled('email')) {
            $mail->line(trans('site.mail').': '.$this->request->input('email'));
        }
        $mail->line(trans('site.comment').': '.$this->request->input('comment'));

        return $mail;
    }

    /**
     * Set a callback that should be used when building the notification mail message.
     *
     * @param  \Closure  $callback
     * @return void
     */
    public static function toMailUsing($callback)
    {
        static::$toMailCallback = $callback;
    }
}
