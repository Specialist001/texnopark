<?php

namespace App\Providers;

use App\Domain\Contacts\Models\Contact;
use App\Domain\Forum\Models\ForumAnswer;
use App\Domain\Forum\Models\ForumRespond;
use App\Domain\Forum\Models\ForumTheme;
use App\Domain\Investments\Models\Investment;
use App\Domain\PageCategories\Models\PageCategory;
use App\Domain\Pages\Models\Page;
use App\Domain\Projects\Models\Project;
use App\Domain\ProofReadings\Models\ProofReading;
use App\Domain\Users\Models\UserFavourites;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['admin.menu-left', 'portal.header', 'portal.profile.menu', 'portal.profile.investor-submenu'], function ($view) {

            if (\LaravelLocalization::getCurrentLocale() == 'ru') {
                $current_route_name = \Request::segment(1);
            } else {
                $current_route_name = \Request::segment(2);
            }
            /**
             * @var $view \Illuminate\View\View|\Illuminate\Contracts\View\Factory
             */
            $view->with('current_route_name', $current_route_name);
        });
        view()->composer([ 'site.header', 'site.footer'], function ($view) {

            if (\LaravelLocalization::getCurrentLocale() == 'ru') {
                $current_route_name = \Request::segment(1);
                $page_id = \Request::segment(2);
            } else {
                $current_route_name = \Request::segment(2);
                $page_id = \Request::segment(3);
            }
            /**
             * @var $view \Illuminate\View\View|\Illuminate\Contracts\View\Factory
             */
            $view->with('current_route_name', $current_route_name);
            $view->with('page_id', $page_id);
        });
        view()->composer(['portal.profile.menu'], function ($view) {

            $notifications_count = 0;

            if (\Auth::check()) {
                $notifications_count = \Auth::user()->customNotifications()->where('viewed', 0)->count();
            }
            /**
             * @var $view \Illuminate\View\View|\Illuminate\Contracts\View\Factory
             */
            $view->with('notifications_count', $notifications_count);
        });

        view()->composer(['admin.menu-left'], function ($view) {

            $new_projects = Project::where('is_moderated', 0)->count();
            $new_investments = Investment::where('is_moderated', 0)->count();

            $new_themes = ForumTheme::where('is_moderated', 0)->count();
            $new_answers = ForumAnswer::where('is_moderated', 0)->count();
            $new_responds = ForumRespond::where('is_moderated', 0)->count();
            $new_proofs = ProofReading::where('is_moderated', 0)->count();
            /**
             * @var $view \Illuminate\View\View|\Illuminate\Contracts\View\Factory
             */
            $view->with('new', ($new_investments + $new_projects) > 9? '9+': ($new_investments + $new_projects));
            $view->with('new_projects', $new_projects > 9? '9+': $new_projects);
            $view->with('new_investments', $new_investments > 9? '9+': $new_investments);

            $view->with('new_forum_all', ($new_themes + $new_answers + $new_responds) > 9? '9+': ($new_themes + $new_answers + $new_responds));
            $view->with('new_forum', ($new_themes + $new_answers) > 9? '9+': ($new_themes + $new_answers));
            $view->with('new_responds', $new_responds > 9? '9+': $new_responds);
            $view->with('new_proofs', $new_proofs > 9? '9+': $new_proofs);
        });

        view()->composer(['portal.footer', 'site.footer', 'site.header', 'portal.forum.right'], function ($view) {
            /**
             * @var $view \Illuminate\View\View|\Illuminate\Contracts\View\Factory
             */
            $view->with('contact', Contact::all()->keyBy('key'));
        });

        view()->composer(['portal.header', 'site.header', 'site.footer'], function ($view) {
            $system_page = Page::where('system', 1)->first();

            $page_categories = PageCategory::where('active', 1)
                ->where('top', 1)->orderBy('order')
                ->withTranslation()
                ->withActivePages()->get();
            $top_pages = Page::pages()->where('active', 1)
                ->where('top', 1)
                ->withTranslation()->orderBy('order')->get();

            $about_pages = Page::about()
                ->where('active',1)
                ->where('system',1)
                ->where('top',1)
                ->withTranslation()->get();

            $page_contact = Page::contact()
                ->where('active',1)
                ->where('system',1)
                ->where('top',1)
                ->withTranslation()->first();

            $labs = Page::labs()
                ->where('active', 1)
                ->withTranslation()
                ->orderBy('order')
                ->get();

            /**
             * @var $view \Illuminate\View\View|\Illuminate\Contracts\View\Factory
             */
            $view->with('system_page', $system_page);
            $view->with('page_categories', $page_categories);
            $view->with('top_pages', $top_pages);
            $view->with('labs', $labs);
            $view->with('about_pages', $about_pages);
            $view->with('page_contact', $page_contact);
        });

        view()->composer(['site.footer'], function ($view) {
            $bot_page_categories = PageCategory::where('active', 1)
                ->where('bottom', 1)->orderBy('order')
                ->withTranslation()
                ->withActivePages()->get();

            /**
             * @var $view \Illuminate\View\View|\Illuminate\Contracts\View\Factory
             */
            $view->with('bot_page_categories', $bot_page_categories);
        });

        view()->composer(['portal.forum.right'], function ($view) {
            $popular = ForumTheme::where('status', ForumTheme::STATUS_PUBLISHED)
                ->withCount('answers')
                ->orderBy('view', 'desc')
                ->limit(5)->get();

            $count = ForumTheme::where('status', ForumTheme::STATUS_PUBLISHED)->count();

            /**
             * @var $view \Illuminate\View\View|\Illuminate\Contracts\View\Factory
             */
            $view->with('popular', $popular);
            $view->with('themes_count', $count);
        });

        view()->composer(['portal.search', 'portal.profile.investor-submenu'], function ($view) {

            $favorites_array = [];

            if (\Auth::check()) {
                $favorites = UserFavourites::where('user_id', \Auth::user()->id)->get();
                $favorites_array = array_keys($favorites->keyBy('project_id')->toArray());
            }

            /**
             * @var $view \Illuminate\View\View|\Illuminate\Contracts\View\Factory
             */
            $view->with('favorites', $favorites_array);
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
