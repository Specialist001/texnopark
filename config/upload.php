<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 01.03.2019
 * Time: 11:28
 */

return [
    'category' => [
        'width' => 150,
        'height' => 150,
        'default' => 'uploads/defaults/category.png',
    ],
    'poster' => [
        'width' => 1600,
        'height' => 800,
        'default' => 'uploads/defaults/poster.png',
    ],
    'poster_thumb' => [
        'width' => 500,
        'height' => 250,
        'default' => 'uploads/defaults/gallery.png',
    ],
    'projectLogo' => [
        'width' => 300,
        'height' => 300,
        'default' => 'uploads/defaults/logo.png',
    ],
    'projectImage' => [
        'width' => 780,
        'height' => 312,
        'default' => 'uploads/defaults/image.png',
    ],
    'sites' => [
        'width' => 150,
        'height' => 150,
        'default' => 'uploads/defaults/sites-logo.png',
    ],
    'logo' => [
        'width' => 300,
        'height' => 300,
        'default' => 'uploads/defaults/logo.png',
    ],
    'icon' => [
        'width' => 200,
        'height' => 200,
        'default' => 'uploads/defaults/vacancy.png',
    ],
];
