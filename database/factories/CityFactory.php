<?php

use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->defineAs(\App\Domain\Cities\Models\City::class, 'main', function (Faker $faker) {
    return [
    ];
});

$factory->defineAs(\App\Domain\Cities\Models\CityTranslation::class, 'main_ru', function (Faker $faker) {
    return [
        'name' => 'Ташкент',
        'locale' => 'ru'
    ];
});
