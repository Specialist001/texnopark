<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

//$factory->define(App\User::class, function (Faker $faker) {
//    return [
//        'name' => $faker->name,
//        'email' => $faker->unique()->safeEmail,
//        'email_verified_at' => now(),
//        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
//        'remember_token' => str_random(10),
//    ];
//});

$factory->defineAs(\App\Domain\Contacts\Models\Contact::class, 'phone', function (Faker $faker) {
    return [
        'key' => 'phone',
        'value' => '0 (371) 256-34-11'
    ];
});
$factory->defineAs(\App\Domain\Contacts\Models\Contact::class, 'email', function (Faker $faker) {
    return [
        'key' => 'email',
        'value' => 'Email@gsupport.uz'
    ];
});
$factory->defineAs(\App\Domain\Contacts\Models\Contact::class, 'address_ru', function (Faker $faker) {
    return [
        'key' => 'address_ru',
        'value' => 'г.Ташкент, ул. Т.Шевченко, 1'
    ];
});
$factory->defineAs(\App\Domain\Contacts\Models\Contact::class, 'address_uz', function (Faker $faker) {
    return [
        'key' => 'address_uz',
        'value' => 'г.Ташкент, ул. Т.Шевченко, 1'
    ];
});
$factory->defineAs(\App\Domain\Contacts\Models\Contact::class, 'address_en', function (Faker $faker) {
    return [
        'key' => 'address_en',
        'value' => 'г.Ташкент, ул. Т.Шевченко, 1'
    ];
});
$factory->defineAs(\App\Domain\Contacts\Models\Contact::class, 'facebook', function (Faker $faker) {
    return [
        'key' => 'facebook',
        'value' => 'https://facebook.com'
    ];
});
$factory->defineAs(\App\Domain\Contacts\Models\Contact::class, 'telegram', function (Faker $faker) {
    return [
        'key' => 'telegram',
        'value' => 'https://telegram.org'
    ];
});
