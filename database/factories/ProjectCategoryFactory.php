<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

//$factory->define(App\User::class, function (Faker $faker) {
//    return [
//        'name' => $faker->name,
//        'email' => $faker->unique()->safeEmail,
//        'email_verified_at' => now(),
//        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
//        'remember_token' => str_random(10),
//    ];
//});


$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'health', function (Faker $faker) {
    return [
        'icon' => null
    ];
});
$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'health_ru', function (Faker $faker) {
    return [
        'name' => 'Здравоохранение',
        'locale' => 'ru'
    ];
});

$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'it', function (Faker $faker) {
    return [
        'icon' => null
    ];
});
$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'it_ru', function (Faker $faker) {
    return [
        'name' => 'Цифровизация и информационные техналогии',
        'locale' => 'ru'
    ];
});

$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'village', function (Faker $faker) {
    return [
        'icon' => null
    ];
});
$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'village_ru', function (Faker $faker) {
    return [
        'name' => 'Сельское хозяйство',
        'locale' => 'ru'
    ];
});

$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'production', function (Faker $faker) {
    return [
        'icon' => null
    ];
});
$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'production_ru', function (Faker $faker) {
    return [
        'name' => 'Промышленность',
        'locale' => 'ru'
    ];
});

$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'building', function (Faker $faker) {
    return [
        'icon' => null
    ];
});
$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'building_ru', function (Faker $faker) {
    return [
        'name' => 'Строительство',
        'locale' => 'ru'
    ];
});

$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'transport', function (Faker $faker) {
    return [
        'icon' => null
    ];
});
$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'transport_ru', function (Faker $faker) {
    return [
        'name' => 'Транспорт',
        'locale' => 'ru'
    ];
});

$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'edu', function (Faker $faker) {
    return [
        'icon' => null
    ];
});
$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'edu_ru', function (Faker $faker) {
    return [
        'name' => 'Образование',
        'locale' => 'ru'
    ];
});

$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'connection', function (Faker $faker) {
    return [
        'icon' => null
    ];
});
$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'connection_ru', function (Faker $faker) {
    return [
        'name' => 'Связь',
        'locale' => 'ru'
    ];
});

$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'tech', function (Faker $faker) {
    return [
        'icon' => null
    ];
});
$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'tech_ru', function (Faker $faker) {
    return [
        'name' => 'Материально-техническое обеспечение',
        'locale' => 'ru'
    ];
});

$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'science', function (Faker $faker) {
    return [
        'icon' => null
    ];
});
$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'science_ru', function (Faker $faker) {
    return [
        'name' => 'Наука',
        'locale' => 'ru'
    ];
});

$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'manage', function (Faker $faker) {
    return [
        'icon' => null
    ];
});
$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'manage_ru', function (Faker $faker) {
    return [
        'name' => 'Управление',
        'locale' => 'ru'
    ];
});

$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'sell', function (Faker $faker) {
    return [
        'icon' => null
    ];
});
$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'sell_ru', function (Faker $faker) {
    return [
        'name' => 'Торговля',
        'locale' => 'ru'
    ];
});

$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'other', function (Faker $faker) {
    return [
        'icon' => null
    ];
});
$factory->defineAs(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'other_ru', function (Faker $faker) {
    return [
        'name' => 'Прочие отрасли',
        'locale' => 'ru'
    ];
});
