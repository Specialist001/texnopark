<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

//$factory->define(App\User::class, function (Faker $faker) {
//    return [
//        'name' => $faker->name,
//        'email' => $faker->unique()->safeEmail,
//        'email_verified_at' => now(),
//        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
//        'remember_token' => str_random(10),
//    ];
//});

$factory->defineAs(\App\Domain\ProjectTypes\Models\ProjectType::class, 'innovation', function (Faker $faker) {
    return [
    ];
});
$factory->defineAs(\App\Domain\ProjectTypes\Models\ProjectTypeTranslation::class, 'innovation_ru', function (Faker $faker) {
    return [
        'name' => 'Инновационный',
        'locale' => 'ru'
    ];
});

$factory->defineAs(\App\Domain\ProjectTypes\Models\ProjectType::class, 'science', function (Faker $faker) {
    return [
    ];
});
$factory->defineAs(\App\Domain\ProjectTypes\Models\ProjectTypeTranslation::class, 'science_ru', function (Faker $faker) {
    return [
        'name' => 'Научный',
        'locale' => 'ru'
    ];
});

$factory->defineAs(\App\Domain\ProjectTypes\Models\ProjectType::class, 'patent', function (Faker $faker) {
    return [
    ];
});
$factory->defineAs(\App\Domain\ProjectTypes\Models\ProjectTypeTranslation::class, 'patent_ru', function (Faker $faker) {
    return [
        'name' => 'Запатентованный',
        'locale' => 'ru'
    ];
});
