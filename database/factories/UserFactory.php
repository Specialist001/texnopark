<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

//$factory->define(App\User::class, function (Faker $faker) {
//    return [
//        'name' => $faker->name,
//        'email' => $faker->unique()->safeEmail,
//        'email_verified_at' => now(),
//        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
//        'remember_token' => str_random(10),
//    ];
//});

$factory->defineAs(\App\Domain\Users\Models\User::class, 'admin', function (Faker $faker) {
    return [
        'username' => 'admin',
        'role' => \App\Domain\Users\Models\User::ROLE_ADMIN,
        'name' => 'Admin',
        'email' => 'admin@spcenter.uz',
        'email_verified_at' => now(),
        'password' => Hash::make('123123'), // secret
        'remember_token' => str_random(10),
        'active' => 1,
    ];
});

$factory->defineAs(\App\Domain\Users\Models\User::class, 'manager', function (Faker $faker) {
    return [
        'username' => 'manager',
        'role' => \App\Domain\Users\Models\User::ROLE_MANAGER,
        'name' => 'Manager',
        'email' => 'manager@spcenter.uz',
        'email_verified_at' => now(),
        'password' => Hash::make('123123'), // secret
        'remember_token' => str_random(10),
        'active' => 1,
    ];
});

$factory->defineAs(\App\Domain\Users\Models\User::class, 'proof', function (Faker $faker) {
    return [
        'username' => 'proof',
        'role' => \App\Domain\Users\Models\User::ROLE_PROOF,
        'name' => 'Proof',
        'email' => 'proof@spcenter.uz',
        'email_verified_at' => now(),
        'password' => Hash::make('123123'), // secret
        'remember_token' => str_random(10),
        'active' => 1,
    ];
});

$factory->defineAs(\App\Domain\Users\Models\User::class, 'investor', function (Faker $faker) {
    return [
        'username' => 'investor',
        'role' => \App\Domain\Users\Models\User::ROLE_INVESTOR,
        'name' => 'Investor',
        'email' => 'investor@spcenter.uz',
        'email_verified_at' => now(),
        'password' => Hash::make('123123'), // secret
        'remember_token' => str_random(10),
        'active' => 1,
    ];
});

$factory->defineAs(\App\Domain\Users\Models\User::class, 'startup', function (Faker $faker) {
    return [
        'username' => 'startup',
        'role' => \App\Domain\Users\Models\User::ROLE_STARTUP,
        'name' => 'StartUp',
        'email' => 'startup@spcenter.uz',
        'email_verified_at' => now(),
        'password' => Hash::make('123123'), // secret
        'remember_token' => str_random(10),
        'active' => 1,
    ];
});
