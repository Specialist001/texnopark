<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();

            //Отрасль, в которой реализуется проект
            $table->integer('project_category_id')->unsigned();
            $table->integer('project_type_id')->unsigned();

            //Логотип
            $table->string('logo')->nullable();

            $table->string('image')->nullable();

            //Наименование проекта
            $table->string('title');

            //Стадия проекта (идея, действующая модель, работающий бизнес)
            $table->string('stage');

            //Разработчик проекта
            $table->string('developer');

            //Иностранный партнер/инвестор
            $table->boolean('foreign')->default(0);

            //Готовность проекта(в %)
            $table->integer('readiness')->default(0);

            //Номер патента/заявки на патент
            $table->string('patent')->nullable();

            //Сроки реализации проекта (мес.)
            $table->integer('implementation_period')->nullable();

            //Срок окупаемости проекта (мес.)
            $table->integer('payback_period')->nullable();

            //Общая стоимость проекта для масштабирования разработки, в т. ч.:
            //в национальной валюте (млн. сум.)
            $table->string('sum_cost')->nullable();
            //в иностранной валюте (тыс. долл.)
            $table->string('usd_cost')->nullable();

            //Требуемые инвестиции (денежные средства/активы)
            //в национальной валюте (млн. сум.)
            $table->string('sum_need')->nullable();
            //в иностранной валюте (тыс. долл.)
            $table->string('usd_need')->nullable();

            //Собственные денежные средства/активы
            //в национальной валюте (млн. сум.)
            $table->string('sum_current')->nullable();
            //в иностранной валюте (тыс. долл.)
            $table->string('usd_current')->nullable();

            //Вновь создаваемые рабочие места (чел.)
            $table->integer('workers')->nullable();

            //Презентация, Бизнес план
            $table->string('presentation')->nullable();

            //Статус
            $table->string('status');
            $table->bigInteger('view')->default(0);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('project_category_id')
                ->references('id')
                ->on('project_categories')
                ->onDelete('cascade');

            $table->foreign('project_type_id')
                ->references('id')
                ->on('project_types')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
