<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_translations', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('project_id')->unsigned();

            //О руководителе
            $table->text('about_developer')->nullable();

            //Место реализации проекта(город, район, улица, дом)
            $table->text('place')->nullable();

            //Краткое описание проекта
            $table->text('short_description')->nullable();

            //Полное описание проекта
            $table->text('description')->nullable();

            //Предприятие для внедрения/потенциальный потребитель
            $table->text('target')->nullable();

            //Конечный продукт и проектная мощность (тн., кг и пр.) (например 310 тыс.тонн масличных культур)
            $table->text('finally')->nullable();

            //Текущее состояние
            $table->text('state')->nullable();

            //О презентации
            $table->text('about_presentation')->nullable();

            //Доп сведения
            $table->text('additional_info')->nullable();

            $table->string('locale')->index();

            $table->unique(['project_id', 'locale']);
            $table->foreign('project_id')
                ->references('id')
                ->on('projects')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_translations');
    }
}
