<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForumAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forum_answers', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('forum_theme_id')->unsigned();

            $table->integer('user_id')->nullable()->default(null);
            $table->string('user_name')->nullable()->default(null);
            $table->text('text');
            $table->string('file')->nullable();
            $table->boolean('is_moderated')->nullable()->default(1);

            $table->foreign('forum_theme_id')
                ->references('id')
                ->on('forum_themes')
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forum_answers');
    }
}
