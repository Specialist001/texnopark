<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('page_category_id')->unsigned()->nullable();
            $table->string('type');
            $table->string('poster')->nullable();
            $table->string('thumb')->nullable();

            $table->boolean('active')->default(1);
            $table->integer('order')->default(0);

            $table->boolean('top')->default(0);
            $table->boolean('system')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
