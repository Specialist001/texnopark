<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsForumThemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('forum_themes', function (Blueprint $table) {
            $table->text('organization_title')->nullable();
            $table->text('category')->nullable();
            $table->text('required_solution')->nullable();
            $table->text('expected_effect')->nullable();
            $table->text('required_organization')->nullable();
            $table->text('contacts')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('forum_themes', function (Blueprint $table) {
            $table->dropColumn('organization_title');
            $table->dropColumn('category');
            $table->dropColumn('required_solution');
            $table->dropColumn('expected_effect');
            $table->dropColumn('required_organization');
            $table->dropColumn('contacts');
        });
    }
}
