<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNationalSiteTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('national_site_translations', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('national_site_id')->unsigned();
            $table->string('name');

            $table->string('locale')->index();

            $table->unique(['national_site_id', 'locale']);
            $table->foreign('national_site_id')
                ->references('id')
                ->on('national_sites')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('national_site_translations');
    }
}
