<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProdCapacityToProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->integer('prod_capacity')->nullable();
            $table->integer('prod_capacity_unit')->unsigned()->nullable();
            $table->integer('metal_structures')->nullable();
            $table->integer('sandwich_panels')->nullable();
            $table->integer('indoor_area')->nullable();

            $table->foreign('prod_capacity_unit')
                ->references('id')
                ->on('units')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropForeign(['prod_capacity_unit']);
            $table->dropColumn('prod_capacity');
            $table->dropColumn('metal_structures');
            $table->dropColumn('sandwich_panels');
            $table->dropColumn('indoor_area');
        });
    }
}
