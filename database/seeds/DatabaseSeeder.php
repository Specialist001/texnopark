<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(\App\Domain\Users\Models\User::class, 'admin', 1)->create();
        factory(\App\Domain\Users\Models\User::class, 'manager', 1)->create();
        factory(\App\Domain\Users\Models\User::class, 'proof', 1)->create();
        factory(\App\Domain\Users\Models\User::class, 'investor', 1)->create();
        factory(\App\Domain\Users\Models\User::class, 'startup', 1)->create();

        factory(\App\Domain\Contacts\Models\Contact::class, 'phone', 1)->create();
        factory(\App\Domain\Contacts\Models\Contact::class, 'email', 1)->create();
        factory(\App\Domain\Contacts\Models\Contact::class, 'address_ru', 1)->create();
        factory(\App\Domain\Contacts\Models\Contact::class, 'address_uz', 1)->create();
        factory(\App\Domain\Contacts\Models\Contact::class, 'address_en', 1)->create();
        factory(\App\Domain\Contacts\Models\Contact::class, 'facebook', 1)->create();
        factory(\App\Domain\Contacts\Models\Contact::class, 'telegram', 1)->create();

        factory(\App\Domain\ProjectTypes\Models\ProjectType::class, 'innovation', 1)->create()->each(function ($type) {
            /**
             * @var $type \App\Domain\ProjectTypes\Models\ProjectType
             */
            $type->translations()->saveMany(factory(\App\Domain\ProjectTypes\Models\ProjectTypeTranslation::class, 'innovation_ru', 1)->make());
        });
        factory(\App\Domain\ProjectTypes\Models\ProjectType::class, 'science', 1)->create()->each(function ($type) {
            /**
             * @var $type \App\Domain\ProjectTypes\Models\ProjectType
             */
            $type->translations()->saveMany(factory(\App\Domain\ProjectTypes\Models\ProjectTypeTranslation::class, 'science_ru', 1)->make());
        });
        factory(\App\Domain\ProjectTypes\Models\ProjectType::class, 'patent', 1)->create()->each(function ($type) {
            /**
             * @var $type \App\Domain\ProjectTypes\Models\ProjectType
             */
            $type->translations()->saveMany(factory(\App\Domain\ProjectTypes\Models\ProjectTypeTranslation::class, 'patent_ru', 1)->make());
        });

        factory(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'health', 1)->create()->each(function ($category) {
            /**
             * @var $category \App\Domain\ProjectCategories\Models\ProjectCategory
             */
            $category->translations()->saveMany(factory(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'health_ru', 1)->make());
        });
        factory(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'it', 1)->create()->each(function ($category) {
            /**
             * @var $category \App\Domain\ProjectCategories\Models\ProjectCategory
             */
            $category->translations()->saveMany(factory(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'it_ru', 1)->make());
        });
        factory(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'village', 1)->create()->each(function ($category) {
            /**
             * @var $category \App\Domain\ProjectCategories\Models\ProjectCategory
             */
            $category->translations()->saveMany(factory(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'village_ru', 1)->make());
        });
        factory(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'production', 1)->create()->each(function ($category) {
            /**
             * @var $category \App\Domain\ProjectCategories\Models\ProjectCategory
             */
            $category->translations()->saveMany(factory(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'production_ru', 1)->make());
        });
        factory(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'building', 1)->create()->each(function ($category) {
            /**
             * @var $category \App\Domain\ProjectCategories\Models\ProjectCategory
             */
            $category->translations()->saveMany(factory(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'building_ru', 1)->make());
        });
        factory(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'transport', 1)->create()->each(function ($category) {
            /**
             * @var $category \App\Domain\ProjectCategories\Models\ProjectCategory
             */
            $category->translations()->saveMany(factory(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'transport_ru', 1)->make());
        });
        factory(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'edu', 1)->create()->each(function ($category) {
            /**
             * @var $category \App\Domain\ProjectCategories\Models\ProjectCategory
             */
            $category->translations()->saveMany(factory(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'edu_ru', 1)->make());
        });
        factory(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'connection', 1)->create()->each(function ($category) {
            /**
             * @var $category \App\Domain\ProjectCategories\Models\ProjectCategory
             */
            $category->translations()->saveMany(factory(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'connection_ru', 1)->make());
        });
        factory(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'tech', 1)->create()->each(function ($category) {
            /**
             * @var $category \App\Domain\ProjectCategories\Models\ProjectCategory
             */
            $category->translations()->saveMany(factory(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'tech_ru', 1)->make());
        });
        factory(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'science', 1)->create()->each(function ($category) {
            /**
             * @var $category \App\Domain\ProjectCategories\Models\ProjectCategory
             */
            $category->translations()->saveMany(factory(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'science_ru', 1)->make());
        });
        factory(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'manage', 1)->create()->each(function ($category) {
            /**
             * @var $category \App\Domain\ProjectCategories\Models\ProjectCategory
             */
            $category->translations()->saveMany(factory(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'manage_ru', 1)->make());
        });
        factory(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'sell', 1)->create()->each(function ($category) {
            /**
             * @var $category \App\Domain\ProjectCategories\Models\ProjectCategory
             */
            $category->translations()->saveMany(factory(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'sell_ru', 1)->make());
        });
        factory(\App\Domain\ProjectCategories\Models\ProjectCategory::class, 'other', 1)->create()->each(function ($category) {
            /**
             * @var $category \App\Domain\ProjectCategories\Models\ProjectCategory
             */
            $category->translations()->saveMany(factory(\App\Domain\ProjectCategories\Models\ProjectCategoryTranslation::class, 'other_ru', 1)->make());
        });


        factory(\App\Domain\Cities\Models\City::class, 'main', 1)->create()->each(function ($city) {
            /**
             * @var $city \App\Domain\Cities\Models\City
             */
            $city->translations()->saveMany(factory(\App\Domain\Cities\Models\CityTranslation::class, 'main_ru', 1)->make());
        });
    }
}
