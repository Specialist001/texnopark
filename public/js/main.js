(function($){

	$(function(){

		var w = $(window),
			b = $('body');

		/*------------------------------*/

		var menuVisible = $('.menu-visible'),
			menuHidden = $('.menu-hidden'),
			menuHiddenUl = menuHidden.find('.dropdown-menu'),
			menuVisibleLinks = menuVisible.find('.nav-item');

		function menuFlex() {

			var first = menuVisibleLinks.first(),
				firstHeight = first.outerHeight(),
				firstPosition = first.position();

			menuHiddenUl.html('');
			menuVisibleLinks.removeClass('d-none')

			menuVisible.addClass('overflow-hidden');

			menuVisibleLinks.each(function(){
				var link = $(this);

				var linkText = link.find('>a').text(),
					linkHref = link.find('>a').attr('href');

				var linkHeight = link.outerHeight(),
					linkPosition = link.position();

				var linkNew = $('<a class="dropdown-item">');

					linkNew.attr('href', linkHref);
					linkNew.text(linkText);

				if (linkPosition.top > 0) {
					linkNew.appendTo(menuHiddenUl);
					link.addClass('d-none');
				} else {
					link.removeClass('d-none');
				}

			})

			if (menuHiddenUl.is(':empty')) {
				menuHidden.addClass('d-none');
			} else {
				menuHidden.removeClass('d-none');
			}

			menuVisible.removeClass('overflow-hidden');
		}

		menuFlex();
		w.on('load', menuFlex);
		w.resize(menuFlex);

		/*------------------------------ */
			
		$('.search-btn').click(function(e){
			e.preventDefault();
			b.toggleClass('show-search');

		})

		/* ----------------------------- */

		$('.site-header').stick_in_parent({

		});

		/* ---------------------------- */

		var hasIntro = $('.intro-slider').length && $('.intro-slider-2').length;

		var intro_slider = new Swiper('.intro-slider', {
			effect: 'fade',
			navigation: {
	        	nextEl: '.swiper-next',
	        	prevEl: '.swiper-prev',
	      	},
		});

		var intro_slider_2 = new Swiper('.intro-slider-2', {
	      	pagination: {
		        el: '.swiper-pagination',
	      	},
		})

		if (hasIntro) {
			intro_slider.controller.control = intro_slider_2;
    		intro_slider_2.controller.control = intro_slider;
		}

    	var tablet_slider = new Swiper('.tablet-slider', {
    		pagination: {
    			el: '.tablet-pagination'
    		},
    		navigation: {
	        	nextEl: '.tablet-next',
	        	prevEl: '.tablet-prev',
	      	},
    	});

    	var projects_slider = new Swiper('.projects-slider', {
    		preloadImages: false,
		    lazy: true,
		    watchSlidesVisibility:true,
		    slidesPerView: 5,
		    slidesPerGroup:5,
		    slidesPerColumn:2,
		    spaceBetween:16,
		    pagination: {
			    el: '.swiper-pagination',
			    type: 'bullets',
		  	},
		});

    	/* ----------------------------- */


		var mobileMenu = $('.mobile-menu-links');

		mobileMenu.append(menuVisibleLinks.clone());

		mobileMenu.find('.d-none').removeClass('d-none');

    	$('#menu-toggle').click(function(){
    		b.toggleClass('menu-show');
    	})

    	$('.mobile-menu-overlay').click(function(){
    		b.removeClass('menu-show');
    	});

	})

})(jQuery);