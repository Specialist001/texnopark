<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Имя пользователя и пароль не совпадают.',
    'throttle' => 'Слишком много попыток входа. Пожалуйста, попробуйте еще раз через :seconds секунд.',

    'login' => 'Login',
    'register' => 'Register',
    'phone' => 'Phone',
    'city' => 'City',
    'site' => 'Site',
    'username' => 'Login',
    'restore' => 'Restore',
    'save' => 'Save',
    'legal' => 'Legal',
    'individual' => 'Individual',
    'email' => 'E-mail',
    'name' => 'Full name',
    'password' => 'Password',
    'password_confirm' => 'Password confirm',
    'reset' => 'Forgot your password',
    'remember' => 'remember',
    'enter' => 'Enter',
    'logout' => 'Logout',

    'password_change' => 'Password change',
    'current_password' => 'Current password',
    'new_password' => 'New password',
    'password_confirmation' => 'password confirmation',
    'password_not_changed' => 'password not changed',
    'password_wrong_current' => 'Password wrong current',
];
