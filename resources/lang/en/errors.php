<?php

return [
    '404' => [
        'title' => 'Eror 404',
        'description' => 'The page can be moved or deleted. Check the page address or go to the main page.',
        'home' => 'to Home'
    ]
];
