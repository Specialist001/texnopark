<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 23.03.2019
 * Time: 20:43
 */

return [
    'organization_title' => 'Name of organization / enterprise',
    'title' => 'Name of a specific industry / enterprise problem',
    'text' => 'Brief description of the industry problem',
    'category' => 'industry',
    'required_solution' => 'Required way to solve the problem',
    'expected_effect' => 'Expected outcome from problem solving',
    'required_organization' => 'Organization / developer required to solve the problem',
    'contacts' => 'Contacts',
];
