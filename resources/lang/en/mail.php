<?php

return [
    'verify' => [
        'subject' => 'Confirmation E-mail address',
        'top_text' => 'To confirm your email, click the button below.',
        'button' => 'Confirm',
        'no_further' => 'If you are not registered, just ignore this letter.',
    ],
    'reset' => [
        'subject' => 'reset password',
        'top_text' => 'You received this email because we were asked to reset your password.',
        'button' => 'Reset password',
        'bottom_text' => 'Link is active :count minutes.',
        'no_further' => 'If you did not request a reset, then just ignore this email.',
    ],
    'contact' => [
        'subject' => 'Application from the site spcenter.uz',
        'no_further' => '',
    ],
];
