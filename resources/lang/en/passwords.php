<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'The password must be at least eight characters and match the confirmation.',
    'reset'    => 'Your password has been reset!',
    'sent'     => 'Link to reset your password has been sent!',
    'token'    => 'Wrong password reset code.',
    'user'     => 'Could not find user with specified email address.',

];
