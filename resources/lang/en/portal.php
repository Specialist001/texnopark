<?php

return [
    'notify_non_authorized' => "To use the various functions of the Portal of scientific research and innovative ideas, projects, You need to get registered. To view the full information about the projects, as well as to ensure direct communication with the initiators, you must register as an Investor. To submit a new project, you must also be officially registered as a \"Developer\". If you have questions regarding registration and submission of projects, contact the Ministry of Innovative Development of the Republic of Uzbekistan. ",
    'profile' => 'Profile',
    'home' => 'Home',
    'portal_link' => 'Database of developments ',
    'portal_home' => 'Database of developments ',
    'portal_forum' => 'database of problems ',
    'find_project' => 'Find project',
    'title_main' => 'Find innovative projects',
    'title_search' => 'Find innovative projects',
    'search_project' => 'Find a project',
    'banner_title' => 'A little about portal',
    'banner_text' => 'Specialized Portal of scientific developments and innovative projects, will provide continuous communication "customer-researcher-investor". The portal will allow to create access to innovative developments of all interested parties, including investors, industries and enterprises, to form the demand-problems of industries and enterprises and proposals of scientific institutions, which will ensure the effectiveness of innovative developments and their effective commercialization.',

    'fl_search' => 'Find all projects',
    'fl_add' => 'Add project',
    'fl_reg' => 'register as investor',
    'personal_area' => 'Личный кабинет',
    'personal_data' => 'personal data',
    'last_projects' => 'Recent projects',
    'categories' => 'categories',
    'no_projects' => 'not projects',
    'no_categories' => 'not categories',
    'projects_choice' => 'проект|проекта|проектов',
    'more' => 'Read more',

    'contacts' => 'Contact details',
    'facebook' => 'Facebook',
    'telegram' => 'Telegram',
    'copyright' => 'All rights reserved',
    'qwerty' => 'website developed by <a href="https://qwerty.uz" class="qwerty_link" target="_blank">QWERTY</a>',
    'filter_category' => 'Select industy',
    'filter_type' => 'Project type',
    'filter_readiness' => 'Project readlines (in %)',
    'filter_payback' => 'Project payback period',
    'filter_cost' => 'Total amount',
    'filter_need' => 'Investments required',
    'filter_patent' => 'Patent',
    'all_patent' => 'All',
    'with_patent' => 'with patent',
    'without_patent' => 'without patent',
    'patent_number' => 'number',
    'search_filter' => 'keywords search',
    'from' => 'From',
    'till' => 'to',
    'mth' => 'month.',
    'sum' => 'sum',
    'usd' => '$',
    'yes' => 'yes',
    'no' => 'no',
    'reset_filter' => 'reset filter',
    'apply_filter' => 'search projects',
    'additional_filters' => 'additional filters',
    'found_on_page' => 'found on page',
    'invested' => 'invested',
    'short_description' => 'destination developments',
    'category_id' => 'industy',
    'state_readiness' => 'Project readlines',
    'invest' => 'Invest',
    'project_description' => 'Project description',
    'project_stats' => 'Indicators',
    'project_about_presentation' => 'Documents',
    'project_additional_info' => 'Project additional info',
    'download_presentation' => 'Download presentation',

    'project_stat_1' => 'Name of the project',
    'project_stat_2' => 'Foreing investor',
    'project_stat_3' => 'product patent',
    'project_stat_4' => 'The branch in which the project is being implemented',
    'project_stat_5' => 'Project readlines',
    'project_stat_6' => 'Project location',
    'project_stat_7' => 'Project payback period',
    'project_stat_8' => 'Project Development Cost',
    'project_stat_9' => 'Required Project Investment',
    'project_stat_10' => 'Own funds',
    'project_stat_11' => 'Final product and design capacity',
    'project_stat_12' => 'current state',
//    'project_stat_12' => 'Стадия проекта',

    'no_current_sum' => 'There is none',
    'no_cost_sum' => 'Not specifed',
    'na' => 'Not specifed',
    'authorization' => 'authentication',
    'reset' => 'reset',
    'restore_password' => 'restore password',
    'not_verified_title' => 'mail verifications',
    'not_verified_text' => 'Account is not active. You have not confirmed your E-mail address. Please go to the email address registered during registration and find the activation letter.',
    'not_verified_notice' => 'If you did not find the letter, look in the Spam folder or resend the letter again.',
    'resend_verify' => 'Resend verify',
    'verified' => 'Verified',
    'resent_verify' => 'Resent verifed',
    'registration' => 'Register',
    'investor' => 'Investor',
    'startup' => 'Developer',
    'accept_agreement' => 'By checking this box, you confirm that you have read and understood the privacy statement.',
    'registration_notify' => 'This information is available only to registered users and will not be distributed.',
    'my_projects' => 'My projects',
    'my_investments' => 'My investments',
    'notifications' => 'notifications',
    'save' => 'Save',
    'profile_notice' => 'This information is available only to registered users.',
    'relogin_startup' => 'You can not add a project because authorized as an investor. To add your project, log out and log in / register as a developer.',
    'relogin_investor' => 'You cannot invest the project because authorized as a developer. To invest the project, log out and log in / register as an investor.',

    'user_id' => 'User ID',
    'user_locale' => 'Email language',
    'logout' => 'Exit',
    'project' => 'Project',
    'projects' => 'Investment project',
    'project_views' => 'Project views',
    'project_investments' => 'Project Requests',
    'all_investors' => 'All investors',
    'investors_id' => 'Investors Id',
    'no_investors' => 'no investors',
    'id' => 'id',
    'close' => 'Close',
    'project_passport' => 'Project passport',
    'editing_project' => 'Editing a project will result in repeated moderation.',
    'my_invested' => 'Amount',
    'your_investment' => 'Your investment',
    'became_investor' => 'I want to become an investor of this project',
    'invested_send' => 'Application accepted. We will contact you in the near future.',
    'invested_not_send' => 'An error has occurred. Repeat later.',
    'not_registered' => 'To invest in a project, register or log in as an investor.',
    'hasInvested' => 'You have already submitted an application for investing this project.',
    'favorites' => 'Favorites',


    'notifications_type' => [
        'project' => [
            'published' => 'Your project <strong>:project</strong> has been approved and published.',
            'blocked' => 'Your project <strong>:project</strong> was not approved and blocked.',
            'invested' => 'Approval of investment in your project <strong>:project</strong>.',
            'invested_canceled' => 'Cancellation of investment to your project <strong>:project</strong>.',
        ],
        'investment' => [
            'ok' => 'Investments to project <strong>:project</strong> approved',
            'cancel' => 'Investments to project <strong>:project</strong> rejected',
        ],
    ],


    'theme_filter' => 'search the forum',
    'hot_themes' => 'Hot themes',
    'theme_add' => 'Add theme',
    'theme_edit' => 'Edit',
    'theme_edit_notice' => 'Editing rivers to re-moderation. Or you can simply add a topic with your comment.',
    'theme_respond' => 'I want to help',
    'theme_respond_notice' => 'Leave your contacts and we will contact you.',
    'my_themes' => 'My themes',
    'other_themes' => 'Other themes',
    'add' => 'Add',
    'theme_add_text' => 'Become a member of the community and get all the answers first.',
    'theme' => 'Theme',
    'text' => 'Text',
    'theme_statuses' => [
        'moderating' => 'Moderating',
        'published' => 'Published',
        'blocked' => 'Blocked'
    ],
    'theme_file' => 'Attach file',
    'theme_file_attached' => 'File',
    'theme_file_attached_show' => 'Attached file',
    'answers' => 'Answers',
    'user_name' => 'Full name',
    'answer' => 'Answer',
    'leave_answer' => 'Leave answer',
    'leave_answer_btn' => 'Reply',
    'answers_by_count' => 'ответ|ответа|ответов',
    'name' => 'Full name',
    'phone' => 'Phone',
    'email' => 'E-mail',
    'comment' => 'Comment',
    'send' => 'Send',
    'not_send' => 'Failed to submit a request. Check the form or repeat later.',
    'partners' => 'Our partners',
    'all_partners' => 'All partners',
    'news' => 'News',
    'all_news' => 'All news',
    'themes_count' => 'Total in base',
    'themes_count_by_count' => 'проблема|проблемы|проблем',
];
