<?php

return [
    'project' => 'Project', //current_user|number
    'id' => 'ID', //current_user|number
    'user_id' => 'User ID', //current_user|number
    'project_category_id' => 'category',//, в которой реализуется проект', //select category
    'project_type_id' => 'Type of project', //select type
    'logo' => 'Logo', //file
    'image' => 'Image', //file
    'title' => 'Title of project', //string
    'stage' => 'Stage of project', //select (идея, действующая модель, работающий бизнес)
    'developer' => 'Developer of project',//string
    'foreign' => 'foreign partner/investor', //select да|нет
    'readiness' => 'readlines of project (in %)', //number|0-100
    'hasPatent' => 'has patent', //string
    'patent' => 'number of project/pattent application', //string
    'implementation_period' => 'Project timeline',//number|months
    'payback_period' => 'Payback period',//number|months
    'cost' => 'The total cost of the project to scale development',//string
    'need' => 'Investment required',//string
    'current' => 'Own funds',//string
    'workers' => 'Newly created jobs',//number|peoples
    'presentation' => 'presentation, business plan',//file
    'status' => 'status',//select (moderating|published|blocked)
    'view' => 'Views', //--
    'importable' => 'Import substitution/exportorientations', //--

    'sum' => 'In national currency',
    'usd' => 'In foreign currency',

    'from' => 'from',
    'till' => 'till',

    'yes' => 'Yes',
    'no' => 'No',

    'period_unit' => 'month.',
    'worker_unit' => 'person.',

    'cost_filter' => 'total cost',

    'about_developer' => 'Uniqueness/innovativeness',
    'place' => 'Project location',//string
    'short_description' => 'Designation purpose',
    'description' => 'Full project description',
    'target' => 'Enterprise for introduction / potential consumer',//string
    'finally' => 'Final product and design capacity per year',//string
    'state' => 'Current state',//string
    'about_presentation' => 'Documents',
    'additional_info' => 'Additional info',

    'place_unit' => 'City, area, street, flat',
    'finally_unit' => 'for example 310 thousand tons of oilseeds',
    'statuses' => [
        'moderating' => 'Moderating',
        'published' => 'Published',
        'blocked' => 'Blocked',
    ],
    'investment_statuses' => [
        'moderating' => 'Moderating',
        'ok' => 'OK',
        'cancel' => 'cancel',
    ],
    'stages' => [
        'idea' => 'Idea',
        'model' => 'Model',
        'business' => 'Business',
    ],
    'investment_sum' => 'Sum',
    'prod_capacity'=>'Годовая производственная мощность',
    'prod_capacity_unit'=>'Ед. изм',
    'metal_structures'=>'Металлических конструкций',
    'sandwich_panels'=>'Сэндвич-панели',
    'indoor_area'=>'Площадь крытых помещений',
];
