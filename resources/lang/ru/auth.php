<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Имя пользователя и пароль не совпадают.',
    'throttle' => 'Слишком много попыток входа. Пожалуйста, попробуйте еще раз через :seconds секунд.',

    'login' => 'Вход',
    'register' => 'Зарегистрироваться',
    'phone' => 'Телефон',
    'city' => 'Город',
    'site' => 'Сайт',
    'username' => 'Логин',
    'restore' => 'Восстановить',
    'save' => 'Сохранить',
    'legal' => 'Юр. лицо',
    'individual' => 'Физ. лицо',
    'email' => 'E-mail',
    'name' => 'ФИО',
    'password' => 'Пароль',
    'password_confirm' => 'Подтвердите пароль',
    'reset' => 'Забыли пароль?',
    'remember' => 'Запомнить',
    'enter' => 'Войти',
    'logout' => 'Выход',

    'password_change' => 'Смена пароля',
    'current_password' => 'Текущий пароль',
    'new_password' => 'Новый пароль',
    'password_confirmation' => 'Подтверждение нового пароля',
    'password_not_changed' => 'Пароль не изменен',
    'password_wrong_current' => 'Не правильный текущий пароль',
];
