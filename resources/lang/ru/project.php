<?php

return [
    'project' => 'Проект', //current_user|number
    'id' => 'ID', //current_user|number
    'user_id' => 'ID пользователя', //current_user|number
    'project_category_id' => 'Отрасль',//, в которой реализуется проект', //select category
    'project_type_id' => 'Тип проекта', //select type
    'logo' => 'Логотип', //file
    'image' => 'Обложка', //file
    'title' => 'Наименование проекта', //string
    'stage' => 'Стадия проекта', //select (идея, действующая модель, работающий бизнес)
    'developer' => 'Разработчик проекта',//string
    'foreign' => 'Иностранный партнер/инвестор', //select да|нет
    'readiness' => 'Готовность проекта (в %)', //number|0-100
    'hasPatent' => 'Есть патент', //string
    'patent' => 'Номер патента/заявки на патент', //string
    'implementation_period' => 'Сроки реализации проекта',//number|months
    'payback_period' => 'Срок окупаемости проекта',//number|months
    'cost' => 'Общая стоимость проекта для масштабирования разработки',//string
    'need' => 'Требуемые инвестиции',//string
    'current' => 'Собственные денежные средства',//string
    'workers' => 'Вновь создаваемые рабочие места',//number|peoples
    'presentation' => 'Презентация, Бизнес план',//file
    'status' => 'Статус',//select (moderating|published|blocked)
    'view' => 'Просмотров', //--
    'importable' => 'Импортозамещение/экспртоориентированность', //--

    'sum' => 'В национальной валюте',
    'usd' => 'В иностранной валюте',

    'from' => 'от',
    'till' => 'до',

    'yes' => 'Да',
    'no' => 'Нет',

    'period_unit' => 'мес.',
    'worker_unit' => 'чел.',

    'cost_filter' => 'Общая стоимость',

    'about_developer' => 'Уникальность/инновационность',
    'place' => 'Место реализации проекта',//string
    'short_description' => 'Название',
    'description' => 'Полное описание проекта',
    'target' => 'Предприятие для внедрения/потенциальный потребитель',//string
    'finally' => 'Конечный продукт и проектная мощность в год',//string
    'state' => 'Текущее состояние',//string
    'about_presentation' => 'Документы',
    'additional_info' => 'Дополнительные сведения',

    'place_unit' => 'Город, район, улица, дом',
    'finally_unit' => 'Например 310 тыс.тонн масличных культур',
    'statuses' => [
        'moderating' => 'На модерации',
        'published' => 'Опубликован',
        'blocked' => 'Заблокирован',
    ],
    'investment_statuses' => [
        'moderating' => 'На модерации',
        'ok' => 'Одобрен',
        'cancel' => 'Отклонен',
    ],
    'stages' => [
        'idea' => 'Идея',
        'model' => 'Действующая модель',
        'business' => 'Работающий бизнес',
    ],
    'investment_sum' => 'Сумма',
    'prod_capacity' => 'Годовая производственная мощность',
    'prod_capacity_unit' => 'Ед. изм',
    'metal_structures' => 'Металлических конструкций',
    'sandwich_panels' => 'Сэндвич-панели',
    'indoor_area' => 'Площадь крытых помещений',
];
