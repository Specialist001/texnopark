﻿<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Имя пользователя и пароль не совпадают.',
    'throttle' => 'Слишком много попыток входа. Пожалуйста, попробуйте еще раз через :seconds секунд.',

    'login' => 'Kirish',
    'register' => 'Registraciyadan o\'tish',
    'phone' => 'Telefon',
    'city' => 'Shahar',
    'site' => 'Sayt',
    'username' => 'Login',
    'restore' => 'Tiklash',
    'save' => 'Saqlash',
    'legal' => 'Yur. shaxs',
    'individual' => 'Fiz. shaxs',
    'email' => 'E-mail',
    'name' => 'FISH',
    'password' => 'Parol',
    'password_confirm' => 'Parolni tasdiqlash',
    'reset' => 'Parolni unutdingizmi',
    'remember' => 'Eslash',
    'enter' => 'Kirish',
    'logout' => 'Chiqish',

    'password_change' => 'Parolni o\'zgartirish',
    'current_password' => 'Faol parol',
    'new_password' => 'Yangi parol',
    'password_confirmation' => 'Yangi parolni tasdiqlash',
    'password_not_changed' => 'Parol ozgarmadi',
    'password_wrong_current' => 'Parol noto\'g\'ri',
];
