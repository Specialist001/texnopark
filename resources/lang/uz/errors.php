﻿<?php

return [
    '404' => [
        'title' => 'Hato 404',
        'description' => 'Siz izlayotgan sahifa mavjud emas: uni ko\'chirish yoki o\'chirish mumkin. Sahifa manzilini tekshiring yoki asosiy sahifaga o\'ting.',
        'home' => 'Sahifa boshiga'
    ]
];
