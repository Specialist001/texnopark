﻿<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 23.03.2019
 * Time: 20:43
 */

return [
    'organization_title' => 'Tashkilot nomi / korxona',
    'title' => 'Muayyan sanoat / korxona muammosi nomi',
    'text' => 'Sektor muammosining qisqacha tavsifi',
    'category' => 'Otrasl / Sanoat',
    'required_solution' => 'Muammoni hal qilishning zarur usuli',
    'expected_effect' => 'Muammoni hal qilishning kutilayotgan natijalari',
    'required_organization' => 'Muammoni hal qilish uchun zarur tashkilot / ishlab chiquvchi',
    'contacts' => 'Bog\'lanish',
];
