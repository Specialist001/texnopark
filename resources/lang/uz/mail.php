﻿<?php

return [
    'verify' => [
        'subject' => 'E-mail manzilini tasdiqlang.',
        'top_text' => 'E-mailni tasdiqlash uchun quyidagi tugmani bosing.',
        'button' => 'Tasdiqlash',
        'no_further' => 'Agar siz ro\'yxatdan o\'tmagan bo\'lsangiz, ushbu maktubni e\'tiborsiz qoldiring.',
    ],
    'reset' => [
        'subject' => 'Parolni tiklash',
        'top_text' => 'Siz ushbu parolni qabul qildingiz, chunki parolni qayta tiklash so\'ralgan.',
        'button' => 'Parolni tiklash',
        'bottom_text' => 'Ulanish faol :count minut.',
        'no_further' => 'Agar siz asl holatini tiklashni so\'ramagan bo\'lsangiz, unda bu elektron xabarni e\'tiborsiz qoldiring.',
    ],
    'contact' => [
        'subject' => 'spcenter.uz saytidan ariza',
        'no_further' => '',
    ],
];
