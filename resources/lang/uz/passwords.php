﻿<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Parol kamida sakkizta belgidan iborat bo\'lishi kerak va tasdiqlash bilan mos bo\'lishi kerak.',
    'reset'    => 'Parolingiz qayta tiklandi!',
    'sent'     => 'Parolni tiklash uchun havola yuborildi!',
    'token'    => 'Noto\'g\'ri parolni tiklash kodi.',
    'user'     => 'Belgilangan elektron pochta manziliga ega foydalanuvchi topilmadi.',

];
