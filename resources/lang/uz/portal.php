<?php

return [
    'notify_non_authorized' => "Ilmiy ishlanmalar va innovatsion g‘oya-loyihalar portalining turli funksiyalari va imkoniyatlaridan foydalanish uchun Siz ro‘yhatdan va avtorizatsiyadan o‘tishingiz kerak bo‘ladi. Loyihalar haqida to‘liq ma’lumotlar bilan tanishish, shuningdek tashabbuskorlar bilan to‘g‘ridan-to‘g‘ri aloqani ta’minlash uchun Investor sifatida ro‘yhatdan o‘tishingiz lozim bo‘ladi. Yangi loyihani kiritish uchun esa, “Ishlanma yaratuvchi” sifatida rasman ro‘yhatdan o‘tishingiz shart bo‘ladi. Ro‘yhatdan o‘tish va loyihalarni kiritish bo‘yicha savollar paydo bo‘lsa, Innovatsion rivojlanish vazirligiga murojaat qilinish mumkin.",
    'profile' => 'Profil',
    'home' => 'Bosh sahifa',
    'portal_link' => 'Ishlanmalar bazasi',
    'portal_home' => 'Ishlanmalar bazasi',
    'portal_forum' => 'Muammolar bazasi',
    'find_project' => 'Loyiha qidirish',
    'title_main' => 'Loyihani tanlash',
    'title_search' => 'Innovatsion loyihalarni qidirish',
    'search_project' => 'Loyihani topish',
    'banner_title' => 'Portal haqida',
    'banner_text' => "Ilmiy taraqqiyot portali va innovatsion g'oyalar-loyihalar portali 'mijoz-tadqiqotchi-investor' uzluksiz muloqotni ta'minlaydi. Portal barcha manfaatdor tomonlar, shu jumladan investorlar, sanoat va korxonalar innovatsion ishlanmalariga kirish imkonini yaratadi, innovatsion ishlanmalar samaradorligini va samarali tijoratlashuvini ta'minlaydigan sanoat korxonalari va korxonalarning talab muammolarini va ilmiy institutlarning takliflarini shakllantirishga imkon beradi.",

    'fl_search' => 'Barcha loyihalarni izlash',
    'fl_add' => "Loyihani qo'shish",
    'fl_reg' => "Investor sifatida ro'yxatdan o'ting",
    'personal_area' => 'Shaxsiy kabinet',
    'personal_data' => "Shaxsiy ma'lumotlar",
    'last_projects' => 'Oxirgi loyihalar',
    'categories' => 'Kategoriyalar',
    'no_projects' => "Loyihalar yo'q",
    'no_categories' => "Kategoriyalar yo'q'",
    'projects_choice' => 'проект|проекта|проектов',
    'more' => 'Davomi',

    'contacts' => "Bo'glanish",
    'facebook' => 'Facebook',
    'telegram' => 'Telegram',
    'copyright' => 'Barcha huquqlar himoyalangan',
    'qwerty' => 'Saytni tayyorlagan studiya <a href="https://qwerty.uz" class="qwerty_link" target="_blank">QWERTY</a>',
    'filter_category' => 'Sanoatni tanlang',
    'filter_type' => 'Loyiha turi',
    'filter_readiness' => 'Loyiha tayyorligi (%)',
    'filter_payback' => 'Loyihani qaytarish muddati',
    'filter_cost' => 'Umumiy qiymati',
    'filter_need' => 'Kerakli investitsiyalar',
    'filter_patent' => 'Patent',
    'all_patent' => 'Barchasi',
    'with_patent' => 'Patent bilan',
    'without_patent' => 'Patentsiz',
    'patent_number' => 'Nomer',
    'search_filter' => "Kalit so'zni qidirish",
    'from' => 'Boshlab',
    'till' => 'Qadar',
    'mth' => 'Oy.',
    'sum' => 'sum',
    'usd' => '$',
    'yes' => 'ha',
    'no' => "yo'q",
    'reset_filter' => 'Maydonlarni tozalash',
    'apply_filter' => 'Qidiruv loyihasi',
    'additional_filters' => "Qo'shimcha filtrlar",
    'found_on_page' => "Sizning iltimosingiz bo'yicha topildi",
    'invested' => 'Moliyalashtirildi',
    'short_description' => 'Belgilanish maqsadi',
    'category_id' => 'Sanoat',
    'state_readiness' => 'Loyiha tayyorligi',
    'invest' => 'Investitsiya qilish',
    'project_description' => "Loyiha ta'rifi",
    'project_stats' => "Ko'rsatkichlar",
    'project_about_presentation' => 'Hujjatlar',
    'project_additional_info' => "Qo'shimcha ma'lumotlar",
    'download_presentation' => 'Ish rejasini yuklab oling',

    'project_stat_1' => 'Loyiha nomi',
    'project_stat_2' => 'Xorijiy investor',
    'project_stat_3' => 'Mahsulot patenti',
    'project_stat_4' => "Loyiha amalga oshiralayotgan bo'lim",
    'project_stat_5' => 'Loyiha tayyorligi',
    'project_stat_6' => 'Loyiha joyi',
    'project_stat_7' => 'Loyihani qaytarish muddati',
    'project_stat_8' => 'Loyihani ishlab chiqish qiymati',
    'project_stat_9' => 'Kerakli loyiha investitsiyasi',
    'project_stat_10' => "O'z mablag'lari",
    'project_stat_11' => 'Yakuniy mahsulot va dizayn hajmi',
    'project_stat_12' => 'Hozirgi holat',
//    'project_stat_12' => 'Стадия проекта',

    'no_current_sum' => 'Mavjud emas',
    'no_cost_sum' => "Ko'rsatilmagan",
    'na' => "Ko'rsatilmagan",
    'authorization' => 'Avtorizatsiya',
    'reset' => 'Parolni tiklash',
    'restore_password' => 'Parolni tiklash',
    'not_verified_title' => 'Pochtani tekshirish',
    'not_verified_text' => "Hisob yoqilmagan. Siz E-mail manzilingizni tasdiqlamadingiz. Iltimos, ro'yxatdan o'tish paytida ro'yxatdan o'tgan elektron pochta manziliga kiring va faollashtirish xatini toping.",
    'not_verified_notice' => 'Agar xatni topmasangiz, Spam papkasiga qarang yoki xatni qayta yuboring.',
    'resend_verify' => 'Maktubni qayta yuborish',
    'verified' => 'Pochta muvaffaqiyatli tekshirildi',
    'resent_verify' => 'Maktub qayta yuborildi!',
    'registration' => "Ro'yxatga olish",
    'investor' => 'Investor',
    'startup' => 'Dasturlovchi',
    'accept_agreement' => "Ushbu bayroqni belgilab, maxfiylik bayonnomasini o'qib, tushunganingizni tasdiqlaysiz.",
    'registration_notify' => "Ushbu ma'lumot faqat ro'yxatdan o'tgan foydalanuvchilar uchun mavjud va tarqatilmaydi.",
    'my_projects' => 'Mening loyihalarim',
    'my_investments' => 'Mening investitsiyalarim',
    'notifications' => 'Bildirishnomalar',
    'save' => 'Saqlash',
    'profile_notice' => "Ushbu ma'lumot faqat ro'yxatdan o'tgan foydalanuvchilar uchun mavjud.",
    'relogin_startup' => "Chunki siz loyihani qo'sha olmaysiz investor sifatida ruxsat berilgan. Loyihangizni qo'shish uchun tizimdan chiqing, tizimga kiring va ro",
    'relogin_investor' => "Siz loyihani investitsiya qila olmaysiz, chunki Ishlab chiquvchi sifatida tasdiqlangan. Loyihani moliyalash uchun investor sifatida tizimdan chiqing va tizimga kiring.",

    'user_id' => 'Sizning ID',
    'user_locale' => 'Pochtaning tili',
    'logout' => 'chiqish',
    'project' => 'loyiha',
    'projects' => 'Investitsiya loyihasi',
    'project_views' => "Loyihani ko'rish",
    'project_investments' => "Loyiha so'rovlari",
    'all_investors' => 'Barcha investorlar',
    'investors_id' => 'Investorlar Id si',
    'no_investors' => 'Investorlar mavjud emas',
    'id' => 'id',
    'close' => 'Yopish',
    'project_passport' => 'Loyiha passporti',
    'editing_project' => 'Loyihani tahrir qilish qayta tartibga solish bilan yakunlanadi..',
    'my_invested' => 'Summa',
    'your_investment' => 'Sizning investitsiyangiz',
    'became_investor' => "Ushbu loyihaning investori bo'lishni xohlayman",
    'invested_send' => "Ariza qabul qilindi. Yaqin kelajakda siz bilan bog'lanamiz.",
    'invested_not_send' => "Xato ro'y berdi. Keyinroq takrorlang.",
    'not_registered' => "Loyihaga investitsiya qilish, investor sifatida ro'yxatdan o'tish yoki tizimga kirish.",
    'hasInvested' => "Siz ushbu loyihani investitsiya qilish uchun ariza topshirdingiz.",
    'favorites' => 'Sevimlilar',


    'notifications_type' => [
        'project' => [
            'published' => 'Sizning loyihangiz <strong>:project</strong> tasdiqlandi va chop etildi.',
            'blocked' => 'Ваш проект <strong>:project</strong> был не одобрен и заблокирован.',
            'invested' => 'Loyihangizga investitsiyalarni tasdiqlash <strong>:project</strong>.',
            'invested_canceled' => "Loyihangizga investitsiyalarni o'chirib qo'ying<strong>:project</strong>.",
        ],
        'investment' => [
            'ok' => "Loyihaga ko'z tikish <strong>:project</strong> 
tomonidan tasdiqlangan",
            'cancel' => "Loyihaga ko'z tikish <strong>:project</strong> rad etildi",
        ],
    ],


    'theme_filter' => 'Forumdan qidirish',
    'hot_themes' => 'Ommaviy mavzular',
    'theme_add' => "Mavzu qo'shish",
    'theme_edit' => 'Tahrir qilish uchun',
    'theme_edit_notice' => "Tahririyatni takroriy boshqarishga olib keladi. Yoki sharhingiz bilan bir mavzu qo'shishingiz mumkin.",
    'theme_respond' => 'Men yordam berishni xohlayman',
    'theme_respond_notice' => "Kontaktlaringizni qoldiring va biz siz bilan bog'lanamiz.",
    'my_themes' => 'Mening mavzular',
    'other_themes' => 'Boshqa mavzular',
    'add' => "Qo'shish",
    'theme_add_text' => "Hamjamiyatga a'zo bo'ling va avval barcha javoblarni oling.",
    'theme' => 'Mavzu',
    'text' => 'Matn',
    'theme_statuses' => [
        'moderating' => 'Moderatsiyada',
        'published' => 'Qoshildi',
        'blocked' => 'Blokirovka qilindi'
    ],
    'theme_file' => 'Faylni biriktiring',
    'theme_file_attached' => 'Fayl',
    'theme_file_attached_show' => 'Kiritilgan fayl',
    'answers' => 'Javoblar',
    'user_name' => 'FISH',
    'answer' => 'Javoblar',
    'leave_answer' => 'Sizning javobingiz',
    'leave_answer_btn' => 'Javob qaytarish',
    'answers_by_count' => 'ответ|ответа|ответов',
    'name' => 'FISH',
    'phone' => 'Telefon',
    'email' => 'E-mail',
    'comment' => 'Izoh',
    'send' => 'Ilova yuborildi',
    'not_send' => "So'rovni yuborib bo'lmadi. Shaklni tekshiring yoki keyinroq takrorlang.",
    'partners' => 'Hamkorlarimiz',
    'all_partners' => 'Barcha hamkorlar',
    'news' => 'Yangiliklar',
    'all_news' => 'Barcha yangiliklar',
    'themes_count' => "Jami ma'lumotlar bazasi",
    'themes_count_by_count' => 'проблема|проблемы|проблем',
];
