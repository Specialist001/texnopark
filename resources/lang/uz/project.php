﻿<?php

return [
    'project' => 'Loyiha', //current_user|number
    'id' => 'ID', //current_user|number
    'user_id' => 'ID foydalanuvchi', //current_user|number
    'project_category_id' => 'Sanoat',//, в которой реализуется проект', //select category
    'project_type_id' => 'Loyiha turi', //select type
    'logo' => 'Logotip', //file
    'image' => 'Muqova', //file
    'title' => 'Loyiha nomi', //string
    'stage' => 'Loyiha holati', //select (идея, действующая модель, работающий бизнес)
    'developer' => 'Loyiha asoschisi',//string
    'foreign' => 'Иностранный партнер/инвестор', //select да|нет
    'readiness' => 'Loyiha tayyorligi (в %)', //number|0-100
    'hasPatent' => 'Patent mavjud', //string
    'patent' => 'Patent nomeri/patent uchun ariza', //string
    'implementation_period' => 'Loyiha tayyorlov vaqti',//number|months
    'payback_period' => '
Loyihani qaytarish muddati',//number|months
    'cost' => '
Loyihani rivojlantirishning umumiy xarajati',//string
    'need' => '
Investitsiya kerak',//string
    'current' => '
O\'z mablag\'lari',//string
    'workers' => 'Yangi yaratilgan ishlar',//number|peoples
    'presentation' => 'Taqdimot, Biznes reja',//file
    'status' => 'Status',//select (moderating|published|blocked)
    'view' => 'Ko\'rishlar', //--
    'importable' => '
Importni almashtirish / eksportchilik', //--

    'sum' => 'Milliy valyuta',
    'usd' => 'Chet le valyuta',

    'from' => 'dan',
    'till' => 'gacha',

    'yes' => 'Ha',
    'no' => 'Yo\'q',

    'period_unit' => 'Oy',
    'worker_unit' => 'odam',

    'cost_filter' => '
umumiy qiymati',

    'about_developer' => '
O\'zgarishlar / innovatsiyalar',
    'place' => '
Loyiha joyi',//string
    'short_description' => 'Belgilanish maqsadi',
    'description' => '
Loyihaning to\'liq tavsifi',
    'target' => '
Kirish / potentsial iste\'molchi korxonasi',//string
    'finally' => 'Yiliga yakuniy mahsulot va dizayn hajmi',//string
    'state' => '
Hozirgi holat',//string
    'about_presentation' => 'Hujjatlar',
    'additional_info' => 'Qo\'shimcha ma\'lumotlar',

    'place_unit' => 'Shahar, tuman, ko\'cha, uy',
    'finally_unit' => 'Misol uchun, 310 ming tonna yog\'li urug\'lar',
    'statuses' => [
        'moderating' => 'Moderatsiyada',
        'published' => 'hop etildi',
        'blocked' => '
Bloklangan',
    ],
    'investment_statuses' => [
        'moderating' => 'Moderatsiyada',
        'ok' => 'Tasdiqlandi',
        'cancel' => '
Rad etildi',
    ],
    'stages' => [
        'idea' => 'Fikr',
        'model' => '
Joriy model',
        'business' => '
Ish faoliyati',
    ],
    'investment_sum' => 'Miqdor',
    'prod_capacity' => 'Годовая производственная мощность',
    'prod_capacity_unit' => 'Birligi',
    'metal_structures' => 'Металлических конструкций',
    'sandwich_panels' => 'Сэндвич-панели',
    'indoor_area' => 'Площадь крытых помещений',
];
