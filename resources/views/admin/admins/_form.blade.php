<div class="form-group row {!! $errors->first('name', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="name">@lang('admin.name')</label>
    <div class="col-md-6">
        <input type="text" name="name" class="form-control input-sm" id="name" value="{{ old('name', $user->name ?? '') }}" required autofocus>
        {!! $errors->first('name', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
<div class="form-group row {!! $errors->first('username', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="username">@lang('admin.username')</label>
    <div class="col-md-6">
        <input type="text" name="username" class="form-control input-sm" id="username" value="{{ old('username', $user->username ?? '') }}" required>
        {!! $errors->first('username', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('password', 'has-danger')!!}" >
    <label class="col-md-3 text-md-right col-form-label-sm" for="password">@lang('auth.password')</label>
    <div class="col-md-6">
        <input type="password" name="password" class="form-control input-sm password" id="password" {{ isset($user->id) ? '': 'required' }}>
        {!! $errors->first('password', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 text-md-right col-form-label-sm" for="active">@lang('admin.status')</label>
    <div class="col-md-6">
        {{--<select name="active" id="active" class="form-control input-sm">--}}
            {{--<option value="1">@lang('common.active')</option>--}}
            {{--<option value="0">@lang('common.not_active')</option>--}}
        {{--</select>--}}
        <div class="btn-group btn-group-sm" data-toggle="buttons">
            <label class="btn btn-outline-success {{ old('active', $user->active ?? 1) == 1? 'active': '' }}">
                <input type="radio" name="active" value="1" {{ old('active', $user->active ?? 1) == 1? 'checked': '' }} required>
                @lang('admin.active')
            </label>
            <label class="btn btn-outline-danger {{ old('active', $user->active ?? 1) == 0? 'active': '' }}">
                <input type="radio" name="active" value="0" {{ old('active', $user->active ?? 1) == 0? 'checked': '' }} required>
                @lang('admin.not_active')
            </label>
        </div>
    </div>
</div>
{{--<div class="form-group row">--}}
    {{--<label class="col-md-3 text-md-right col-form-label-sm" for="verified">@lang('common.verification')</label>--}}
    {{--<div class="col-md-6">--}}
        {{--<select name="verified" id="verified" class="form-control input-sm">--}}
            {{--<option value="1">@lang('common.verified')</option>--}}
            {{--<option value="0">@lang('common.not_verified')</option>--}}
        {{--</select>--}}
        {{--<div class="btn-group btn-group-sm" data-toggle="buttons">--}}
            {{--<label class="btn btn-outline-success {{ old('verified', $user->hasVerifiedPhone() ? 1: 0) == 1? 'active': '' }}">--}}
                {{--<input type="radio" name="verified" value="1" {{ old('verified', $user->hasVerifiedPhone() ? 1: 0) == 1? 'checked': '' }} required>--}}
                {{--@lang('common.verified')--}}
            {{--</label>--}}
            {{--<label class="btn btn-outline-danger {{ old('verified', $user->hasVerifiedPhone() ? 1: 0) == 0? 'active': '' }}">--}}
                {{--<input type="radio" name="verified" value="0" {{ old('verified', $user->hasVerifiedPhone() ? 1: 0) == 0? 'checked': '' }} required>--}}
                {{--@lang('common.not_verified')--}}
            {{--</label>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

@component('component.password-stacks')@endcomponent
