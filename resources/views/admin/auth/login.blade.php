@extends('common.empty')
@section('content')
    <div class="cat__pages__login" style="background-image: url('{{ asset('images/login_bg.jpg') }}');">
        {{--<div class="cat__pages__login__header">--}}
        {{--<div class="row">--}}
        {{--<div class="col-lg-8">--}}
        {{--<div class="cat__pages__login__header__logo">--}}
        {{--<a href="/">--}}
        {{--<img src="{{ asset('images/logo.png') }}" alt="{{ config('app.name') }}">--}}
        {{--</a>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-lg-4">--}}
        {{--<div class="cat__pages__login__header__menu">--}}
        {{--<ul class="list-unstyled list-inline">--}}
        {{--<li class="list-inline-item"><a href="javascript: history.back();">← Back</a></li>--}}
        {{--</ul>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        <div class="cat__pages__login__block">
            <div class="row">
                <div class="col-xl-12">
                    <div class="cat__pages__login__block__promo text-white text-center">
                        <h1 class="mb-3">
                            <span>@lang('admin.login_title', ['appName' => config('app.name')])</span>
                        </h1>
                        <p>@lang('admin.login_text')</p>
                    </div>
                    <div class="cat__pages__login__block__inner">
                        <div class="cat__pages__login__block__form">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="text-uppercase">
                                        <span>@lang('auth.login')</span>
                                    </h4></div>
                                {{--<div class="col-md-6">--}}
                                    {{--<div class="float-right d-inline-block">--}}
                                        {{--@include('common.lang-dropdown')--}}
                                    {{--</div>--}}
                                    {{--<div class="clearfix"></div>--}}
                                {{--</div>--}}
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    @if (session()->has('success'))
                                        @component('component.alert', ['type' => 'success'])
                                            {{ session('success') }}
                                        @endcomponent
                                    @endif
                                    @if (session()->has('warning'))
                                        @component('component.alert', ['type' => 'warning'])
                                            {{ session('warning') }}
                                        @endcomponent
                                    @endif
                                    @if (count($errors) > 0 || session()->has('danger'))
                                        @component('component.alert', ['type' => 'danger'])
                                            <div>
                                                {{ session('danger') }}
                                            </div>
                                            @foreach ($errors->all() as $error)
                                                <div>{{ $error }}</div>
                                            @endforeach
                                        @endcomponent
                                    @endif
                                </div>
                            </div>
                            <form id="form-validation" name="form-validation" action="{{ route('admin.auth.login') }}"
                                  method="POST">
                                @csrf
                                <div class="form-group">
                                    <label class="form-label" for="username">@lang('auth.username')</label>
                                    <input id="username" class="form-control" autofocus required value="{{ old('username', '') }}"
                                           name="username" type="text">
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="password">@lang('auth.password')</label>
                                    <div class="input-append input-group"><input
                                            class="form-control password"
                                            name="password"  required
                                            id="password"
                                            type="password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="remember">
                                                    @lang('auth.remember')
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-primary mr-3">@lang('auth.enter')</button>
                                </div>
                                {{--<div class="form-group">--}}
                                {{--<span>--}}
                                {{--Use another service to Log In--}}
                                {{--</span>--}}
                                {{--<div class="mt-2">--}}
                                {{--<a href="javascript: void(0);" class="btn btn-icon">--}}
                                {{--<i class="icmn-facebook"></i>--}}
                                {{--</a>--}}
                                {{--<a href="javascript: void(0);" class="btn btn-icon">--}}
                                {{--<i class="icmn-google"></i>--}}
                                {{--</a>--}}
                                {{--<a href="javascript: void(0);" class="btn btn-icon">--}}
                                {{--<i class="icmn-windows"></i>--}}
                                {{--</a>--}}
                                {{--<a href="javascript: void(0);" class="btn btn-icon">--}}
                                {{--<i class="icmn-twitter"></i>--}}
                                {{--</a>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--<div class="cat__pages__login__footer text-center">--}}
        {{--<ul class="list-unstyled list-inline">--}}
        {{--<li class="list-inline-item"><a href="javascript: void(0);">Terms of Use</a></li>--}}
        {{--<li class="active list-inline-item"><a href="javascript: void(0);">Compliance</a></li>--}}
        {{--<li class="list-inline-item"><a href="javascript: void(0);">Confidential Information</a></li>--}}
        {{--<li class="list-inline-item"><a href="javascript: void(0);">Support</a></li>--}}
        {{--<li class="list-inline-item"><a href="javascript: void(0);">Contacts</a></li>--}}
        {{--</ul>--}}
        {{--</div>--}}
    </div>
@endsection
@component('component.password-stacks')@endcomponent
