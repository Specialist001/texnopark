@extends('admin.layout')

@section('center_content')
    <form action="{{ route('admin.cities.update', $city) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        @component('component.card', ['title' => trans('admin.nav.cities').': '.trans('admin.editing')])
            @include('admin.cities._form')
            @slot('bottom')
                <a href="{{ route('admin.cities.index') }}" class="btn btn-sm btn-danger float-left">@lang('admin.back')</a>
                <button class="btn btn-sm btn-primary float-right">@lang('admin.save')</button>
                <div class="clearfix"></div>
            @endslot
        @endcomponent
    </form>
@endsection
