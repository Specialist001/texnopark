@extends('admin.layout')

@section('center_content')
    <form action="{{ route('admin.contacts.update') }}" method="post">
        @csrf
        @method('put')
        @component('component.card', ['title' => trans('admin.nav.contacts')])
            @include('admin.contacts._form')
            @slot('bottom')
                    <button class="btn btn-sm btn-primary ">@lang('admin.save')</button>
            @endslot
        @endcomponent
    </form>
@endsection
