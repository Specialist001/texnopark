<div class="form-group row {!! $errors->first('organization_title', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="organization_title">@lang('forum.organization_title')</label>
    <div class="col-md-6">
        <input type="text"   name="organization_title" class="form-control input-sm" id="organization_title" value="{{ old('organization_title', $theme->organization_title ?? '') }}" >
        {!! $errors->first('organization_title', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
<div class="form-group row {!! $errors->first('title', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="title">@lang('forum.title')</label>
    <div class="col-md-6">
        <input type="text"   name="title" class="form-control input-sm" id="title" value="{{ old('title', $theme->title ?? '') }}" required >
        {!! $errors->first('title', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('text', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="text">@lang('forum.text')</label>
    <div class="col-md-6">
        <textarea name="text" id="text" required class="form-control input-sm">{{ old('text', $theme->text ?? '') }}</textarea>
        {!! $errors->first('text', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
<div class="form-group row {!! $errors->first('category', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="category">@lang('forum.category')</label>
    <div class="col-md-6">
        <input type="text"   name="category" class="form-control input-sm" id="category" value="{{ old('category', $theme->category ?? '') }}" >
        {!! $errors->first('category', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('required_solution', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="required_solution">@lang('forum.required_solution')</label>
    <div class="col-md-6">
        <textarea name="required_solution" id="required_solution" required class="form-control input-sm">{{ old('required_solution', $theme->required_solution ?? '') }}</textarea>
        {!! $errors->first('required_solution', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>


<div class="form-group row {!! $errors->first('expected_effect', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="expected_effect">@lang('forum.expected_effect')</label>
    <div class="col-md-6">
        <textarea name="expected_effect" id="expected_effect" required class="form-control input-sm">{{ old('expected_effect', $theme->expected_effect ?? '') }}</textarea>
        {!! $errors->first('expected_effect', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
<div class="form-group row {!! $errors->first('required_organization', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="required_organization">@lang('forum.required_organization')</label>
    <div class="col-md-6">
        <input type="text"   name="required_organization" class="form-control input-sm" id="required_organization" value="{{ old('required_organization', $theme->required_organization ?? '') }}" >
        {!! $errors->first('required_organization', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('contacts', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="contacts">@lang('forum.contacts')</label>
    <div class="col-md-6">
        <textarea name="contacts" id="contacts" required class="form-control input-sm">{{ old('contacts', $theme->contacts ?? '') }}</textarea>
        {!! $errors->first('contacts', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('file', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="file">
        @if(isset($theme)) @lang('admin.theme_file_attached') @else @lang('admin.theme_file') @endif
    </label>
    <div class="col-md-6">
        @if(isset($theme) && $theme->file)
            <p id="file_attached">
                <a href="{{$theme->fileUrl()}}" target="_blank">@lang('admin.file')</a>
                <br>
                <small id="delete_file" class="text-danger cur-pointer" ><i class="icmn-cross"></i> @lang('admin.destroy')</small>
            </p>
        @endif
        <input type="file" name="file" class="form-control input-sm" id="file" >
        {!! $errors->first('file', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('status', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="status">@lang('project.status')</label>
    <div class="col-md-6">
        <select name="status" id="status" class="form-control input-sm" required>
            @foreach(\App\Domain\Forum\Models\ForumTheme::statuses() as $status)
                <option value="{{$status}}" {{ (old('status', $theme->status ?? '') == $status) ? 'selected': ''}}>@lang('admin.theme_statuses.'.$status)</option>
            @endforeach
        </select>
        {!! $errors->first('status', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
