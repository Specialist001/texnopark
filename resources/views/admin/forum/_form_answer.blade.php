<div class="form-group row {!! $errors->first('text', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="text">@lang('admin.text')</label>
    <div class="col-md-6">
        <textarea name="text" id="text" required class="form-control input-sm">{{ old('text', $answer->text ?? '') }}</textarea>
        {!! $errors->first('text', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('file', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="file">
        @if(isset($answer)) @lang('admin.theme_file_attached') @else @lang('admin.theme_file') @endif
    </label>
    <div class="col-md-6">
        @if(isset($answer) && $answer->file)
            <p id="file_attached">
                <a href="{{$answer->fileUrl()}}" target="_blank">@lang('admin.file')</a>
                <br>
                <small id="delete_file" class="text-danger cur-pointer" ><i class="icmn-cross"></i> @lang('admin.destroy')</small>
            </p>
        @endif
        <input type="file" name="file" class="form-control input-sm" id="file" >
        {!! $errors->first('file', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
