@extends('admin.layout')

@section('center_content')

    <form action="{{ route('admin.forum.update', $theme) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        @component('component.card', ['title' => trans('admin.nav.forum').': '.trans('admin.editing')])
            @include('admin.forum._form')
            @slot('bottom')
                <a href="{{ route('admin.forum.index') }}" class="btn btn-sm btn-danger float-left">@lang('admin.back')</a>
                <button class="btn btn-sm btn-primary float-right">@lang('admin.save')</button>
                <div class="clearfix"></div>
            @endslot
        @endcomponent
    </form>

    <form action="{{ route('admin.forum.answer', $theme) }}" method="post" enctype="multipart/form-data">
        @csrf
        @component('component.card', ['title' => trans('admin.answers'), 'bodyClass' => 'card-body-no-padding'])
            <table class="table">
                @foreach($answers as $ans)
                    <tr>
                        <td class="p-4 {{ $ans->is_moderated == 0 ? 'new_row': '' }}">
                            <div class="theme_user_name">
                                {{ $ans->user_id ? $ans->user->name: $ans->user_name }}
                                <br>
                                <span>
                            {{ $ans->created_at->format('d.m.Y, H:i') }}
                        </span>
                            </div>
                            <div class="answer_text p-4 ml-4 mb-2">
                                {!! nl2br($ans->text) !!}
                            </div>
                            @if($ans->file)
                                <div class=" ml-4 mb-2">
                                    <i class="icmn-file-empty text-success"></i> <a href="{{$ans->fileUrl()}}" target="_blank">@lang('admin.theme_file_attached')</a>
                                </div>
                            @endif

                            <div class="">
                                <a href="{{ route('admin.forum.answer.edit', $ans) }}" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="@lang('admin.edit')">
                                    <i class="icmn-pencil"></i>
                                </a>
                                <a href="{{ route('admin.forum.answer.destroy', $ans) }}" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="@lang('admin.destroy')">
                                    <i class="icmn-cross"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>

        <div class="">

        </div>
            <div class=" row ">
                <div class="col-md-3 text-md-right " ><h4 class=" mt-5 mb-3 ">@lang('admin.leave_answer')</h4></div>
            </div>

            @include('admin.forum._form_answer')

            <div class=" row ">
                <div class="offset-md-3 col-md-6 " >
                    <button class="btn btn-primary btn-sm d-inline-block mb-3" type="submit">
                        @lang('admin.leave_answer_btn')
                    </button>
                </div>
            </div>

            @slot('bottom')
                @include('common.pagination', ['data' => $answers])
            @endslot
        @endcomponent
    </form>
@endsection

@push('scripts')
    <script>
        $(function () {
            $('#delete_file').on('click', function () {
                $.ajax({
                    method: "DELETE",
                    url: "{{ route('admin.forum.destroy.file') }}/{{$theme->id}}",
                    success: function(response) {
                        if(response.result === "success") {
                            $('#file_attached').remove();
                            return false;
                        } else {
                            alert("{{trans('admin.ajax_error')}}");
                        }
                    },
                    error: function(response) {
                        alert("{{trans('admin.ajax_error')}}");
                    }
                });
            });
        });
    </script>
@endpush
