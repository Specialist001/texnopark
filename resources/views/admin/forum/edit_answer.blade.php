@extends('admin.layout')

@section('center_content')

    <form action="{{ route('admin.forum.answer.update', $answer) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        @component('component.card', ['title' => trans('admin.answer')])
            @include('admin.forum._form_answer')
            @slot('bottom')
                <a href="{{ route('admin.forum.edit', $answer->forum_theme_id) }}" class="btn btn-sm btn-danger float-left">@lang('admin.back')</a>
                <button class="btn btn-sm btn-primary float-right">@lang('admin.save')</button>
                <div class="clearfix"></div>
            @endslot
        @endcomponent
    </form>
@endsection

@push('scripts')
    <script>
        $(function () {
            $('#delete_file').on('click', function () {
                $.ajax({
                    method: "DELETE",
                    url: "{{ route('admin.forum.answer.destroy.file') }}/{{$answer->id}}",
                    success: function(response) {
                        if(response.result === "success") {
                            $('#file_attached').remove();
                            return false;
                        } else {
                            alert("{{trans('admin.ajax_error')}}");
                        }
                    },
                    error: function(response) {
                        alert("{{trans('admin.ajax_error')}}");
                    }
                });
            });
        });
    </script>
@endpush
