@extends('admin.layout')

@section('center_content')
    @component('component.card', ['title' => trans('admin.nav.forum'), 'bodyClass' => 'card-body-no-padding'])
        @slot('buttons')
            <a href="{{ route('admin.forum.create') }}" class="btn btn-sm btn-primary ml-2">
                <span class="d-none d-sm-inline-block">@lang('admin.create')</span> <i class="icmn-plus"><!-- --></i>
            </a>
        @endslot

        <div id="filters">
            <form action="{{ route('admin.forum.index') }}" method="get">
                <div class="row">
                    <div class="col-sm-6 col-md-3">
                        <div class="form-group">
                            <label for="title">
                                <small>@lang('admin.theme_filter')</small>
                            </label>
                            <input class="form-control input-sm"  name="title" id="title" type="text"   value="{{ $filters['title'] ?? '' }}"/>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <div class="form-group">
                            <label for="status">
                                <small>@lang('project.status')</small>
                            </label>
                            <select name="status" id="status" class="form-control input-sm">
                                <option value=""></option>
                                @foreach(\App\Domain\Forum\Models\ForumTheme::statuses() as $status)
                                    <option value="{{$status}}" {{ (isset($filters['status']) && $filters['status'] == $status) ? 'selected': ''}}>@lang('admin.theme_statuses.'.$status)</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="float-right">
                            <a href="{{ route('admin.forum.index') }}" class="btn btn-sm btn-danger">@lang('admin.filters_reset')</a>
                            <button class="btn btn-sm btn-success">@lang('admin.filters_apply')</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </form>
        </div>



        @if($themes->isNotEmpty())
            <div class="table-responsive">
                <table class="table">
                    <thead class="thead-default">
                    <tr>
                        <th>@lang('admin.theme')</th>
                        <th>@lang('admin.status')</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($themes as $theme)
                        <tr>
                            <td class="{{ $theme->is_moderated == 0 || $theme->new_answers_count > 0 ? 'new_row': '' }}">
                                <div class="theme_title">
                                    {{ $theme->title }}
                                </div>
                                <div class="theme_user_name">
                                    {{ $theme->organization_title ?? '' }}
                                </div>
                                <div class="theme_user_name">
                                    {{ $theme->user->name }}
                                    <span>
                                        {{ $theme->created_at->format('d.m.Y, H:i') }}
                                    </span>
                                </div>
                                <div class="theme_short">
                                    @if(mb_strlen($theme->text) > 50)
                                        {{ mb_substr($theme->text, 0, 50).'...' }}
                                    @else
                                        {{ $theme->text }}
                                    @endif
                                </div>
                                <div class="theme_icons">
                                    <i class="icmn-bubble text-success"></i> {{ $theme->answers_count }} | <i class="icmn-plus text-success"></i> {{ $theme->new_answers_count }} / <i class="icmn-eye text-success"></i> {{ $theme->view }}
                                    @if($theme->file)
                                          / <i class="icmn-file-empty text-success"></i> <a href="{{$theme->fileUrl()}}" target="_blank">@lang('admin.theme_file_attached')</a>
                                    @endif
                                </div>
                            </td>
                            <td>@lang('admin.theme_statuses.'.$theme->status)</td>
                            <td>
                                <a href="{{ route('admin.forum.edit', $theme) }}" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="@lang('admin.edit')">
                                    <i class="icmn-pencil"></i>
                                </a>
                                <form action="{{ route('admin.forum.destroy', $theme) }}" class="d-inline-block" method="post">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-sm btn-danger"
                                            onclick="return confirm('@lang('admin.destroy_foreign_confirm')');"
                                            data-toggle="tooltip" data-placement="top" title="@lang('admin.destroy')">
                                        <i class="icmn-cross"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif

        @slot('bottom')
            @include('common.pagination', ['data' => $themes])
        @endslot
    @endcomponent
@endsection
