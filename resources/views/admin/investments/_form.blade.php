<div class="form-group row {!! $errors->first('user_id', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="user_id">@lang('project.user_id')</label>
    <div class="col-md-6">
        <input type="number" step="1" min="1" name="user_id" class="form-control input-sm" id="user_id" value="{{ old('user_id', $investment->user_id ?? '') }}" required autofocus>
        {!! $errors->first('user_id', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('project_id', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="project_id">@lang('project.id')</label>
    <div class="col-md-6">
        <input type="number" step="1" min="1" name="project_id" class="form-control input-sm" id="project_id" value="{{ old('project_id', $investment->project_id ?? '') }}" required >
        {!! $errors->first('project_id', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('sum', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="sum">@lang('project.investment_sum')</label>
    <div class="col-md-6">
        <input type="number" step="1" min="1" name="sum" class="form-control input-sm" id="sum" value="{{ old('sum', $investment->sum ?? '') }}" required >
        {!! $errors->first('sum', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('status', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="status">@lang('project.status')</label>
    <div class="col-md-6">
        <select name="status" id="status" class="form-control input-sm" required>
            @foreach(\App\Domain\Investments\Models\Investment::statuses() as $status)
                <option value="{{$status}}" {{ (old('status', $investment->status ?? '') == $status) ? 'selected': ''}}>@lang('project.investment_statuses.'.$status)</option>
            @endforeach
        </select>
        {!! $errors->first('status', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

