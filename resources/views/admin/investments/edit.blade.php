@extends('admin.layout')

@section('center_content')

    <section class="relations mb-3">
        <a href="{{ route('admin.projects.edit', $investment->project_id) }}" class="btn btn-sm btn-primary mr-2">@lang('admin.investment_project')</a>
        <a href="{{ route('admin.investors.edit', ['id' => $investment->user_id]) }}" class="btn btn-sm btn-primary mr-2">@lang('admin.investment_user')</a>
    </section>

    <form action="{{ route('admin.investments.update', $investment) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        @component('component.card', ['title' => trans('admin.nav.investments').': '.trans('admin.editing')])
            @include('admin.investments._form')
            @slot('bottom')
                <a href="{{ route('admin.investments.index') }}" class="btn btn-sm btn-danger float-left">@lang('admin.back')</a>
                <button class="btn btn-sm btn-primary float-right">@lang('admin.save')</button>
                <div class="clearfix"></div>
            @endslot
        @endcomponent
    </form>
@endsection
