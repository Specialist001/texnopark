@extends('admin.layout')

@section('center_content')
    @component('component.card', ['title' => trans('admin.nav.investments'), 'bodyClass' => 'card-body-no-padding'])
        @slot('buttons')
            <a href="{{ route('admin.investments.create') }}" class="btn btn-sm btn-primary ml-2">
                <span class="d-none d-sm-inline-block">@lang('admin.create')</span> <i class="icmn-plus"><!-- --></i>
            </a>
        @endslot

        <div id="filters">
            <form action="{{ route('admin.investments.index') }}" method="get">
                <div class="row">
                    <div class="col-md-12">
                        <p>
                            <strong>@lang('admin.filters')</strong>
                        </p>
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <div class="form-group">
                            <label for="id">
                                <small>@lang('admin.id')</small>
                            </label>
                            <input class="form-control input-sm" name="id" id="id" type="number" step="1"  value="{{ $filters['id'] ?? '' }}"/>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <div class="form-group">
                            <label for="user_id">
                                <small>@lang('admin.user_id')</small>
                            </label>
                            <input class="form-control input-sm" name="user_id" id="user_id" type="number" step="1" value="{{ $filters['user_id'] ?? '' }}"/>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <div class="form-group">
                            <label for="project_id">
                                <small>@lang('project.id')</small>
                            </label>
                            <input class="form-control input-sm" name="project_id" id="project_id" type="number" step="1" value="{{ $filters['project_id'] ?? '' }}"/>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <div class="form-group">
                            <label for="sum">
                                <small>@lang('project.investment_sum')</small>
                            </label>
                            <input class="form-control input-sm" name="sum" id="sum" type="number" step="1"  value="{{ $filters['sum'] ?? '' }}"/>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label for="status">
                                <small>@lang('project.status')</small>
                            </label>
                            <select name="status" id="status" class="form-control input-sm">
                                <option value=""></option>
                                @foreach(\App\Domain\Investments\Models\Investment::statuses() as $status)
                                    <option value="{{$status}}" {{ (isset($filters['status']) && $filters['status'] == $status) ? 'selected': ''}}>@lang('project.investment_statuses.'.$status)</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label for="sort">
                                <small>@lang('admin.sort')</small>
                            </label>
                            <select name="sort" id="sort" class="form-control input-sm">
                                <option value=""></option>
                                <option value="id" {{ (isset($filters['sort']) && $filters['sort'] == 'id') ? 'selected': ''}}>@lang('admin.id') (@lang('admin.asc'))</option>
                                <option value="-id" {{ (isset($filters['sort']) && $filters['sort'] == '-id') ? 'selected': ''}}>@lang('admin.id') (@lang('admin.desc'))</option>

                                <option value="project_id" {{ (isset($filters['sort']) && $filters['sort'] == 'project_id') ? 'selected': ''}}>@lang('project.id') (@lang('admin.asc'))</option>
                                <option value="-project_id" {{ (isset($filters['sort']) && $filters['sort'] == '-project_id') ? 'selected': ''}}>@lang('project.id') (@lang('admin.desc'))</option>

                                <option value="sum" {{ (isset($filters['sort']) && $filters['sort'] == 'sum') ? 'selected': ''}}>@lang('project.investment_sum') (@lang('admin.asc'))</option>
                                <option value="-sum" {{ (isset($filters['sort']) && $filters['sort'] == '-sum') ? 'selected': ''}}>@lang('project.investment_sum') (@lang('admin.desc'))</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="float-right">
                            <a href="{{ route('admin.investments.index') }}" class="btn btn-sm btn-danger">@lang('admin.filters_reset')</a>
                            <button class="btn btn-sm btn-success">@lang('admin.filters_apply')</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </form>
        </div>

        @if($investments->isNotEmpty())
            <div class="table-responsive">
                <table class="table">
                    <thead class="thead-default">
                    <tr>
                        <th>@lang('admin.id')</th>
                        <th>@lang('project.id')</th>
                        <th>@lang('admin.project')</th>
                        <th>@lang('project.cost_filter')</th>
                        <th>@lang('project.investment_sum')</th>
                        <th>@lang('project.readiness')</th>
                        <th>@lang('project.status')</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($investments as $investment)
                        <tr>
                            <td class="{{ $investment->is_moderated == 0 ? 'new_row': '' }}">{{ $investment->id }}</td>
                            <td>{{ $investment->project_id }}</td>
                            <td>
                                @lang('project.title'): <strong>{{ $investment->project->title }}</strong>
                                <br>
                                @lang('project.project_category_id'): <strong>{{ $investment->project->category->name }}</strong>
                                <br>
                                @lang('project.project_type_id'): <strong>{{ $investment->project->type->name }}</strong>
                                <br>
                            </td>
                            <td>{{ $investment->project->sum_cost }}</td>
                            <td>{{ $investment->sum }}</td>
                            <td>{{ $investment->project->readiness }}</td>
                            <td>@lang('project.investment_statuses.'.$investment->status)</td>
                            <td>
                                <a href="{{ route('admin.investments.edit', $investment) }}" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="@lang('admin.edit')">
                                    <i class="icmn-pencil"></i>
                                </a>
                                <form action="{{ route('admin.investments.destroy', $investment) }}" class="d-inline-block" method="post">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-sm btn-danger"
                                            onclick="return confirm('@lang('admin.destroy_confirm')');"
                                            data-toggle="tooltip" data-placement="top" title="@lang('admin.destroy')">
                                        <i class="icmn-cross"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif

        @slot('bottom')
            @include('common.pagination', ['data' => $investments])
        @endslot
    @endcomponent
@endsection
