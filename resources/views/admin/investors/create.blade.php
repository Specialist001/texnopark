@extends('admin.layout')

@section('center_content')
    <form action="{{ route('admin.investors.store') }}" method="post">
        @csrf
        @component('component.card', ['title' => trans('admin.nav.investors').': '.trans('admin.creating')])
            @include('admin.investors._form')
            @slot('bottom')
                <a href="{{ route('admin.investors.index') }}" class="btn btn-sm btn-danger float-left">@lang('admin.back')</a>
                <button class="btn btn-sm btn-primary float-right">@lang('admin.create')</button>
                <div class="clearfix"></div>
            @endslot
        @endcomponent
    </form>
@endsection
