@extends('admin.layout')

@section('center_content')

    <section class="relations mb-3">
        <a href="{{ route('admin.investments.index', ['user_id' => $user->id]) }}" class="btn btn-sm btn-primary mr-2">@lang('admin.user_investments')</a>
    </section>

    <form action="{{ route('admin.investors.update', $user) }}" method="post">
        @csrf
        @method('put')
        @component('component.card', ['title' => trans('admin.nav.investors').': '.trans('admin.editing')])
            @include('admin.investors._form')
            @slot('bottom')
                <a href="{{ route('admin.investors.index') }}" class="btn btn-sm btn-danger float-left">@lang('admin.back')</a>
                <button class="btn btn-sm btn-primary float-right">@lang('admin.save')</button>
                <div class="clearfix"></div>
            @endslot
        @endcomponent
    </form>
@endsection
