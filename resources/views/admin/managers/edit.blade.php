@extends('admin.layout')

@section('center_content')
    <form action="{{ route('admin.managers.update', $user) }}" method="post">
        @csrf
        @method('put')
        @component('component.card', ['title' => trans('admin.nav.managers').': '.trans('admin.editing')])
            @include('admin.managers._form')
            @slot('bottom')
                <a href="{{ route('admin.managers.index') }}" class="btn btn-sm btn-danger float-left">@lang('admin.back')</a>
                <button class="btn btn-sm btn-primary float-right">@lang('admin.save')</button>
                <div class="clearfix"></div>
            @endslot
        @endcomponent
    </form>
@endsection
