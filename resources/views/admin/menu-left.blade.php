@extends('common.menu-left')

@section('menu-left-items')

        {{--<!-- START: menu-left item -->--}}
        <li class="cat__menu-left__item {{ $current_route_name == ''? 'cat__menu-left__item--active': '' }}">
            <a href="{{ route('admin.home') }}">
                <span class="cat__menu-left__icon icmn-home "></span>
                @lang('admin.nav.home')
            </a>
        </li>
        @if(\Request::user()->isManager() || \Request::user()->isAdmin())
{{--            <li class="cat__menu-left__item cat__menu-left__submenu {{ ($current_route_name == 'projects' || $current_route_name == 'investments')? 'cat__menu-left__item--active cat__menu-left__submenu--toggled': '' }}">--}}
{{--                <a href="#" onclick="return false;">--}}
{{--                    <span class="cat__menu-left__icon icmn-database"></span>--}}
{{--                    @lang('admin.nav.portal') {!! $new? '<span class="badge badge-success">'.$new.'</span>' : ''  !!}--}}
{{--                </a>--}}
{{--                <ul class="cat__menu-left__list" style="{{ ($current_route_name == 'projects' || $current_route_name == 'investments')? 'display: block;': '' }}">--}}
{{--                    <li class="cat__menu-left__item {{ $current_route_name == 'projects'? 'cat__menu-left__item--active': '' }}">--}}
{{--                        <a href="{{ route('admin.projects.index') }}">--}}
{{--                            <span class="cat__menu-left__icon icmn-database "></span>--}}
{{--                            @lang('admin.nav.projects') {!! $new_projects? '<span class="badge badge-success">'.$new_projects.'</span>' : ''  !!}--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="cat__menu-left__item {{ $current_route_name == 'investments'? 'cat__menu-left__item--active': '' }}">--}}
{{--                        <a href="{{ route('admin.investments.index') }}">--}}
{{--                            <span class="cat__menu-left__icon icmn-coin-dollar "></span>--}}
{{--                            @lang('admin.nav.investments') {!! $new_investments? '<span class="badge badge-success">'.$new_investments.'</span>' : ''  !!}--}}
{{--                        </a>--}}
{{--                    </li>--}}

{{--                </ul>--}}
{{--            </li>--}}
            <li class="cat__menu-left__item {{ $current_route_name == 'projects'? 'cat__menu-left__item--active': '' }}">
                <a href="{{ route('admin.projects.index') }}">
                    <span class="cat__menu-left__icon icmn-database "></span>
                    @lang('admin.nav.projects') {!! $new_projects? '<span class="badge badge-success">'.$new_projects.'</span>' : ''  !!}
                </a>
            </li>

<li class="cat__menu-left__item cat__menu-left__submenu {{ ($current_route_name == 'vacancy' || $current_route_name == 'vacancy-responds')? 'cat__menu-left__item--active cat__menu-left__submenu--toggled': '' }}">
    <a href="#" onclick="return false;">
        <span class="cat__menu-left__icon icmn-office"></span>
        @lang('admin.nav.vacancy')
    </a>
    <ul class="cat__menu-left__list" style="{{ ($current_route_name == 'vacancy' || $current_route_name == 'vacancy-responds')? 'display: block;': '' }}">

        <li class="cat__menu-left__item {{ $current_route_name == 'vacancy'? 'cat__menu-left__item--active': '' }}">
            <a href="{{ route('admin.vacancy.index') }}">
                <span class="cat__menu-left__icon icmn-office "></span>
                @lang('admin.nav.vacancy')
            </a>
        </li>
        <li class="cat__menu-left__item {{ $current_route_name == 'vacancy-responds'? 'cat__menu-left__item--active': '' }}">
            <a href="{{ route('admin.vacancy-responds.index') }}">
                <span class="cat__menu-left__icon icmn-user-check "></span>
                @lang('admin.nav.vacancy-responds')
            </a>
        </li>
    </ul>
</li>
        @endif
        @if(\Request::user()->isProof() || \Request::user()->isAdmin())

{{--            <li class="cat__menu-left__item {{ $current_route_name == 'proof-readings'? 'cat__menu-left__item--active': '' }}">--}}
{{--                <a href="{{ route('admin.proof-readings.index') }}">--}}
{{--                    <span class="cat__menu-left__icon icmn-file-word "></span>--}}
{{--                    @lang('admin.nav.proof_readings') {!! $new_proofs ? '<span class="badge badge-success">'.$new_proofs.'</span>' : ''  !!}--}}
{{--                </a>--}}
{{--            </li>--}}
        @endif

        @if(\Request::user()->isAdmin())


            <li class="cat__menu-left__item cat__menu-left__submenu {{ ($current_route_name == 'admins' || $current_route_name == 'managers' || $current_route_name == 'proofs' || $current_route_name == 'startups' || $current_route_name == 'investors')? 'cat__menu-left__item--active cat__menu-left__submenu--toggled': '' }}">
                <a href="#" onclick="return false;">
                    <span class="cat__menu-left__icon icmn-users"></span>
                    @lang('admin.nav.users')
                </a>
                <ul class="cat__menu-left__list" style="{{ ($current_route_name == 'admins' || $current_route_name == 'managers' || $current_route_name == 'proofs' || $current_route_name == 'startups' || $current_route_name == 'investors')? 'display: block;': '' }}">

                    <li class="cat__menu-left__item {{ $current_route_name == 'admins'? 'cat__menu-left__item--active': '' }}">
                        <a href="{{ route('admin.admins.index') }}">
                            <span class="cat__menu-left__icon icmn-user-tie "></span>
                            @lang('admin.nav.admins')
                        </a>
                    </li>
{{--                    <li class="cat__menu-left__item {{ $current_route_name == 'managers'? 'cat__menu-left__item--active': '' }}">--}}
{{--                        <a href="{{ route('admin.managers.index') }}">--}}
{{--                            <span class="cat__menu-left__icon icmn-user-check "></span>--}}
{{--                            @lang('admin.nav.managers')--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="cat__menu-left__item {{ $current_route_name == 'proofs'? 'cat__menu-left__item--active': '' }}">--}}
{{--                        <a href="{{ route('admin.proofs.index') }}">--}}
{{--                            <span class="cat__menu-left__icon icmn-user-check "></span>--}}
{{--                            @lang('admin.nav.proofs')--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="cat__menu-left__item {{ $current_route_name == 'investors'? 'cat__menu-left__item--active': '' }}">--}}
{{--                        <a href="{{ route('admin.investors.index') }}">--}}
{{--                            <span class="cat__menu-left__icon icmn-user "></span>--}}
{{--                            @lang('admin.nav.investors')--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="cat__menu-left__item {{ $current_route_name == 'startups'? 'cat__menu-left__item--active': '' }}">--}}
{{--                        <a href="{{ route('admin.startups.index') }}">--}}
{{--                            <span class="cat__menu-left__icon icmn-user "></span>--}}
{{--                            @lang('admin.nav.startups')--}}
{{--                        </a>--}}
{{--                    </li>--}}

                </ul>
            </li>

{{--            <li class="cat__menu-left__item cat__menu-left__submenu {{ ($current_route_name == 'page-categories' || $current_route_name == 'pages')? 'cat__menu-left__item--active cat__menu-left__submenu--toggled': '' }}">--}}
{{--                <a href="#" onclick="return false;">--}}
{{--                    <span class="cat__menu-left__icon icmn-files-empty"></span>--}}
{{--                    @lang('admin.nav.pages')--}}
{{--                </a>--}}
{{--                <ul class="cat__menu-left__list" style="{{ ($current_route_name == 'page-categories' || $current_route_name == 'pages')? 'display: block;': '' }}">--}}

{{--                    <li class="cat__menu-left__item {{ $current_route_name == 'page-categories'? 'cat__menu-left__item--active': '' }}">--}}
{{--                        <a href="{{ route('admin.page-categories.index') }}">--}}
{{--                            <span class="cat__menu-left__icon icmn-menu "></span>--}}
{{--                            @lang('admin.nav.page-categories')--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="cat__menu-left__item {{ $current_route_name == 'pages'? 'cat__menu-left__item--active': '' }}">--}}
{{--                        <a href="{{ route('admin.pages.index') }}">--}}
{{--                            <span class="cat__menu-left__icon icmn-profile "></span>--}}
{{--                            @lang('admin.nav.static-page')--}}
{{--                        </a>--}}
{{--                    </li>--}}

{{--                </ul>--}}
{{--            </li>--}}

            <li class="cat__menu-left__item {{ $current_route_name == 'news'? 'cat__menu-left__item--active': '' }}">
                <a href="{{ route('admin.news.index') }}">
                    <span class="cat__menu-left__icon icmn-newspaper "></span>
                    @lang('admin.nav.news')
                </a>
            </li>

            <li class="cat__menu-left__item {{ $current_route_name == 'responds' ? 'cat__menu-left__item--active': '' }}">
                <a href="{{ route('admin.responds.index') }}">
                    <span class="cat__menu-left__icon icmn-bell "></span>
                    @lang('admin.nav.responds') {!! $new_responds? '<span class="badge badge-success">'.$new_responds.'</span>' : ''  !!}
                </a>
            </li>

            <li class="cat__menu-left__item {{ $current_route_name == 'subscribers' ? 'cat__menu-left__item--active': '' }}">
                <a href="{{ route('admin.subscribers.index') }}">
                    <span class="cat__menu-left__icon icmn-bell "></span>
                    @lang('admin.nav.subscribers')
                </a>
            </li>

{{--            <li class="cat__menu-left__item {{ $current_route_name == 'labs'? 'cat__menu-left__item--active': '' }}">--}}
{{--                <a href="{{ route('admin.labs.index') }}">--}}
{{--                    <span class="cat__menu-left__icon icmn-office "></span>--}}
{{--                    @lang('admin.nav.labs')--}}
{{--                </a>--}}
{{--            </li>--}}

{{--            <li class="cat__menu-left__item {{ $current_route_name == 'videos'? 'cat__menu-left__item--active': '' }}">--}}
{{--                <a href="{{ route('admin.videos.index') }}">--}}
{{--                    <span class="cat__menu-left__icon icmn-film"></span>--}}
{{--                    @lang('admin.nav.videos')--}}
{{--                </a>--}}
{{--            </li>--}}


            <li class="cat__menu-left__item cat__menu-left__submenu {{ ($current_route_name == 'partners' || $current_route_name == 'national-sites' || $current_route_name == 'project-categories' || $current_route_name == 'project-types' || $current_route_name == 'cities' || $current_route_name == 'contacts' || $current_route_name == 'pages')? 'cat__menu-left__item--active cat__menu-left__submenu--toggled': '' }}">
                <a href="#" onclick="return false;">
                    <span class="cat__menu-left__icon icmn-book"></span>
                    @lang('admin.nav.handbooks')
                </a>
                <ul class="cat__menu-left__list" style="{{ ($current_route_name == 'partners' || $current_route_name == 'national-sites' || $current_route_name == 'project-categories' || $current_route_name == 'project-types' || $current_route_name == 'cities' || $current_route_name == 'contacts')? 'display: block;': '' }}">

                    <li class="cat__menu-left__item {{ $current_route_name == 'project-categories'? 'cat__menu-left__item--active': '' }}">
                        <a href="{{ route('admin.project-categories.index') }}">
                            <span class="cat__menu-left__icon icmn-menu "></span>
                            @lang('admin.nav.project-categories')
                        </a>
                    </li>
{{--                    <li class="cat__menu-left__item {{ $current_route_name == 'project-types'? 'cat__menu-left__item--active': '' }}">--}}
{{--                        <a href="{{ route('admin.project-types.index') }}">--}}
{{--                            <span class="cat__menu-left__icon icmn-tree "></span>--}}
{{--                            @lang('admin.nav.project-types')--}}
{{--                        </a>--}}
{{--                    </li>--}}
                    <li class="cat__menu-left__item {{ $current_route_name == 'cities'? 'cat__menu-left__item--active': '' }}">
                        <a href="{{ route('admin.cities.index') }}">
                            <span class="cat__menu-left__icon icmn-map2 "></span>
                            @lang('admin.nav.cities')
                        </a>
                    </li>
{{--                    <li class="cat__menu-left__item {{ $current_route_name == 'national-sites'? 'cat__menu-left__item--active': '' }}">--}}
{{--                        <a href="{{ route('admin.national-sites.index') }}">--}}
{{--                            <span class="cat__menu-left__icon icmn-link "></span>--}}
{{--                            @lang('admin.nav.national-sites')--}}
{{--                        </a>--}}
{{--                    </li>--}}
                    <li class="cat__menu-left__item {{ $current_route_name == 'partners'? 'cat__menu-left__item--active': '' }}">
                        <a href="{{ route('admin.partners.index') }}">
                            <span class="cat__menu-left__icon icmn-link "></span>
                            @lang('admin.nav.partners')
                        </a>
                    </li>
                    <li class="cat__menu-left__item {{ $current_route_name == 'contacts'? 'cat__menu-left__item--active': '' }}">
                        <a href="{{ route('admin.contacts.index') }}">
                            <span class="cat__menu-left__icon icmn-phone "></span>
                            @lang('admin.nav.contacts')
                        </a>
                    </li>
                    <li class="cat__menu-left__item {{ $current_route_name == 'pages'? 'cat__menu-left__item--active': '' }}">
                        <a href="{{ route('admin.pages.index') }}">
                            <span class="cat__menu-left__icon icmn-phone "></span>
                            @lang('admin.nav.pages')
                        </a>
                    </li>

                </ul>
            </li>



{{--            <li class="cat__menu-left__item cat__menu-left__submenu {{ ($current_route_name == 'forum' || $current_route_name == 'answer' || $current_route_name == 'responds')? 'cat__menu-left__item--active cat__menu-left__submenu--toggled': '' }}">--}}
{{--                <a href="#" onclick="return false;">--}}
{{--                    <span class="cat__menu-left__icon icmn-bubbles"></span>--}}
{{--                    @lang('admin.nav.forum') {!! $new_forum_all? '<span class="badge badge-success">'.$new_forum_all.'</span>' : ''  !!}--}}
{{--                </a>--}}
{{--                <ul class="cat__menu-left__list" style="{{ ($current_route_name == 'forum' || $current_route_name == 'answer' || $current_route_name == 'responds')? 'display: block;': '' }}">--}}

{{--                    <li class="cat__menu-left__item {{ $current_route_name == 'forum' || $current_route_name == 'answer'? 'cat__menu-left__item--active': '' }}">--}}
{{--                        <a href="{{ route('admin.forum.index') }}">--}}
{{--                            <span class="cat__menu-left__icon icmn-bubbles3 "></span>--}}
{{--                            @lang('admin.nav.themes') {!! $new_forum? '<span class="badge badge-success">'.$new_forum.'</span>' : ''  !!}--}}
{{--                        </a>--}}
{{--                    </li>--}}

{{--                    <li class="cat__menu-left__item {9{ $current_route_name == 'responds' ? 'cat__menu-left__item--active': '' }}">--}}
{{--                        <a href="{{ route('admin.responds.index') }}">--}}
{{--                            <span class="cat__menu-left__icon icmn-bell "></span>--}}
{{--                            @lang('admin.nav.responds') {!! $new_responds? '<span class="badge badge-success">'.$new_responds.'</span>' : ''  !!}--}}
{{--                        </a>--}}
{{--                    </li>--}}

{{--                </ul>--}}
{{--            </li>--}}
{{--            <li class="cat__menu-left__item {{ $current_route_name == 'subscribers'? 'cat__menu-left__item--active': '' }}">--}}
{{--                <a href="{{ route('admin.subscribers.index') }}">--}}
{{--                    <span class="cat__menu-left__icon icmn-mail2"></span>--}}
{{--                    @lang('admin.nav.subscribers')--}}
{{--                </a>--}}
{{--            </li>--}}


        @endif
        {{--<!-- END: menu-left item -->--}}

        {{--<!-- START: menu-left item divider-->--}}
        {{--<li class="cat__menu-left__divider"><!-- --></li>--}}
        {{--<!-- END: menu-left item divider -->--}}

        {{--<!-- START: menu-left item with submenu -->--}}

        {{--<!-- END: menu-left item with submenu -->--}}
        {{--<li class="cat__menu-left__item cat__menu-left__submenu {{ ($current_route_name == 'cities' || $current_route_name == 'kinds' || $current_route_name == 'kitchens' || $current_route_name == 'options' || $current_route_name == 'payments')? 'cat__menu-left__item--active cat__menu-left__submenu--toggled': '' }}">--}}
            {{--<a href="#" onclick="return false;">--}}
                {{--<span class="cat__menu-left__icon icmn-info"></span>--}}
                {{--@lang('admin.nav.hand_books')--}}
            {{--</a>--}}
            {{--<ul class="cat__menu-left__list" style="{{ ($current_route_name == 'cities' || $current_route_name == 'kinds' || $current_route_name == 'kitchens' || $current_route_name == 'options' || $current_route_name == 'payments')? 'display: block;': '' }}">--}}
                {{--<li class="cat__menu-left__item {{ $current_route_name == 'cities'? 'cat__menu-left__item--active': '' }}">--}}
                    {{--<a href="{{ route('admin.cities.index') }}">--}}
                        {{--<span class="cat__menu-left__icon icmn-map2"></span>--}}
                        {{--@lang('admin.nav.cities')--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="cat__menu-left__item {{ $current_route_name == 'kinds'? 'cat__menu-left__item--active': '' }}">--}}
                    {{--<a href="{{ route('admin.kinds.index') }}">--}}
                        {{--<span class="cat__menu-left__icon icmn-library"></span>--}}
                        {{--@lang('admin.nav.kinds')--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="cat__menu-left__item {{ $current_route_name == 'kitchens'? 'cat__menu-left__item--active': '' }}">--}}
                    {{--<a href="{{ route('admin.kitchens.index') }}">--}}
                        {{--<span class="cat__menu-left__icon icmn-earth"></span>--}}
                        {{--@lang('admin.nav.kitchens')--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="cat__menu-left__item {{ $current_route_name == 'options'? 'cat__menu-left__item--active': '' }}">--}}
                    {{--<a href="{{ route('admin.options.index') }}">--}}
                        {{--<span class="cat__menu-left__icon icmn-checkmark"></span>--}}
                        {{--@lang('admin.nav.options')--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="cat__menu-left__item {{ $current_route_name == 'payments'? 'cat__menu-left__item--active': '' }}">--}}
                    {{--<a href="{{ route('admin.payments.index') }}">--}}
                        {{--<span class="cat__menu-left__icon icmn-coin-dollar"></span>--}}
                        {{--@lang('admin.nav.payments')--}}
                    {{--</a>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</li>--}}
@endsection
