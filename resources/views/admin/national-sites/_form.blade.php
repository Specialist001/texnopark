@component('component.translations', ['form' => 'admin.national-sites._translations_form', 'model' => $nationalSite?? null])@endcomponent

<div class="form-group row {!! $errors->first('icon', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="icon">@lang('admin.icon')</label>
    <div class="col-md-6">
        @if(isset($nationalSite) && $nationalSite->icon)
            <p id="poster">
                <img src="{{$nationalSite->iconUrl()}}" alt="{{$nationalSite->name}}" class="img-thumbnail d-block">
                <small id="delete_poster" class="text-danger cur-pointer"><i class="icmn-cross"></i> @lang('admin.destroy')</small>
            </p>
        @endif
        <input type="file" name="icon" class="form-control input-sm" id="icon" >
        {!! $errors->first('icon', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>


<div class="form-group row {!! $errors->first('order', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="order">@lang('admin.order')</label>
    <div class="col-md-6">
        <input type="number" step="1" min="0" name="order" class="form-control input-sm" id="order" value="{{ old('order', $nationalSite->order ?? 0) }}"  >
        {!! $errors->first('order', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>


<div class="form-group row {!! $errors->first('link', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="link">@lang('admin.link')</label>
    <div class="col-md-6">
        <input type="text" name="link" class="form-control input-sm" id="link" value="{{ old('link', $nationalSite->link ?? '') }}"  >
        {!! $errors->first('link', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
