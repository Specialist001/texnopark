@extends('admin.layout')

@section('center_content')
    <form action="{{ route('admin.national-sites.update', $nationalSite) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        @component('component.card', ['title' => trans('admin.nav.national-sites').': '.trans('admin.editing')])
            @include('admin.national-sites._form')
            @slot('bottom')
                <a href="{{ route('admin.national-sites.index') }}" class="btn btn-sm btn-danger float-left">@lang('admin.back')</a>
                <button class="btn btn-sm btn-primary float-right">@lang('admin.save')</button>
                <div class="clearfix"></div>
            @endslot
        @endcomponent
    </form>
@endsection

@push('scripts')
    <script>
        $(function () {
            $('#delete_poster').on('click', function () {
                $.ajax({
                    method: "DELETE",
                    url: "{{ route('admin.national-sites.destroy.poster') }}/{{$nationalSite->id}}",
                    success: function(response) {
                        if(response.result === "success") {
                            $('#poster').remove();
                            return false;
                        } else {
                            alert("{{trans('admin.ajax_error')}}");
                        }
                    },
                    error: function(response) {
                        alert("{{trans('admin.ajax_error')}}");
                    }
                });
            });
        });
    </script>
@endpush
@push('styles')
    <style>
        #image {
            max-width: 300px;
        }
    </style>
@endpush
