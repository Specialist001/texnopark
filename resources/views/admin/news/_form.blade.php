@component('component.translations', ['form' => 'admin.news._translations_form', 'model' => $page?? null])@endcomponent

<hr>
<div class="form-group row {!! $errors->first('poster', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="poster">@lang('admin.poster')</label>
    <div class="col-md-6">
        @if(isset($page) && $page->poster)
            <p id="poster">
                <img src="{{$page->thumbUrl()}}" alt="{{$page->name}}" class="img-thumbnail d-block">
                <small id="delete_poster" class="text-danger cur-pointer"><i class="icmn-cross"></i> @lang('admin.destroy')</small>
            </p>
        @endif
        <input type="file" name="poster" class="form-control input-sm" id="poster" >
        {!! $errors->first('poster', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('top', 'has-danger')!!}">
    <div class="col-md-6 offset-md-3">
        <div class="input-group d-inline-block">
            <label class="custom-control  custom-checkbox cur-pointer mr-0 d-inline-block">
                <span class="custom-control-description">@lang('admin.top')</span>
                <input type="checkbox"  name="top" value="1" class="custom-control-input" {{ (old('top', $page->top ?? 0) == 1) ? 'checked': ''}}>
                <span class="custom-control-indicator"></span>
            </label>
        </div>
        <div class="">
            {!! $errors->first('top', '<small class="form-control-feedback">:message</small>') !!}
        </div>

    </div>
</div>

@if(isset($page) && $page->system != 1)
    <div class="form-group row {!! $errors->first('active', 'has-danger')!!}">
        <label class="col-md-3 text-md-right col-form-label-sm" for="active">@lang('admin.status')</label>
        <div class="col-md-6">
            <select name="active" id="active" class="form-control input-sm" required>
                <option value="1" {{ (old('active', $page->active ?? 1) == 1) ? 'selected': ''}}>@lang('admin.active')</option>
                <option value="0" {{ (old('active', $page->active ?? 1) == 0) ? 'selected': ''}}>@lang('admin.not_active')</option>
            </select>
            {!! $errors->first('active', '<small class="form-control-feedback">:message</small>') !!}
        </div>
    </div>

    <div class="form-group row {!! $errors->first('created_at', 'has-danger')!!}">
        <label class="col-md-3 text-md-right col-form-label-sm" for="created_at">@lang('admin.created_at')</label>
        <div class="col-md-6">
            <input type="text" name="created_at" class="form-control input-sm" id="created_at" value="{{ old('created_at', $page->created_at ?? null) }}"  >
            {!! $errors->first('created_at', '<small class="form-control-feedback">:message</small>') !!}
        </div>
    </div>
@endif

@component('component.tiny-mc')@endcomponent
