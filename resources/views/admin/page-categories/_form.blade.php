@component('component.translations', ['form' => 'admin.page-categories._translations_form', 'model' => $category?? null])@endcomponent


<div class="form-group row {!! $errors->first('top', 'has-danger')!!}">
    <div class="col-md-6 offset-md-3">
        <div class="input-group d-inline-block">
            <label class="custom-control  custom-checkbox cur-pointer mr-0 d-inline-block">
                <span class="custom-control-description">@lang('admin.top')</span>
                <input type="checkbox"  name="top" value="1" class="custom-control-input" {{ (old('top', $category->top ?? 0) == 1) ? 'checked': ''}}>
                <span class="custom-control-indicator"></span>
            </label>
        </div>
        <div class="">
            {!! $errors->first('top', '<small class="form-control-feedback">:message</small>') !!}
        </div>

    </div>
</div>

<div class="form-group row {!! $errors->first('bottom', 'has-danger')!!}">
    <div class="col-md-6 offset-md-3">
        <div class="input-group d-inline-block">
            <label class="custom-control  custom-checkbox cur-pointer mr-0 d-inline-block">
                <span class="custom-control-description">@lang('admin.bottom')</span>
                <input type="checkbox"  name="bottom" value="1" class="custom-control-input" {{ (old('bottom', $category->bottom ?? 0) == 1) ? 'checked': ''}}>
                <span class="custom-control-indicator"></span>
            </label>
        </div>
        <div class="">
            {!! $errors->first('bottom', '<small class="form-control-feedback">:message</small>') !!}
        </div>

    </div>
</div>

<div class="form-group row {!! $errors->first('order', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="order">@lang('admin.order')</label>
    <div class="col-md-6">
        <input type="number" step="1" min="0" name="order" class="form-control input-sm" id="order" value="{{ old('order', $category->order ?? 0) }}"  >
        {!! $errors->first('order', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('active', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="active">@lang('admin.status')</label>
    <div class="col-md-6">
        <select name="active" id="active" class="form-control input-sm" required>
            <option value="1" {{ (old('active', $category->active ?? 1) == 1) ? 'selected': ''}}>@lang('admin.active')</option>
            <option value="0" {{ (old('active', $category->active ?? 1) == 0) ? 'selected': ''}}>@lang('admin.not_active')</option>
        </select>
        {!! $errors->first('active', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
