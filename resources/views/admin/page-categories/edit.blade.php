@extends('admin.layout')

@section('center_content')
    <form action="{{ route('admin.page-categories.update', $category) }}" method="post" >
        @csrf
        @method('put')
        @component('component.card', ['title' => trans('admin.nav.page-categories').': '.trans('admin.editing')])
            @include('admin.page-categories._form')
            @slot('bottom')
                <a href="{{ route('admin.page-categories.index') }}" class="btn btn-sm btn-danger float-left">@lang('admin.back')</a>
                <button class="btn btn-sm btn-primary float-right">@lang('admin.save')</button>
                <div class="clearfix"></div>
            @endslot
        @endcomponent
    </form>
@endsection
