@component('component.translations', ['form' => 'admin.pages._translations_form', 'model' => $page?? null])@endcomponent

<hr>

<div class="form-group row {!! $errors->first('page_category_id', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="page_category_id">@lang('admin.category')</label>
    <div class="col-md-6">
        <select name="page_category_id" id="page_category_id" class="form-control input-sm" >
            <option value=""></option>
            @foreach(\App\Domain\PageCategories\Models\PageCategory::all() as $category)
                <option value="{{$category->id}}" {{ (old('page_category_id', $page->page_category_id ?? '') == $category->id) ? 'selected': ''}}>{{ $category->name }}</option>
            @endforeach
        </select>
        {!! $errors->first('page_category_id', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('poster', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="poster">@lang('admin.poster')</label>
    <div class="col-md-6">
        @if(isset($page) && $page->poster)
            <p id="poster">
                <img src="{{$page->thumbUrl()}}" alt="{{$page->name}}" class="img-thumbnail d-block">
                <small id="delete_poster" class="text-danger cur-pointer"><i class="icmn-cross"></i> @lang('admin.destroy')</small>
            </p>
        @endif
        <input type="file" name="poster" class="form-control input-sm" id="poster" >
        {!! $errors->first('poster', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('top', 'has-danger')!!}">
    <div class="col-md-6 offset-md-3">
        <div class="input-group d-inline-block">
            <label class="custom-control  custom-checkbox cur-pointer mr-0 d-inline-block">
                <span class="custom-control-description">@lang('admin.top')</span>
                <input type="checkbox"  name="top" value="1" class="custom-control-input" {{ (old('top', $page->top ?? 0) == 1) ? 'checked': ''}}>
                <span class="custom-control-indicator"></span>
            </label>
        </div>
        <div class="">
            {!! $errors->first('top', '<small class="form-control-feedback">:message</small>') !!}
        </div>

    </div>
</div>

<div class="form-group row {!! $errors->first('order', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="order">@lang('admin.order')</label>
    <div class="col-md-6">
        <input type="number" step="1" min="0" name="order" class="form-control input-sm" id="order" value="{{ old('order', $page->order ?? 0) }}"  >
        {!! $errors->first('order', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

@if(isset($page) && $page->system != 1)
    <div class="form-group row {!! $errors->first('active', 'has-danger')!!}">
        <label class="col-md-3 text-md-right col-form-label-sm" for="active">@lang('admin.status')</label>
        <div class="col-md-6">
            <select name="active" id="active" class="form-control input-sm" required>
                <option value="1" {{ (old('active', $page->active ?? 1) == 1) ? 'selected': ''}}>@lang('admin.active')</option>
                <option value="0" {{ (old('active', $page->active ?? 1) == 0) ? 'selected': ''}}>@lang('admin.not_active')</option>
            </select>
            {!! $errors->first('active', '<small class="form-control-feedback">:message</small>') !!}
        </div>
    </div>
@endif

@component('component.tiny-mc')@endcomponent
