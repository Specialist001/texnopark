<?php $value = old('translations.'.$lang.'.name', (isset($model) && $model->translate($lang)) ? $model->translate($lang)->name: '') ?>
<div class="form-group row {!! $errors->first('translations.'.$lang.'.name', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="{{$lang}}-name">@lang('admin.title')</label>
    <div class="col-md-6">
        <input type="text" name="translations[{{ $lang }}][name]" class="form-control input-sm" id="{{$lang}}-name" value="{{ $value }}" {{ ($lang == LaravelLocalization::getDefaultLocale())? 'required autofocus': '' }} >
        {!! $errors->first('translations.'.$lang.'.name', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
