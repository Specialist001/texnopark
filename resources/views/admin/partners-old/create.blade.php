@extends('admin.layout')

@section('center_content')
    <form action="{{ route('admin.partners.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        @component('component.card', ['title' => trans('admin.nav.partners').': '.trans('admin.creating')])
            @include('admin.partners._form')
            @slot('bottom')
                <a href="{{ route('admin.partners.index') }}" class="btn btn-sm btn-danger float-left">@lang('admin.back')</a>
                <button class="btn btn-sm btn-primary float-right">@lang('admin.create')</button>
                <div class="clearfix"></div>
            @endslot
        @endcomponent
    </form>
@endsection
