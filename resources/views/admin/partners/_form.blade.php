<div class="form-group row {!! $errors->first('name', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="name">@lang('admin.title')</label>
    <div class="col-md-6">
        <input type="text" name="name" class="form-control input-sm" id="name" value="{{ old('name', $partner->name ?? null) }}"  >
        {!! $errors->first('name', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('order', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="order">@lang('admin.order')</label>
    <div class="col-md-6">
        <input type="number" name="order" class="form-control input-sm" id="order" value="{{ old('order', $partner->order ?? null) }}"  >
        {!! $errors->first('order', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('logo', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="logo">@lang('admin.logo')</label>
    <div class="col-md-6">
        @if(isset($partner) && $partner->logo)
            <p id="poster">
                <img src="{{$partner->logoUrl()}}" alt="{{$partner->name}}" class="img-thumbnail d-block">
                <small id="delete_logo" class="text-danger cur-pointer"><i class="icmn-cross"></i> @lang('admin.destroy')</small>
            </p>
        @endif
        <input type="file" name="logo" class="form-control input-sm" id="icon" >
        {!! $errors->first('logo', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
