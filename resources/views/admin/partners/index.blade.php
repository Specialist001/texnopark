@extends('admin.layout')

@section('center_content')
    @component('component.card', ['title' => trans('admin.nav.partners'), 'bodyClass' => 'card-body-no-padding'])
        @slot('buttons')
            <a href="{{ route('admin.partners.create') }}" class="btn btn-sm btn-primary ml-2">
                <span class="d-none d-sm-inline-block">@lang('admin.create')</span> <i class="icmn-plus"><!-- --></i>
            </a>
        @endslot

        <div id="filters">
            <form action="{{ route('admin.partners.index') }}" method="get">
                <div class="row">
                    <div class="col-md-12">
                        <p>
                            <strong>@lang('admin.filters')</strong>
                        </p>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="form-group">
                            <label for="name">
                                <small>@lang('admin.title')</small>
                            </label>
                            <input class="form-control input-sm" name="name" id="name" type="text"  value="{{ $filters['name'] ?? '' }}"/>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label for="sort">
                                <small>@lang('admin.sort')</small>
                            </label>
                            <select name="sort" id="sort" class="form-control input-sm">
                                <option value=""></option>
                                <option value="id" {{ (isset($filters['sort']) && $filters['sort'] == 'id') ? 'selected': ''}}>@lang('admin.id') (@lang('admin.asc'))</option>
                                <option value="-id" {{ (isset($filters['sort']) && $filters['sort'] == '-id') ? 'selected': ''}}>@lang('admin.id') (@lang('admin.desc'))</option>

                                <option value="order" {{ (isset($filters['sort']) && $filters['sort'] == 'order') ? 'selected': ''}}>@lang('admin.order') (@lang('admin.asc'))</option>
                                <option value="-order" {{ (isset($filters['sort']) && $filters['sort'] == '-order') ? 'selected': ''}}>@lang('admin.order') (@lang('admin.desc'))</option>

                                <option value="name" {{ (isset($filters['sort']) && $filters['sort'] == 'name') ? 'selected': ''}}>@lang('admin.title') (@lang('admin.asc'))</option>
                                <option value="-name" {{ (isset($filters['sort']) && $filters['sort'] == '-name') ? 'selected': ''}}>@lang('admin.title') (@lang('admin.desc'))</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="float-right">
                            <a href="{{ route('admin.partners.index') }}" class="btn btn-sm btn-danger">@lang('admin.filters_reset')</a>
                            <button class="btn btn-sm btn-success">@lang('admin.filters_apply')</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </form>
        </div>

        @if($partners->isNotEmpty())
            <div class="table-responsive">
                <table class="table">
                    <thead class="thead-default">
                        <tr>
                            <th>@lang('admin.title')</th>
                            <th>@lang('admin.order')</th>
                            <th>@lang('admin.logo')</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($partners as $item)
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->order }}</td>
                            <td><img src="{{ $item->logoUrl() }}" class="img-thumbnail" style="width: 80px"></td>
                            <td>
                                <a href="{{ route('admin.partners.edit', $item) }}" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="@lang('admin.edit')">
                                    <i class="icmn-pencil"></i>
                                </a>

                                <form action="{{ route('admin.partners.destroy', $item) }}" class="d-inline-block" method="post">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-sm btn-danger"
                                            onclick="return confirm('@lang('admin.destroy_confirm')');"
                                            data-toggle="tooltip" data-placement="top" title="@lang('admin.destroy')">
                                        <i class="icmn-cross"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif

        @slot('bottom')
            @include('common.pagination', ['data' => $partners])
        @endslot
    @endcomponent
@endsection
