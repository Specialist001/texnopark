@extends('admin.layout')

@section('center_content')
    <form action="{{ route('admin.profile.update') }}" method="post">
        @csrf
        @method('put')
        @component('component.card', ['title' => trans('admin.profile')])
            @include('admin.profile._form')
            @slot('bottom')
{{--                <a href="{{ route('partner.home') }}" class="btn btn-sm btn-danger float-left">@lang('partner.nav.home')</a>--}}
                <button class="btn btn-sm btn-primary float-right">@lang('admin.save')</button>
                <div class="clearfix"></div>
            @endslot
        @endcomponent
    </form>
@endsection
