@component('component.translations', ['form' => 'admin.project-categories._translations_form', 'model' => $category?? null])@endcomponent

<div class="form-group row {!! $errors->first('icon', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="icon">@lang('admin.icon')</label>
    <div class="col-md-6">
        @if(isset($category) && $category->icon)
            <p id="image">
                <img src="{{$category->iconUrl()}}" alt="{{$category->name}}" class="img-thumbnail d-block">
                <small id="delete_image" class="text-danger cur-pointer"><i class="icmn-cross"></i> @lang('admin.delete_icon')</small>
            </p>
        @endif
        <input type="file" name="icon" class="form-control input-sm" id="icon" >
        {!! $errors->first('icon', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('order', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="order">@lang('admin.order')</label>
    <div class="col-md-6">
        <input type="number" step="1" min="0" name="order" class="form-control input-sm" id="order" value="{{ old('order', $category->order ?? 0) }}"  >
        {!! $errors->first('order', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
