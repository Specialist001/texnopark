@extends('admin.layout')

@section('center_content')
    <form action="{{ route('admin.project-categories.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        @component('component.card', ['title' => trans('admin.nav.project-categories').': '.trans('admin.creating')])
            @include('admin.project-categories._form')
            @slot('bottom')
                <a href="{{ route('admin.project-categories.index') }}" class="btn btn-sm btn-danger float-left">@lang('admin.back')</a>
                <button class="btn btn-sm btn-primary float-right">@lang('admin.create')</button>
                <div class="clearfix"></div>
            @endslot
        @endcomponent
    </form>
@endsection
