@extends('admin.layout')

@section('center_content')
    <form action="{{ route('admin.project-types.update', $type) }}" method="post" >
        @csrf
        @method('put')
        @component('component.card', ['title' => trans('admin.nav.project-types').': '.trans('admin.editing')])
            @include('admin.project-types._form')
            @slot('bottom')
                <a href="{{ route('admin.project-types.index') }}" class="btn btn-sm btn-danger float-left">@lang('admin.back')</a>
                <button class="btn btn-sm btn-primary float-right">@lang('admin.save')</button>
                <div class="clearfix"></div>
            @endslot
        @endcomponent
    </form>
@endsection
