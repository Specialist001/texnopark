<div class="form-group row {!! $errors->first('user_id', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="user_id">@lang('project.user_id')</label>
    <div class="col-md-6">
        <input type="number" step="1" min="1" name="user_id" class="form-control input-sm" id="user_id" value="{{ old('user_id', $project->user_id ?? '') }}" required autofocus>
        {!! $errors->first('user_id', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('project_category_id', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="project_category_id">@lang('project.project_category_id')</label>
    <div class="col-md-6">
        <select name="project_category_id" id="project_category_id" class="form-control input-sm" required>
            @foreach(\App\Domain\ProjectCategories\Models\ProjectCategory::orderBy('order')->get() as $category)
                <option value="{{$category->id}}" {{ (old('project_category_id', $project->project_category_id ?? '') == $category->id) ? 'selected': ''}}>{{ $category->name }}</option>
            @endforeach
        </select>
        {!! $errors->first('project_category_id', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

{{--<div class="form-group row {!! $errors->first('project_type_id', 'has-danger')!!}">--}}
{{--    <label class="col-md-3 text-md-right col-form-label-sm" for="project_type_id">@lang('project.project_type_id')</label>--}}
{{--    <div class="col-md-6">--}}
{{--        <select name="project_type_id" id="project_type_id" class="form-control input-sm" required>--}}
{{--            @foreach(\App\Domain\ProjectTypes\Models\ProjectType::all() as $type)--}}
{{--                <option value="{{$type->id}}" {{ (old('project_type_id', $project->project_type_id ?? '') == $type->id) ? 'selected': ''}}>{{ $type->name }}</option>--}}
{{--            @endforeach--}}
{{--        </select>--}}
{{--        {!! $errors->first('project_type_id', '<small class="form-control-feedback">:message</small>') !!}--}}
{{--    </div>--}}
{{--</div>--}}

<div class="form-group row {!! $errors->first('logo', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="logo">@lang('project.logo')</label>
    <div class="col-md-6">
        @if(isset($project) && $project->logo)
            <p id="logo">
                <img src="{{$project->logoUrl()}}" alt="{{$project->title}}" class="img-fluid d-block">
                <small id="delete_logo" class="text-danger cur-pointer"><i class="icmn-cross"></i> @lang('admin.destroy')</small>
            </p>
        @endif
        <input type="file" name="logo" class="form-control input-sm" id="logo" >
        {!! $errors->first('logo', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('image', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="image">@lang('project.image')</label>
    <div class="col-md-6">
        @if(isset($project) && $project->image)
            <p id="image">
                <img src="{{$project->imageUrl()}}" alt="{{$project->title}}" class="img-fluid d-block">
                <small id="delete_image" class="text-danger cur-pointer"><i class="icmn-cross"></i> @lang('admin.destroy')</small>
            </p>
        @endif
        <input type="file" name="image" class="form-control input-sm" id="image" >
        {!! $errors->first('image', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>


<div class="form-group row {!! $errors->first('title', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="title">@lang('project.title')</label>
    <div class="col-md-6">
        <input type="text" name="title" class="form-control input-sm" id="title" value="{{ old('title', $project->title ?? '') }}" required >
        {!! $errors->first('title', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('prod_capacity', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="prod_capacity">@lang('project.prod_capacity')</label>
    <div class="col-md-3">
        <input type="number" name="prod_capacity" class="form-control input-sm" id="title" value="{{ old('prod_capacity', $project->prod_capacity ?? '') }}" >
        {!! $errors->first('prod_capacity', '<small class="form-control-feedback">:message</small>') !!}
    </div>
    <div class="col-md-2 pl-1 pr-4">
        <select name="prod_capacity_unit" id="prod_capacity_unit" class="form-control input-sm">
            @foreach(\App\Domain\Units\Models\Unit::orderBy('id')->get() as $unit)
                <option value="{{$unit->id}}" {{ (old('prod_capacity_unit', $project->prod_capacity_unit ?? '') == $unit->id) ? 'selected': ''}}>{{ $unit->name }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row {!! $errors->first('metal_structures', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="metal_structures">@lang('project.metal_structures')</label>
    <div class="col-md-6">
        <input type="number" name="metal_structures" class="form-control input-sm" id="metal_structures" value="{{ old('metal_structures', $project->metal_structures ?? '') }}" placeholder="тонна" >
        {!! $errors->first('metal_structures', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('sandwich_panels', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="sandwich_panels">@lang('project.sandwich_panels')</label>
    <div class="col-md-6">
        <input type="number" name="sandwich_panels" class="form-control input-sm" id="sandwich_panels" value="{{ old('sandwich_panels', $project->sandwich_panels ?? '') }}" placeholder="м²" >
        {!! $errors->first('sandwich_panels', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('indoor_area', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="indoor_area">@lang('project.indoor_area')</label>
    <div class="col-md-6">
        <input type="number" name="indoor_area" class="form-control input-sm" id="indoor_area" value="{{ old('indoor_area', $project->indoor_area ?? '') }}" placeholder="м²" >
        {!! $errors->first('indoor_area', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>


<div class="form-group row {!! $errors->first('implementation_period', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="implementation_period">@lang('project.implementation_period')</label>
    <div class="col-md-6">
        <input type="number" step="1" placeholder="@lang('project.period_unit')" min="1" name="implementation_period" class="form-control input-sm" id="implementation_period" value="{{ old('implementation_period', $project->implementation_period ?? '') }}" required >
        {!! $errors->first('implementation_period', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

{{--<div class="form-group row {!! $errors->first('stage', 'has-danger')!!}">--}}
{{--    <label class="col-md-3 text-md-right col-form-label-sm" for="stage">@lang('project.stage')</label>--}}
{{--    <div class="col-md-6">--}}
{{--        <select name="stage" id="stage" class="form-control input-sm" required>--}}
{{--            @foreach(\App\Domain\Projects\Models\Project::stages() as $stage)--}}
{{--                <option value="{{$stage}}" {{ (old('stage', $project->stage ?? '') == $stage) ? 'selected': ''}}>@lang('project.stages.'.$stage)</option>--}}
{{--            @endforeach--}}
{{--        </select>--}}
{{--        {!! $errors->first('stage', '<small class="form-control-feedback">:message</small>') !!}--}}
{{--    </div>--}}
{{--</div>--}}

{{--<div class="form-group row {!! $errors->first('developer', 'has-danger')!!}">--}}
{{--    <label class="col-md-3 text-md-right col-form-label-sm" for="developer">@lang('project.developer')</label>--}}
{{--    <div class="col-md-6">--}}
{{--        <input type="text" name="developer" class="form-control input-sm" id="developer" value="{{ old('developer', $project->developer ?? '') }}" required >--}}
{{--        {!! $errors->first('developer', '<small class="form-control-feedback">:message</small>') !!}--}}
{{--    </div>--}}
{{--</div>--}}

{{--<div class="form-group row {!! $errors->first('foreign', 'has-danger')!!}">--}}
{{--    <label class="col-md-3 text-md-right col-form-label-sm" for="foreign">@lang('project.foreign')</label>--}}
{{--    <div class="col-md-6">--}}
{{--        <select name="foreign" id="foreign" class="form-control input-sm" required>--}}
{{--            <option value="0" {{ (old('foreign', $project->foreign ?? 0) == 0) ? 'selected': ''}}>@lang('project.no')</option>--}}
{{--            <option value="1" {{ (old('foreign', $project->foreign ?? 0) == 1) ? 'selected': ''}}>@lang('project.yes')</option>--}}
{{--        </select>--}}
{{--        {!! $errors->first('foreign', '<small class="form-control-feedback">:message</small>') !!}--}}
{{--    </div>--}}
{{--</div>--}}

<div class="form-group row {!! $errors->first('readiness', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="readiness">@lang('project.readiness')</label>
    <div class="col-md-6">
        <input type="number" step="1" min="0" max="100" name="readiness" class="form-control input-sm" id="readiness" value="{{ old('readiness', $project->readiness ?? '') }}" required >
        {!! $errors->first('readiness', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

{{--<div class="form-group row {!! $errors->first('patent', 'has-danger')!!}">--}}
{{--    <label class="col-md-3 text-md-right col-form-label-sm" for="patent">@lang('project.patent')</label>--}}
{{--    <div class="col-md-6">--}}
{{--        <input type="text" name="patent" class="form-control input-sm" id="patent" value="{{ old('patent', $project->patent ?? '') }}" >--}}
{{--        {!! $errors->first('patent', '<small class="form-control-feedback">:message</small>') !!}--}}
{{--    </div>--}}
{{--</div>--}}

{{--<div class="form-group row {!! $errors->first('payback_period', 'has-danger')!!}">--}}
{{--    <label class="col-md-3 text-md-right col-form-label-sm" for="payback_period">@lang('project.payback_period')</label>--}}
{{--    <div class="col-md-6">--}}
{{--        <input type="number" step="1" placeholder="@lang('project.period_unit')" min="1" name="payback_period" class="form-control input-sm" id="payback_period" value="{{ old('payback_period', $project->payback_period ?? '') }}" required >--}}
{{--        {!! $errors->first('payback_period', '<small class="form-control-feedback">:message</small>') !!}--}}
{{--    </div>--}}
{{--</div>--}}


{{--<div class="form-group row">--}}
{{--    <label class="col-md-3 text-md-right col-form-label-sm" for="sum_cost">@lang('project.cost')</label>--}}
{{--    <div class="col-md-6">--}}

{{--        <small class="form-control-feedback text-muted">@lang('project.sum')</small>--}}
{{--        <input type="number" step="1"  min="0" name="sum_cost" class="form-control input-sm" id="sum_cost" value="{{ old('sum_cost', $project->sum_cost ?? '') }}"  >--}}
{{--        {!! $errors->first('sum_cost', '<small class="form-control-feedback">:message</small>') !!}--}}

{{--        <small class="form-control-feedback text-muted">@lang('project.usd')</small>--}}
{{--        <input type="number" step="1"  min="0" name="usd_cost" class="form-control input-sm" id="usd_cost" value="{{ old('usd_cost', $project->usd_cost ?? '') }}"  >--}}
{{--        {!! $errors->first('usd_cost', '<small class="form-control-feedback">:message</small>') !!}--}}
{{--    </div>--}}
{{--</div>--}}

{{--<div class="form-group row">--}}
{{--    <label class="col-md-3 text-md-right col-form-label-sm" for="sum_need">@lang('project.need')</label>--}}
{{--    <div class="col-md-6">--}}

{{--        <small class="form-control-feedback text-muted">@lang('project.sum')</small>--}}
{{--        <input type="number" step="1"  min="1" name="sum_need" class="form-control input-sm" id="sum_need" value="{{ old('sum_need', $project->sum_need ?? '') }}" required >--}}
{{--        {!! $errors->first('sum_need', '<small class="form-control-feedback">:message</small>') !!}--}}

{{--        <small class="form-control-feedback text-muted">@lang('project.usd')</small>--}}
{{--        <input type="number" step="1"  min="0" name="usd_need" class="form-control input-sm" id="usd_need" value="{{ old('usd_need', $project->usd_need ?? '') }}"  >--}}
{{--        {!! $errors->first('usd_need', '<small class="form-control-feedback">:message</small>') !!}--}}
{{--    </div>--}}
{{--</div>--}}

{{--<div class="form-group row">--}}
{{--    <label class="col-md-3 text-md-right col-form-label-sm" for="sum_current">@lang('project.current')</label>--}}
{{--    <div class="col-md-6">--}}

{{--        <small class="form-control-feedback text-muted">@lang('project.sum')</small>--}}
{{--        <input type="number" step="1"  min="0" name="sum_current" class="form-control input-sm" id="sum_current" value="{{ old('sum_current', $project->sum_current ?? '') }}"  >--}}
{{--        {!! $errors->first('sum_current', '<small class="form-control-feedback">:message</small>') !!}--}}

{{--        <small class="form-control-feedback text-muted">@lang('project.usd')</small>--}}
{{--        <input type="number" step="1"  min="0" name="usd_current" class="form-control input-sm" id="usd_current" value="{{ old('usd_current', $project->usd_current ?? '') }}"  >--}}
{{--        {!! $errors->first('usd_current', '<small class="form-control-feedback">:message</small>') !!}--}}
{{--    </div>--}}
{{--</div>--}}

<div class="form-group row {!! $errors->first('workers', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="workers">@lang('project.workers')</label>
    <div class="col-md-6">
        <input type="number" step="1" placeholder="@lang('project.worker_unit')" min="0" name="workers" class="form-control input-sm" id="workers" value="{{ old('workers', $project->workers ?? '') }}" >
        {!! $errors->first('workers', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('presentation', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="presentation">@lang('project.presentation')</label>
    <div class="col-md-6">
        @if(isset($project) && $project->presentation)
            <p id="presentation">
                <a href="{{$project->presentationUrl()}}" target="_blank">@lang('admin.file')</a>
                <br>
                <small id="delete_presentation" class="text-danger cur-pointer"><i class="icmn-cross"></i> @lang('admin.destroy')</small>
            </p>
        @endif
        <input type="file" name="presentation" class="form-control input-sm" id="presentation" >
        {!! $errors->first('presentation', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>


{{--<div class="form-group row {!! $errors->first('importable', 'has-danger')!!}">--}}
{{--    <label class="col-md-3 text-md-right col-form-label-sm" for="importable">@lang('project.importable')</label>--}}
{{--    <div class="col-md-6">--}}
{{--        <select name="importable" id="importable" class="form-control input-sm" required>--}}
{{--            <option value="0" {{ (old('importable', $project->importable ?? 0) == 0) ? 'selected': ''}}>@lang('project.no')</option>--}}
{{--            <option value="1" {{ (old('importable', $project->importable ?? 0) == 1) ? 'selected': ''}}>@lang('project.yes')</option>--}}
{{--        </select>--}}
{{--        {!! $errors->first('importable', '<small class="form-control-feedback">:message</small>') !!}--}}
{{--    </div>--}}
{{--</div>--}}

<div class="form-group row {!! $errors->first('status', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="status">@lang('project.status')</label>
    <div class="col-md-6">
        <select name="status" id="status" class="form-control input-sm" required>
            @foreach(\App\Domain\Projects\Models\Project::statuses() as $status)
                <option value="{{$status}}" {{ (old('status', $project->status ?? '') == $status) ? 'selected': ''}}>@lang('project.statuses.'.$status)</option>
            @endforeach
        </select>
        {!! $errors->first('status', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

@component('component.translations', ['form' => 'admin.projects._translations_form', 'model' => $project?? null])@endcomponent
@component('component.tiny-mc')@endcomponent
