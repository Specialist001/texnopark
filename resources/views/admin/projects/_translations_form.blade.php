<?php $value = old('translations.'.$lang.'.place', (isset($model) && $model->translate($lang)) ? $model->translate($lang)->place: '') ?>
<div class="form-group row {!! $errors->first('translations.'.$lang.'.place', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="{{$lang}}-place">@lang('project.place')</label>
    <div class="col-md-6">
        <input type="text" placeholder="@lang('project.place_unit')" name="translations[{{ $lang }}][place]" class="form-control input-sm" id="{{$lang}}-place" value="{{ $value }}" {{ ($lang == LaravelLocalization::getDefaultLocale())? 'required': '' }}>
        {!! $errors->first('translations.'.$lang.'.place', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<?php $value = old('translations.'.$lang.'.short_description', (isset($model) && $model->translate($lang)) ? $model->translate($lang)->short_description: '') ?>
<div class="form-group row {!! $errors->first('translations.'.$lang.'.short_description', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="{{$lang}}-short_description">@lang('project.short_description')</label>
    <div class="col-md-6">
        <input type="text" placeholder="@lang('project.title')" name="translations[{{ $lang }}][short_description]" class="form-control input-sm" id="{{$lang}}-short_description" value="{{ $value }}" {{ ($lang == LaravelLocalization::getDefaultLocale())? 'required': '' }}>
        {!! $errors->first('translations.'.$lang.'.short_description', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<?php $value = old('translations.'.$lang.'.description', (isset($model) && $model->translate($lang)) ? $model->translate($lang)->description: '') ?>
<div class="form-group row {!! $errors->first('translations.'.$lang.'.description', 'has-danger')!!}">
    <label class="col-md-3 text-md-left col-form-label-sm" for="{{$lang}}-description">@lang('project.description')</label>
    <div class="col-md-12">
        <textarea name="translations[{{ $lang }}][description]" class="form-control text-editor" id="{{$lang}}-description" {{ ($lang == LaravelLocalization::getDefaultLocale())? '': '' }}>{{ $value }}</textarea>
        {!! $errors->first('translations.'.$lang.'.description', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>


<?php $value = old('translations.'.$lang.'.additional_info', (isset($model) && $model->translate($lang)) ? $model->translate($lang)->additional_info: '') ?>
<div class="form-group row {!! $errors->first('translations.'.$lang.'.additional_info', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="{{$lang}}-additional_info">@lang('project.additional_info')</label>
    <div class="col-md-6">
        <textarea name="translations[{{ $lang }}][additional_info]" class="form-control" id="{{$lang}}-additional_info" >{{ $value }}</textarea>
        {!! $errors->first('translations.'.$lang.'.additional_info', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
