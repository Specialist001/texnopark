@extends('admin.layout')

@section('center_content')


    <form action="{{ route('admin.projects.update', $project) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        @component('component.card', ['title' => trans('admin.nav.projects').': '.trans('admin.editing')])
            @include('admin.projects._form')
            @slot('bottom')
                <a href="{{ route('admin.projects.index') }}" class="btn btn-sm btn-danger float-left">@lang('admin.back')</a>
                <button class="btn btn-sm btn-primary float-right">@lang('admin.save')</button>
                <div class="clearfix"></div>
            @endslot
        @endcomponent
    </form>
@endsection

@push('scripts')
    <script>
        $(function () {
            $('#delete_image').on('click', function () {
                $.ajax({
                    method: "DELETE",
                    url: "{{ route('admin.projects.destroy.image') }}/{{$project->id}}",
                    success: function(response) {
                        if(response.result === "success") {
                            $('#image').remove();
                            return false;
                        } else {
                            alert("{{trans('admin.ajax_error')}}");
                        }
                    },
                    error: function(response) {
                        alert("{{trans('admin.ajax_error')}}");
                    }
                });
            });
            $('#delete_logo').on('click', function () {
                $.ajax({
                    method: "DELETE",
                    url: "{{ route('admin.projects.destroy.logo') }}/{{$project->id}}",
                    success: function(response) {
                        if(response.result === "success") {
                            $('#logo').remove();
                            return false;
                        } else {
                            alert("{{trans('admin.ajax_error')}}");
                        }
                    },
                    error: function(response) {
                        alert("{{trans('admin.ajax_error')}}");
                    }
                });
            });
            $('#delete_presentation').on('click', function () {
                $.ajax({
                    method: "DELETE",
                    url: "{{ route('admin.projects.destroy.presentation') }}/{{$project->id}}",
                    success: function(response) {
                        if(response.result === "success") {
                            $('#presentation').remove();
                            return false;
                        } else {
                            alert("{{trans('admin.ajax_error')}}");
                        }
                    },
                    error: function(response) {
                        alert("{{trans('admin.ajax_error')}}");
                    }
                });
            });
        });
    </script>
@endpush
@push('styles')
    <style>
        #image, #logo {
            max-width: 300px;
        }
    </style>
@endpush
