@extends('admin.layout')

@section('center_content')
    @component('component.card', ['title' => trans('admin.nav.projects'), 'bodyClass' => 'card-body-no-padding'])
        @slot('buttons')
            <a href="{{ route('admin.projects.create') }}" class="btn btn-sm btn-primary ml-2">
                <span class="d-none d-sm-inline-block">@lang('admin.create')</span> <i class="icmn-plus"><!-- --></i>
            </a>
        @endslot

        <div id="filters">
            <form action="{{ route('admin.projects.index') }}" method="get">
                <div class="row">
                    <div class="col-md-12">
                        <p>
                            <strong>@lang('admin.filters')</strong>
                        </p>
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <div class="form-group">
                            <label for="id">
                                <small>@lang('project.id')</small>
                            </label>
                            <input class="form-control input-sm" name="id" id="id" type="number" step="1" min="1" value="{{ $filters['id'] ?? '' }}"/>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-2">
                        <div class="form-group">
                            <label for="title">
                                <small>@lang('project.title')</small>
                            </label>
                            <input class="form-control input-sm" name="title" id="title" type="text"  value="{{ $filters['title'] ?? '' }}"/>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label for="project_category_id">
                                <small>@lang('project.project_category_id')</small>
                            </label>
                            <select name="project_category_id" id="project_category_id" class="form-control input-sm">
                                <option value=""></option>
                                @foreach(\App\Domain\ProjectCategories\Models\ProjectCategory::all() as $category)
                                    <option value="{{$category->id}}" {{ (isset($filters['project_category_id']) && $filters['project_category_id'] == $category->id) ? 'selected': ''}}>{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <div class="form-group">
                            <label for="readiness_form">
                                <small>@lang('project.readiness')</small>
                            </label>
                            <div class="row">
                                <div class="col-md-6">
                                    <input class="form-control input-sm" placeholder="@lang('project.from')" name="readiness[]" id="readiness_form" type="text"  value="{{ $filters['readiness'][0] ?? '' }}"/>
                                </div>
                                <div class="col-md-6">
                                    <input class="form-control input-sm" placeholder="@lang('project.till')" name="readiness[]" id="readiness_till" type="text"  value="{{ $filters['readiness'][1] ?? '' }}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label for="status">
                                <small>@lang('project.status')</small>
                            </label>
                            <select name="status" id="status" class="form-control input-sm">
                                <option value=""></option>
                                @foreach(\App\Domain\Projects\Models\Project::statuses() as $status)
                                    <option value="{{$status}}" {{ (isset($filters['status']) && $filters['status'] == $status) ? 'selected': ''}}>@lang('project.statuses.'.$status)</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label for="sort">
                                <small>@lang('admin.sort')</small>
                            </label>
                            <select name="sort" id="sort" class="form-control input-sm">
                                <option value=""></option>
                                <option value="id" {{ (isset($filters['sort']) && $filters['sort'] == 'id') ? 'selected': ''}}>@lang('project.id') (@lang('admin.asc'))</option>
                                <option value="-id" {{ (isset($filters['sort']) && $filters['sort'] == '-id') ? 'selected': ''}}>@lang('project.id') (@lang('admin.desc'))</option>

                                <option value="name" {{ (isset($filters['sort']) && $filters['sort'] == 'name') ? 'selected': ''}}>@lang('project.title') (@lang('admin.asc'))</option>
                                <option value="-name" {{ (isset($filters['sort']) && $filters['sort'] == '-name') ? 'selected': ''}}>@lang('project.title') (@lang('admin.desc'))</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="float-right">
                            <a href="{{ route('admin.projects.index') }}" class="btn btn-sm btn-danger">@lang('admin.filters_reset')</a>
                            <button class="btn btn-sm btn-success">@lang('admin.filters_apply')</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </form>
        </div>

        @if($projects->isNotEmpty())
            <div class="table-responsive">
                <table class="table">
                    <thead class="thead-default">
                    <tr>
                        <th>@lang('project.id')</th>
                        <th>@lang('project.project')</th>
                        <th>@lang('project.readiness')</th>
                        <th>@lang('project.status')</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($projects as $project)
                        <tr>
                            <td class="{{ $project->is_moderated == 0 ? 'new_row': '' }}">{{ $project->id }}</td>
                            <td>
                                @lang('project.title'): <strong>{{ $project->short_description }}</strong>
                                <br>
                                @lang('project.project_category_id'): <strong>{{ $project->category->name }}</strong>
                                <br>
                            </td>
                            <td>{{ $project->readiness }}</td>
                            <td>@lang('project.statuses.'.$project->status)</td>
                            <td>
                                <a href="{{ route('admin.projects.edit', $project) }}" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="@lang('admin.edit')">
                                    <i class="icmn-pencil"></i>
                                </a>
                                <form action="{{ route('admin.projects.destroy', $project) }}" class="d-inline-block" method="post">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-sm btn-danger"
                                            onclick="return confirm('@lang('admin.destroy_foreign_confirm')');"
                                            data-toggle="tooltip" data-placement="top" title="@lang('admin.destroy')">
                                        <i class="icmn-cross"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif

        @slot('bottom')
            @include('common.pagination', ['data' => $projects])
        @endslot
    @endcomponent
@endsection
