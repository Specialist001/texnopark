@extends('admin.layout')

@section('center_content')
    <form action="{{ route('admin.proofs.store') }}" method="post">
        @csrf
        @component('component.card', ['title' => trans('admin.nav.proofs').': '.trans('admin.creating')])
            @include('admin.proofs._form')
            @slot('bottom')
                <a href="{{ route('admin.proofs.index') }}" class="btn btn-sm btn-danger float-left">@lang('admin.back')</a>
                <button class="btn btn-sm btn-primary float-right">@lang('admin.create')</button>
                <div class="clearfix"></div>
            @endslot
        @endcomponent
    </form>
@endsection
