@extends('admin.layout')

@section('center_content')
    <form action="{{ route('admin.proofs.update', $user) }}" method="post">
        @csrf
        @method('put')
        @component('component.card', ['title' => trans('admin.nav.proofs').': '.trans('admin.editing')])
            @include('admin.proofs._form')
            @slot('bottom')
                <a href="{{ route('admin.proofs.index') }}" class="btn btn-sm btn-danger float-left">@lang('admin.back')</a>
                <button class="btn btn-sm btn-primary float-right">@lang('admin.save')</button>
                <div class="clearfix"></div>
            @endslot
        @endcomponent
    </form>
@endsection
