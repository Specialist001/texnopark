<div class="table-responsive">

    <table class="table-striped table">
        <tr>
            <td>
                <strong>@lang('site.proof_reading_attributes.name')</strong>
            </td>
            <td>
                {{ $reading->name }}
            </td>
        </tr>
        <tr>
            <td>
                <strong>@lang('site.proof_reading_attributes.email')</strong>
            </td>
            <td>
                {{ $reading->email }}
            </td>
        </tr>
        <tr>
            <td>
                <strong>@lang('site.proof_reading_attributes.phone')</strong>
            </td>
            <td>
                {{ $reading->phone ?? '--' }}
            </td>
        </tr>
        <tr>
            <td>
                <strong>@lang('site.proof_reading_attributes.type')</strong>
            </td>
            <td>
                {{ trans('site.proof_reading_types.'.$reading->type) }}
            </td>
        </tr>
        @if($reading->type == 'publish')
            <tr>
                <td>
                    <strong>@lang('site.proof_reading_attributes.wallpaper')</strong>
                </td>
                <td>
                    {{ $reading->wallpaper ?? '--' }}
                </td>
            </tr>
        @endif
        <tr>
            <td>
                <strong>@lang('site.proof_reading_attributes.file')</strong>
            </td>
            <td>
                <a href="{{ $reading->fileUrl() }}">
                    @lang('site.download')
                </a>
            </td>
        </tr>
        <tr>
            <td>
                <strong>@lang('site.proof_reading_attributes.status')</strong>
            </td>
            <td>
                {{ trans('site.proof_reading_statuses.'.$reading->status) }}
            </td>
        </tr>
    </table>
</div>
<hr>
@if(\Auth::user()->isAdmin())
<div class="form-group row {!! $errors->first('attached_user', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="attached_user">@lang('admin.user_id')</label>
    <div class="col-md-6">
        <input type="number" step="1" min="1" name="attached_user" class="form-control input-sm" id="attached_user" value="{{ old('attached_user', $reading->attached_user ?? '') }}"  >
        {!! $errors->first('attached_user', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
@endif

<div class="form-group row {!! $errors->first('status', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="status">@lang('admin.status')</label>
    <div class="col-md-6">
        <select name="status" id="status" class="form-control input-sm" required>
            @foreach(\App\Domain\ProofReadings\Models\ProofReading::statuses() as $status)
                <option value="{{$status}}" {{ (old('status', $reading->status ?? '') == $status) ? 'selected': ''}}>@lang('site.proof_reading_statuses.'.$status)</option>
            @endforeach
        </select>
        {!! $errors->first('status', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
