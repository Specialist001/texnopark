@extends('admin.layout')

@section('center_content')
    <form action="{{ route('admin.proof-readings.update', $reading) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        @component('component.card', ['title' => trans('admin.nav.proof_readings').': '.trans('admin.editing')])
            @include('admin.readings._form')
            @slot('bottom')
                <a href="{{ route('admin.proof-readings.index') }}" class="btn btn-sm btn-danger float-left">@lang('admin.back')</a>
                <button class="btn btn-sm btn-primary float-right">@lang('admin.save')</button>
                <div class="clearfix"></div>
            @endslot
        @endcomponent
    </form>
@endsection
