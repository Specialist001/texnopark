@extends('admin.layout')

@section('center_content')
    @component('component.card', ['title' => trans('admin.nav.proof_readings'), 'bodyClass' => 'card-body-no-padding'])
        @slot('buttons')
            <a href="{{ route('admin.proof-readings.index') }}" class="btn btn-sm btn-primary ml-2">
                <span class="d-none d-sm-inline-block">@lang('admin.create')</span> <i class="icmn-plus"><!-- --></i>
            </a>
        @endslot

        @if($readings->isNotEmpty())
            <div class="table-responsive">
                <table class="table">
                    <thead class="thead-default">
                        <tr>
                            @if(\Auth::user()->isAdmin())
                                <th>@lang('admin.accepted_user')</th>
                            @endif
                            <th>@lang('site.proof_reading_attributes.type')</th>
                            <th>@lang('site.proof_reading_attributes.name')</th>
                            <th>@lang('site.proof_reading_attributes.email')</th>
                            <th>@lang('site.proof_reading_attributes.phone')</th>
                            <th>@lang('site.proof_reading_attributes.status')</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($readings as $item)
                        <tr class="{{ $item->is_moderated == 0 ? 'new_row': '' }}">
                            @if(\Auth::user()->isAdmin())
                                <td>{{ $item->user ? $item->user->name : trans('admin.not_accepted') }}</td>
                            @endif
                            <td>{{ trans('site.proof_reading_types.'.$item->type) }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->email }}</td>
                            <td>{{ $item->phone ?? '==' }}</td>
                            <td>{{ trans('site.proof_reading_statuses.'.$item->status) }}</td>
                            <td>
                                <a href="{{ route('admin.proof-readings.edit', $item) }}" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="@lang('admin.edit')">
                                    <i class="icmn-pencil"></i>
                                </a>

                                @if(\Auth::user()->isAdmin())
                                <form action="{{ route('admin.proof-readings.destroy', $item) }}" class="d-inline-block" method="post">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-sm btn-danger"
                                            onclick="return confirm('@lang('admin.destroy_confirm')');"
                                            data-toggle="tooltip" data-placement="top" title="@lang('admin.destroy')">
                                        <i class="icmn-cross"></i>
                                    </button>
                                    @endif
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif

        @slot('bottom')
            @include('common.pagination', ['data' => $readings])
        @endslot
    @endcomponent
@endsection
