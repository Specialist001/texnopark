@extends('admin.layout')

@section('center_content')
    @component('component.card', ['title' => trans('admin.nav.responds'), 'bodyClass' => 'card-body-no-padding'])

        @if($responds->isNotEmpty())
            <div class="table-responsive">
                <table class="table">
                    <thead class="thead-default">
                    <tr>
                        <th>@lang('admin.name')</th>
                        <th>@lang('admin.phone')</th>
                        <th>@lang('admin.email')</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($responds as $respond)
                        <tr>
                            <td class="{{ $respond->is_moderated == 0 ? 'new_row': '' }}">{{ $respond->name }}</td>
                            <td>{{ $respond->phone }}</td>
                            <td>{{ $respond->email }}</td>
                            <td>
                                <a href="{{ route('admin.responds.show', $respond) }}" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="@lang('admin.show')">
                                    <i class="icmn-eye"></i>
                                </a>
                                <form action="{{ route('admin.responds.destroy', $respond) }}" class="d-inline-block" method="post">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-sm btn-danger"
                                            onclick="return confirm('@lang('admin.destroy_confirm')');"
                                            data-toggle="tooltip" data-placement="top" title="@lang('admin.destroy')">
                                        <i class="icmn-cross"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif

        @slot('bottom')
            @include('common.pagination', ['data' => $responds])
        @endslot
    @endcomponent
@endsection
