@extends('admin.layout')

@section('center_content')
    @component('component.card', ['title' => trans('admin.respond')])

        <table class="table table-striped">
            <tr>
                <td>@lang('admin.name')</td>
                <td>{{ $respond->name }}</td>
            </tr>
            <tr>
                <td>@lang('admin.phone')</td>
                <td>{{ $respond->phone }}</td>
            </tr>
            <tr>
                <td>@lang('admin.email')</td>
                <td>{{ $respond->email }}</td>
            </tr>
            <tr>
                <td>@lang('admin.comment')</td>
                <td>{{ $respond->comment }}</td>
            </tr>
        </table>

        @slot('bottom')
            <a href="{{ route('admin.responds.index') }}" class="btn btn-sm btn-danger float-left">@lang('admin.back')</a>
            <div class="clearfix"></div>
        @endslot
    @endcomponent
    @component('component.card', ['title' => trans('admin.theme')])

        <div class="theme_title">
            {{ $respond->theme->title }}
        </div>
        <div class="theme_user_name">
            {{ $respond->theme->organization_title ?? '' }}
        </div>
        <div class="theme_user_name">
            {{ $respond->theme->user->name }}
            <span>
                {{ $respond->theme->created_at->format('d.m.Y, H:i') }}
            </span>
        </div>
        <div class="theme_short">
            {{ nl2br($respond->theme->text) }}
        </div>
        @slot('bottom')
            <a href="{{ route('admin.forum.edit', $respond->theme->id) }}" class="btn btn-sm btn-primary float-left">@lang('admin.go_to_respond')</a>
            <div class="clearfix"></div>
        @endslot
    @endcomponent
@endsection
