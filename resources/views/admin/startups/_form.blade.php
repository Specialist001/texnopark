<div class="form-group row {!! $errors->first('name', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="name">@lang('admin.name')</label>
    <div class="col-md-6">
        <input type="text" name="name" class="form-control input-sm" id="name" value="{{ old('name', $user->name ?? '') }}" required autofocus>
        {!! $errors->first('name', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
<div class="form-group row {!! $errors->first('username', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="username">@lang('admin.username')</label>
    <div class="col-md-6">
        <input type="text" name="username" class="form-control input-sm" id="username" value="{{ old('username', $user->username ?? '') }}" required>
        {!! $errors->first('username', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
<div class="form-group row {!! $errors->first('email', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="email">@lang('admin.email')</label>
    <div class="col-md-6">
        <input type="email" name="email" class="form-control input-sm" id="email" value="{{ old('email', $user->email ?? '') }}" required>
        {!! $errors->first('email', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-md-right col-form-label-sm" for="verified">@lang('admin.is_verified')</label>
    <div class="col-md-6">
        <div class="btn-group btn-group-sm" data-toggle="buttons">
            <label class="btn btn-outline-success {{ old('verified', isset($user) && $user->hasVerifiedEmail() ? 1: 0) == 1? 'active': '' }}">
                <input type="radio" name="verified" value="1" {{ old('verified', $user->verified ?? 1) == 1? 'checked': '' }} required>
                @lang('admin.verified')
            </label>
            <label class="btn btn-outline-danger {{ old('verified', isset($user) && $user->hasVerifiedEmail() ? 1: 0) == 0? 'active': '' }}">
                <input type="radio" name="verified" value="0" {{ old('verified', $user->verified ?? 1) == 0? 'checked': '' }} required>
                @lang('admin.not_verified')
            </label>
        </div>
    </div>
</div>

<div class="form-group row {!! $errors->first('city_id', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="city_id">@lang('auth.city')</label>
    <div class="col-md-6">
        <select name="city_id" id="city_id" class="form-control input-sm" required>
            @foreach(\App\Domain\Cities\Models\City::withTranslation()->get() as $city)
                <option value="{{ $city->id }}" @if(old('city_id', $user->city_id ?? 0) == $city->id) selected @endif>{{ $city->name }}</option>
            @endforeach
        </select>
        {!! $errors->first('city_id', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>


<div class="form-group row {!! $errors->first('phone', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="phone">@lang('auth.phone')</label>
    <div class="col-md-6">
        <input type="text" name="phone" class="form-control phone-input input-sm" id="phone" value="{{ old('phone', $user->phone ?? '') }}" required >
        {!! $errors->first('phone', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('site', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="site">@lang('auth.site')</label>
    <div class="col-md-6">
        <input type="text" name="site" class="form-control input-sm" id="site" value="{{ old('site', $user->site ?? '') }}"  >
        {!! $errors->first('site', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
<div class="form-group row {!! $errors->first('personality', 'has-danger')!!}">
    <div class="col-md-6 offset-md-3">
        <div class="form-group text-center row">
            <div class="col-md-4">
                <label class="custom-control  custom-radio cur-pointer  m-0">
                    <span class="custom-control-description">@lang('auth.legal')</span>
                    <input type="radio" required
                           @if(old('personality', $user->personality ?? 'legal') == 'legal' )
                           checked
                           @endif
                           name="personality" value="legal" class="custom-control-input">
                    <span class="custom-control-indicator"></span>
                </label>
            </div>
            <div class="col-md-4">
                <label class="custom-control  custom-radio cur-pointer  m-0">
                    <span class="custom-control-description">@lang('auth.individual')</span>
                    <input type="radio" required
                           @if(old('personality', $user->personality ?? 'legal') == 'individual' )
                           checked
                           @endif
                           name="personality" value="individual" class="custom-control-input">
                    <span class="custom-control-indicator"></span>
                </label>
            </div>
            <div class="col-md-12 text-center">
                {!! $errors->first('personality', '<small class="form-control-feedback">:message</small>') !!}
            </div>
        </div>
    </div>
</div>

<div class="form-group row {!! $errors->first('locale', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="locale">@lang('portal.user_locale')</label>
    <div class="col-md-6">
        <select name="locale" id="locale" class="form-control input-sm" required>
            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                <option value="{{ $localeCode }}" @if(old('locale', $user->locale ?? '') == $localeCode) selected @endif>@lang('admin.locales.'.$localeCode)</option>
            @endforeach
        </select>
        {!! $errors->first('locale', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('password', 'has-danger')!!}" >
    <label class="col-md-3 text-md-right col-form-label-sm" for="password">@lang('auth.password')</label>
    <div class="col-md-6">
        <input type="password" name="password" class="form-control input-sm password" id="password" {{ isset($user->id) ? '': 'required' }}>
        {!! $errors->first('password', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 text-md-right col-form-label-sm" for="active">@lang('admin.status')</label>
    <div class="col-md-6">
        {{--<select name="active" id="active" class="form-control input-sm">--}}
            {{--<option value="1">@lang('common.active')</option>--}}
            {{--<option value="0">@lang('common.not_active')</option>--}}
        {{--</select>--}}
        <div class="btn-group btn-group-sm" data-toggle="buttons">
            <label class="btn btn-outline-success {{ old('active', $user->active ?? 1) == 1? 'active': '' }}">
                <input type="radio" name="active" value="1" {{ old('active', $user->active ?? 1) == 1? 'checked': '' }} required>
                @lang('admin.active')
            </label>
            <label class="btn btn-outline-danger {{ old('active', $user->active ?? 1) == 0? 'active': '' }}">
                <input type="radio" name="active" value="0" {{ old('active', $user->active ?? 1) == 0? 'checked': '' }} required>
                @lang('admin.not_active')
            </label>
        </div>
    </div>
</div>
{{--<div class="form-group row">--}}
    {{--<label class="col-md-3 text-md-right col-form-label-sm" for="verified">@lang('common.verification')</label>--}}
    {{--<div class="col-md-6">--}}
        {{--<select name="verified" id="verified" class="form-control input-sm">--}}
            {{--<option value="1">@lang('common.verified')</option>--}}
            {{--<option value="0">@lang('common.not_verified')</option>--}}
        {{--</select>--}}
        {{--<div class="btn-group btn-group-sm" data-toggle="buttons">--}}
            {{--<label class="btn btn-outline-success {{ old('verified', $user->hasVerifiedPhone() ? 1: 0) == 1? 'active': '' }}">--}}
                {{--<input type="radio" name="verified" value="1" {{ old('verified', $user->hasVerifiedPhone() ? 1: 0) == 1? 'checked': '' }} required>--}}
                {{--@lang('common.verified')--}}
            {{--</label>--}}
            {{--<label class="btn btn-outline-danger {{ old('verified', $user->hasVerifiedPhone() ? 1: 0) == 0? 'active': '' }}">--}}
                {{--<input type="radio" name="verified" value="0" {{ old('verified', $user->hasVerifiedPhone() ? 1: 0) == 0? 'checked': '' }} required>--}}
                {{--@lang('common.not_verified')--}}
            {{--</label>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

@component('component.password-stacks')@endcomponent
@component('component.phone-stacks')@endcomponent
