@extends('admin.layout')

@section('center_content')
    <form action="{{ route('admin.startups.store') }}" method="post">
        @csrf
        @component('component.card', ['title' => trans('admin.nav.startups').': '.trans('admin.creating')])
            @include('admin.startups._form')
            @slot('bottom')
                <a href="{{ route('admin.startups.index') }}" class="btn btn-sm btn-danger float-left">@lang('admin.back')</a>
                <button class="btn btn-sm btn-primary float-right">@lang('admin.create')</button>
                <div class="clearfix"></div>
            @endslot
        @endcomponent
    </form>
@endsection
