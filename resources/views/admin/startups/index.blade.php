@extends('admin.layout')

@section('center_content')
    @component('component.card', ['title' => trans('admin.nav.startups'), 'bodyClass' => 'card-body-no-padding'])
        @slot('buttons')
            <a href="{{ route('admin.startups.create') }}" class="btn btn-sm btn-primary ml-2">
                <span class="d-none d-sm-inline-block">@lang('admin.create')</span> <i class="icmn-plus"><!-- --></i>
            </a>
        @endslot

        <div id="filters">
            <form action="{{ route('admin.startups.index') }}" method="get">
                <div class="row">
                    <div class="col-md-12">
                        <p>
                            <strong>@lang('admin.filters')</strong>
                        </p>
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <div class="form-group">
                            <label for="id">
                                <small>@lang('admin.id')</small>
                            </label>
                            <input class="form-control input-sm" name="id" id="id" type="number" step="1" min="1" value="{{ $filters['id'] ?? '' }}"/>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <div class="form-group">
                            <label for="username">
                                <small>@lang('admin.username')</small>
                            </label>
                            <input class="form-control input-sm" name="username" id="username" type="text"  value="{{ $filters['username'] ?? '' }}"/>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <div class="form-group">
                            <label for="email">
                                <small>@lang('admin.email')</small>
                            </label>
                            <input class="form-control input-sm" name="email" id="email" type="text"  value="{{ $filters['email'] ?? '' }}"/>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="form-group">
                            <label for="phone">
                                <small>@lang('admin.phone')</small>
                            </label>
                            <input class="form-control input-sm" name="phone" id="phone" type="text"  value="{{ $filters['phone'] ?? '' }}"/>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="form-group">
                            <label for="name">
                                <small>@lang('admin.name')</small>
                            </label>
                            <input class="form-control input-sm" name="name" id="name" type="text"  value="{{ $filters['name'] ?? '' }}"/>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <div class="form-group">
                            <label for="active">
                                <small>@lang('admin.status')</small>
                            </label>
                            <select name="active" id="active" class="form-control input-sm">
                                <option value=""></option>
                                <option value="1" {{ (isset($filters['active']) && $filters['active'] == 1) ? 'selected': ''}}>@lang('admin.active')</option>
                                <option value="0" {{ (isset($filters['active']) && $filters['active'] == 0) ? 'selected': ''}}>@lang('admin.not_active')</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label for="sort">
                                <small>@lang('admin.sort')</small>
                            </label>
                            <select name="sort" id="sort" class="form-control input-sm">
                                <option value=""></option>
                                <option value="id" {{ (isset($filters['sort']) && $filters['sort'] == 'id') ? 'selected': ''}}>@lang('admin.id') (@lang('admin.asc'))</option>
                                <option value="-id" {{ (isset($filters['sort']) && $filters['sort'] == '-id') ? 'selected': ''}}>@lang('admin.id') (@lang('admin.desc'))</option>

                                <option value="name" {{ (isset($filters['sort']) && $filters['sort'] == 'name') ? 'selected': ''}}>@lang('admin.name') (@lang('admin.asc'))</option>
                                <option value="-name" {{ (isset($filters['sort']) && $filters['sort'] == '-name') ? 'selected': ''}}>@lang('admin.name') (@lang('admin.desc'))</option>

                                <option value="username" {{ (isset($filters['sort']) && $filters['sort'] == 'username') ? 'selected': ''}}>@lang('admin.username') (@lang('admin.asc'))</option>
                                <option value="-username" {{ (isset($filters['sort']) && $filters['sort'] == '-username') ? 'selected': ''}}>@lang('admin.username') (@lang('admin.desc'))</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="float-right">
                            <a href="{{ route('admin.startups.index') }}" class="btn btn-sm btn-danger">@lang('admin.filters_reset')</a>
                            <button class="btn btn-sm btn-success">@lang('admin.filters_apply')</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </form>
        </div>

        @if($users->isNotEmpty())
            <div class="table-responsive">
                <table class="table">
                    <thead class="thead-default">
                        <tr>
                            <th>@lang('admin.id')</th>
                            <th>@lang('admin.username')</th>
                            <th>@lang('admin.email')</th>
                            <th>@lang('admin.phone')</th>
                            <th>@lang('admin.name')</th>
                            <th>@lang('admin.status')</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->email }}
                                <span class="small text-muted d-block">
                                    {{ $user->hasVerifiedEmail() ? trans('admin.verified'): trans('admin.not_verified') }}
                                </span>
                            </td>
                            <td>{{ $user->phone ?? '--' }}</td>
                            <td>{{ $user->name }}</td>
                            <td>
                                {!! $user->active? '<span class="badge badge-success">'.trans('admin.active').'</span>': '<span class="badge badge-danger">'.trans('admin.not_active').'</span>' !!}
                            </td>
                            <td>
                                <a href="{{ route('admin.startups.edit', $user) }}" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="@lang('admin.edit')">
                                    <i class="icmn-pencil"></i>
                                </a>
                                <form action="{{ route('admin.startups.destroy', $user) }}" class="d-inline-block" method="post">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-sm btn-danger"
                                            onclick="return confirm('@lang('admin.destroy_foreign_confirm')');"
                                            data-toggle="tooltip" data-placement="top" title="@lang('admin.destroy')">
                                        <i class="icmn-cross"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif

        @slot('bottom')
            @include('common.pagination', ['data' => $users])
        @endslot
    @endcomponent
@endsection
