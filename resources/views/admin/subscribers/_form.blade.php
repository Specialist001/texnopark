<div class="form-group row {!! $errors->first('name', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="name">@lang('admin.name')</label>
    <div class="col-md-6">
        <input type="text" name="name" class="form-control input-sm" id="name" value="{{ old('name', $subscriber->name ?? '') }}" required>
        {!! $errors->first('name', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
<div class="form-group row {!! $errors->first('email', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="email">@lang('admin.email')</label>
    <div class="col-md-6">
        <input type="email" name="email" class="form-control input-sm" id="email" value="{{ old('email', $subscriber->email ?? '') }}" required>
        {!! $errors->first('email', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-md-right col-form-label-sm" for="status">@lang('admin.status')</label>
    <div class="col-md-6">
        {{--<select name="active" id="active" class="form-control input-sm">--}}
        {{--<option value="1">@lang('common.active')</option>--}}
        {{--<option value="0">@lang('common.not_active')</option>--}}
        {{--</select>--}}
        <div class="btn-group btn-group-sm" data-toggle="buttons">
            <label class="btn btn-outline-success {{ old('status', $subscriber->status ?? 1) == 1? 'active': '' }}">
                <input type="radio" name="status" value="1" {{ old('status', $subscriber->status ?? 1) == 1? 'checked': '' }} required>
                @lang('admin.active')
            </label>
            <label class="btn btn-outline-danger {{ old('status', $subscriber->status ?? 1) == 0? 'active': '' }}">
                <input type="radio" name="status" value="0" {{ old('status', $subscriber->status ?? 1) == 0? 'checked': '' }} required>
                @lang('admin.not_active')
            </label>
        </div>
    </div>
</div>
