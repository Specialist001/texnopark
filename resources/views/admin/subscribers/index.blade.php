@extends('admin.layout')

@section('center_content')
    @component('component.card', ['title' => trans('admin.nav.subscribers'), 'bodyClass' => 'card-body-no-padding'])
        @slot('buttons')
            <a href="{{ route('admin.subscribers.create') }}" class="btn btn-sm btn-primary ml-2">
                <span class="d-none d-sm-inline-block">@lang('admin.create')</span> <i class="icmn-plus"><!-- --></i>
            </a>
            <a href="{{ route('admin.subscribers.mailing.form') }}" class="btn btn-sm btn-success ml-2">
                <span class="d-none d-sm-inline-block">@lang('admin.mailing')</span> <i class="icmn-mail2"><!-- --></i>
            </a>
        @endslot

        <div id="filters">
            <form action="{{ route('admin.subscribers.index') }}" method="get">
                <div class="row">
                    <div class="col-md-12">
                        <p>
                            <strong>@lang('admin.filters')</strong>
                        </p>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="form-group">
                            <label for="name">
                                <small>@lang('admin.name')</small>
                            </label>
                            <input class="form-control input-sm" name="name" id="name" type="text"  value="{{ $filters['name'] ?? '' }}"/>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="form-group">
                            <label for="name">
                                <small>@lang('admin.email')</small>
                            </label>
                            <input class="form-control input-sm" name="email" id="email" type="text"  value="{{ $filters['email'] ?? '' }}"/>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-2">
                        <div class="form-group">
                            <label for="status">
                                <small>@lang('admin.status')</small>
                            </label>
                            <select name="status" id="status" class="form-control input-sm">
                                <option value=""></option>
                                <option value="1" {{ (isset($filters['status']) && $filters['status'] == 1) ? 'selected': ''}}>@lang('admin.active')</option>
                                <option value="0" {{ (isset($filters['status']) && $filters['status'] == 0) ? 'selected': ''}}>@lang('admin.not_active')</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="float-right">
                            <a href="{{ route('admin.subscribers.index') }}" class="btn btn-sm btn-danger">@lang('admin.filters_reset')</a>
                            <button class="btn btn-sm btn-success">@lang('admin.filters_apply')</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </form>
        </div>

        @if($subscribers->isNotEmpty())
            <div class="table-responsive">
                <table class="table">
                    <thead class="thead-default">
                        <tr>
                            <th>@lang('admin.name')</th>
                            <th>@lang('admin.email')</th>
                            <th>@lang('admin.status')</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($subscribers as $item)
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->email }}</td>
                            <td>{{ $item->status ? trans('admin.active'): trans('admin.not_active') }}</td>
                            <td>
                                <a href="{{ route('admin.subscribers.edit', $item) }}" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="@lang('common.edit')">
                                    <i class="icmn-pencil"></i>
                                </a>
                                <form action="{{ route('admin.subscribers.destroy', $item) }}" class="d-inline-block" method="post">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-sm btn-danger"
                                            onclick="return confirm('@lang('admin.destroy_confirm')');"
                                            data-toggle="tooltip" data-placement="top" title="@lang('admin.destroy')">
                                        <i class="icmn-cross"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif

        @slot('bottom')
            @include('common.pagination', ['data' => $subscribers])
        @endslot
    @endcomponent
@endsection
