@extends('admin.layout')

@section('center_content')
    <form action="{{ route('admin.subscribers.mailing.send') }}" method="post" enctype="multipart/form-data">
        @csrf
        @component('component.card', ['title' => trans('admin.mailing')])
            {{--@include('admin.subscribers._form')--}}


            <div class="form-group row {!! $errors->first('subject', 'has-danger')!!}">
                <label class="col-md-3 text-md-right col-form-label-sm" for="subject">@lang('admin.subject')</label>
                <div class="col-md-6">
                    <input type="text" name="subject" class="form-control input-sm" id="subject" value="{{ old('subject', $user->subject ?? '') }}" required>
                    {!! $errors->first('subject', '<small class="form-control-feedback">:message</small>') !!}
                </div>
            </div>
        
            <?php $value = old('content', '') ?>
            <div class="form-group row {!! $errors->first('content', 'has-danger')!!}">
                <label class="col-md-3 text-md-right col-form-label-sm" for="content">@lang('admin.content')</label>
                <div class="col-md-6">
                    <textarea name="content" class="form-control text-editor " rows="10" id="content" >{{ $value }}</textarea>
                    {!! $errors->first('content', '<small class="form-control-feedback">:message</small>') !!}
                </div>
            </div>

            <div class="form-group row {!! $errors->first('subscribers', 'has-danger')!!}">
                <div class="col-md-6 offset-md-3">
                    <div class="input-group d-inline-block">
                        <label class="custom-control  custom-checkbox cur-pointer mr-0 d-inline-block">
                            <span class="custom-control-description">@lang('admin.subscribers')</span>
                            <input type="checkbox"  name="subscribers" value="1" class="custom-control-input" {{ (old('subscribers', 1) == 1) ? 'checked': ''}}>
                            <span class="custom-control-indicator"></span>
                        </label>
                    </div>
                    <div class="">
                        {!! $errors->first('subscribers', '<small class="form-control-feedback">:message</small>') !!}
                    </div>
                </div>
            </div>

            <div class="form-group row {!! $errors->first('users', 'has-danger')!!}">
                <div class="col-md-6 offset-md-3">
                    <div class="input-group d-inline-block">
                        <label class="custom-control  custom-checkbox cur-pointer mr-0 d-inline-block">
                            <span class="custom-control-description">@lang('admin.users')</span>
                            <input type="checkbox"  name="users" value="1" class="custom-control-input" {{ (old('users', 1) == 1) ? 'checked': ''}}>
                            <span class="custom-control-indicator"></span>
                        </label>
                    </div>
                    <div class="">
                        {!! $errors->first('users', '<small class="form-control-feedback">:message</small>') !!}
                    </div>
                </div>
            </div>

        @slot('bottom')
                <a href="{{ route('admin.subscribers.index') }}" class="btn btn-sm btn-danger float-left">@lang('admin.back')</a>
                <button class="btn btn-sm btn-primary float-right">@lang('admin.create')</button>
                <div class="clearfix"></div>
            @endslot
        @endcomponent
    </form>
@endsection

@component('component.tiny-mc')@endcomponent
