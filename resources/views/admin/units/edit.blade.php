@extends('admin.layout')

@section('center_content')
    <form action="{{ route('admin.units.update', $category) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        @component('component.card', ['title' => trans('admin.nav.units').': '.trans('admin.editing')])
            @include('admin.units._form')
            @slot('bottom')
                <a href="{{ route('admin.units.index') }}" class="btn btn-sm btn-danger float-left">@lang('admin.back')</a>
                <button class="btn btn-sm btn-primary float-right">@lang('admin.save')</button>
                <div class="clearfix"></div>
            @endslot
        @endcomponent
    </form>
@endsection

@push('scripts')
    <script>
        $(function () {
            $('#delete_image').on('click', function () {
                $.ajax({
                    method: "DELETE",
                    url: "{{ route('admin.units.destroy.icon') }}/{{$category->id}}",
                    success: function(response) {
                        if(response.result === "success") {
                            $('#image').remove();
                            return false;
                        } else {
                            alert("{{trans('admin.ajax_error')}}");
                        }
                    },
                    error: function(response) {
                        alert("{{trans('admin.ajax_error')}}");
                    }
                });
            });
        });
    </script>
@endpush
@push('styles')
    <style>
        #image {
            max-width: 300px;
        }
    </style>
@endpush
