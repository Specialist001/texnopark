
<div class="form-group row {!! $errors->first('title', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="title">@lang('admin.title')</label>
    <div class="col-md-6">
        <input type="text" name="title" class="form-control input-sm" id="title" value="{{ old('title', $video->title ?? '') }}" required>
        {!! $errors->first('title', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
<div class="form-group row {!! $errors->first('link', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="link">@lang('admin.link')</label>
    <div class="col-md-6">
        <input type="text" name="link" class="form-control input-sm" id="link" value="{{ old('link', $video->link ?? '') }}" required>
        {!! $errors->first('link', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('order', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="order">@lang('admin.order')</label>
    <div class="col-md-6">
        <input type="number" step="1" min="0" name="order" class="form-control input-sm" id="order" value="{{ old('order', $video->order ?? 0) }}"  >
        {!! $errors->first('order', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
