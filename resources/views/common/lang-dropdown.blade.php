<div class="dropdown lang-dropdown">
    <a id="dLabel" class="no-decoration dropdown-toggle cur-pointer"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{ strtoupper(LaravelLocalization::getCurrentLocale()) }}
    </a>
    <div class="dropdown-menu dropdown-menu-lang" >
        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
            @if($localeCode != LaravelLocalization::getCurrentLocale())
                <a class="dropdown-item" rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                    {{ strtoupper($localeCode) }}
                </a>
            @endif
        @endforeach
    </div>
</div>
