<section class="custom-bvi py-2" style="display: none" id="bvi-panel">
    <ul class="list-unstyled d-block text-center mr-auto">
        <li class="d-inline-block bvi mr-2">
            <p class="bvi">
                @lang('site.font_size')
            </p>
            <div class="btn-group">
                <button class="btn btn-default bvi bvi-font cur-pointer" data-operator="minus">
                    A-
                </button>
                <button class="btn btn-default bvi bvi-font cur-pointer" data-operator="unset">
                    A
                </button>
                <button class="btn btn-default bvi bvi-font cur-pointer" data-operator="plus">
                    A+
                </button>
            </div>
        </li>
        <li class="d-inline-block bvi mr-2">
            <p class="bvi">
                @lang('site.body')
            </p>
            <div class="btn-group">
                <button class="btn btn-default bvi bvi-body cur-pointer {{ Request::cookie("body") == 'unset' || Request::cookie("body") == '' ? 'bg-blue': '' }}" data-filter="unset">
                    @lang('site.body_unset')
                </button>
                <button class="btn btn-default bvi bvi-body cur-pointer {{ Request::cookie("body") == 'grayscale(100%)'? 'bg-blue': '' }}" data-filter="grayscale(100%)">
                    @lang('site.body_grey')
                </button>
            </div>
        </li>
        <li class="d-inline-block bvi mr-2">
            <p class="bvi">
                @lang('site.img')
            </p>
            <div class="btn-group">
                <button title="@lang('site.img_unset')" class="btn btn-default bvi bvi-img  {{ Request::cookie("img") == 'unset' || Request::cookie("img") == '' ? 'bg-blue': '' }} cur-pointer " data-filter="unset">
                    <i class="icmn-image bvi text-blue"></i>
                </button>
                <button title="@lang('site.img_grey')" class="btn btn-default bvi bvi-img cur-pointer {{ Request::cookie("img") == 'grayscale(100%)' ? 'bg-blue': '' }}"  data-filter="grayscale(100%)">
                    <i class="icmn-image bvi" style="filter: grayscale(100%)"></i>
                </button>
                <button title="@lang('site.img_off')" class="btn btn-default bvi bvi-img cur-pointer {{ Request::cookie("img") == 'off' ? 'bg-blue': '' }}"  data-filter="off">
                    <i class="icmn-cross bvi"></i>
                </button>
            </div>
        </li>
        <li class="d-inline-block bvi ">
            <p class="bvi">
                @lang('site.voice')
            </p>
            <div class="btn-group">
                <button class="btn btn bvi-voice bvi {{ Request::cookie("voice-on") == 'on' ? 'voice-on': '' }} cur-pointer" data-status="on">
                    <i class="icmn-volume-high  bvi"></i>
                </button>
                <button class="btn btn-default bvi bvi-voice {{ Request::cookie("voice-on") != 'on' ? 'voice-on': '' }} cur-pointer" data-status="off">
                    <i class="icmn-volume-mute2 bvi"></i>
                </button>
            </div>
        </li>
    </ul>
</section>
@push('scripts')
    <script type="text/javascript" src="{{ asset('vendor/bvi/js/responsivevoice.js') }}"></script>
    <script>
        function getSelectionText() {
            var text = "";
            if (window.getSelection) {
                text = window.getSelection().toString();
                // for Internet Explorer 8 and below. For Blogger, you should use &amp;&amp; instead of &&.
            } else if (document.selection && document.selection.type != "Control") {
                text = document.selection.createRange().text;
            }
            return text;
        }

        $(document).ready(function (){ // when the document has completed loading
            $(document).mouseup(function (e){ // attach the mouseup event for all div and pre tags
                if ($('.voice-on').data('status') === "on") {
                    setTimeout(function() { // When clicking on a highlighted area, the value stays highlighted until after the mouseup event, and would therefore stil be captured by getSelection. This micro-timeout solves the issue.
                        responsiveVoice.cancel(); // stop anything currently being spoken
                        responsiveVoice.speak(getSelectionText(), "Russian Female"); //speak the text as returned by getSelectionText
                    }, 1);
                }
            });
        });

        var fsz_scale = {{ Request::cookie("fsz_size", 0) }};

        function setFont(operator) {
            if(operator === 'plus') {
                if(fsz_scale >= 5) return false;
                $('p, li, span, i, b, strong, h1, h2, h3, h4, h5, h6, em, button, a').each(function() {
                    var orgSize = $(this).css("font-size");
                    var increaseFont = parseInt(orgSize) + 2;
                    if(!$(this).hasClass('bvi')) {
                        $(this).css("font-size", increaseFont);
                    }
                })
                fsz_scale++;
            } else if(operator === 'minus') {
                if(fsz_scale <= 0) return false;
                $('p, li, span, i, b, strong, h1, h2, h3, h4, h5, h6, em, button, a').each(function() {
                    var orgSize = $(this).css("font-size");
                    var increaseFont = parseInt(orgSize) - 2;
                    if(!$(this).hasClass('bvi')) {
                        $(this).css("font-size", increaseFont);
                    }
                })
                fsz_scale--;
            } else {
                $('p, li, span, i, b, strong, h1, h2, h3, h4, h5, h6, em, button, a').each(function() {
                    var orgSize = parseInt($(this).css("font-size")) - fsz_scale * 2;
                    if(!$(this).hasClass('bvi')) {
                        $(this).css("font-size", orgSize);
                    }
                })
                fsz_scale = 0;
            }
        }

        function setBody(filter)
        {
            $('body').css('filter', filter)
        }
        function setImg(filter)
        {
            if (filter === 'off') {
                $('img').hide();
            } else {
                $('img').show();
                $('img').css('filter', filter);
            }
        }

        $('.bvi-body').on('click', function () {
            setBody($(this).data('filter'));
            $.cookie('body', $(this).data('filter'));
            $('.bvi-body').removeClass('bg-blue');
            $(this).addClass('bg-blue');
        });
        $('.bvi-font').on('click', function () {
            setFont($(this).data('operator'));
            $.cookie('fsz_size', fsz_scale);
        });

        $('.bvi-img').on('click', function () {
            setImg($(this).data('filter'));
            $.cookie('img', $(this).data('filter'));
            $('.bvi-img').removeClass('bg-blue');
            $(this).addClass('bg-blue');
        });

        if ($.cookie('img') != undefined || $.cookie('img') != '') {
            setImg($.cookie('img'));
        }

        if ($.cookie('body') != undefined || $.cookie('body') != '') {
            setBody($.cookie('body'));
        }

        if ($.cookie('fsz_size') != undefined || $.cookie('fsz_size') != '') {
            for(var i = 0; i < fsz_scale; i++) {
                $('p, li, span, i, b, strong, h1, h2, h3, h4, h5, h6, em, button, a').each(function() {
                    var orgSize = $(this).css("font-size");
                    var increaseFont = parseInt(orgSize) + 2;
                    if(!$(this).hasClass('bvi')) {
                        $(this).css("font-size", increaseFont);
                    }
                })
            }
        }



        $('.bvi-voice').on('click', function () {
            $('.voice-on').removeClass('voice-on');
            $.cookie('voice-on', $(this).data('status'));
            $(this).addClass('voice-on');
        });

    </script>
@endpush
