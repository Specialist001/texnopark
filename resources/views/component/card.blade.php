<section class="card">
    <div class="card-header">
        <span class="cat__core__title">
            <strong>{!! $title ?? '' !!}</strong>
            @if(isset($buttons))
            <span class="float-right">
            {!! $buttons ?? '' !!}
            </span>
            <span class="clearfix"></span>
            @endif
        </span>
    </div>
    <div class="card-body {{ $bodyClass ?? '' }}">
        {{ $slot }}
    </div>
    @if(isset($bottom))
    <div class="card-footer">
        {!! $bottom ?? '' !!}
    </div>
    @endif
</section>
