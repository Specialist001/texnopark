@push('scripts')
    <script src="https://api-maps.yandex.ru/2.1/?apikey=b9ce5d4c-250e-4b9d-8b23-a55bb49bd314&lang=ru_RU"
            type="text/javascript">
    </script>

    <script type="text/javascript">
        // Функция ymaps.ready() будет вызвана, когда
        // загрузятся все компоненты API, а также когда будет готово DOM-дерево.
        ymaps.ready(init);
        function init(){
            //
            @if(isset($model) && ($model->geo_lat && $model->geo_lng))
                var myLatLng = [{{ $model->geo_lat }}, {{ $model->geo_lng }}];
                var zoom = 16;
            @else
                var myLatLng = [40.78206, 72.34424];
                var zoom = 12;
            @endif

            // Создание карты.
            var myMap = new ymaps.Map("{{$target ?? 'map_wrapper'}}", {
                // Координаты центра карты.
                // Порядок по умолчанию: «широта, долгота».
                // Чтобы не определять координаты центра карты вручную,
                // воспользуйтесь инструментом Определение координат.
                center: myLatLng,
                // Уровень масштабирования. Допустимые значения:
                // от 0 (весь мир) до 19.
                zoom: zoom,
                // control.
                controls: []
            });

            myMap.controls.add('fullscreenControl');
            myMap.controls.add('zoomControl');
            myMap.behaviors.disable('scrollZoom');


            var myPlacemark = new ymaps.Placemark(myLatLng, {}, {
                draggable: true,
                preset: 'islands#redIcon',
            });

            myPlacemark.events.add([
                'dragend'
            ], function (e) {
                var placemarkPosition = myMap.options.get('projection').fromGlobalPixels(
                    myMap.converter.pageToGlobal(e.get('position')),
                    myMap.getZoom()
                );

                if (placemarkPosition.length === 2) {
                    $('#geo_lat').val(placemarkPosition[0]);
                    $('#geo_lng').val(placemarkPosition[1]);
                }
            });


            myMap.geoObjects.add(myPlacemark);
        }
    </script>

@endpush
@push('styles')
    <style>
        #{{$target ?? 'map_wrapper'}} {
            width: 100%;
            height: 400px;
        }
    </style>
@endpush
