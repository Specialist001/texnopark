@push('scripts')
    <script type="text/javascript"
            src="{{ asset('vendor/inputmask/jquery.inputmask.bundle.js') }}"></script>
    <script>
        $(function () {
            @if(isset($all) && $all == true)
            $('.phone-input').inputmask("+\\9\\9\\8 (99) 999-99-99");
            @else
            $('.phone-input').inputmask("+\\9\\9\\8 (99) 999-99-99");
            @endif
        });

    </script>
@endpush
