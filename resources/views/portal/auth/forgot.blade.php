@extends('portal.layout')

@section('title')
    @lang('portal.reset')
@endsection

@section('center_content')
    <section class="mb-4 ">
        <div class="row">
            <div class="col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                @if (session()->has('success'))
                    @component('component.alert', ['type' => 'success'])
                        {{ session('success') }}
                    @endcomponent
                @endif
                @if (session()->has('warning'))
                    @component('component.alert', ['type' => 'warning'])
                        {{ session('warning') }}
                    @endcomponent
                @endif
                @if (count($errors) > 0 || session()->has('danger'))
                    @component('component.alert', ['type' => 'danger'])
                        <div>
                            {{ session('danger') }}
                        </div>
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    @endcomponent
                @endif

                <div class="sp-card p-5 project-card">
                    <div class="form-group">
                        <form id="form-validation" name="form-validation" action="{{ route('portal.auth.forgot') }}"
                              method="POST">
                            @csrf
                            <div class="form-group text-center">
                                <input id="email" placeholder="@lang('auth.email')" class="form-control d-inline-block" autofocus required value="{{ old('email', '') }}"
                                       name="email" type="email">
                            </div>
                            <div class="form-actions text-center">
                                <button type="submit" class="d-inline-block btn cur-pointer btn-submit ">@lang('auth.restore')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@component('component.password-stacks')@endcomponent
