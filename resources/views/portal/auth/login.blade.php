@extends('portal.layout')

@section('title')
    @lang('portal.authorization')
@endsection

@section('center_content')
    <section class="mb-4 ">
        <div class="row">
            <div class="col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                @if (session()->has('success'))
                    @component('component.alert', ['type' => 'success'])
                        {{ session('success') }}
                    @endcomponent
                @endif
                @if (session()->has('warning'))
                    @component('component.alert', ['type' => 'warning'])
                        {{ session('warning') }}
                    @endcomponent
                @endif
                @if (count($errors) > 0 || session()->has('danger'))
                    @component('component.alert', ['type' => 'danger'])
                        <div>
                            {{ session('danger') }}
                        </div>
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    @endcomponent
                @endif

                <div class="sp-card p-5 project-card">
                    <div class="form-group">
                        <form id="form-validation" name="form-validation" action="{{ route('portal.auth.login') }}"
                              method="POST">
                            @csrf
                            <div class="form-group text-center">
                                <input id="username" placeholder="@lang('auth.username')" class="form-control d-inline-block" autofocus required value="{{ old('username', '') }}"
                                       name="username" type="text">
                            </div>
                            <div class="form-group text-center">
                                <div class="input-append input-group d-inline-block"><input
                                        class="form-control password"
                                        name="password" required  placeholder="@lang('auth.password')"
                                        id="password"
                                        type="password">
                                </div>
                                <br>
                                <div class="text-right input-group d-inline-block">
                                    <a href="{{ route('portal.auth.forgotForm') }}">
                                        @lang('auth.reset')
                                    </a>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <label class="custom-control  custom-checkbox cur-pointer d-inline-block mr-0">
                                    <span class="custom-control-description">@lang('auth.remember')</span>
                                    <input type="checkbox" name="remember" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </div>
                            <div class="form-actions text-center">
                                <button type="submit" class="d-inline-block btn cur-pointer btn-submit mb-3">@lang('auth.enter')</button>
                            </div>
                            <div class="text-center">
                                <a href="{{ route('portal.auth.registerForm') }}">
                                    @lang('portal.registration')
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@component('component.password-stacks')@endcomponent
