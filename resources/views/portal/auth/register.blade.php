@extends('portal.layout')

@section('title')
    @lang('portal.registration')
@endsection

@section('center_content')


    <section class="mb-4 ">

        <div class="row">
            <div class="col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3">


                <form id="form-validation" name="form-validation" action="{{ route('portal.auth.register') }}" method="POST">
                @if (session()->has('success'))
                    @component('component.alert', ['type' => 'success'])
                        {{ session('success') }}
                    @endcomponent
                @endif
                @if (session()->has('warning'))
                    @component('component.alert', ['type' => 'warning'])
                        {{ session('warning') }}
                    @endcomponent
                @endif

                @if (session()->has('danger'))
                    @component('component.alert', ['type' => 'danger'])
                        {{ session('danger') }}
                    @endcomponent
                @endif

                <div class="alert alert-light mx-0 px-5 text-center">
                    @lang('portal.registration_notify')
                </div>

                <div class="form-group text-center row">
                        <div class="col-md-6">
                            <label class="custom-control  custom-radio cur-pointer  m-0">
                                <span class="custom-control-description">@lang('portal.investor')</span>
                                <input type="radio" required
                                       @if(old('role', 'investor') == 'investor' )
                                       checked
                                       @endif
                                       name="role" value="investor" class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>
                        <div class="col-md-6">
                            <label class="custom-control  custom-radio cur-pointer  m-0">
                                <span class="custom-control-description">@lang('portal.startup')</span>
                                <input type="radio" required
                                       @if(old('role', 'investor') == 'startup' )
                                       checked
                                       @endif
                                       name="role" value="startup" class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>
                        <div class="col-md-12 text-center">
                            {!! $errors->first('role', '<small class="form-control-feedback">:message</small>') !!}
                        </div>
                    </div>


                <div class="sp-card p-5 project-card">
                    <div class="form-group">
                            @csrf
                            <div class="form-group text-center">
                                <input id="username" placeholder="@lang('auth.username')" class="form-control d-inline-block" autofocus required value="{{ old('username', '') }}"
                                       name="username" type="text">
                                <div class="text-center">
                                    {!! $errors->first('username', '<small class="form-control-feedback">:message</small>') !!}
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <div class="input-append input-group d-inline-block"><input
                                        class="form-control password"
                                        name="password" required  placeholder="@lang('auth.password')"
                                        id="password"
                                        type="password">
                                </div>
                                <div class="text-center">
                                    {!! $errors->first('password', '<small class="form-control-feedback">:message</small>') !!}
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <div class="input-append input-group d-inline-block"><input
                                        class="form-control password"
                                        name="password_confirmation" required  placeholder="@lang('auth.password_confirm')"
                                        id="password_confirmation"
                                        type="password">
                                </div>
                                <div class="text-center">
                                    {!! $errors->first('password_confirmation', '<small class="form-control-feedback">:message</small>') !!}
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <input id="email" placeholder="@lang('auth.email')" class="form-control d-inline-block" required value="{{ old('email', '') }}"
                                       name="email" type="email">
                                <div class="text-center">
                                    {!! $errors->first('email', '<small class="form-control-feedback">:message</small>') !!}
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <input id="name" placeholder="@lang('auth.name')" class="form-control d-inline-block" required value="{{ old('name', '') }}"
                                       name="name" type="text">
                                <div class="text-center">
                                    {!! $errors->first('name', '<small class="form-control-feedback">:message</small>') !!}
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <input id="phone" placeholder="@lang('auth.phone')" class="form-control phone-input d-inline-block" required value="{{ old('phone', '') }}"
                                       name="phone" type="text">
                                <div class="text-center">
                                    {!! $errors->first('phone', '<small class="form-control-feedback">:message</small>') !!}
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <input id="site" placeholder="@lang('auth.site')" class="form-control d-inline-block"  value="{{ old('site', '') }}"
                                       name="site" type="text">
                                <div class="text-center">
                                    {!! $errors->first('site', '<small class="form-control-feedback">:message</small>') !!}
                                </div>
                            </div>
                            <div class="form-group text-center row">
                                <div class="col-md-6">
                                    <label class="custom-control  custom-radio cur-pointer  m-0">
                                        <span class="custom-control-description">@lang('auth.legal')</span>
                                        <input type="radio" required
                                               @if(old('personality', 'legal') == 'legal' )
                                               checked
                                               @endif
                                               name="personality" value="legal" class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="custom-control  custom-radio cur-pointer  m-0">
                                        <span class="custom-control-description">@lang('auth.individual')</span>
                                        <input type="radio" required
                                               @if(old('personality', 'legal') == 'individual' )
                                               checked
                                               @endif
                                               name="personality" value="individual" class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                    </label>
                                </div>
                                <div class="col-md-12 text-center">
                                    {!! $errors->first('personality', '<small class="form-control-feedback">:message</small>') !!}
                                </div>
                            </div>

                            <div class="form-group text-center">
                                <select name="city_id" id="city_id" class="form-control d-inline-block" required>
                                    @foreach(\App\Domain\Cities\Models\City::withTranslation()->get() as $city)
                                        <option value="{{ $city->id }}">{{ $city->name }}</option>
                                    @endforeach
                                </select>
                                <div class="text-center">
                                    {!! $errors->first('city_id', '<small class="form-control-feedback">:message</small>') !!}
                                </div>
                            </div>

                            <div class="form-group text-center">
                                <div class="input-group d-inline-block text-left  ">
                                    <label class="custom-control  custom-checkbox cur-pointer mr-0 d-inline-block small text-muted">
                                        <span class="custom-control-description">@lang('portal.accept_agreement')</span>
                                        <input type="checkbox" required name="accept" value="yes" class="custom-control-input" checked>
                                        <span class="custom-control-indicator"></span>
                                    </label>
                                    <div class="text-center">
                                        {!! $errors->first('accept', '<small class="form-control-feedback">:message</small>') !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-actions text-center">
                                <button type="submit" class="d-inline-block btn cur-pointer btn-submit mb-3">@lang('auth.register')</button>
                            </div>

                            <div class="text-center">
                                <a href="{{ route('portal.auth.loginForm') }}">
                                    @lang('portal.authorization')
                                </a>
                            </div>
                    </div>
                </div>


                </form>
            </div>
        </div>
    </section>
@endsection

@component('component.phone-stacks')@endcomponent
@component('component.password-stacks')@endcomponent
