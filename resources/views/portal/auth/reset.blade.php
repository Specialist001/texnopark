@extends('portal.layout')

@section('title')
    @lang('portal.restore_password')
@endsection

@section('center_content')
    <section class="mb-4 ">
        <div class="row">
            <div class="col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                @if (session()->has('success'))
                    @component('component.alert', ['type' => 'success'])
                        {{ session('success') }}
                    @endcomponent
                @endif
                @if (session()->has('warning'))
                    @component('component.alert', ['type' => 'warning'])
                        {{ session('warning') }}
                    @endcomponent
                @endif
                @if (count($errors) > 0 || session()->has('danger'))
                    @component('component.alert', ['type' => 'danger'])
                        <div>
                            {{ session('danger') }}
                        </div>
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    @endcomponent
                @endif

                <div class="sp-card p-5 project-card">
                    <div class="form-group">
                        <form id="form-validation" name="form-validation" action="{{ route('portal.auth.reset') }}"
                              method="POST">
                            @csrf
                            <input type="hidden" name="email" value="{{ $email ?? '' }}">
                            <input type="hidden" name="token" value="{{ $token ?? '' }}">

                            <div class="form-group ">
                                <input id="password" placeholder="@lang('auth.new_password')" class="form-control password " autofocus required
                                       name="password" type="password">
                            </div>

                            <div class="form-group ">
                                <input id="password_confirmation" placeholder="@lang('auth.password_confirmation')" class="form-control password " required
                                       name="password_confirmation" type="password">
                            </div>
                            <div class="form-actions text-center">
                                <button type="submit" class="d-inline-block btn cur-pointer btn-submit ">@lang('auth.save')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@component('component.password-stacks')@endcomponent
