@extends('portal.layout')

@section('title')
    @lang('portal.not_verified_title')
@endsection

@section('center_content')
    <section class="mb-4 ">
        <div class="row">
            <div class="col-12">
                <div class="alert alert-header mx-0 px-5">
                    <p>
                        @lang('portal.not_verified_text')
                    </p>
                    <p class="small text-muted">
                        @lang('portal.not_verified_notice')
                    </p>
                    <p>
                        <a href="{{ route('portal.auth.verify.resend') }}" class="alert-link">
                            @lang('portal.resend_verify')
                        </a>
                    </p>
                    @if (session()->has('resent'))
                        <p class="text">
                            @lang('portal.resent_verify')
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection

