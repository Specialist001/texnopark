@extends('portal.layout')

@section('center_content')
    <div class="text-center background-404">
        <div class="w-50 d-inline-block pt-5 pb-5 mt-5 mb-5">
            <h1 class="mb-4"><strong>@lang('errors.404.title')</strong></h1>
            <p class="mb-4">@lang('errors.404.description')</p>
            <a href="{{ route('portal.home') }}" class="btn btn-link">@lang('errors.404.home')</a>
        </div>
    </div>
@endsection
