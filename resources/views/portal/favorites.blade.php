@extends('portal.layout')

@section('center_content')

    @include('portal.profile.menu')
    @include('portal.profile.investor-submenu')

    @foreach($favorites_list->chunk(2) as $chunks)
        <div class="row">
            @foreach($chunks as $favorite)
                @php
                $project = $favorite->project;
                @endphp
                <div class="col-lg-6">
                    <a href="{{ route('portal.project', $project->id) }}">
                        <div class="sp-card mb-3 p-4 project-card p-relative">
                            @if(Auth::check() && Auth::user()->isInvestor())
                                <i class="icmn-star-full fav_icon cur-pointer" data-project="{{$project->id}}"></i>
                            @endif
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="project_card-title my-4">
                                        @if(mb_strlen($project->title) > 50)
                                            {{ mb_substr($project->title, 0, 50).'...' }}
                                        @else
                                            {{ $project->title }}
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3  ">
                                    <div class="mb-4 text-center">
                                        <img src="{{ $project->logoUrl() }}" alt="{{ $project->title }}"
                                             class="img-fluid project_image d-inline-block">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="project_card-invested">
                                        {{ $project->calcInvested() }}% @lang('portal.invested')
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: {{ $project->calcInvested() }}%" aria-valuenow="{{ $project->calcInvested() }}" aria-valuemin="0" aria-valuemax="{{ $project->calcInvested() }}"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="project_card-short_description">
                                        <p class="title mb-1">
                                            @lang('portal.short_description'):
                                        </p>
                                        <p class="short_description" style="min-height: 120px;">
                                            @if(mb_strlen($project->short_description) > 200)
                                                {{ mb_substr($project->short_description, 0, 200).'...' }}
                                            @else
                                                {{ $project->short_description }}
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="project_card-hr my-3"></div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="project_card-short_description">
                                        <p class="title mb-1">
                                            @lang('portal.category_id'):
                                        </p>
                                        <p class="short_description">
                                            {{ $project->category->name }}
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="project_card-short_description">
                                        <p class="title mb-1">
                                            @lang('portal.state_readiness'):
                                        </p>
                                        <p class="short_description">
                                            {{ $project->state }}
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="project_card-hr my-3"></div>

                            <div class="float-md-right mt-2">
                                <a href="{{ route('portal.project', $project->id) }}" class="btn btn-additional-filters  px-4 pr-2 cur-pointer mb-2 text-capitalize">
                                    @lang('portal.more')
                                </a>
                                <a href="{{ (Auth::check()) ? route('portal.project', $project->id)."#invest" : route('portal.auth.loginForm') }}" class="btn btn-apply-filters   px-4 pr-2 cur-pointer mb-2 text-capitalize">@lang('portal.invest')</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    @endforeach

    <section class="mb-4">
        @include('site.pagination', ['data' => $favorites_list])
    </section>

@endsection

@push('scripts')
    <script>
        $(function () {
            $('.fav_icon').on('click', function () {
                var btn = $(this);
                var id = btn.data('project');
                $.ajax({
                    method: "PATCH",
                    url: "{{ route('portal.favorites.add') }}/" + id,
                    success: function(response) {
                        if(response.result === "success") {
                            if (btn.hasClass('icmn-star-empty')) {
                                btn.removeClass('icmn-star-empty');
                                btn.addClass('icmn-star-full');
                            } else {
                                btn.removeClass('icmn-star-full');
                                btn.addClass('icmn-star-empty');
                            }
                            return false;
                        } else {
                            alert("{{trans('admin.ajax_error')}}");
                        }
                    },
                    error: function(response) {
                        alert("{{trans('admin.ajax_error')}}");
                    }
                });

                return false;
            });
        });
    </script>
@endpush
