<div class="container">
    <div class="row">
        <div class="col-md-4 text-center text-md-left">

            <a  href="{{ route('portal.home') }}">
                <img src="{{ asset('images/logo_white.png') }}" class="d-inline-block mb-2 mb-md-0 img-fluid align-top" alt="{{ config('app.name') }}">
            </a>
        </div>
        <div class="col-md-8">
            <div class="contacts-block">
                <span class="contacts text-center text-md-left d-block mb-3">
                    @lang('portal.contacts')
                </span>
            </div>
            <div class="row">
                    {{--@if(isset($contact['phone']) && $contact['phone']->value != '')--}}
                    <div class="col-lg-4 text-center text-md-left">
                        <div class="d-inline-block mb-2 mb-lg-0 phone ">
                            <img src="{{ asset('images/i_phone.png') }}" class="mr-1" alt="."> +998(71) 203-32-23
                        </div>
                    </div>
                    {{--@endif--}}
                    @if(isset($contact['email']) && $contact['email']->value != '')
                            <div class="col-lg-4 text-center text-md-left">
                        <div class="d-inline-block mb-2 mb-lg-0 email">
                            <img src="{{ asset('images/i_email.png') }}" class="mr-1" alt="."> {!! nl2br($contact['email']->value) !!}
                        </div>
                            </div>
                    @endif
                    @if(isset($contact['address_'.app()->getLocale()]) && $contact['address_'.app()->getLocale()]->value != '')
                            <div class="col-lg-4 text-center text-md-left">
                        <div class="d-inline-block mb-2 mb-lg-0 address">
                            <img src="{{ asset('images/i_map.png') }}" class="mr-1" alt="."> {!! nl2br($contact['address_'.app()->getLocale()]->value) !!}
                        </div>
                            </div>
                    @endif
                </div>
        </div>
    </div>
    <div class="hr"></div>
    <div class="row">
        <div class="col-md-4">

            <div class="copyright mb-2 mb-md-0 text-center text-md-left">
                &copy; {{ date('Y') }} @lang('portal.copyright')
            </div>

        </div>
        <div class="col-md-4">
            <ul class="list-unstyled text-center text-md-right my-0">
                @if(isset($contact['facebook']) && $contact['facebook']->value != '')
                <li class="d-inline-block mb-2 mb-md-0">
                    <a href="{{ $contact['facebook']->value }}" class="social_link mr-4" target="_blank">
                        <img src="{{ asset('images/i_fb.png') }}" class="mr-2" alt="@lang('portal.facebook')"> @lang('portal.facebook')
                    </a>
                </li>
                @endif
                @if(isset($contact['telegram']) && $contact['telegram']->value != '')
                <li class="d-inline-block mb-2 mb-md-0">
                    <a href="{{ $contact['telegram']->value }}" class="social_link" target="_blank">
                        <img src="{{ asset('images/i_tg.png') }}" class="mr-2" alt="@lang('portal.telegram')"> @lang('portal.telegram')
                    </a>
                </li>
                    @endif
            </ul>
        </div>
        <div class="col-md-4 text-center text-md-right ">

            <div class="powered_by">
                @lang('portal.qwerty')
            </div>
        </div>
    </div>
</div>
