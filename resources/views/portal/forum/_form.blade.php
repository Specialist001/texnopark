<div class="form-group row {!! $errors->first('organization_title', 'has-danger')!!}">
    <div class="col-md-12">
        <input type="text" placeholder="@lang('forum.organization_title')"  name="organization_title" class="form-control input-sm" id="organization_title" value="{{ old('organization_title', $theme->organization_title ?? '') }}" >
        {!! $errors->first('organization_title', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
<div class="form-group row {!! $errors->first('title', 'has-danger')!!}">
    <div class="col-md-12">
        <input type="text"  placeholder="@lang('forum.title')" name="title" class="form-control input-sm" id="title" value="{{ old('title', $theme->title ?? '') }}" required >
        {!! $errors->first('title', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('text', 'has-danger')!!}">
    <div class="col-md-12">
        <textarea name="text" placeholder="@lang('forum.text')" id="text" required class="form-control input-sm">{{ old('text', $theme->text ?? '') }}</textarea>
        {!! $errors->first('text', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
<div class="form-group row {!! $errors->first('category', 'has-danger')!!}">
    <div class="col-md-12">
        <input type="text"  placeholder="@lang('forum.category')" name="category" class="form-control input-sm" id="category" value="{{ old('category', $theme->category ?? '') }}" >
        {!! $errors->first('category', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('required_solution', 'has-danger')!!}">
    <div class="col-md-12">
        <textarea name="required_solution" placeholder="@lang('forum.required_solution')" id="required_solution" required class="form-control input-sm">{{ old('required_solution', $theme->required_solution ?? '') }}</textarea>
        {!! $errors->first('required_solution', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>


<div class="form-group row {!! $errors->first('expected_effect', 'has-danger')!!}">
    <div class="col-md-12">
        <textarea name="expected_effect" placeholder="@lang('forum.expected_effect')" id="expected_effect" required class="form-control input-sm">{{ old('expected_effect', $theme->expected_effect ?? '') }}</textarea>
        {!! $errors->first('expected_effect', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
<div class="form-group row {!! $errors->first('required_organization', 'has-danger')!!}">
    <div class="col-md-12">
        <input type="text"  placeholder="@lang('forum.required_organization')" name="required_organization" class="form-control input-sm" id="required_organization" value="{{ old('required_organization', $theme->required_organization ?? '') }}" >
        {!! $errors->first('required_organization', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('contacts', 'has-danger')!!}">
    <div class="col-md-12">
        <textarea name="contacts" placeholder="@lang('forum.contacts')" id="contacts" required class="form-control input-sm">{{ old('contacts', $theme->contacts ?? '') }}</textarea>
        {!! $errors->first('contacts', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>


<div class="form-group row {!! $errors->first('file', 'has-danger')!!}">
    <div class="col-md-12">
        @if(isset($forumTheme) && $forumTheme->file)
            <p id="file_attached">
                <a href="{{$forumTheme->fileUrl()}}" class="text-white d-block mb-2" target="_blank">@lang('admin.file')</a>
                <small id="delete_file" class="text-white cur-pointer" data-delete="{{ route('portal.forum.destroy.file', $forumTheme) }}"><i class="icmn-cross"></i> @lang('admin.destroy')</small>
            </p>
        @endif
        <input type="file" name="file" class="form-control input-sm" id="file" >
        {!! $errors->first('file', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
