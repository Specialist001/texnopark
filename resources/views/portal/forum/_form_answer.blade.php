@if(!Auth::check())
    <div class="form-group row {!! $errors->first('user_name', 'has-danger')!!}">
        <label class="col-md-3 text-md-right col-form-label-sm" for="user_name">@lang('portal.user_name')</label>
        <div class="col-md-6">
            <input type="text" id="user_name" class="form-control" name="user_name" required>
            {!! $errors->first('user_name', '<small class="form-control-feedback">:message</small>') !!}
        </div>
    </div>
@endif
<div class="form-group row {!! $errors->first('text', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="text">@lang('admin.text')</label>
    <div class="col-md-6">
        <textarea name="text" id="text" required class="form-control input-sm">{{ old('text', $answer->text ?? '') }}</textarea>
        {!! $errors->first('text', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('file', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="file">
        @lang('admin.theme_file')
    </label>
    <div class="col-md-6">
        <input type="file" name="file" class="form-control input-sm" id="file" >
        {!! $errors->first('file', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
