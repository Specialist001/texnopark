<div class="form-group row {!! $errors->first('name', 'has-danger')!!}">
    <div class="col-md-12 text-center">
        <input type="text" id="name" class="form-control d-inline-block" name="name" placeholder="@lang('portal.name')" value="{{ Auth::check() ? Auth::user()->name : '' }}" required>
        {!! $errors->first('name', '<small class="form-control d-inline-block-feedback">:message</small>') !!}
    </div>
</div>
<div class="form-group row {!! $errors->first('phone', 'has-danger')!!}">
    <div class="col-md-12 text-center">
        <input type="text" id="phone" class="form-control d-inline-block phone-input" placeholder="@lang('portal.phone')" required name="phone" value="{{ Auth::check() ? Auth::user()->phone : '' }}" >
        {!! $errors->first('phone', '<small class="form-control d-inline-block-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('email', 'has-danger')!!}">
    <div class="col-md-12 text-center">
        <input type="email" id="email" class="form-control d-inline-block" placeholder="@lang('portal.email')" name="email" value="{{ Auth::check() ? Auth::user()->email : '' }}" >
        {!! $errors->first('email', '<small class="form-control d-inline-block-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('comment', 'has-danger')!!}">
    <div class="col-md-12 text-center">
        <textarea name="comment" placeholder="@lang('portal.comment')" id="comment" required class="form-control d-inline-block input-sm">{{ old('comment', '') }}</textarea>
        {!! $errors->first('comment', '<small class="form-control d-inline-block-feedback">:message</small>') !!}
    </div>
</div>
@component('component.phone-stacks')@endcomponent
