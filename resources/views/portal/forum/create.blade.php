@extends('admin.layout')

@section('center_content')
    <form action="{{ route('admin.forum.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        @component('component.card', ['title' => trans('admin.nav.forum').': '.trans('admin.creating')])
            @include('admin.forum._form')
            @slot('bottom')
                <a href="{{ route('admin.forum.index') }}" class="btn btn-sm btn-danger float-left">@lang('admin.back')</a>
                <button class="btn btn-sm btn-primary float-right">@lang('admin.create')</button>
                <div class="clearfix"></div>
            @endslot
        @endcomponent
    </form>
@endsection
