<form action="{{ route('portal.forum.update', $forumTheme) }}" method="post" enctype="multipart/form-data">
    @include('portal.forum._form')
    <div class="">
        <button  class="btn btn-submit btn-sm float-right">@lang('portal.save')</button>
        <div class="clearfix"></div>
    </div>
</form>
