@extends('portal.layout')

@section('title')
    @lang('portal.portal_forum')
@endsection

@section('center_content')
    <div class="row">
        <div class="col-lg-9">
            <section class="mb-4">
                <div id="filters" class="mb-3">
                    <form action="{{ route('portal.forum.index') }}" method="get" id="forum_search">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group p-relative">
                                    <input class="form-control forum-input input-sm" placeholder="@lang('portal.theme_filter')" style="max-width: 100%!important;" name="title" id="title" type="text"   value="{{ $filters['title'] ?? '' }}"/>
                                    <img src="{{ asset('images/i_forum_search.png') }}" alt="." class="forum_search_button">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                @if (session()->has('success'))
                    @component('component.alert', ['type' => 'success'])
                        {{ session('success') }}
                    @endcomponent
                @endif
                @if (session()->has('verified'))
                    @component('component.alert', ['type' => 'success'])
                        @lang('portal.verified')
                    @endcomponent
                @endif
                @if (session()->has('warning'))
                    @component('component.alert', ['type' => 'warning'])
                        {{ session('warning') }}
                    @endcomponent
                @endif
                @if (session()->has('danger'))
                    @component('component.alert', ['type' => 'danger'])
                        {{ session('danger') }}
                    @endcomponent
                @endif

                @if($my_themes)
                <div class="mb-3">
                    <h3 style="color: #434343">@lang('portal.my_themes')</h3>
                    @foreach($my_themes as $theme)

                        <a href="{{ route('portal.forum.show', $theme) }}">
                            <div class="sp-card mb-2 p-4">
                                <div class="theme_title mb-2">
                                    <div class="float-left ">
                                        {{ $theme->title }}
                                    </div>

                                    <div class="float-right ">
                                        <div class="theme_icons ">
                                            <i class="icmn-bubble2 text-success"></i> {{ $theme->answers_count }} <i class="icmn-eye text-success ml-3"></i> {{ $theme->view }}
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="theme_user_name mb-4">
                                    @if($theme->organization_title != '')
                                        {{ $theme->organization_title }}
                                    @else
                                        {{ $theme->user->name }}
                                    @endif
                                    <span>
                                    {{ $theme->created_at->format('d.m.Y, H:i') }}
                                </span>
                                </div>
                                <div class="theme_short">
                                    @if(mb_strlen($theme->text) > 500)
                                        {{ mb_substr($theme->text, 0, 500).'...' }}
                                    @else
                                        {{ $theme->text }}
                                    @endif
                                </div>
                                <div class="text-muted mt-2">
                                    @lang('portal.theme_statuses.'.$theme->status)
                                </div>
                                <div class="mt-2">
                                    <a data-url="{{ route('portal.forum.edit', $theme) }}" data-update="{{ route('portal.forum.update', $theme) }}"  data-toggle="modal" data-target="#edit_theme" class="text-success mr-2 cur-pointer" >
                                        <i class="icmn-pencil text-success"></i> @lang('admin.edit')
                                    </a>
                                    <form action="{{ route('portal.forum.destroy', $theme) }}" class="d-inline-block" method="post">
                                        @csrf
                                        @method('delete')
                                        <button role="button" class="btn btn-sm btn-danger"
                                                onclick="return confirm('@lang('admin.destroy_foreign_confirm')');">
                                            <i class="icmn-bin text-white"></i>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>

                    <div class="modal fade" id="edit_theme" tabindex="-1" role="dialog" aria-labelledby="edit_themeLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <form id="edit_theme_form" method="post" enctype="multipart/form-data">
                                @csrf
                                @method('put')
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="edit_themeLabel">@lang('portal.theme_edit')</h5>
                                        <button type="button" class="close cur-pointer" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p class="text-white small">
                                            @lang('portal.theme_edit_notice')
                                        </p>
                                        <div class="form"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                @endif

                <div class="mb-3">

                    @if($my_themes)
                        <h3 style="color: #434343">@lang('portal.other_themes')</h3>
                    @endif

                    @foreach($themes as $theme)

                            <a href="{{ route('portal.forum.show', $theme) }}">
                                <div class="sp-card mb-2 p-4">
                                    <div class="theme_title mb-2">
                                        <div class="float-left ">
                                            {{ $theme->title }}
                                        </div>

                                        <div class="float-right ">
                                            <div class="theme_icons ">
                                                <i class="icmn-bubble2 text-success"></i> {{ $theme->answers_count }} <i class="icmn-eye text-success ml-3"></i> {{ $theme->view }}
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="theme_user_name mb-4">
                                        @if($theme->organization_title != '')
                                            {{ $theme->organization_title }}
                                        @else
                                            {{ $theme->user->name }}
                                        @endif
                                        <span>
                                    {{ $theme->created_at->format('d.m.Y, H:i') }}
                                </span>
                                    </div>
                                    <div class="theme_short">
                                        @if(mb_strlen($theme->text) > 500)
                                            {{ mb_substr($theme->text, 0, 500).'...' }}
                                        @else
                                            {{ $theme->text }}
                                        @endif
                                    </div>
                                </div>
                            </a>
                    @endforeach

                    @include('site.pagination', ['data' => $themes])
                </div>
            </section>
        </div>
        <div class="col-lg-3">
            <div class="mb-3">
                <a href="{{ (!Auth::check()) ? route('portal.auth.loginForm') :'' }}" @if(Auth::check()) data-toggle="modal" data-target="#add_theme" @endif>
                    <div class="add_theme_button">
                        <div class="d-inline-block add_theme_button-icon">
                            <img src="{{ asset('images/i_add_theme.png') }}" alt=".">
                        </div><div class="d-inline-block add_theme_button-text ">
                            @lang('portal.theme_add')
                            <span class="d-block">
                            @lang('portal.theme_add_text')
                            </span>
                        </div>
                    </div>
                </a>
            </div>
            @if(Auth::check())
                <!-- Modal -->
                    <div class="modal fade" id="add_theme" tabindex="-1" role="dialog" aria-labelledby="add_themeLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <form action="{{ route('portal.forum.store') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="add_themeLabel">@lang('portal.theme_add')</h5>
                                        <button type="button" class="close cur-pointer" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        @include('portal.forum._form')
                                    </div>
                                    <div class="modal-footer">
                                        <button  class="btn btn-submit btn-sm">@lang('portal.add')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
            @endif
            @include('portal.forum.right')
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(function () {
            $('.forum_search_button').on('click', function () {
                $('#forum_search').submit();
            });
        })
    </script>
@endpush

@push('scripts')
    <script>
        $(function () {
            $('#edit_theme').on('click', '#delete_file', function () {
                var delete_url = $(this).data('delete')
                $.ajax({
                    method: "DELETE",
                    url: delete_url,
                    success: function(response) {
                        if(response.result === "success") {
                            $('#file_attached').remove();
                            return false;
                        } else {
                            alert("{{trans('admin.ajax_error')}}");
                        }
                    },
                    error: function(response) {
                        alert("{{trans('admin.ajax_error')}}");
                    }
                });
            });
            $('#edit_theme').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var url = button.data('url') // Extract info from data-* attributes
                var update = button.data('update') // Extract info from data-* attributes

                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                $.ajax({
                    method: "GET",
                    url: url,
                    success: function(response) {
                        modal.find('#edit_theme_form').attr('action', update);
                        modal.find('.form').html(response);
                        return false;
                    },
                    error: function(response) {
                        alert("{{trans('admin.ajax_error')}}");
                    }
                });

            })
            $('#edit_theme').on('hide.bs.modal', function (event) {
                var modal = $(this)
                modal.find('.form').html('');

            })
        });
    </script>
@endpush
