<div class="row mb-4">
    @if(isset($contact['facebook']) && $contact['facebook']->value != '')
    <div class="col-md-6  text-center text-lg-left mb-1 ">
        <a href="{{ $contact['facebook']->value }}" target="_blank">
            <div class="fb_left_button d-inline-block text-left">
                <div class="d-inline-block fb_left_button-icon">
                    <img src="{{ asset('images/i_fb_left.png') }}" alt="." class="img-fluid">
                </div><div class="d-inline-block fb_left_button-text ">
                    @lang('portal.facebook')
                </div>
            </div>
        </a>
    </div>
    @endif
    @if(isset($contact['telegram']) && $contact['telegram']->value != '')
    <div class="col-md-6  text-center text-lg-right">
        <a href="{{ $contact['telegram']->value }}" target="_blank">
            <div class="tg_left_button d-inline-block text-left">
                <div class="d-inline-block tg_left_button-icon">
                    <img src="{{ asset('images/i_tg_left.png') }}" alt=".">
                </div><div class="d-inline-block tg_left_button-text ">
                    @lang('portal.telegram')
                </div>
            </div>
        </a>
    </div>
    @endif
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="">
            <div class="mb-4">
                <div class="hot_themes-title d-inline-block ">
                    @lang('portal.themes_count')
                </div><div class="hot_themes-count text-center d-inline-block">
                    {{ $themes_count ?? 0 }}
                    <span class="d-block">
                @choice('portal.themes_count_by_count', $themes_count ?? 0)
            </span>
                </div>
            </div>
        </div>
    </div>
</div>

@if($popular->isNotEmpty())
<div class="row">
    <div class="col-lg-12">
        <div class="hot_themes_header mb-2">
            <div class="hot_themes_header-icon d-inline-block mr-2">
                <img src="{{ asset('images/i_hot_themes.png') }}" alt="." class="img-fluid">
            </div>
            <div class="hot_themes_header-text d-inline-block">
                @lang('portal.hot_themes')
            </div>
        </div>
        <div class="">
            @foreach($popular as $theme)
                <a href="{{ route('portal.forum.show', $theme) }}">
                    <div class="mb-2">

                        <div class="hot_themes-title d-inline-block ">
                            {{ $theme->title }}
                        </div><div class="hot_themes-count text-center d-inline-block">
                            {{ $theme->answers_count }}
                            <span class="d-block">
                        @choice('portal.answers_by_count', $theme->answers_count)
                    </span>
                        </div>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
</div>
@endif
