@extends('portal.layout')

@section('title')
    @lang('portal.portal_forum')
@endsection

@section('center_content')


    <div class="row">
        <div class="col-lg-9">

            @if (session()->has('success'))
                @component('component.alert', ['type' => 'success'])
                    {{ session('success') }}
                @endcomponent
            @endif
            @if (session()->has('verified'))
                @component('component.alert', ['type' => 'success'])
                    @lang('portal.verified')
                @endcomponent
            @endif
            @if (session()->has('warning'))
                @component('component.alert', ['type' => 'warning'])
                    {{ session('warning') }}
                @endcomponent
            @endif
            @if (session()->has('danger'))
                @component('component.alert', ['type' => 'danger'])
                    {{ session('danger') }}
                @endcomponent
            @endif

            <section class="mb-4">
                <div class="sp-card">
                    <div class="py-5 px-4">
                            <div class="theme_title mb-2">
                                <div class="float-left ">
                                    {{ $theme->title }}
                                </div>

                                <div class="float-right ">
                                    <div class="theme_icons ">
                                        <i class="icmn-bubble2 text-success"></i> {{ $answers_count }} <i class="icmn-eye text-success ml-3"></i> {{ $theme->view }}
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="theme_user_name mb-4">
                                @if($theme->organization_title != '')
                                    {{ $theme->organization_title }}
                                @else
                                    {{ $theme->user->name }}
                                @endif
                                <span>
                                    {{ $theme->created_at->format('d.m.Y, H:i') }}
                                </span>
                            </div>
                        <hr>
                            <div class="theme_short">
                                <p>
                                    {!! nl2br($theme->text) !!}
                                </p>
                                @if($theme->category != '')
                                    <p class="">
                                        <strong>@lang('forum.category')</strong> <span class="d-block">{{ $theme->category }}</span>
                                    </p>
                                @endif
                                @if($theme->required_solution != '')
                                    <p class="">
                                        <strong>@lang('forum.required_solution')</strong> <span class="d-block">{!! nl2br($theme->required_solution) !!}</span>
                                    </p>
                                @endif
                                @if($theme->expected_effect != '')
                                    <p class="">
                                        <strong>@lang('forum.expected_effect')</strong> <span class="d-block">{!! nl2br($theme->expected_effect) !!}</span>
                                    </p>
                                @endif
                                @if($theme->required_organization != '')
                                    <p class="">
                                        <strong>@lang('forum.required_organization')</strong> <span class="d-block">{{ $theme->required_organization }}</span>
                                    </p>
                                @endif
                                @if($theme->contacts != '')
                                    <p class="">
                                        <strong>@lang('forum.contacts')</strong> <span class="d-block">{!! nl2br($theme->contacts) !!}</span>
                                    </p>
                                @endif

                            </div>
                        @if($theme->file)
                            <a href="{{$theme->fileUrl()}}" target="_blank">@lang('portal.theme_file_attached_show')</a>
                        @endif
                    </div>
                    @if($answers->isNotEmpty())
                        <div class="answers_heading py-2 px-4 mb-4">
                            @lang('portal.answers')
                        </div>
                        @foreach($answers as $ans)

                            <div class="py-3 px-4">
                                    <div class="theme_user_name">
                                        {{ $ans->user_id ? $ans->user->name: $ans->user_name }}
                                        <br>
                                        <span>
                            {{ $ans->created_at->format('d.m.Y, H:i') }}
                        </span>
                                    </div>
                                    <div class="answer_text p-4 ml-4 mb-2">
                                        {!! nl2br($ans->text) !!}
                                    </div>
                                    @if($ans->file)
                                        <div class=" ml-4 mb-2">
                                            <i class="icmn-file-empty text-success"></i> <a href="{{$ans->fileUrl()}}" class="mr-2" target="_blank">@lang('admin.theme_file_attached')</a>
                                            @if(Auth::check() && Auth::user()->id == $ans->user_id)
                                                    <a href="{{ route('portal.forum.answer.destroy.file', $ans) }}" class=" text-danger " data-toggle="tooltip" data-placement="top" title="@lang('admin.destroy')">
                                                        <i class="icmn-cross text-danger small"></i>
                                                    </a>
                                            @endif
                                        </div>
                                    @endif

                                @if(Auth::check() && Auth::user()->id == $ans->user_id)
                                    <div class="text-right">
                                        <a href="{{ route('portal.forum.answer.destroy', $ans) }}" class=" text-danger small" data-toggle="tooltip" data-placement="top" title="@lang('admin.destroy')">
                                            @lang('admin.destroy')
                                        </a>
                                    </div>
                                @endif
                            </div>
                            @if(!$loop->last)
                                <hr>
                            @endif
                        @endforeach

                        <div class="my-4">
                            @include('site.pagination', ['data' => $answers])
                        </div>

                    @endif
                    <div class="mb-4">

                        <form action="{{ route('portal.forum.answer', $theme) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class=" row ">
                                <div class="col-md-3 text-md-right " ><h4 class=" mt-5 mb-3 ">@lang('portal.leave_answer')</h4></div>
                            </div>

                            @include('portal.forum._form_answer')

                            <div class=" row ">
                                <div class="offset-md-3 col-md-6 " >
                                    <button class="btn btn-primary btn-sm d-inline-block mb-3" type="submit">
                                        @lang('portal.leave_answer_btn')
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-lg-3">

            <div class="mb-3">
                    <div class="add_theme_button cur-pointer"  data-toggle="modal" data-target="#respond_theme">
                        <div class="d-inline-block add_theme_button-icon">
                            <img src="{{ asset('images/i_add_theme.png') }}" alt=".">
                        </div><div class="d-inline-block add_theme_button-text ">
                            @lang('portal.theme_respond')
                            <span class="d-block">
                            @lang('portal.theme_respond_notice')
                            </span>
                        </div>
                    </div>
            </div>
            @include('portal.forum.right')
        </div>
    </div>

    <div class="modal fade" id="respond_theme" tabindex="-1" role="dialog" aria-labelledby="respond_themeLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form id="respond_theme_form" action="{{ route('portal.forum.respond', $theme) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="respond_themeLabel">@lang('portal.theme_respond')</h5>
                        <button type="button" class="close cur-pointer" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @include('portal.forum._form_respond')
                    </div>
                    <div class="modal-footer">
                        <button  class="btn btn-submit btn-sm">@lang('portal.theme_respond')</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
