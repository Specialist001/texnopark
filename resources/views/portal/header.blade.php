<div class="container">
    <nav class="navbar navbar-search navbar-expand-lg navbar-light px-0 mx-0">

        <a class="navbar-brand" href="{{ route('portal.home') }}">
            <img src="{{ asset('images/logo.png') }}" class="d-inline-block img-fluid align-top" alt="{{ config('app.name') }}">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <div class="mr-auto">
                <ul class="navbar-nav d-md-none ">
                    <li class="nav-item  d-inline-block mr-lg-1">
                        <a class="nav-link" href="{{ route('site.home') }}">@lang('site.home') </a>
                    </li>

                    @foreach($page_categories as $category)
                        @if($category->pages->isNotEmpty())
                            <li class="nav-item  d-inline-block mr-lg-1 p-relative">
                                <a class="nav-link dropdown-toggle dropdown-toggle-no-icon cur-pointer" data-toggle="dropdown" >{{ $category->name }}</a>
                                <div class="dropdown-menu">
                                    @foreach($category->pages as $page)
                                        <div class="dropdown-item">
                                            <a href="{{ $page->id == '83' ? 'http://constitution.uz/oz': route('site.page', $page->id) }}" class="">
                                                {{ $page->name }}
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </li>
                        @endif
                    @endforeach
                        @if($labs->isNotEmpty())
                            <li class="nav-item  d-inline-block mr-lg-1 p-relative">
                                <a class="nav-link dropdown-toggle dropdown-toggle-no-icon cur-pointer" data-toggle="dropdown" >@lang('site.labs')</a>
                                <div class="dropdown-menu">
                                    @foreach($labs as $lab)
                                        <div class="dropdown-item">
                                            <a href="{{ route('site.labs', $lab->id) }}" class="">
                                                {{ $lab->name }}
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </li>
                        @endif

                    <li class="dropdown-divider"></li>

                    <li class="nav-item {{ $current_route_name == ''? 'active': '' }} d-inline-block">
                        <a class="nav-link" href="{{ route('portal.home') }}" >@lang('portal.portal_link')</a>
                    </li>

                    <li class="nav-item d-inline-block {{ $current_route_name == 'forum'? 'active': '' }}">
                        <a class="nav-link" href="{{ route('portal.forum.index') }}">@lang('portal.portal_forum')</a>
                    </li>

                    <li class="nav-item d-inline-block {{ $current_route_name == 'search'? 'active': '' }}">
                        <a class="nav-link" href="{{ route('portal.search') }}">@lang('portal.find_project')</a>
                    </li>

                </ul>
            </div>
            <form class="form-inline my-2 my-lg-0" action="{{ route('portal.search') }}" method="get">
                <div class="input-group mr-sm-2">
                    <input type="text" class="form-control search" name="title" value="{{ $filters['title'] ?? ''}}">
                    <div class="input-group-addon m-0 p-0">
                        <span class="input-group-text">
                            <button class="btn search-button border-0 " type="submit">
                                <img src="{{ asset('images/i_search.png') }}" alt=".">
                            </button>
                        </span>
                    </div>
                </div>
            </form>
            <div class="my-2 my-lg-0 ml-3 d-inline-block">
                <a href="{{ (Auth::check()) ? route('portal.profile') : route('portal.auth.loginForm') }}">
                    <img src="{{ asset('images/i_user.png') }}" alt="@lang('portal.profile')">
                </a>
            </div>
            <div class="my-2 my-lg-0 ml-3 d-inline-block">
                @include('common.lang-dropdown')
            </div>
        </div>

        <img src="{{ asset('images/bg_header.png') }}" alt="." class="bg_header d-none d-lg-inline-block">
    </nav>

    @if(!Auth::check())
        <div class="alert alert-header mx-0 px-5">
            @lang('portal.notify_non_authorized')
        </div>
    @endif
</div>


<div class="container-fluid px-0 bg_menu  d-none d-md-block">
    <div class="container p-relative">
        <nav class="navbar  px-0 mx-0">
            <ul class="navbar-nav d-block top-menu">
                <li class="nav-item  d-inline-block mr-lg-1">
                    <a class="nav-link" href="{{ route('site.home') }}">@lang('site.home') </a>
                </li>

                @foreach($page_categories as $category)
                    @if($category->pages->isNotEmpty())
                        <li class="nav-item  dropdown d-inline-block mr-lg-1">
                            <a class="nav-link dropdown-toggle dropdown-toggle-no-icon cur-pointer" data-toggle="dropdown" >{{ $category->name }}</a>
                            <div class="dropdown-menu">
                                @foreach($category->pages as $page)
                                    <div class="dropdown-item">
                                        <a href="{{ $page->id == '83' ? 'http://constitution.uz/oz': route('site.page', $page->id) }}" class="">
                                            {{ $page->name }}
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </li>
                    @endif
                @endforeach
                @if($labs->isNotEmpty())
                    <li class="nav-item  dropdown d-inline-block mr-lg-1">
                        <a class="nav-link dropdown-toggle dropdown-toggle-no-icon cur-pointer" data-toggle="dropdown" >@lang('site.labs')</a>
                        <div class="dropdown-menu">
                            @foreach($labs as $lab)
                                <div class="dropdown-item">
                                    <a href="{{ route('site.labs', $lab->id) }}" class="">
                                        {{ $lab->name }}
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </li>
                @endif
                {{--<li class="nav-item  d-inline-block mr-lg-1">--}}
                    {{--<a class="nav-link" href="{{ route('site.page', $system_page->id) }}">@lang('site.about') </a>--}}
                {{--</li>--}}
            </ul>

            <div class="float-right text-right d-none d-lg-inline-block">
                <a href="{{ route('portal.home') }}" class="portal-home pb-1">@lang('portal.portal_link')</a>
            </div>
        </nav>

        <nav class="navbar  px-0 mx-0">
            <ul class="navbar-nav d-block portal-menu">
                <li class="nav-item {{ $current_route_name == ''? 'active': '' }} d-inline-block">
                    <a class="nav-link" href="{{ route('portal.home') }}">@lang('portal.portal_link')</a>
                </li>
                <li class="nav-item divider"></li>

                <li class="nav-item d-inline-block {{ $current_route_name == 'forum'? 'active': '' }}">
                    <a class="nav-link" href="{{ route('portal.forum.index') }}">@lang('portal.portal_forum')</a>
                </li>
                <li class="nav-item divider"></li>

                <li class="nav-item d-inline-block {{ $current_route_name == 'search'? 'active': '' }}">
                    <a class="nav-link" href="{{ route('portal.search') }}">@lang('portal.find_project')</a>
                </li>
            </ul>
        </nav>
        <div class="hr"></div>
        <h1>
            @yield('title', config('app.name'))
        </h1>

        {{--<div class="rectangle d-none d-md-inline-block"></div>--}}
    </div>
</div>

<div class="bg-blue d-md-none p-3">
    <h1>
        @yield('title', config('app.name'))
    </h1>
</div>
