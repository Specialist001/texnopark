<div class="form-group row {!! $errors->first('sum', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="sum">@lang('project.investment_sum')</label>
    <div class="col-md-6">
        <input type="number" step="1" min="1" max="{{ $investment->project->sum_need - $investment->project->activeInvestments->sum('sum')  }}" name="sum" class="form-control input-sm" id="sum" value="{{ old('sum', $investment->sum ?? '') }}" required >
        {!! $errors->first('sum', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
