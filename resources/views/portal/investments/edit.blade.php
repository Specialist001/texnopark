@extends('portal.layout')

@section('center_content')

    @include('portal.profile.menu')

    <form action="{{ route('portal.investments.update', $investment) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        <section class="mb-4">
            <div class="sp-card project-card p-5">
                @include('portal.investments._form')

                <div class="text-center">
                    <a href="{{ route('portal.investments.index') }}" class="btn btn-sm btn-reset-filters d-inline-block">@lang('admin.back')</a>
                    <button class="btn  btn-submit d-inline-block">@lang('admin.save')</button>
                </div>
            </div>
        </section>
    </form>
@endsection
