@extends('portal.layout')

@section('center_content')

    @include('portal.profile.menu')
    @include('portal.profile.investor-submenu')

    @if($investments->isNotEmpty())
        @foreach($investments as $investment)
            <a href="{{ route('portal.investments.show', $investment) }}">
            <div class="sp-card mb-3 project-card ">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th>@lang('portal.project')</th>
                            <th>@lang('project.project_category_id')</th>
                            <th>@lang('project.cost_filter')</th>
                            <th>@lang('portal.my_invested')</th>
                            <th>@lang('project.readiness')</th>
                            <th>@lang('project.id')</th>
                            {{--<th></th>--}}
                        </tr>
                        <tr>
                            <td>
                                <p class="pa-project-text">
                                    {{ $investment->project->title }}
                                </p>
                                <div class="text-center">
                                    <img src="{{ $investment->project->logoUrl() }}" alt="{{ $investment->project->title }}" class="img-fluid d-inline-block" style="max-width: 150px;">
                                </div>
                            </td>
                            <td>
                                <div class="pa-project-text">{{ $investment->project->category->name }}</div>
                            </td>
                            <td>
                                <div class="pa-project-text">
                                    {{ number_format($investment->project->usd_cost, 0, '', ' ') }} @lang('portal.sum')
                                </div>
                            </td>
                            <td>
                                <div class="pa-project-text">
                                    <strong>{{ number_format($investment->sum, 0, '', ' ') }} @lang('portal.sum')</strong>
                                    <br>
                                    <span class="small">
                                        @lang('project.investment_statuses.'.$investment->status)
                                    </span>
                                </div>
                            </td>
                            <td>
                                <div class="pa-project-text">
                                    {{ $investment->project->readiness }}
                                </div>
                            </td>
                            <td>
                                <div class="pa-project-text">
                                    {{ $investment->project->id }}
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            </a>
        @endforeach
    @endif

    @include('site.pagination', ['data' => $investments])
@endsection
