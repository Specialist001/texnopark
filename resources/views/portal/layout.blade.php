@extends('site.layout')

{{--@section('menu-left')--}}
    {{--@include('partner.menu-left')--}}
{{--@endsection--}}

@section('header')
    @include('portal.header')
@endsection

@section('content')
    @yield('center_content')
@endsection

@section('footer')
    @include('portal.footer')
@endsection
@push('styles')
    <link href="{{ asset('css/portal.css') }}" rel="stylesheet">
@endpush
