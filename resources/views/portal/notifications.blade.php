@extends('portal.layout')

@section('center_content')

    @include('portal.profile.menu')


    @foreach($notifications as $notification)
        <section class="mb-4">
            <div class="sp-card mb-3 p-3 {{ $notification->viewed == 0? 'new_row': '' }}">
                <p>
                    @lang($notification->action, ['project' => $notification->project->title])
                </p>
                <div class="text-muted small">
                    {{ $notification->created_at->format('d.m.Y H:i') }}
                </div>
            </div>
        </section>
    @endforeach

    <section class="mb-4">
        @include('site.pagination', ['data' => $notifications])
    </section>

@endsection
