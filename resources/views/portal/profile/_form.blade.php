<div class="form-group row ">
    <label class="col-md-3 text-md-right col-form-label-sm" >@lang('auth.username')</label>
    <label class="col-md-6 col-form-label-sm" ><strong>{{ $user->username }}</strong></label>
</div>

<div class="form-group row {!! $errors->first('name', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="name">@lang('admin.name')</label>
    <div class="col-md-6">
        <input type="text" name="name" class="form-control input-sm" id="name" value="{{ old('name', $user->name ?? '') }}" required autofocus>
        {!! $errors->first('name', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="row">
    <div class="col-md-6 offset-md-3">
        <p>
            <strong>@lang('auth.password_change')</strong>
        </p>
    </div>
</div>

<div class="form-group row {!! $errors->first('current_password', 'has-danger')!!}" >
    <label class="col-md-3 text-md-right col-form-label-sm" for="current_password">@lang('auth.current_password')</label>
    <div class="col-md-6">
        <input type="password" name="current_password" class="form-control input-sm password" id="current_password" >
        {!! $errors->first('current_password', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('password', 'has-danger')!!}" >
    <label class="col-md-3 text-md-right col-form-label-sm" for="password">@lang('auth.new_password')</label>
    <div class="col-md-6">
        <input type="password" name="password" class="form-control input-sm password" id="password" >
        {!! $errors->first('password', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<div class="form-group row {!! $errors->first('password_confirmation', 'has-danger')!!}" >
    <label class="col-md-3 text-md-right col-form-label-sm" for="password_confirmation">@lang('auth.password_confirmation')</label>
    <div class="col-md-6">
        <input type="password" name="password_confirmation" class="form-control input-sm password" id="password_confirmation" >
        {!! $errors->first('password_confirmation', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>
{{--<div class="form-group row">--}}
    {{--<label class="col-md-3 text-md-right col-form-label-sm" for="verified">@lang('common.verification')</label>--}}
    {{--<div class="col-md-6">--}}
        {{--<select name="verified" id="verified" class="form-control input-sm">--}}
            {{--<option value="1">@lang('common.verified')</option>--}}
            {{--<option value="0">@lang('common.not_verified')</option>--}}
        {{--</select>--}}
        {{--<div class="btn-group btn-group-sm" data-toggle="buttons">--}}
            {{--<label class="btn btn-outline-success {{ old('verified', $user->hasVerifiedPhone() ? 1: 0) == 1? 'active': '' }}">--}}
                {{--<input type="radio" name="verified" value="1" {{ old('verified', $user->hasVerifiedPhone() ? 1: 0) == 1? 'checked': '' }} required>--}}
                {{--@lang('common.verified')--}}
            {{--</label>--}}
            {{--<label class="btn btn-outline-danger {{ old('verified', $user->hasVerifiedPhone() ? 1: 0) == 0? 'active': '' }}">--}}
                {{--<input type="radio" name="verified" value="0" {{ old('verified', $user->hasVerifiedPhone() ? 1: 0) == 0? 'checked': '' }} required>--}}
                {{--@lang('common.not_verified')--}}
            {{--</label>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

@component('component.password-stacks')@endcomponent
