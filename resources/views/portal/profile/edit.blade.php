@extends('portal.layout')

@section('center_content')

    @include('portal.profile.menu')

    <div class="sp-card project-card p-5">
        <div class="text-center">
            <p class="profile_notice">
                @lang('portal.profile_notice')
            </p>
        </div>

        <form action="{{ route('portal.profile.update') }}" method="post">
            @csrf
            @method('put')

            <div class="form-group row ">
                <label class="col-md-3 text-md-right col-form-label-sm" >@lang('portal.user_id')</label>
                <label class="col-md-6 col-form-label-sm" ><strong>{{ $user->id }}</strong></label>
            </div>

            <div class="form-group row ">
                <label class="col-md-3 text-md-right col-form-label-sm" >@lang('auth.username')</label>
                <label class="col-md-6 col-form-label-sm" ><strong>{{ $user->username }}</strong></label>
            </div>

            <div class="form-group row ">
                <label class="col-md-3 text-md-right col-form-label-sm" >@lang('auth.email')</label>
                <label class="col-md-6 col-form-label-sm" ><strong>{{ $user->email }}</strong></label>
            </div>

            <div class="form-group row {!! $errors->first('name', 'has-danger')!!}">
                <label class="col-md-3 text-md-right col-form-label-sm" for="name">@lang('admin.name')</label>
                <div class="col-md-6">
                    <input type="text" name="name" class="form-control input-sm" id="name" value="{{ old('name', $user->name ?? '') }}" required autofocus>
                    {!! $errors->first('name', '<small class="form-control-feedback">:message</small>') !!}
                </div>
            </div>

            <div class="form-group row {!! $errors->first('phone', 'has-danger')!!}">
                <label class="col-md-3 text-md-right col-form-label-sm" for="phone">@lang('auth.phone')</label>
                <div class="col-md-6">
                    <input type="text" name="phone" class="form-control phone-input input-sm" id="phone" value="{{ old('phone', $user->phone ?? '') }}" required >
                    {!! $errors->first('phone', '<small class="form-control-feedback">:message</small>') !!}
                </div>
            </div>

            <div class="form-group row {!! $errors->first('site', 'has-danger')!!}">
                <label class="col-md-3 text-md-right col-form-label-sm" for="site">@lang('auth.site')</label>
                <div class="col-md-6">
                    <input type="text" name="site" class="form-control input-sm" id="site" value="{{ old('site', $user->site ?? '') }}"  >
                    {!! $errors->first('site', '<small class="form-control-feedback">:message</small>') !!}
                </div>
            </div>

            <div class="form-group row {!! $errors->first('personality', 'has-danger')!!}">
                <div class="col-md-6 offset-md-3">
                    <div class="form-group text-center row">
                        <div class="col-md-4">
                            <label class="custom-control  custom-radio cur-pointer  m-0">
                                <span class="custom-control-description">@lang('auth.legal')</span>
                                <input type="radio" required
                                       @if(old('personality', $user->personality ?? 'legal') == 'legal' )
                                       checked
                                       @endif
                                       name="personality" value="legal" class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>
                        <div class="col-md-4">
                            <label class="custom-control  custom-radio cur-pointer  m-0">
                                <span class="custom-control-description">@lang('auth.individual')</span>
                                <input type="radio" required
                                       @if(old('personality', $user->personality ?? 'legal') == 'individual' )
                                       checked
                                       @endif
                                       name="personality" value="individual" class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>
                        <div class="col-md-12 text-center">
                            {!! $errors->first('personality', '<small class="form-control-feedback">:message</small>') !!}
                        </div>
                    </div>
                </div>
            </div>


            <div class="form-group row {!! $errors->first('city_id', 'has-danger')!!}">
                <label class="col-md-3 text-md-right col-form-label-sm" for="city_id">@lang('auth.city')</label>
                <div class="col-md-6">
                    <select name="city_id" id="city_id" class="form-control " required>
                        @foreach(\App\Domain\Cities\Models\City::withTranslation()->get() as $city)
                            <option value="{{ $city->id }}" @if(old('city_id', $user->city_id ?? 0) == $city->id) selected @endif>{{ $city->name }}</option>
                        @endforeach
                    </select>
                    {!! $errors->first('city_id', '<small class="form-control-feedback">:message</small>') !!}
                </div>
            </div>

            <div class="form-group row {!! $errors->first('locale', 'has-danger')!!}">
                <label class="col-md-3 text-md-right col-form-label-sm" for="locale">@lang('portal.user_locale')</label>
                <div class="col-md-6">
                    <select name="locale" id="locale" class="form-control " required>
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <option value="{{ $localeCode }}" @if(old('locale', $user->locale ?? '') == $localeCode) selected @endif>@lang('admin.locales.'.$localeCode)</option>
                        @endforeach
                    </select>
                    {!! $errors->first('locale', '<small class="form-control-feedback">:message</small>') !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <p>
                        <strong>@lang('auth.password_change')</strong>
                    </p>
                </div>
            </div>

            <div class="form-group row {!! $errors->first('current_password', 'has-danger')!!}" >
                <label class="col-md-3 text-md-right col-form-label-sm" for="current_password">@lang('auth.current_password')</label>
                <div class="col-md-6">
                    <input type="password" name="current_password" class="form-control input-sm password" id="current_password" >
                    {!! $errors->first('current_password', '<small class="form-control-feedback">:message</small>') !!}
                </div>
            </div>

            <div class="form-group row {!! $errors->first('password', 'has-danger')!!}" >
                <label class="col-md-3 text-md-right col-form-label-sm" for="password">@lang('auth.new_password')</label>
                <div class="col-md-6">
                    <input type="password" name="password" class="form-control input-sm password" id="password" >
                    {!! $errors->first('password', '<small class="form-control-feedback">:message</small>') !!}
                </div>
            </div>

            <div class="form-group row {!! $errors->first('password_confirmation', 'has-danger')!!}" >
                <label class="col-md-3 text-md-right col-form-label-sm" for="password_confirmation">@lang('auth.password_confirmation')</label>
                <div class="col-md-6">
                    <input type="password" name="password_confirmation" class="form-control input-sm password" id="password_confirmation" >
                    {!! $errors->first('password_confirmation', '<small class="form-control-feedback">:message</small>') !!}
                </div>
            </div>

            @component('component.phone-stacks')@endcomponent
            @component('component.password-stacks')@endcomponent


            <div class="text-center">
                <button class="btn btn-sm btn-submit d-inline-block cur-pointer">@lang('portal.save')</button>
            </div>

        </form>
    </div>
@endsection
