<ul class="nav nav-pills mb-3 investors-submenu">
    <li class="nav-item">
        <a class="nav-link {{ $current_route_name == 'investments'? 'active': '' }}" href="{{ route('portal.investments.index') }}"><i class="icmn-briefcase tab-icons mr-2"></i> @lang('portal.my_investments')</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ $current_route_name == 'favorites'? 'active': '' }}" href="{{ route('portal.favorites.index') }}"><i class="icmn-star-full tab-icons mr-2"></i> @lang('portal.favorites') <span>({{ count($favorites) }})</span></a>
    </li>
</ul>
