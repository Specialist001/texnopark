@section('title')
    @lang('portal.personal_area')
@endsection
<ul class="nav profile-nav mb-4">
    <li class="nav-item">
        <a class="nav-link {{ $current_route_name == 'profile'? 'active': '' }}" href="{{ route('portal.profile') }}">@lang('portal.personal_data')</a>
    </li>
    @if(Auth::user()->isStartUp())
        <li class="nav-item">
            <a class="nav-link {{ $current_route_name == 'projects'? 'active': '' }} " href="{{ route('portal.projects.index') }}">@lang('portal.my_projects')</a>
        </li>
    @endif
    @if(Auth::user()->isInvestor())
        <li class="nav-item">
            <a class="nav-link {{ $current_route_name == 'investments' || $current_route_name == 'favorites'? 'active': '' }}"  href="{{ route('portal.investments.index') }}">@lang('portal.my_investments')</a>
        </li>
    @endif
    <li class="nav-item">
        <a class="nav-link {{ $current_route_name == 'notifications'? 'active': '' }}" href="{{ route('portal.notifications') }}">
            @lang('portal.notifications')
            @if($notifications_count > 0)
                <span class="badge badge-success ml-2">{{ $notifications_count }}</span>
            @endiF
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-danger" href="{{ route('portal.auth.logout') }}">
            @lang('portal.logout')
        </a>
    </li>
</ul>

@if (session()->has('success'))
    @component('component.alert', ['type' => 'success'])
        {{ session('success') }}
    @endcomponent
@endif
@if (session()->has('verified'))
    @component('component.alert', ['type' => 'success'])
        @lang('portal.verified')
    @endcomponent
@endif
@if (session()->has('warning'))
    @component('component.alert', ['type' => 'warning'])
        {{ session('warning') }}
    @endcomponent
@endif
@if (session()->has('danger'))
    @component('component.alert', ['type' => 'danger'])
        {{ session('danger') }}
    @endcomponent
@endif
