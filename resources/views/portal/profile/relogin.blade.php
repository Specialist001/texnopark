@extends('portal.layout')

@section('center_content')

    @include('portal.profile.menu')
    <div class="text-center">
        @if($user->isStartup())
            <p class="profile_notice">
                @lang('portal.relogin_investor')
            </p>
        @endif
        @if($user->isInvestor())
            <p class="profile_notice">
                @lang('portal.relogin_startup')
            </p>
        @endif
    </div>
@endsection
