@extends('portal.layout')

@section('title')
    {{ $project->title }}
@endsection

@section('project_content')
    project_content
@endsection

@section('center_content')
    <section class="mb-5">
        <div class="row">
            <div class="col-lg-4">
                <img src="{{ $project->logoUrl() }}" alt="{{ $project->title }}"
                     class="img-fluid project_logo  d-block mb-3">
                {{--<div class="project_page-developer mb-3">--}}
                {{--{{ $project->developer }}--}}
                {{--</div>--}}
            </div>
            <div class="col-lg-4 text-center">
                <img src="{{ $project->imageUrl() }}" alt="{{ $project->title }}"
                     class="img-fluid  d-inline-block mb-3">
            </div>
            <div class="col-lg-4">
                <div class="project_page-about_developer mb-3">
                    <h5 class="project_page-desc-title mb-3" style="font-weight: bold;">@lang('project.about_developer')</h5>
                    <p class="project_page-desc">
                        {!! nl2br($project->about_developer) !!}
                    </p>
                </div>
            </div>
        </div>
    </section>
    @if($project->short_description != '')
        <section class="mb-5">
            <h2 class="project_page-desc-title mb-3">@lang('project.short_description')</h2>
            <p class="project_page-desc">
                {!! nl2br($project->short_description) !!}
            </p>
        </section>
    @endif
    <section class="mb-5">
        <h2 class="project_page-desc-title mb-3">@lang('portal.project_description')</h2>
        <p class="project_page-desc">
            {!! nl2br($project->description) !!}
        </p>
    </section>

    <section class="mb-5">
        <h2 class="project_page-desc-title mb-3">
            @lang('portal.project_stats')
        </h2>
        <div class="row">
            <div class="col-lg-3 col-md-6 mb-3">
                <div class="row">
                    <div class="col-3 col-sm-2 text-center">
                        <img src="{{ asset('images/i_stat_1.png') }}" alt="@lang('portal.project_stat_1')"
                             class="stat_img d-inline-block">
                    </div>
                    <div class="col-9 col-sm-10">
                        <div class="stat_title">
                            @lang('portal.project_stat_1')
                        </div>
                        <div class="stat_body">
                            {{ $project->title }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 mb-3">
                <div class="row">
                    <div class="col-3 col-sm-2 text-center">
                        <img src="{{ asset('images/i_stat_2.png') }}" alt="@lang('portal.project_stat_2')"
                             class="stat_img d-inline-block">
                    </div>
                    <div class="col-9 col-sm-10">
                        <div class="stat_title">
                            @lang('portal.project_stat_2')
                        </div>
                        <div class="stat_body">
                            {{ $project->foreign == 1 ? trans('portal.yes') : trans('portal.no') }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 mb-3">
                <div class="row">
                    <div class="col-3 col-sm-2 text-center">
                        <img src="{{ asset('images/i_stat_3.png') }}" alt="@lang('portal.project_stat_3')"
                             class="stat_img d-inline-block">
                    </div>
                    <div class="col-9 col-sm-10">
                        <div class="stat_title">
                            @lang('portal.project_stat_3')
                        </div>
                        <div class="stat_body">
                            {{ $project->patent ?? trans('portal.no') }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 mb-3">
                <div class="row">
                    <div class="col-3 col-sm-2 text-center">
                        <img src="{{ asset('images/i_stat_4.png') }}" alt="@lang('portal.project_stat_4')"
                             class="stat_img d-inline-block">
                    </div>
                    <div class="col-9 col-sm-10">
                        <div class="stat_title">
                            @lang('portal.project_stat_4')
                        </div>
                        <div class="stat_body">
                            {{ $project->category->name }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6 mb-3">
                <div class="row">
                    <div class="col-3 col-sm-2 text-center">
                        <img src="{{ asset('images/i_stat_5.png') }}" alt="@lang('portal.project_stat_5')"
                             class="stat_img d-inline-block">
                    </div>
                    <div class="col-9 col-sm-10">
                        <div class="stat_title">
                            @lang('portal.project_stat_5')
                        </div>
                        <div class="stat_body">
                            {{ $project->readiness }}%
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 mb-3">
                <div class="row">
                    <div class="col-3 col-sm-2 text-center">
                        <img src="{{ asset('images/i_stat_6.png') }}" alt="@lang('portal.project_stat_6')"
                             class="stat_img d-inline-block">
                    </div>
                    <div class="col-9 col-sm-10">
                        <div class="stat_title">
                            @lang('portal.project_stat_6')
                        </div>
                        <div class="stat_body">
                            {{ $project->place ?? '' }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 mb-3">
                <div class="row">
                    <div class="col-3 col-sm-2 text-center">
                        <img src="{{ asset('images/i_stat_7.png') }}" alt="@lang('portal.project_stat_7')"
                             class="stat_img d-inline-block">
                    </div>
                    <div class="col-9 col-sm-10">
                        <div class="stat_title">
                            @lang('portal.project_stat_7') (@lang('portal.mth'))
                        </div>
                        <div class="stat_body">
                            {{ $project->payback_period ?? '' }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 mb-3">
                <div class="row">
                    <div class="col-3 col-sm-2 text-center">
                        <img src="{{ asset('images/i_stat_8.png') }}" alt="@lang('portal.project_stat_8')"
                             class="stat_img d-inline-block">
                    </div>
                    <div class="col-9 col-sm-10">
                        <div class="stat_title">
                            @lang('portal.project_stat_8')
                        </div>
                        <div class="stat_body">
                            @if($project->sum_cost == 0 && $project->usd_cost == 0)
                                @lang('portal.no_cost_sum')
                            @else
                                @if($project->sum_cost != 0)
                                    {{ number_format($project->sum_cost, 0, '', ' ') }} @lang('portal.sum')
                                @endif
                                @if($project->usd_cost != 0)
                                    <br>
                                    {{ number_format($project->usd_cost, 0, '', ' ') }} @lang('portal.usd')
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-3 col-md-6 mb-3">
                <div class="row">
                    <div class="col-3 col-sm-2 text-center">
                        <img src="{{ asset('images/i_stat_9.png') }}" alt="@lang('portal.project_stat_9')"
                             class="stat_img d-inline-block">
                    </div>

                    <div class="col-9 col-sm-10">
                        <div class="stat_title">
                            @lang('portal.project_stat_9')
                        </div>
                        <div class="stat_body">
                            {{ number_format($project->sum_need, 0, '', ' ') }} @lang('portal.sum')
                            @if($project->usd_need != 0)
                                <br>
                                {{ number_format($project->usd_need, 0, '', ' ') }} @lang('portal.usd')
                            @endif
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-3 col-md-6 mb-3">
                <div class="row">
                    <div class="col-3 col-sm-2 text-center">
                        <img src="{{ asset('images/i_stat_10.png') }}" alt="@lang('portal.project_stat_10')"
                             class="stat_img d-inline-block">
                    </div>

                    <div class="col-9 col-sm-10">
                        <div class="stat_title">
                            @lang('portal.project_stat_10')
                        </div>
                        <div class="stat_body">
                            @if($project->sum_current == 0 && $project->usd_current == 0)
                                @lang('portal.no_current_sum')
                            @else
                                @if($project->sum_current != 0)
                                    {{ number_format($project->sum_current, 0, '', ' ') }} @lang('portal.sum')
                                @endif
                                @if($project->usd_current != 0)
                                    <br>
                                    {{ number_format($project->usd_current, 0, '', ' ') }} @lang('portal.usd')
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-3 col-md-6 mb-3">
                <div class="row">
                    <div class="col-3 col-sm-2 text-center">
                        <img src="{{ asset('images/i_stat_11.png') }}" alt="@lang('portal.project_stat_11')"
                             class="stat_img d-inline-block">
                    </div>

                    <div class="col-9 col-sm-10">
                        <div class="stat_title">
                            @lang('portal.project_stat_11')
                        </div>
                        <div class="stat_body">
                            {{ $project->finally ?? trans('portal.na') }}
                        </div>
                    </div>
                </div>
            </div>

            {{--<div class="col-lg-3 col-md-6 mb-3">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-3 col-sm-2 text-center">--}}
                        {{--<img src="{{ asset('images/i_stat_12.png') }}" alt="@lang('portal.project_stat_12')"--}}
                             {{--class="stat_img d-inline-block">--}}
                    {{--</div>--}}

                    {{--<div class="col-9 col-sm-10">--}}
                        {{--<div class="stat_title">--}}
                            {{--@lang('portal.project_stat_12')--}}
                        {{--</div>--}}
                        {{--<div class="stat_body">--}}
                            {{--{{ $project->state ??  trans('portal.na') }}--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </section>


    <section class="mb-5">
        <div class="row">
            @if($project->about_presentation != '' || $project->presentation)
                <div class="col-md-12">
                    <h3 class="project_page_stats mb-3">
                        @lang('portal.project_about_presentation')
                    </h3>
                    @if($project->about_presentation != '')
                    <p class="project_page-desc mb-3">
                        {!! nl2br($project->about_presentation) !!}
                    </p>
                    @endif
                    @if($project->presentation)
                        <a href="{{ $project->presentationUrl() }}" class="project_page-presentation mb-3 d-inline-block" target="_blank">
                            @lang('portal.download_presentation') <img src="{{ asset("images/i_download.png") }}"
                                                                       alt="@lang('portal.download_presentation')" class="d-inline-block  ml-3 download-icon align-bottom">
                        </a>
                    @endif

                </div>
            @endif
            @if($project->state != '')
                <div class="col-md-12">
                    <h3 class="project_page_stats mb-3">
                        @lang('portal.project_stat_12')
                    </h3>
                    <p class="project_page-desc mb-3">
                        {!! nl2br($project->state) !!}
                    </p>
                </div>
            @endif
            @if($project->additional_info != '')
            <div class="col-md-12">
                <h3 class="project_page_stats mb-3">
                    @lang('portal.project_additional_info')
                </h3>
                <p class="project_page-desc mb-3">
                    {!! nl2br($project->additional_info) !!}
                </p>
            </div>
                @endif
        </div>
    </section>
    <hr>
    <section class="mb-5" id="invest">
        <h2 class="project_page-desc-title mb-3 text-center">@lang('portal.became_investor')</h2>
        @if(!Auth::check())
            <div class="text-center">
                <p>
                    @lang('portal.not_registered')
                </p>
                <a class="btn btn-submit d-inline-block text-white" href="{{ route('portal.auth.registerForm') }}">@lang('portal.registration')</a>
                <a class="btn btn-submit d-inline-block text-white" href="{{ route('portal.auth.loginForm') }}">@lang('portal.authorization')</a>
            </div>
        @elseif(Auth::user()->role != \App\Domain\Users\Models\User::ROLE_INVESTOR)
            <div class="text-center">
                @lang('portal.relogin_investor')
            </div>
        @elseif(Auth::user()->role == \App\Domain\Users\Models\User::ROLE_INVESTOR)
            @if($hasInvested)
                <div class="text-center">
                    <p>
                        @lang('portal.hasInvested')
                    </p>
                    <a class="btn btn-submit d-inline-block text-white" href="{{ route('portal.investments.index') }}">@lang('portal.my_investments')</a>
                </div>
            @else
                <form action="{{ route('portal.investments.invest', $project) }}" method="post">
                    @csrf
                    <div class="form-group  {!! $errors->first('sum', 'has-danger')!!}">
                        <div class="text-center">
                            <input type="number" placeholder="@lang('project.investment_sum')" step="1" min="1" max="{{ $project->sum_need - $project->activeInvestments->sum('sum')  }}" name="sum" class="form-control d-inline-block" id="sum" value="{{ old('sum', $investment->sum ?? '') }}"  required >
                            {!! $errors->first('sum', '<small class="form-control-feedback">:message</small>') !!}
                        </div>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-submit d-inline-block">@lang('portal.invest')</button>
                    </div>
                </form>
            @endif
        @endif
    </section>
@endsection
