<?php $value = old('translations.'.$lang.'.place', (isset($model) && $model->translate($lang)) ? $model->translate($lang)->place: '') ?>
<div class="form-group row {!! $errors->first('translations.'.$lang.'.place', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="{{$lang}}-place">@lang('project.place')</label>
    <div class="col-md-6">
        <input type="text" placeholder="@lang('project.place_unit')" name="translations[{{ $lang }}][place]" class="form-control input-sm" id="{{$lang}}-place" value="{{ $value }}" {{ ($lang == LaravelLocalization::getDefaultLocale())? 'required': '' }}>
        {!! $errors->first('translations.'.$lang.'.place', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<?php $value = old('translations.'.$lang.'.short_description', (isset($model) && $model->translate($lang)) ? $model->translate($lang)->short_description: '') ?>
<div class="form-group row {!! $errors->first('translations.'.$lang.'.short_description', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="{{$lang}}-short_description">@lang('project.short_description')</label>
    <div class="col-md-6">
        <textarea name="translations[{{ $lang }}][short_description]" class="form-control" id="{{$lang}}-short_description" {{ ($lang == LaravelLocalization::getDefaultLocale())? 'required': '' }}>{{ $value }}</textarea>
        {!! $errors->first('translations.'.$lang.'.short_description', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<?php $value = old('translations.'.$lang.'.description', (isset($model) && $model->translate($lang)) ? $model->translate($lang)->description: '') ?>
<div class="form-group row {!! $errors->first('translations.'.$lang.'.description', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="{{$lang}}-description">@lang('project.description')</label>
    <div class="col-md-6">
        <textarea name="translations[{{ $lang }}][description]" class="form-control" id="{{$lang}}-description" {{ ($lang == LaravelLocalization::getDefaultLocale())? 'required': '' }}>{{ $value }}</textarea>
        {!! $errors->first('translations.'.$lang.'.description', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<?php $value = old('translations.'.$lang.'.target', (isset($model) && $model->translate($lang)) ? $model->translate($lang)->target: '') ?>
<div class="form-group row {!! $errors->first('translations.'.$lang.'.target', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="{{$lang}}-target">@lang('project.target')</label>
    <div class="col-md-6">
        <textarea name="translations[{{ $lang }}][target]" class="form-control " id="{{$lang}}-target" {{ ($lang == LaravelLocalization::getDefaultLocale())? 'required': '' }}>{{ $value }}</textarea>
        {!! $errors->first('translations.'.$lang.'.target', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<?php $value = old('translations.'.$lang.'.finally', (isset($model) && $model->translate($lang)) ? $model->translate($lang)->finally: '') ?>
<div class="form-group row {!! $errors->first('translations.'.$lang.'.finally', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="{{$lang}}-finally">@lang('project.finally')</label>
    <div class="col-md-6">
        <input type="text" placeholder="@lang('project.finally_unit')" name="translations[{{ $lang }}][finally]" class="form-control input-sm" id="{{$lang}}-finally" value="{{ $value }}" >
        {!! $errors->first('translations.'.$lang.'.finally', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>


<?php $value = old('translations.'.$lang.'.state', (isset($model) && $model->translate($lang)) ? $model->translate($lang)->state: '') ?>
<div class="form-group row {!! $errors->first('translations.'.$lang.'.state', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="{{$lang}}-state">@lang('project.state')</label>
    <div class="col-md-6">
        <textarea name="translations[{{ $lang }}][state]" class="form-control input-sm" id="{{$lang}}-state" >{{ $value }}</textarea>
        {!! $errors->first('translations.'.$lang.'.state', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<?php $value = old('translations.'.$lang.'.about_developer', (isset($model) && $model->translate($lang)) ? $model->translate($lang)->about_developer: '') ?>
<div class="form-group row {!! $errors->first('translations.'.$lang.'.about_developer', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="{{$lang}}-about_developer">@lang('project.about_developer')</label>
    <div class="col-md-6">
        <textarea name="translations[{{ $lang }}][about_developer]" class="form-control" id="{{$lang}}-about_developer" >{{ $value }}</textarea>
        {!! $errors->first('translations.'.$lang.'.about_developer', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>


<?php $value = old('translations.'.$lang.'.about_presentation', (isset($model) && $model->translate($lang)) ? $model->translate($lang)->about_presentation: '') ?>
<div class="form-group row {!! $errors->first('translations.'.$lang.'.about_presentation', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="{{$lang}}-about_presentation">@lang('project.about_presentation')</label>
    <div class="col-md-6">
        <textarea name="translations[{{ $lang }}][about_presentation]" class="form-control" id="{{$lang}}-about_presentation" >{{ $value }}</textarea>
        {!! $errors->first('translations.'.$lang.'.about_presentation', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<?php $value = old('translations.'.$lang.'.additional_info', (isset($model) && $model->translate($lang)) ? $model->translate($lang)->additional_info: '') ?>
<div class="form-group row {!! $errors->first('translations.'.$lang.'.additional_info', 'has-danger')!!}">
    <label class="col-md-3 text-md-right col-form-label-sm" for="{{$lang}}-additional_info">@lang('project.additional_info')</label>
    <div class="col-md-6">
        <textarea name="translations[{{ $lang }}][additional_info]" class="form-control" id="{{$lang}}-additional_info" >{{ $value }}</textarea>
        {!! $errors->first('translations.'.$lang.'.additional_info', '<small class="form-control-feedback">:message</small>') !!}
    </div>
</div>

<input type="hidden" name="translations[{{ $lang }}][locale]" value="{{ $lang }}">
