@extends('portal.layout')

@section('center_content')

    @include('portal.profile.menu')


    <form action="{{ route('portal.projects.store') }}" method="post" enctype="multipart/form-data">
    @csrf
    <section class="mb-4">
        <div class="sp-card project-card p-5">
            @include('portal.projects._form')

            <div class="text-center">
                <a href="{{ route('portal.projects.index') }}" class="btn btn-sm btn-reset-filters d-inline-block">@lang('admin.back')</a>
                <button class="btn  btn-submit d-inline-block">@lang('admin.create')</button>
            </div>
        </div>
    </section>
    </form>
@endsection
