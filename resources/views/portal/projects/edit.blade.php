@extends('portal.layout')

@section('center_content')

    @include('portal.profile.menu')

    <div class="alert alert-header mx-0 px-5">
        @lang('portal.editing_project')
    </div>

    <form action="{{ route('portal.projects.update', $project) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        <section class="mb-4">
            <div class="sp-card project-card p-5">
                @include('portal.projects._form')

                <div class="text-center">
                    <a href="{{ route('portal.projects.index') }}" class="btn btn-sm btn-reset-filters d-inline-block">@lang('admin.back')</a>
                    <button class="btn  btn-submit d-inline-block">@lang('admin.save')</button>
                </div>
            </div>
        </section>
    </form>
@endsection

@push('scripts')
    <script>
        $(function () {
            $('#delete_image').on('click', function () {
                $.ajax({
                    method: "DELETE",
                    url: "{{ route('portal.projects.destroy.image') }}/{{$project->id}}",
                    success: function(response) {
                        if(response.result === "success") {
                            $('#image').remove();
                            return false;
                        } else {
                            alert("{{trans('admin.ajax_error')}}");
                        }
                    },
                    error: function(response) {
                        alert("{{trans('admin.ajax_error')}}");
                    }
                });
            });
            $('#delete_logo').on('click', function () {
                $.ajax({
                    method: "DELETE",
                    url: "{{ route('portal.projects.destroy.logo') }}/{{$project->id}}",
                    success: function(response) {
                        if(response.result === "success") {
                            $('#logo').remove();
                            return false;
                        } else {
                            alert("{{trans('admin.ajax_error')}}");
                        }
                    },
                    error: function(response) {
                        alert("{{trans('admin.ajax_error')}}");
                    }
                });
            });
            $('#delete_presentation').on('click', function () {
                $.ajax({
                    method: "DELETE",
                    url: "{{ route('portal.projects.destroy.presentation') }}/{{$project->id}}",
                    success: function(response) {
                        if(response.result === "success") {
                            $('#presentation').remove();
                            return false;
                        } else {
                            alert("{{trans('admin.ajax_error')}}");
                        }
                    },
                    error: function(response) {
                        alert("{{trans('admin.ajax_error')}}");
                    }
                });
            });
        });
    </script>
@endpush
@push('styles')
    <style>
        #image, #logo {
            max-width: 300px;
        }
    </style>
@endpush
