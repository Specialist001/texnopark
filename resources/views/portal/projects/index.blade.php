@extends('portal.layout')

@section('center_content')

    @include('portal.profile.menu')

    <a href="{{ route('portal.projects.create') }}" class="btn btn-sm btn-primary mb-3">
        <span class="d-none d-sm-inline-block text-white">@lang('admin.create')</span> <i class="icmn-plus text-white"><!-- --></i>
    </a>

    @if($projects->isNotEmpty())
        @foreach($projects as $project)
            <a href="{{ route('portal.projects.show', $project) }}">
            <div class="sp-card mb-3 project-card ">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th>@lang('portal.project')</th>
                            <th>@lang('project.project_category_id')</th>
                            <th>@lang('project.cost_filter')</th>
                            <th>@lang('portal.invested')</th>
                            <th>@lang('project.readiness')</th>
                            <th>@lang('project.id')</th>
                            {{--<th></th>--}}
                        </tr>
                        <tr>
                            <td>
                                <p class="pa-project-text">
                                    {{ $project->title }}
                                </p>
                                <div class="text-center">
                                    <img src="{{ $project->logoUrl() }}" alt="{{ $project->title }}" class="img-fluid d-inline-block" style="max-width: 150px;">
                                </div>
                            </td>
                            <td>
                                <div class="pa-project-text">{{ $project->category->name }}</div>
                            </td>
                            <td>
                                <div class="pa-project-text">
                                    {{ number_format($project->usd_cost, 0, '', ' ') }} @lang('portal.sum')
                                </div>
                            </td>
                            <td>
                                <div class="pa-project-text">
                                    {{ number_format($project->activeInvestments->sum('sum'), 0, '', ' ') }} @lang('portal.sum')
                                </div>
                            </td>
                            <td>
                                <div class="pa-project-text">
                                    {{ $project->readiness }}
                                </div>
                            </td>
                            <td>
                                <div class="pa-project-text">
                                    {{ $project->id }}
                                    <br>
                                    <span class="small">
                                        @lang('project.statuses.'.$project->status)
                                    </span>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            </a>
        @endforeach
    @endif

    @include('site.pagination', ['data' => $projects])
@endsection
