@extends('portal.layout')

@section('center_content')

    @include('portal.profile.menu')


    <section class="mb-4">
        <div class="row">
            <div class="col-lg-3 col-md-6 text-center">
                <img src="{{ $project->imageUrl() }}" alt="{{ $project->title }}" class="d-inline-block img-fluid">
            </div>
            <div class="col-lg-9 col-md-6 ">
                <p class="passport-title">
                    <img src="{{ asset('images/i_stat_9.png') }}" alt="@lang('portal.projects')"
                         class="d-inline-block" style="width: 35px;">
                    @lang('portal.projects')
                </p>
                <div class="row mb-3">
                    <div class="col-lg-4">
                        <a href="{{ route('portal.projects.index') }}">
                            <div class="passport-navigation">
                                @lang('portal.my_projects') <span>({{ Auth::user()->projects->count() }})</span>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4">
                        <div class="passport-navigation">
                            @lang('portal.project_views') <span>({{ number_format($project->view, 0, '', ' ') }})</span>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="passport-navigation">
                            @lang('portal.project_investments') <span>({{ number_format($project->activeInvestments->count(), 0, '', ' ') }})</span>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-lg-12">
                        <p class="passport-title">
                            @lang('portal.investors_id')
                        </p>
                    </div>
                </div>
                @if($project->activeInvestments->isNotEmpty())
                    <div class="row ">
                        <div class="col-lg-8 mb-3">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    @foreach($project->activeInvestments->slice(0, 3) as $investment)
                                        <tr>
                                            <td class="passport-investor-name">{{ $investment->user->name }}</td>
                                            <td class="text-right passport-investor-id">
                                                @lang('portal.id') {{ $investment->user->id }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="col-lg-4 align-self-end">
                            <button class="btn btn-additional-filters  px-4 pr-2 cur-pointer "
                                    style="white-space: unset;" data-toggle="modal" data-target="#investments_modal">
                                @lang('portal.all_investors')
                                <img src="{{ asset('images/i_additional_filters.png') }}"
                                     alt="@lang('portal.additional_filters')" class="filter-icon pl-2">
                            </button>

                            <div class="modal fade" id="investments_modal" tabindex="-1" role="dialog"
                                 aria-labelledby="investments_modalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title"
                                                id="investments_modalLabel">@lang('portal.all_investors')</h5>
                                            <button type="button" class="close cur-pointer" data-dismiss="modal"
                                                    aria-label="Close">
                                                <span class="text-white" aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped">
                                                    @foreach($project->activeInvestments as $investment)
                                                        <tr>
                                                            <td class="passport-investor-name text-white">{{ $investment->user->name }}</td>
                                                            <td class="passport-investor-name text-white">{{ number_format($investment->sum, 0, '', ' ') }} @lang('portal.sum')</td>
                                                            <td class="text-right passport-investor-id">
                                                                @lang('portal.id') {{ $investment->user->id }}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-modal cur-pointer"
                                                    data-dismiss="modal">@lang('portal.close')</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="row ">
                        <div class="col-lg-12">
                            <p class="text-muted">
                                @lang('portal.no_investors')
                            </p>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
    <section class="mb-4">
        <div class="sp-card project-card mb-4">
            <div class="row mb-4">
                <div class="col-md-10 col-9">
                    <div class="passport-project-title my-3 p-3">
                        @lang('portal.project_passport')
                        <br>
                        <span>{{ $project->title }}</span>
                    </div>
                </div>
                <div class="col-md-2 col-3 ">
                    <div class="text-center p-3">
                        <img src="{{ $project->logoUrl() }}" alt="{{ $project->title }}"
                             class="img-fluid project_image d-inline-block">
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-striped table-passport mb-4">
                    <tr>
                        <td>@lang('project.id')</td>
                        <td>{{ $project->id }}</td>
                    </tr>
                    <tr>
                        <td>@lang('project.project_category_id')</td>
                        <td>{{ $project->category->name }}</td>
                    </tr>
                    <tr>
                        <td>@lang('project.project_type_id')</td>
                        <td>{{ $project->type->name }}</td>
                    </tr>
                    <tr>
                        <td>@lang('project.title')</td>
                        <td>{{ $project->title }}</td>
                    </tr>
                    <tr>
                        <td>@lang('project.stage')</td>
                        <td>@lang('project.stages.'.$project->stage)</td>
                    </tr>
                    <tr>
                        <td>@lang('project.developer')</td>
                        <td>{{ $project->developer }}</td>
                    </tr>
                    <tr>
                        <td>@lang('project.foreign')</td>
                        <td>{{ $project->foreign ? trans('project.yes'): trans('project.no') }}</td>
                    </tr>
                    <tr>
                        <td>@lang('project.readiness')</td>
                        <td>{{ $project->readiness }}%</td>
                    </tr>
                    <tr>
                        <td>@lang('project.patent')</td>
                        <td>{{ $project->patent ?? trans('project.no') }}</td>
                    </tr>
                    <tr>
                        <td>@lang('project.implementation_period')</td>
                        <td>{{ $project->implementation_period }} @lang('project.period_unit')</td>
                    </tr>
                    <tr>
                        <td>@lang('project.payback_period')</td>
                        <td>{{ $project->payback_period }} @lang('project.period_unit')</td>
                    </tr>
                    <tr>
                        <td>@lang('project.cost_filter')</td>
                        <td>

                            @if($project->sum_cost > 0)
                                {{ number_format($project->sum_cost, 0, ',', ' ') }} @lang('portal.sum')
                            @else
                                --
                            @endif
                            /
                            @if($project->usd_cost > 0)
                                {{ number_format($project->usd_cost, 0, ',', ' ') }} @lang('portal.usd')
                            @else
                                --
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('project.need')</td>
                        <td>{{ number_format($project->sum_need, 0, ',', ' ') }} @lang('portal.sum')
                            /

                            @if($project->usd_need > 0)
                                {{ number_format($project->usd_need, 0, ',', ' ') }} @lang('portal.usd')
                            @else
                                --
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('portal.invested')</td>
                        <td>{{ number_format($project->activeInvestments->sum('sum'), 0, '', ' ') }} @lang('portal.sum')
                            / {{ $project->calcInvested() }}%
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('project.current')</td>
                        <td>
                            @if($project->sum_current > 0)
                                {{ number_format($project->sum_current, 0, ',', ' ') }} @lang('portal.sum')
                            @else
                                --
                            @endif
                            /
                            @if($project->usd_current > 0)
                                {{ number_format($project->usd_current, 0, ',', ' ') }} @lang('portal.usd')
                            @else
                                --
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('project.workers')</td>
                        <td>{{ $project->workers }} @lang('project.worker_unit')</td>
                    </tr>
                    <tr>
                        <td>@lang('project.importable')</td>
                        <td>{{ $project->importable ? trans('project.yes'): trans('project.no') }}</td>
                    </tr>
                    <tr>
                        <td>@lang('project.presentation')</td>
                        <td>
                            @if($project->presentation)
                                <a href="{{ $project->presentationUrl() }}">
                                    @lang('portal.download_presentation')
                                </a>
                            @else
                                --
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>@lang('project.place')</td>
                        <td>{{ $project->place }}</td>
                    </tr>
                    <tr>
                        <td>@lang('project.short_description')</td>
                        <td>{{ $project->short_description }}</td>
                    </tr>
                    <tr>
                        <td>@lang('project.description')</td>
                        <td>{{ $project->description }}</td>
                    </tr>
                    <tr>
                        <td>@lang('project.target')</td>
                        <td>{{ $project->target }}</td>
                    </tr>
                    <tr>
                        <td>@lang('project.finally')</td>
                        <td>{{ $project->finally  ?? '--' }}</td>
                    </tr>
                    <tr>
                        <td>@lang('project.state')</td>
                        <td>{{ $project->state  ?? '--'  }}</td>
                    </tr>
                    <tr>
                        <td>@lang('project.about_developer')</td>
                        <td>{{ $project->about_developer ?? '--' }}</td>
                    </tr>
                    <tr>
                        <td>@lang('project.about_presentation')</td>
                        <td>{{ $project->about_presentation ?? '--' }}</td>
                    </tr>
                    <tr>
                        <td>@lang('project.additional_info')</td>
                        <td>{{ $project->additional_info ?? '--' }}</td>
                    </tr>
                    <tr>
                        <td>@lang('project.status')</td>
                        <td>@lang('project.statuses.'.$project->status)</td>
                    </tr>
                </table>
            </div>

            <div class="mb-4 text-right p-3">
                @if($project->canUpdate())
                    <a href="{{ route('portal.projects.edit', $project) }}"
                       class="btn btn-additional-filters  cur-pointer ">
                        @lang('admin.edit')
                    </a>
                @endif
                @if($project->canDelete())
                    <form action="{{ route('portal.projects.destroy', $project) }}" class="d-inline-block" method="post">
                        @csrf
                        @method('delete')
                        <button class="btn btn-reset-filters   cur-pointer "
                                onclick="return confirm('@lang('admin.destroy_confirm')');">
                            @lang('admin.destroy')
                        </button>
                    </form>
                @endif
            </div>
            <div class="clearfix"></div>
        </div>
    </section>
@endsection
