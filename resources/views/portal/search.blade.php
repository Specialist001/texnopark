@extends('portal.layout')

@section('title')
    @lang('portal.title_search')
@endsection

@section('center_content')
    <div class="search_title mb-4">
        <h3>@lang('portal.search_project')</h3>
    </div>
    <section class="filters mb-4">
        <form action="{{ route('portal.search') }}" method="get">

            <div class="row">

                <div class="col-md-6 col-lg-3">
                    <div class="dropdown mb-3">
                        <button class="btn btn-filter btn-filter-green  dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @lang('portal.filter_category')
                        </button>
                        <div class="dropdown-menu" >
                            @foreach(\App\Domain\ProjectCategories\Models\ProjectCategory::orderBy('order')->get() as $category)
                                <div class="dropdown-item checkbox-dropdown-item">
                                    <label class="custom-control custom-filter custom-checkbox cur-pointer w-100 m-0">
                                        <span class="custom-control-description">{{ $category->name }}</span>
                                        <input type="checkbox"
                                               @if(isset($filters['project_category_id']))
                                               @if(is_array($filters['project_category_id']))
                                               @if(in_array($category->id, $filters['project_category_id'])) checked @endif
                                               @elseif($filters['project_category_id'] == $category->id)
                                               checked
                                               @endif
                                               @else
                                               @endif
                                               name="project_category_id[]" value="{{ $category->id }}" class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                    </label>
                                </div>
                                @if(!$loop->last)
                                    <div class="dropdown-divider m-0"></div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="dropdown mb-3">
                        <button class="btn btn-filter btn-filter-blue  dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @lang('portal.filter_readiness')
                        </button>
                        <div class="dropdown-menu" >
                            <div class="dropdown-item checkbox-dropdown-item">
                                <div class="form-group row m-0">
                                    <input class="form-control col-6 " step="1" min="0" max="100" type="number" placeholder="@lang('portal.from')" name="readiness[]" value="{{ $filters['readiness'][0] ?? '' }}">
                                    <input class="form-control col-6" step="1" min="0" max="100"  type="number" placeholder="@lang('portal.till')" name="readiness[]" value="{{ $filters['readiness'][1] ?? '' }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="dropdown mb-3">
                        <button class="btn btn-filter btn-filter-green  dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @lang('portal.filter_patent')
                        </button>
                        <div class="dropdown-menu" >
                            <div class="dropdown-item checkbox-dropdown-item">
                                <label class="custom-control custom-filter custom-radio cur-pointer w-100 m-0">
                                    <span class="custom-control-description">@lang('portal.all_patent')</span>
                                    <input type="radio"
                                           @if(isset($filters['hasPatent']))
                                           @if($filters['hasPatent'] != 'yes' && $filters['hasPatent'] != 'no')
                                           checked
                                           @endif
                                               @else
                                               checked
                                           @endif
                                           name="hasPatent" value="" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </div>
                            <div class="dropdown-divider m-0"></div>
                            <div class="dropdown-item checkbox-dropdown-item">
                                <label class="custom-control custom-filter custom-radio cur-pointer w-100 m-0">
                                    <span class="custom-control-description">@lang('portal.with_patent')</span>
                                    <input type="radio"
                                           @if(isset($filters['hasPatent']))
                                           @if($filters['hasPatent'] == 'yes')
                                           checked
                                           @endif
                                           @endif
                                           name="hasPatent" value="yes" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </div>
                            <div class="dropdown-divider m-0"></div>

                            <div class="dropdown-item checkbox-dropdown-item">
                                <label class="custom-control custom-filter custom-radio cur-pointer w-100 m-0">
                                    <span class="custom-control-description">@lang('portal.without_patent')</span>
                                    <input type="radio"
                                           @if(isset($filters['hasPatent']))
                                           @if($filters['hasPatent'] == 'no')
                                           checked
                                           @endif
                                           @endif
                                           name="hasPatent" value="no" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                </label>
                            </div>

                            <div class="dropdown-divider m-0"></div>
                            <div class="dropdown-item checkbox-dropdown-item">
                                <div class="form-group  m-0">
                                    <input class="form-control " type="text" placeholder="@lang('portal.patent_number')" name="patent" value="{{ $filters['patent'] ?? '' }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="dropdown mb-3">
                        <button class="btn btn-filter btn-filter-blue  dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @lang('portal.filter_type')
                        </button>
                        <div class="dropdown-menu" >
                            @foreach(\App\Domain\ProjectTypes\Models\ProjectType::all() as $type)
                                <div class="dropdown-item checkbox-dropdown-item">
                                    <label class="custom-control custom-filter custom-checkbox cur-pointer w-100 m-0">
                                        <span class="custom-control-description">{{ $type->name }}</span>
                                        <input type="checkbox"
                                               @if(isset($filters['project_type_id']))
                                               @if(is_array($filters['project_type_id']))
                                               @if(in_array($type->id, $filters['project_type_id'])) checked @endif
                                               @elseif($filters['project_type_id'] == $type->id)
                                               checked
                                               @endif
                                               @else
                                               checked
                                               @endif
                                               name="project_type_id[]" value="{{ $type->id }}" class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                    </label>
                                </div>
                                @if(!$loop->last)
                                    <div class="dropdown-divider m-0"></div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="hidden filters">

                <div class="col-md-6 col-lg-3">
                    <input class="form-control mb-3 search_filter" type="text" placeholder="@lang('portal.search_filter')" name="title" value="{{ $filters['title'] ?? '' }}">
                </div>

                <div class="col-md-6 col-lg-3 mb-3">
                    <div class="dropdown ">
                        <button class="btn btn-filter btn-filter-green  dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @lang('portal.filter_payback')
                        </button>
                        <div class="dropdown-menu" >
                            <div class="dropdown-item checkbox-dropdown-item">
                                <div class="form-group row m-0">
                                    <input class="form-control col-6 " step="1" min="0" type="number" placeholder="@lang('portal.from')" name="payback_period[]" value="{{ $filters['payback_period'][0] ?? '' }}">
                                    <input class="form-control col-6" step="1" min="0" type="number" placeholder="@lang('portal.till')" name="payback_period[]" value="{{ $filters['payback_period'][1] ?? '' }}">
                                </div>
                                <small class="form-control-feedback text-muted">@lang('portal.mth')</small>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3 mb-3">
                    <div class="dropdown ">
                        <button class="btn btn-filter btn-filter-blue  dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @lang('portal.filter_cost')
                        </button>
                        <div class="dropdown-menu" >
                            <div class="dropdown-item checkbox-dropdown-item">
                                <div class="form-group row m-0">
                                    <input class="form-control col-12 " step="1" min="0" type="number" placeholder="@lang('portal.from')" name="sum_cost[]" value="{{ $filters['sum_cost'][0] ?? '' }}">
                                    <input class="form-control col-12 " step="1" min="0" type="number" placeholder="@lang('portal.till')" name="sum_cost[]" value="{{ $filters['sum_cost'][1] ?? '' }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3 mb-3">
                    <div class="dropdown ">
                        <button class="btn btn-filter btn-filter-green  dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @lang('portal.filter_need')
                        </button>
                        <div class="dropdown-menu" >
                            <div class="dropdown-item checkbox-dropdown-item">
                                <div class="form-group row m-0">
                                    <input class="form-control col-12 " step="1" min="0" type="number" placeholder="@lang('portal.from')" name="sum_need[]" value="{{ $filters['sum_need'][0] ?? '' }}">
                                    <input class="form-control col-12 " step="1" min="0" type="number" placeholder="@lang('portal.till')" name="sum_need[]" value="{{ $filters['sum_need'][1] ?? '' }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="filter-buttons">
                {{--<button class="btn btn-additional-filters  px-4 pr-2 cur-pointer mb-2">--}}
                    {{--@lang('portal.additional_filters')--}}
                    {{--<img src="{{ asset('images/i_additional_filters.png') }}" alt="@lang('portal.additional_filters')" class="filter-icon pl-2">--}}
                {{--</button>--}}
                <div class="float-lg-right">
                    <a href="{{ route('portal.search') }}" class="btn btn-reset-filters   px-4 pr-2 cur-pointer mr-md-2 mb-2">@lang('portal.reset_filter')
                        <img src="{{ asset('images/i_reset_filters.png') }}" alt="@lang('portal.reset_filter')" class="filter-icon pl-2"></a>
                    <button class="btn btn-apply-filters   px-4 pr-2 cur-pointer mb-2">@lang('portal.apply_filter')
                        <img src="{{ asset('images/i_apply_filters.png') }}" alt="@lang('portal.apply_filter')" class="filter-icon pl-2"></button>
                </div>
                <div class="clearfix"></div>
            </div>
        </form>
    </section>

    <section class="search_page_title py-3 mb-4">
        <h4>{{ $total_count }} @choice('portal.projects_choice', $total_count)
        <small>@lang('portal.found_on_page')</small>
        </h4>
    </section>

    <section class="mb-4">
        @include('site.pagination', ['data' => $projects])
    </section>

    @foreach($projects->chunk(2) as $chunks)
        <div class="row">
            @foreach($chunks as $project)
                <div class="col-lg-6">
                    <a href="{{ route('portal.project', $project->id) }}">
                        <div class="sp-card mb-3 p-4 project-card p-relative">
                            @if(Auth::check() && Auth::user()->isInvestor())
                            <i class="{{ in_array($project->id, $favorites)? 'icmn-star-full': 'icmn-star-empty' }} fav_icon cur-pointer" data-project="{{$project->id}}"></i>
                            @endif
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="project_card-title my-4">
                                        @if(mb_strlen($project->title) > 50)
                                            {{ mb_substr($project->title, 0, 50).'...' }}
                                        @else
                                            {{ $project->title }}
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3  ">
                                    <div class="mb-4 text-center">
                                        <img src="{{ $project->logoUrl() }}" alt="{{ $project->title }}"
                                             class="img-fluid project_image d-inline-block">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="project_card-invested">
                                        {{ $project->calcInvested() }}% @lang('portal.invested')
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: {{ $project->calcInvested() }}%" aria-valuenow="{{ $project->calcInvested() }}" aria-valuemin="0" aria-valuemax="{{ $project->calcInvested() }}"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="project_card-short_description">
                                        <p class="title mb-1">
                                            @lang('portal.short_description'):
                                        </p>
                                        <p class="short_description" >
                                            @if(mb_strlen($project->short_description) > 200)
                                                {{ mb_substr($project->short_description, 0, 200).'...' }}
                                            @else
                                                {{ $project->short_description }}
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="project_card-hr my-3"></div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="project_card-short_description">
                                        <p class="title mb-1">
                                            @lang('portal.category_id'):
                                        </p>
                                        <p class="short_description">
                                            {{ $project->category->name }}
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="project_card-short_description">
                                        <p class="title mb-1">
                                            @lang('portal.state_readiness'):
                                        </p>
                                        <p class="short_description">
                                            {{ $project->state }}
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="project_card-hr my-3"></div>

                            <div class="float-md-right mt-2">
                                <a href="{{ route('portal.project', $project->id) }}" class="btn btn-additional-filters  px-4 pr-2 cur-pointer mb-2 text-capitalize">
                                    @lang('portal.more')
                                </a>
                                <a href="{{ (Auth::check()) ? route('portal.project', $project->id)."#invest" : route('portal.auth.loginForm') }}" class="btn btn-apply-filters   px-4 pr-2 cur-pointer mb-2 text-capitalize">@lang('portal.invest')</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    @endforeach

    <section class="mb-4">
        @include('site.pagination', ['data' => $projects])
    </section>
@endsection
@push('scripts')
    <script>
        $(function () {
           $('.fav_icon').on('click', function () {
               var btn = $(this);
               var id = btn.data('project');
               $.ajax({
                   method: "PATCH",
                   url: "{{ route('portal.favorites.add') }}/" + id,
                   success: function(response) {
                       if(response.result === "success") {
                           if (btn.hasClass('icmn-star-empty')) {
                               btn.removeClass('icmn-star-empty');
                               btn.addClass('icmn-star-full');
                           } else {
                               btn.removeClass('icmn-star-full');
                               btn.addClass('icmn-star-empty');
                           }
                           return false;
                       } else {
                           alert("{{trans('admin.ajax_error')}}");
                       }
                   },
                   error: function(response) {
                       alert("{{trans('admin.ajax_error')}}");
                   }
               });

               return false;
           });
        });
    </script>
@endpush
