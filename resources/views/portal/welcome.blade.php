@extends('portal.layout')

@section('title')
    @lang('portal.title_main')
@endsection

@section('center_content')
    <section class="info mb-5">
        <div class="row">
            <div class="col-md-4 text-center">
                <img src="{{ asset('images/bg_info.png') }}" alt="{{ config('app.name') }}" class="img-fluid">
            </div>
            <div class="col-md-8">
                <div class="info_text">
                    <h2>@lang('portal.banner_title')</h2>
                    <p>
                        @lang('portal.banner_text')
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="fast_links mb-5">
        <div class="row">
            <div class="col-md-4">
                <a href="{{ route('portal.search') }}">
                    <div class="sp-card p-3 fast_link-search text-center text-lg-left">
                        <span class="fl_title d-none d-lg-inline-block">@lang('portal.fl_search')</span>
                        <img src="{{ asset('images/i_fl_search.png') }}" alt="@lang('portal.fl_search')"
                             class="img-fluid float-lg-right fl_icon">
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="{{ (Auth::check()) ? route('portal.projects.create'): route('portal.auth.loginForm') }}">
                    <div class="sp-card p-3 fast_link-add text-center text-lg-left">
                        <span class="fl_title d-none d-lg-inline-block">@lang('portal.fl_add')</span>
                        <img src="{{ asset('images/i_fl_add.png') }}" alt="@lang('portal.fl_add')"
                             class="img-fluid float-lg-right fl_icon">
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="{{ (Auth::check()) ? route('portal.investments.index'): route('portal.auth.registerForm') }}">
                    <div class="sp-card p-3 fast_link-reg text-center text-lg-left">
                        <span class="fl_title d-none d-lg-inline-block">@lang('portal.fl_reg')</span>
                        <img src="{{ asset('images/i_fl_reg.png') }}" alt="@lang('portal.fl_reg')"
                             class="img-fluid float-lg-right fl_icon">
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </section>
    <section class="d-none d-lg-block titles mb-3 py-3">
        <div class="row">
            <div class="col-lg-7">
                <h2>@lang('portal.last_projects')</h2>
            </div>
            <div class="col-lg-5">
                <h2>@lang('portal.categories')</h2>
            </div>
        </div>
    </section>
    <section class="content_main_page mb-3">
        <div class="row">
            <div class="col-lg-7">
                <div class="d-block d-lg-none titles mb-3 py-3">
                    <h2>@lang('portal.last_projects')</h2>
                </div>
                <div class="mb-5">
                    @forelse($projects as $project)
                        <a href="{{ route('portal.project', $project->id) }}" class="project_link">
                            <div class="sp-card p-4 mb-3">
                                <div class="row">
                                    <div class="col-8">
                                        <div class="project-date">
                                            {{ $project->created_at->format('d.m.Y') }}
                                        </div>
                                        <div class="project-title mb-2" >
                                            @if(mb_strlen($project->title) > 75)
                                                {{ mb_substr($project->title, 0, 75).'...' }}
                                            @else
                                                {{ $project->title }}
                                            @endif
                                        </div>
                                        <div class="project-short_description" >
                                            @if(mb_strlen($project->short_description) > 200)
                                                {{ mb_substr($project->short_description, 0, 200).'...' }}
                                            @else
                                                {{ $project->short_description }}
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4 text-center">
                                        <img src="{{ $project->logoUrl() }}" alt="{{ $project->title }}"
                                             class="img-fluid project_image d-inline-block">
                                    </div>
                                </div>
                            </div>
                        </a>
                    @empty
                        <p class="text-muted">
                            @lang('portal.no_projects')
                        </p>
                    @endforelse
                </div>
            </div>
            <div class="col-lg-5">
                <div class="d-block d-lg-none titles mb-3 py-3">
                    <h2>@lang('portal.categories')</h2>
                </div>
                <div class="mb-5">
                    @forelse($categories as $category)
                        <a href="{{ route('portal.search', ['project_category_id' => $category->id]) }}" class="category_link">
                            <div class="sp-card p-3 mb-1">
                                <div class="row">
                                    <div class="col-9 col-sm-10">
                                        <span class="cat_title d-block">
                                            {{ $category->name }}
                                        </span>
                                        <span class="cat_projects_count d-block">
                                            <strong>{{ $category->projects_count }}</strong> @choice('portal.projects_choice', $category->projects_count)
                                            <img src="{{ asset('images/i_cat_more.png') }}" alt="@lang('portal.more')"
                                                 class="px-3 d-none d-lg-inline-block">
                                        </span>
                                    </div>
                                    <div class="col-3 col-sm-2 text-center">
                                        <img src="{{ $category->iconUrl() }}" alt="{{ $category->name }}"
                                             class="img-fluid cat_img d-inline-block">
                                    </div>
                                </div>
                            </div>
                        </a>
                    @empty
                        <p class="text-muted">
                            @lang('portal.no_categories')
                        </p>
                    @endforelse
                </div>
            </div>
        </div>
    </section>
    @if($partners->isNotEmpty())
        <section class="mb-5 partners_section ">
            <h2 class="text-center text-md-left mb-4">
                @lang('portal.partners')
                <div class="float-md-right d-none d-md-inline-block">
                    <a href="{{ route('site.partners') }}" class="all-news">
                        @lang('portal.all_partners') <img src="{{ asset('images/site/i_all_news.png') }}"
                                                          alt="@lang('site.all_partners')" class="d-inline-block ml-2">
                    </a>
                </div>
                <div class="clearfix"></div>
            </h2>
            <div class="row ">
            @foreach($partners as $item)
                <div class="col-lg-3 col-md-4 col-6">
                    @if($item->link != '')
                        <a href="{{ $item->link }}" target="_blank">
                            @endif
                            <div class="partners-card p-3 mb-3 text-center">
                                <img src="{{ $item->iconUrl() }}" alt="." class="img-fluid">
                            </div>
                            @if($item->link != '')
                        </a>
                    @endif
                </div>
            @endforeach
            </div>

            <div class="text-center d-md-none my-3">
                <a href="{{ route('site.partners') }}" class="all-news ">
                    @lang('portal.all_partners') <img src="{{ asset('images/site/i_all_news.png') }}"
                                                      alt="@lang('site.all_partners')" class="d-inline-block ml-2">
                </a>
            </div>
        </section>
    @endif
    @if($news->isNotEmpty())
        <section class="mb-3 partners_section ">
            <h2 class="text-center text-md-left mb-4">
                @lang('portal.news')
                <div class="float-md-right d-none d-md-inline-block">
                    <a href="{{ route('site.news.index') }}" class="all-news">
                        @lang('portal.all_news') <img src="{{ asset('images/site/i_all_news.png') }}"
                                                          alt="@lang('site.all_news')" class="d-inline-block ml-2">
                    </a>
                </div>
                <div class="clearfix"></div>
            </h2>
            <div class="row ">
            @foreach($news as $item)
                <div class="col-lg-4 col-md-6">
                    <a href="{{ route('site.news.show', $item->uri) }}" >
                        <div class="news-card mb-md-5 text-center">
                            <p class="title text-left">
                                {{ $item->name }}
                            </p>
                            <img src="{{ $item->thumbUrl() }}" alt="" class="img-fluid">
                            <p class="description text-left">
                                {{ $item->short }}
                            </p>
                        </div>
                    </a>
                </div>
            @endforeach
            </div>

            <div class="text-center d-md-none my-3">
                <a href="{{ route('site.news.index') }}" class="all-news ">
                    @lang('portal.all_news') <img src="{{ asset('images/site/i_all_news.png') }}"
                                                      alt="@lang('site.all_news')" class="d-inline-block ml-2">
                </a>
            </div>
        </section>
    @endif
@endsection
