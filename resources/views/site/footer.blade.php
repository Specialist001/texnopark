<div class="container">
    <div class="row py-4">
        <div class="col-lg-9 col-md-20 my-2">
            <div class="mt-1">
                <a href="#" class="footer-logo">
                    <img src="{{asset('images/footer-logo.png')}}" class="img-fluid" alt="">
                </a>
            </div>
        </div>
        <div class="col-lg-11 col-md-20 my-2">
            <div class="mb-3">
                <strong class="text-light">@lang('site.site_map')</strong>
            </div>
            <ul class="nav font-weight-light flex-column">
                @if(isset($about_pages))
                    @foreach($about_pages as $about_page)
                        <li>
                            <a href="{{ route('site.page', $about_page->id) }}">
                                {{ $about_page->name }}
                            </a>
                        </li>
                    @endforeach
                @endif

                <li><a href="{{ route('site.project.index') }}">@lang('site.projects')</a></li>
                <li><a href="{{ route('site.news.index') }}">@lang('site.news')</a></li>
                <li><a href="{{ route('site.vacancy.index') }}">@lang('site.all_vacs')</a></li>
                @if(isset($page_contact))
                <li>
                    <a href="{{ route('site.page', $page_contact->id) }}">
                        {{ $page_contact->name }}
                    </a>
                </li>
                @endif
            </ul>
        </div>
        <div class="col-lg-18 col-md-40 my-2">
            <div class="mb-3">
                <strong class="text-light">
                    @lang('site.subscribe')
                </strong>
            </div>
            <p>
                @lang('site.subscribe_text')
            </p>
            <form action="/" id="subscribeForm">

                <div class="form-row">
                    <div class="col">
                        <input type="hidden" class="form-control " id="subscribeName"  name="name" placeholder="@lang('site.name')" value="user">
                        <input type="email" class="form-control " name="email" id="subscribeEmail" required placeholder="@lang('site.mail')">

                    </div>
                    <div class="col-auto">
                        <button  class="btn btn-primary">@lang('site.subscribe_button')</button>

                    </div>
                </div>
            </form>
        </div>

        <div class="col-lg-20 col-md-20 my-2">
            <div class="mb-3 ml-3">
                <strong class="text-light">@lang('site.contacts')</strong>
            </div>
            <div class="col-md-auto my-1">
                @if(isset($contact['phone']) && $contact['phone']->value != '')
                    <div class="footer-contact footer_phone mb-3">
                        <i class="fas fa-phone-alt mr-2"></i>
                        {!! nl2br($contact['phone']->value) !!}
                    </div>
                @endif
                @if(isset($contact['email']) && $contact['email']->value != '')
                    <div class="footer-contact footer_mail mb-3">
                        <i class="fas fa-mail-bulk mr-2"></i>
                        {!! nl2br($contact['email']->value) !!}
                    </div>
                @endif
                @if(isset($contact['address_'.app()->getLocale()]) && $contact['address_'.app()->getLocale()]->value != '')
                    <div class="footer-contact footer_address mb-3">
                        <i class="fas fa-map-marker-alt mr-2"></i>
                        {!! nl2br($contact['address_'.app()->getLocale()]->value) !!}
                    </div>
                @endif
            </div>
        </div>


    </div>
    <hr>
    <div class="row py-3 text-center">
        <div class="col-md-auto my-1">
            © @lang('site.copyright') {!! ((date('Y') > 2019) ? '2019-' : '') . date('Y') !!}
        </div>
        <div class="col-md my-1">
            @if(isset($contact['facebook']) && $contact['facebook']->value != '')
                <a href="{{ $contact['facebook']->value }}" class="px-2 link_fb" target="_blank">
                    <i class="fab fa-facebook-f"></i> Facebook
                </a>
            @endif
            @if(isset($contact['telegram']) && $contact['telegram']->value != '')
                <a href="{{ $contact['telegram']->value }}" class="px-2 link_tg" target="_blank">
                    <i class="fab fa-telegram-plane"></i> Telegram
                </a>
            @endif
            @if(isset($contact['youtube']) && $contact['youtube']->value != '')
                <a href="{{ $contact['youtube']->value }}" class="px-2 link_youtube" target="_blank">
                    <i class="fab fa-youtube"></i> Youtube
                </a>
            @endif
            @if(isset($contact['instagram']) && $contact['instagram']->value != '')
                <a href="{{ $contact['instagram']->value }}" class="px-2 link_insta" target="_blank">
                    <i class="fab fa-instagram"></i></i> Instagram
                </a>
            @endif
        </div>
        
        <div class="col-md-auto my-1">
            <span>Разработка сайтов </span> <a href="https://qwerty.uz" target="_blank">QWERTY</a>
        </div>

    </div>
</div>

<div class="mobile-menu">
    <div class="mobile-menu-overlay"></div>
    <div class="mobile-menu-panel">
        <div class="mobile-menu-viewport">
            <div class="mobile-search mb-3">
                
            </div>
            <ul class="nav flex-column mobile-menu-links">
                
            </ul>
        </div>
    </div>
</div>


<div class="modal fade" id="contactUs" tabindex="-1" role="dialog" aria-labelledby="contactUs" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title w-100 text-center text-primary" id="contactUsLabel">@lang('site.callback_form_title')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/" id="contact">
                    @csrf
                    <div class="modal-body">
{{--                        <h4 class="section-heading text-primary p-relative">--}}
{{--                            <span class="main-text">--}}
{{--                                @lang('site.callback_form_title')--}}
{{--                            </span>--}}
{{--                        </h4>--}}
                        <div class="mb-1">
                            <div class="row">
                                <div class="col-lg">
                                    <div class="row">
                                        <div class="col-md-60">
                                            <div class="input-group mb-4">
{{--                                                <div class="input-group-addon">--}}
{{--                                                    <img src="{{ asset('images/site/i_form_name.png') }}" alt="@lang('site.name')" class="my-2" style="width: 15px;">--}}
{{--                                                </div>--}}
                                                <input type="text" class="form-control py-2" id="inlineFormInputGroup" name="name" required placeholder="@lang('site.name')">
                                            </div>
                                        </div>
                                        <div class="col-md-60">
                                            <div class="input-group mb-4">
{{--                                                <div class="input-group-addon">--}}
{{--                                                    <img src="{{ asset('images/site/i_form_phone.png') }}" alt="@lang('site.phone')" class="my-2" style="width: 14px;">--}}
{{--                                                </div>--}}
                                                <input type="text" class="form-control py-2 phone-input" id="inlineFormInputGroup" name="phone"  placeholder="@lang('site.phone')">
                                            </div>
                                        </div>
                                        <div class="col-md-60">
                                            <div class="input-group mb-4">
{{--                                                <div class="input-group-addon">--}}
{{--                                                    <img src="{{ asset('images/site/i_form_mail.png') }}" alt="@lang('site.mail')" class="my-2">--}}
{{--                                                </div>--}}
                                                <input type="email" class="form-control py-2" id="inlineFormInputGroup" name="email" placeholder="@lang('site.mail')">
                                            </div>
                                        </div>
                                        <div class="col-md-60">
                                            <textarea name="comment" required class="form-control" placeholder="@lang('site.comment')" rows="5"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="text-center">
                            <button class="btn btn-primary rounded" >
                                @lang('site.callback_form_button')
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

</div>

@push('scripts')
    <script>
        $(function () {
            $('.form_wrapper input').on('focus', function () {
                 $(this).parent().css('opacity', 1);
            });
            $('.form_wrapper input').on('blur', function () {
                 $(this).parent().css('opacity', '0.7');
            });
            $('#contact').submit(function (e) {
                var form = $(this);
                form.find('button').prop('disabled', true);
                form.find('button').removeClass('animated');
                $.ajax({
                    method: "POST",
                    data: form.serialize(),
                    url: "{{ route('site.contact-us') }}",
                    success: function(response) {
                        if(response.result === "success") {
                            alert("@lang('site.contact_success')");
                            // form.find('button').prop('disabled', false);
                            return false;
                        } else {
                            form.find('button').prop('disabled', false);
                            alert("{{trans('admin.ajax_error')}}");
                        }
                    },
                    error: function(response) {
                        alert("{{trans('admin.ajax_error')}}");
                        form.find('button').prop('disabled', false);
                    }
                });
                return false;
            });

            $('#subscribeForm').submit(function (e) {
                var form = $(this);
                form.find('button').prop('disabled', true);
                // form.find('button').removeClass('animated');
                $.ajax({
                    method: "POST",
                    data: form.serialize(),
                    url: "{{ route('site.subscribe') }}",
                    success: function(response) {
                        if(response.result !== undefined) {
                            alert(response.result);
                            // form.find('button').prop('disabled', false);
                            return false;
                        } else {
                            form.find('button').prop('disabled', false);
                            alert("{{trans('admin.ajax_error')}}");
                        }
                    },
                    error: function(response) {
                        alert("{{trans('admin.ajax_error')}}");
                        form.find('button').prop('disabled', false);
                    }
                });
                return false;
            });

        });
    </script>
@endpush
@component('component.phone-stacks')@endcomponent
@push('styles')
    <style>
        .site-footer a:hover {
            text-decoration:none;
            color: #0598d6!important;
        }
        .site-footer a.link_fb:hover {
            color: #3b5998!important;
        }
        .site-footer a.link_tg:hover {
            color: #0088cc!important;
        }
        .site-footer a.link_youtube:hover {
            color: #ff3434!important;
        }
        .site-footer a.link_insta:hover {
            color: #C13584!important;
        }
    </style>
@endpush