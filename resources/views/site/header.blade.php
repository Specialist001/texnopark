<div class="header-search">
    <form>
        <input type="text" class="form-control" placeholder="Что нужно найти?">
        <button class="header-search-btn">
            <i class="fal fa-search"></i>
        </button>
    </form>
</div>

<div class="container-fluid position-relative">
    <div class="row">
        <div class="col col-md-auto bg-primary flex-centered">
            <div class="p-3">
                <a href="/" class="header-logo">
                    <img src="{{ asset('images/site-logo.png') }}" class="img-fluid" alt="{{ route('site.home') }}">
                </a>
            </div>
        </div>
        <div class="col d-none d-lg-block">
            <nav class="header-menu">
                <ul class="nav d-inline-flex menu-visible overflow-hidden">


{{--                    <li class="nav-item">--}}
{{--                        <a href="{{ route('site.projects.index') }}" class="nav-link">--}}
{{--                            <span>Проекты</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                    @if(isset($about_pages))
                        <li class="nav-item dropdown w-auto">
                            <a class="dropdown-toggle nav-link" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span>@lang('site.about_us')</span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                @foreach($about_pages as $about_page)
                                <a class="dropdown-item" href="{{ route('site.page', $about_page->id) }}">{{ $about_page->name }}</a>
                                @endforeach

                            </div>
                        </li>
                    @endif
{{--                    @if(isset($page_about))--}}
{{--                    <li class="nav-item">--}}
{{--                        <a href="{{ route('site.page', $page_about->id) }}" class="nav-link">--}}
{{--                            <span>--}}
{{--                            {{ $page_about->name }}--}}
{{--                            </span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    @endif--}}

                    <li class="nav-item">
                        <a href="{{ route('site.project.index') }}" class="nav-link">
                            <span>@lang('site.projects')</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('site.news.index') }}" class="nav-link">
                            <span>@lang('site.news')</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('site.vacancy.index') }}" class="nav-link">
                            <span>@lang('site.all_vacs')</span>
                        </a>
                    </li>
                    @if(isset($page_contact))
                    <li class="nav-item">
                        <a href="{{ route('site.page', $page_contact->id) }}" class="nav-link">
                            <span>
                            {{ $page_contact->name }}
                            </span>
                        </a>
                    </li>
                    @endif
{{--                    <li class="nav-item">--}}
{{--                        <a href="{{ route('site.news.index') }}" class="nav-link">--}}
{{--                            <span>Новости</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                </ul>
                <div class="dropdown menu-hidden">
                    <button class="btn btn-link text-muted mx-0" data-toggle="dropdown">
                        <i class="far fa-bars fa-lg"></i>
                    </button>
                    <ul class="dropdown-menu"></ul>
                </div>
            </nav>
        </div>
        <div class="col-auto ml-auto ml-md-0 mr-md-auto flex-centered d-lg-none">
            <button class="btn text-muted px-1" id="menu-toggle">
                <i class="far fa-bars fa-fw fa-lg first-i"></i>
                <i class="far fa-times fa-fw fa-lg second-i"></i>
            </button>
        </div>
        <div class="col-auto flex-centered ml-auto d-none d-lg-flex">
            <a href="#" class="btn btn-outline-primary" data-toggle="modal"
               data-target="#contactUs">
                <span>@lang('site.contact_us')</span>
                <i class="fas fa-plus fa-lg ml-1"></i>
            </a>
{{--            <img src="{{ asset('images/site/i_proof.png') }}" data-toggle="modal"--}}
{{--                 data-target="#proofReading" alt="@lang('site.proof_reading')"--}}
{{--                 class="proof_reading-button cur-pointer">--}}
        </div>

        <div class="col-auto flex-centered d-none d-md-flex">
            <div class="dropdown">
                <button class="btn" data-toggle="dropdown">
                    <img
                        src="{{ asset('images/flag-circle-'.LaravelLocalization::getCurrentLocale().'.png') }}"
                        alt="{{LaravelLocalization::getCurrentLocale()}}"
                        class="align-middle" >
                    <span class="align-middle" style="text-transform: uppercase">{{LaravelLocalization::getCurrentLocale()=='ru' ? 'РУ' : LaravelLocalization::getCurrentLocale()}}</span>
                    <i class="fal fa-angle-down align-middle fa-sm"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        @if($localeCode != LaravelLocalization::getCurrentLocale())
                            <a class="dropdown-item" rel="alternate" hreflang="{{ $localeCode }}"
                               href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                <img src="{{ asset('images/flag-circle-'.$localeCode.'.png') }}"
                                     alt="{{$localeCode}}" class="align-middle">
                                <span class="align-middle" style="text-transform: uppercase">{{ $localeCode == 'ru' ? 'РУ' : $localeCode }}</span>
                            </a>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-auto px-0 d-none d-md-flex">
            <button class="btn btn-primary px-4 h-100 search-btn">
                <i class="fal fa-fw fa-search standby fa-lg"></i>
                <i class="fal fa-fw fa-times fa-lg"></i>
            </button>
        </div>
    </div>
</div>

{{--<section class="header_bg p-relative"--}}
{{--         @if($current_route_name == '')--}}
{{--         style="background: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5));"--}}
{{--         @else--}}
{{--         style="background:linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('@yield('bg', asset('images/site/bg_default.png'))') center center no-repeat"--}}
{{--    @endif--}}
{{-->--}}
{{--    <div class="container">--}}
{{--        <nav class="navbar navbar-search navbar-expand-lg navbar-light px-0 mx-0">--}}

{{--            <a class="navbar-brand" href="{{ route('site.home') }}">--}}
{{--                <img src="{{ asset('images/logo_white.png') }}" class="d-inline-block img-fluid align-top"--}}
{{--                     alt="{{ config('app.name') }}">--}}
{{--            </a>--}}

{{--            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"--}}
{{--                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">--}}
{{--                <span class="icmn-menu"></span>--}}
{{--            </button>--}}

{{--            <div class="collapse navbar-collapse top-navbar-collapse ml-5" id="navbarSupportedContent">--}}
{{--                <div class="mr-auto">--}}
{{--                    <ul class="pl-0  mb-0 ">--}}
{{--                        <li class="nav-item d-block ">--}}
{{--                            <ul class="navbar-nav  top-sub-navbar">--}}
{{--                                <li class="nav-item {{ $current_route_name == '' ? 'active': '' }} d-inline-block mr-lg-1">--}}
{{--                                    <a class="nav-link" href="{{ route('site.home') }}">@lang('site.home') </a>--}}
{{--                                </li>--}}
{{--                                <li class="nav-item {{ $current_route_name == 'news' ? 'active': '' }} d-inline-block mr-lg-1">--}}
{{--                                    <a class="nav-link" href="{{ route('site.news.index') }}">@lang('site.news') </a>--}}
{{--                                </li>--}}
{{--                                <li class="nav-item {{ $current_route_name == 'partners' ? 'active': '' }} d-inline-block mr-lg-1">--}}
{{--                                    <a class="nav-link" href="{{ route('site.partners') }}">@lang('site.partners') </a>--}}
{{--                                </li>--}}
{{--                                <li class="nav-item {{ $current_route_name == 'videos' ? 'active': '' }} d-inline-block mr-lg-1">--}}
{{--                                    <a class="nav-link"--}}
{{--                                       href="{{ route('site.video.index') }}">@lang('site.our_video') </a>--}}
{{--                                </li>--}}
{{--                                <li class="nav-item  d-inline-block mr-lg-1">--}}
{{--                                    <a class="nav-link proof_reading-button cur-pointer" data-toggle="modal"--}}
{{--                                       data-target="#proofReading">@lang('site.proof_reading') </a>--}}
{{--                                </li>--}}
{{--                                @foreach($top_pages as $pg)--}}
{{--                                    <li class="nav-item d-inline-block {{ $current_route_name == 'page' && $page_id == $pg->id ? 'active': '' }}">--}}
{{--                                        <a class="nav-link"--}}
{{--                                           href="{{ $pg->id == '83' ? 'http://constitution.uz/oz': route('site.page', $pg->id) }}">{{ $pg->name }} </a>--}}
{{--                                    </li>--}}
{{--                                @endforeach--}}
{{--                            </ul>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item d-block ">--}}
{{--                            <ul class="navbar-nav  top-navbar">--}}
{{--                                @foreach($page_categories as $category)--}}
{{--                                    @if($category->pages->isNotEmpty())--}}
{{--                                        <li class="nav-item {{ $current_route_name == 'page' && in_array($page_id, array_keys($category->pages->keyBy('id')->toArray())) ? 'active': '' }} d-inline-block mr-lg-2 pb-3 dropdown">--}}
{{--                                            <a class="nav-link dropdown-toggle dropdown-toggle-no-icon cur-pointer"--}}
{{--                                               data-toggle="dropdown">{{ $category->name }}</a>--}}
{{--                                            <div class="dropdown-menu">--}}
{{--                                                @foreach($category->pages as $page)--}}
{{--                                                    <div class="dropdown-item">--}}
{{--                                                        <a href="{{ $page->id == '83' ? 'http://constitution.uz/oz': route('site.page', $page->id) }}"--}}
{{--                                                           class="{{ $page_id == $page->id ? 'active': '' }}">--}}
{{--                                                            {{ $page->name }}--}}
{{--                                                        </a>--}}
{{--                                                    </div>--}}
{{--                                                @endforeach--}}
{{--                                            </div>--}}
{{--                                        </li>--}}
{{--                                    @endif--}}
{{--                                @endforeach--}}
{{--                                @if($labs->isNotEmpty())--}}
{{--                                    <li class="nav-item {{ $current_route_name == 'labs' && in_array($page_id, array_keys($labs->keyBy('id')->toArray())) ? 'active': '' }} d-inline-block mr-lg-2 pb-3 dropdown">--}}
{{--                                        <a class="nav-link dropdown-toggle dropdown-toggle-no-icon cur-pointer"--}}
{{--                                           data-toggle="dropdown">@lang('site.labs')</a>--}}
{{--                                        <div class="dropdown-menu">--}}
{{--                                            @foreach($labs as $lab)--}}
{{--                                                <div class="dropdown-item">--}}
{{--                                                    <a href="{{ route('site.labs', $lab->id) }}"--}}
{{--                                                       class="{{ $page_id == $lab->id ? 'active': '' }}">--}}
{{--                                                        {{ $lab->name }}--}}
{{--                                                    </a>--}}
{{--                                                </div>--}}
{{--                                            @endforeach--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endif--}}

{{--                                <li class="nav-item">--}}
{{--                                    <a href="{{ route('portal.home') }}" class="nav-link ">--}}
{{--                                        @lang('site.portal_link')--}}
{{--                                    </a>--}}
{{--                                </li>--}}

{{--                                --}}{{--<li class="nav-item {{ $current_route_name == 'page' && $page_id == $system_page->id ? 'active': '' }} d-inline-block pb-3">--}}
{{--                                --}}{{--<a class="nav-link" href="{{ route('site.page', $system_page->id) }}">@lang('site.about') </a>--}}
{{--                                --}}{{--</li>--}}
{{--                            </ul>--}}
{{--                        </li>--}}

{{--                    </ul>--}}
{{--                </div>--}}
{{--                <div class="header-right">--}}
{{--                    <div class="mb-3">--}}

{{--                        <div class="d-inline-block mr-2">--}}
{{--                            <a class="cur-pointer bvi-toggle">--}}
{{--                                <img src="{{ asset('images/eye.png') }}">--}}
{{--                            </a>--}}
{{--                        </div>--}}


{{--                        <form class="d-inline-block mr-2" action="{{ route('site.search.index') }}" method="get">--}}
{{--                            <div class="dropdown ">--}}
{{--                                <a id="dLabel" class="no-decoration dropdown-toggle dropdown-toggle-no-icon cur-pointer"--}}
{{--                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--                                    <img src="{{ asset('images/site/i_search.png') }}" alt="@lang('site.search')">--}}
{{--                                </a>--}}
{{--                                <div class="dropdown-menu search-dropdown px-2">--}}
{{--                                    <input type="text" class="form-control  search" name="q"--}}
{{--                                           value="--}}{{--{{ $filters['title'] ?? ''}}--}}{{--">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </form>--}}
{{--                        <div class="d-inline-block mr-2">--}}
{{--                            <img src="{{ asset('images/site/i_pen.png') }}" data-toggle="modal"--}}
{{--                                 data-target="#subscribeModal" alt="@lang('site.subscribe')"--}}
{{--                                 class="subscribe-button cur-pointer">--}}
{{--                        </div>--}}
{{--                        <div class="d-inline-block mr-2 ">--}}
{{--                            <img src="{{ asset('images/site/i_proof.png') }}" data-toggle="modal"--}}
{{--                                 data-target="#proofReading" alt="@lang('site.proof_reading')"--}}
{{--                                 class="proof_reading-button cur-pointer">--}}
{{--                        </div>--}}
{{--                        <div class="d-inline-block ">--}}
{{--                            <div class="dropdown lang-dropdown">--}}
{{--                                <a id="dLabel" class="no-decoration dropdown-toggle cur-pointer" data-toggle="dropdown"--}}
{{--                                   aria-haspopup="true" aria-expanded="false">--}}
{{--                                    <img--}}
{{--                                        src="{{ asset('images/flag-'.LaravelLocalization::getCurrentLocale().'.png') }}"--}}
{{--                                        alt="{{LaravelLocalization::getCurrentLocale()}}">--}}
{{--                                </a>--}}
{{--                                <div class="dropdown-menu dropdown-menu-lang">--}}
{{--                                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)--}}
{{--                                        @if($localeCode != LaravelLocalization::getCurrentLocale())--}}
{{--                                            <a class="dropdown-item" rel="alternate" hreflang="{{ $localeCode }}"--}}
{{--                                               href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">--}}
{{--                                                <img src="{{ asset('images/flag-'.$localeCode.'.png') }}"--}}
{{--                                                     alt="{{$localeCode}}">--}}
{{--                                            </a>--}}
{{--                                        @endif--}}
{{--                                    @endforeach--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <img src="{{ asset('images/site/bg_top_menu.png') }}" alt="." class="bg_top_menu d-none d-xl-inline-block">--}}
{{--        </nav>--}}

{{--        <section class="page-title">--}}

{{--            <div class="row">--}}
{{--                <div class="col-lg-7">--}}
{{--                    <div class="animated fadeInLeft ">--}}
{{--                        @yield('banner')--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-5">--}}
{{--                    <div class="text-lg-right">--}}
{{--                        @if(isset($contact['facebook']) && $contact['facebook']->value != '')--}}
{{--                            <div class="">--}}
{{--                                <a href="{{ $contact['facebook']->value }}" target="_blank"--}}
{{--                                   class="top_social d-inline-block  my-4">--}}
{{--                                    <img src="{{ asset('images/site/i_top_fb.png') }}" alt="@lang('site.facebook')"--}}
{{--                                         class="d-inline-block mr-3"> @lang('site.facebook')--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        @endif--}}
{{--                        @if(isset($contact['telegram']) && $contact['telegram']->value != '')--}}
{{--                            <div class="">--}}

{{--                                <a href="{{ $contact['telegram']->value }}" target="_blank"--}}
{{--                                   class="top_social d-inline-block mt-0 mb-4">--}}
{{--                                    <img src="{{ asset('images/site/i_top_tg.png') }}" alt="@lang('site.telegram')"--}}
{{--                                         class="d-inline-block mr-3"> @lang('site.telegram')--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </section>--}}
{{--    </div>--}}

{{--    <img src="{{ asset('images/site/bg_page_title.png') }}" alt="." class="bg_page_title d-none d-xl-inline-block">--}}
{{--    <div class="">--}}
{{--        <img src="{{ asset('images/site/bg_header_bot.png') }}" alt="." class="bg_header_bot">--}}
{{--        <div class="container p-relative" style="margin-top: -165px;">--}}
{{--            <img src="{{ asset('images/site/bg_header_bot_right.png') }}" alt="." class="bg_header_bot_right">--}}
{{--            <img src="{{ asset('images/site/bg_header_circle.png') }}" alt="." class="bg_header_circle">--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}
{{--@if($current_route_name == '')--}}
{{--    <div class="modal fade " id="mbVideoModal" tabindex="-1" role="dialog" aria-labelledby="mp_videoLabel"--}}
{{--         aria-hidden="true">--}}
{{--        <div class="modal-dialog modal-lg" role="document" style="max-width: 1100px">--}}
{{--            <div class="modal-content">--}}
{{--                <div class="modal-body position-relative">--}}

{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@endif--}}

{{--<div class="modal fade" id="proofReading" tabindex="-1" role="dialog" aria-labelledby="proofReadingLabel"--}}
{{--     aria-hidden="true">--}}
{{--    <div class="modal-dialog" role="document">--}}
{{--        <div class="modal-content">--}}
{{--            <div class="modal-header">--}}
{{--                <h5 class="modal-title" id="proofReadingLabel">@lang('site.proof_reading')</h5>--}}
{{--                <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                    <span aria-hidden="true">&times;</span>--}}
{{--                </button>--}}
{{--            </div>--}}
{{--            <form action="{{ route('site.proof.store') }}" method="post" enctype="multipart/form-data">--}}
{{--                @csrf--}}
{{--                <div class="modal-body">--}}
{{--                    <p class="text-center text-muted small">--}}
{{--                        @lang('site.proof_reading_description')--}}
{{--                    </p>--}}
{{--                    <div class="">--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-lg-12">--}}
{{--                                <div class="input-group mb-4">--}}
{{--                                    <input type="text" class="form-control " name="name" required--}}
{{--                                           placeholder="@lang('site.name')">--}}
{{--                                </div>--}}
{{--                                <div class="input-group mb-4">--}}
{{--                                    <input type="email" class="form-control " name="email" required--}}
{{--                                           placeholder="@lang('site.mail')">--}}
{{--                                </div>--}}
{{--                                <div class="input-group mb-4">--}}
{{--                                    <input type="text" class="form-control phone-input" name="phone"--}}
{{--                                           placeholder="@lang('site.phone')">--}}
{{--                                </div>--}}
{{--                                <div class="input-group mb-4">--}}
{{--                                    <select name="type" class="form-control" required id="proofType">--}}
{{--                                        @foreach(\App\Domain\ProofReadings\Models\ProofReading::types() as $type)--}}
{{--                                            <option--}}
{{--                                                value="{{$type}}">{{ trans('site.proof_reading_types.'.$type) }}</option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                                <div class="input-group mb-4" style="display: none;" id="wallpaperBlock">--}}
{{--                                    <input type="text" class="form-control " name="wallpaper"--}}
{{--                                           placeholder="@lang('site.wallpaper')">--}}
{{--                                </div>--}}

{{--                                <div class="input-group mb-4">--}}
{{--                                    <input type="file" class="form-control " name="file" required>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="modal-footer">--}}
{{--                    <button class="btn btn-primary">@lang('site.callback_form_button')</button>--}}
{{--                </div>--}}
{{--            </form>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

@push('scripts')
    <script>
        $(function () {
            // $('#proofType').on('change', function () {
            //     if ($(this).val() == 'publish') {
            //         $('#wallpaperBlock').show();
            //         $('#wallpaperBlock').find('input').prop('required', true);
            //     } else {
            //         $('#wallpaperBlock').hide();
            //         $('#wallpaperBlock').find('input').prop('required', false);
            //     }
            // });

            {{--$('#subscribeForm').submit(function (e) {--}}
            {{--    var form = $(this);--}}
            {{--    form.find('button').prop('disabled', true);--}}
            {{--    // form.find('button').removeClass('animated');--}}
            {{--    $.ajax({--}}
            {{--        method: "POST",--}}
            {{--        data: form.serialize(),--}}
            {{--        url: "{{ route('site.subscribe') }}",--}}
            {{--        success: function (response) {--}}
            {{--            if (response.result !== undefined) {--}}
            {{--                alert(response.result);--}}
            {{--                // form.find('button').prop('disabled', false);--}}
            {{--                return false;--}}
            {{--            } else {--}}
            {{--                form.find('button').prop('disabled', false);--}}
            {{--                alert("{{trans('admin.ajax_error')}}");--}}
            {{--            }--}}
            {{--        },--}}
            {{--        error: function (response) {--}}
            {{--            alert("{{trans('admin.ajax_error')}}");--}}
            {{--            form.find('button').prop('disabled', false);--}}
            {{--        }--}}
            {{--    });--}}
            {{--    return false;--}}
            {{--});--}}

            $('.bvi-toggle').on('click', function () {
                $('#bvi-panel').toggle();
            });

            {{--$('#mbVideoModal').on('show.bs.modal', function (event) {--}}
            {{--    var modal = $(this);--}}
            {{--    modal.find('.modal-body').html(--}}
            {{--        '<div class="mp_video">' +--}}
            {{--        '        <video autoplay controls >' +--}}
            {{--        '            <source src="{{ asset('video/video.mp4') }}" type="video/mp4">' +--}}
            {{--        '        </video>' +--}}
            {{--        '    </div>'--}}
            {{--    )--}}
            {{--});--}}
            // $('#mbVideoModal').on('hide.bs.modal', function (event) {
            //     var modal = $(this);
            //     modal.find('.modal-body').html('');
            // });

        });
    </script>
@endpush
