<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    @stack('meta')

    <title>@yield('title', config('app.name'))</title>

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('icons/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('icons/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('icons/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('icons/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('icons/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('icons/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('icons/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('icons/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('icons/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('icons/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('icons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('icons/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('icons/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('icons/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#151334">
    <meta name="msapplication-TileImage" content="{{ asset('icons/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#151334">

    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

    <link href="{{ asset('vendor/bootstrap/css/bootstrap-mod.min.css') }}" rel="stylesheet">
{{--    <link href="{{ asset('vendor/fancybox/jquery.fancybox.min.css') }}" rel="stylesheet">--}}
    <link href="{{ asset('vendor/font-gotham/gotham-pro.css') }}" rel="stylesheet">
    <link href="{{ asset('css/swiper.min.css') }}" rel="stylesheet">
    <style>
        .iconText {
            width: 100px !important;
            position: absolute;
            background: rgba(255, 255, 255, 0.9);
            /* z-index: -1; */
            left: -35px;
            top: -92px;
            /* color: black; */
            /* font-size: 13px; */
            padding: 3px 3px 3px 11px;
            border-radius: 18px;
        /* font-weight: bold !important; */
        }
        .iconText img {
            width: 100%;
        }
</style>
    @stack('styles')
</head>
<body >
{{--<div class="cat__menu-right">--}}
    {{--@yield('menu-right')--}}
{{--</div>--}}
{{--<nav class="cat__menu-left">--}}
    {{--@yield('menu-left')--}}
{{--</nav>--}}
{{--@component('component.bvi')@endcomponent--}}
<div class="site-wrapper overflow-hidden d-flex flex-column">
    <header class="site-header">
        @yield('header')
    </header>

    @yield('content')
            {{--<div class="cat__footer">--}}
            {{--@yield('footer')--}}
            {{--</div>--}}
    {{-- <div class="map-section d-flex">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2998.167126800782!2d69.33760031490812!3d41.28346701039481!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNDHCsDE3JzAwLjUiTiA2OcKwMjAnMjMuMiJF!5e0!3m2!1sru!2s!4v1581429037460!5m2!1sru!2s" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div> --}}
{{--    <div id="map"></div>--}}
    <div id="map" style=" width: 100%; height: 500px;"></div>
    <footer class="site-footer mt-auto py-3 text-grey">
        @yield('footer')
    </footer>
</div>
<script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript">
    ymaps.ready(init);
    var myMap,
        myPlacemark;

    function init(){
        myMap = new ymaps.Map("map", {

            center: [41.283463, 69.339789],
            zoom: 16,
            controls: ['zoomControl']
        });
        myMap.behaviors.disable('scrollZoom');

        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
            iconContent: '<b class="iconText"><img src="/images/logo_white.png"> </b>',
            balloonContent: '<div class="sd1"><p>г. Ташкент Яшнабадский р-н, ул. Эльбек, 61 д</p></div>',
            // balloonContentBodyLayout: myBalloonLayout,
            balloonOffset: [32, 32],
            // balloonContentBody:
        }, {
            //iconColor: 'green',
            iconLayout: 'default#imageWithContent',
            iconImageHref: '/images/svg/placeholder.svg',
            iconImageSize: [32, 32],
            iconImageOffset: [-15, -50]
            // preset: "islands#redStretchyIcon"
        });

        myMap.geoObjects.add(myPlacemark);
    }
</script>

<script type="text/javascript" src="{{ asset('vendor/jquery/jquery-3.4.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/bootstrap/js/bootstrap.bundle-4.3.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/swiper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/jquery/jquery.sticky-kit.min.js') }}"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        // beforeSend: function(){
        //     $("#preloader").show();
        // },
        // complete: function(){
        //     $("#preloader").hide();
        // }
    });
</script>
@stack('scripts')
@push('styles')

@endpush
<script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
</body>
</html>
