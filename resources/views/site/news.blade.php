@extends('site.wrapper')

@section('title')
    @lang('site.all_news')
@endsection

@section('banner')
    <h1>@lang('site.all_news')</h1>
@endsection

@section('center_content')
    @if($news->isNotEmpty())
        <section class="news_wrapper all_news_wrapper pt-4" id="newsWrapper">
            <div class="container">
                <h4 class="section-heading p-relative text-primary pb-2">
                    <span class="main-text">
                        @lang('site.actual_news')
                    </span>
                </h4>
                <div class=" p-relative" style="background-color: #f2f3f5;">
                    <div class="row">

                        @foreach($news as $item)
                            <div class="col-lg-15 col-md-30 my-3">
                                <article class="article-item">
                                    <div class="article-item-image">
                                        <a href="{{ route('site.news.show', $item->uri) }}">
                                            <img src="{{ $item->thumbUrl() }}" alt="">
                                        </a>
                                    </div>
                                    <div class="article-item-content">
                                        <header>
                                            <h3 class="article-item-title">
                                                <a href="{{ route('site.news.show', $item->uri) }}">{{ $item->name }}</a>
                                            </h3>
                                        </header>
                                        <p>
                                            {{ $item->short }}
                                        </p>
                                        <footer class="d-flex justify-content-between">
                                            <time datetime="{{ $item->created_at->format('Y-m-d') }}">
                                                <i class="fal fa-clock fa-lg"></i>
                                                {{ $item->created_at->format('d.m.Y') }}
                                            </time>
                                            <span>
                                                <i class="fas fa-eye fa-lg"></i>
                                                {{ $item->views }}
                                            </span>
                                        </footer>
                                    </div>
                                </article>
                            </div>
                        @endforeach
                    </div>

                    <section class="my-5">
                        @include('site.pagination', ['data' => $news])
                    </section>
                </div>
            </div>

        </section>

    @endif

@endsection
