@extends('site.wrapper')

@section('title')
    {{ $page->name }}
@endsection

@section('banner')
    <h1>{{ $page->name }}</h1>
@endsection

@section('bg'){{ $page->posterUrl() }}@endsection

@section('center_content')
    @if($page->type == \App\Domain\Pages\Models\Page::TYPE_NEWS)
    <div class="section-intro flex-shrink-1 w-100">
{{--        <div class="page-header py-5 text-white" style="background-image: url({{asset('images/page-header.jpg')}})">--}}
        <div class="page-header py-5 text-white" style="background-image: url({{ $page->posterUrl() }})">
            <div class="container text-bg-wrap">
                <div class="text-bg text-bg-light" style="left:-20px">{{$page->name}}</div>
{{--                <div class="font-weight-light">Наши</div>--}}
{{--                <h4 class="my-0 font-weight-normal">Новости и обовления</h4>--}}
                <h2 class="my-0"><strong>{{$page->name}}</strong></h2>
            </div>
        </div>
    </div>
    <div class="flex-shrink-0 flex-grow-1 d-flex">
        <div class="site-container w-100 px-0">
            <div class="section-about py-5">
{{--                <div class="py-5">--}}
{{--                    <div class="container position-relative">--}}
{{--                        <h4 class="text-primary my-0"><strong>{!! $page->short !!}</strong></h4>--}}
{{--                    </div>--}}
{{--                </div>--}}
                <div class="pb-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg order-1 order-lg-0 mb-3">
                                <div class="row">
                                    <div class="col-lg">
                                        {!! $page->full !!}
                                    </div>
{{--                                    <div class="col-lg-35">--}}
{{--                                        <p>--}}
{{--                                            Целью Технопарка является производство наиболее востребованной продукции с использованием инновационных технологий, которая будет удовлетворять спрос не только на внутреннем рынке, но и экспортироваться за границу. <strong class="text-primary">Технопарк – крупный производственный комплекс</strong>, в масштабы производства которого входит широкий спектр продукции. На сегодняшний день запланировано ввод в производства новых видов продукции:--}}
{{--                                        </p>--}}
{{--                                    </div>--}}
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @else
    <section class="page_wrapper pt-5">
        <div class="container">
            <div class="row mb-3">
                <div class="col-md">
                    <h3 class="text-center text-primary">
                        {!! $page->name !!}
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md">
                    {!! $page->full !!}
                </div>
            </div>
        </div>
    </section>
    @endif
@endsection
@push('styles')
<link href="{{ asset('vendor/viewer/css/viewer.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
<script type="text/javascript" src="{{ asset('vendor/viewer/js/viewer.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/jquery-viewer/jquery-viewer.min.js') }}"></script>
<script type="text/javascript">
    jQuery(document).ready( function($) {
        var $images = $('.page_wrapper, .section-about');
        var options = {
            url: 'data-original'
        };
        $images.on({ready:  function (e) {
            console.log(e.type);
        }
        }).viewer(options);
    });
</script>
@endpush
