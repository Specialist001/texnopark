@extends('site.wrapper')

@section('title')
    @lang('site.partners')
@endsection

@section('banner')
    <h1>@lang('site.partners')</h1>
@endsection

@section('center_content')
    @if($partners->isNotEmpty())
        <section class="news_wrapper pt-5" id="newsWrapper">
            <div class="container">
                <p class="section-heading p-relative py-5">
                <span class="main-text">
                @lang('site.partners')
                </span>
                    <span class="d-none d-lg-block back-shadow">
                    @lang('site.partners')
                </span>
                </p>
                <!--<p class="text-center mb-5">
                    @lang('site.partners_text')
                </p>-->
                <div class=" p-relative">
                    <div class="row">
                        @foreach($partners as $item)
                            <div class="col-lg-3 col-md-4 col-6">
                                @if($item->link != '')
                                    <a href="{{ $item->link }}" target="_blank">
                                        @endif
                                        <div class="partners-card  mb-3 text-center">
                                            <img src="{{ $item->iconUrl() }}" alt="." class="img-fluid">
                                        </div>
                                        @if($item->link != '')
                                    </a>
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </section>

    @endif

@endsection
