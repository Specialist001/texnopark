@extends('site.wrapper')

@section('title')
    @lang('site.project'): {{$project->title}}
@endsection

{{--@section('banner')--}}
{{--    <h1>@lang('site.projects')</h1>--}}
{{--@endsection--}}

@section('center_content')
    <div class="section-intro flex-shrink-1 w-100">
        <div class="page-header py-5 text-white" style="background-image: url(../images/page-header.jpg)">
            <div class="container text-bg-wrap">
{{--                <div class="text-bg text-bg-light" style="left:-150px">@lang('site.our_vacancy')</div>--}}
{{--                <div class="font-weight-light">@lang('site.our')</div>--}}
{{--                <h4 class="my-0 font-weight-normal">{{$project->category->name}}</h4>--}}
                <h2 class="my-0"><strong>{{$project->category->name}}</strong></h2>
            </div>
        </div>
    </div>
    <div class="flex-shrink-0 flex-grow-1 d-flex">
        <div class="site-container w-100 px-0">
            <div class="project-section py-5">
                <div class="pb-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg order-1 order-lg-0 mb-3">
                                <div class="row mb-4">
                                    <div class="col-lg">
                                        <h4 class="my-0 text-primary text-center">
                                            <strong>{{$project->short_description}}</strong>
                                        </h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg">
                                        {!! $project->description !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if($news->isNotEmpty())
            <div class="news-section">
                <div style="background-color: #f2f3f5;">
                    <div class="pt-4">
                        <div class="container position-relative">
                            <h4 class="my-0 font-weight-normal">@lang('site.last_news')</h4>
                        </div>
                    </div>
                    <div class="container">
                        <div class="articles-list py-5">
                            <div class="row">
                                @foreach($news as $item)
                                    <div class="col-lg-15 col-md-30 my-3">
                                        <article class="article-item">
                                            <div class="article-item-image">
                                                <a href="{{ route('site.news.show', $item->uri) }}">
                                                    <img src="{{ $item->thumbUrl() }}" alt="">
                                                </a>
                                            </div>
                                            <div class="article-item-content">
                                                <header>
                                                    <h3 class="article-item-title">
                                                        <a href="{{ route('site.news.show', $item->uri) }}">{{ $item->name }}</a>
                                                    </h3>
                                                </header>
                                                <p>
                                                    {{ $item->short }}
                                                </p>
                                                <footer class="d-flex justify-content-between">
                                                    <time datetime="{{ $item->created_at->format('Y-m-d') }}">
                                                        <i class="fal fa-clock fa-lg"></i>
                                                        {{ $item->created_at->format('d.m.Y') }}
                                                    </time>
                                                    <span>
                                                    <i class="fas fa-eye fa-lg"></i>
                                                    {{ $item->views }}
                                                </span>
                                                </footer>
                                            </div>
                                        </article>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>






    @push('styles')
        <style>
            a.btn.show-more-projects {
                line-height: 50px;
                padding: 0 35px 1px;
                font-size: 16px;
            }
        </style>
    @endpush



@endsection
@push('scripts')
    <script type="text/javascript">
        $('.gallery ul li a').click(function() {
            var itemID = $(this).attr('href');
            $('.gallery ul').addClass('item_open');
            $(itemID).addClass('item_open');
            return false;
        });
        $('.close').click(function() {
            $('.port, .gallery ul').removeClass('item_open');
            return false;
        });

        $(".gallery ul li a").click(function() {
            $('html, body').animate({
                scrollTop: parseInt($("#top").offset().top)
            }, 400);
        });
    </script>
@endpush

@push('styles')
    <link href="{{ asset('vendor/viewer/css/viewer.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('vendor/viewer/js/viewer.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('vendor/jquery-viewer/jquery-viewer.min.js') }}"></script>
    <script type="text/javascript">
        jQuery(document).ready( function($) {
            var $images = $('.project-section');
            var options = {
                url: 'data-original'
            };
            $images.on({ready:  function (e) {
                console.log(e.type);
            }
            }).viewer(options);
        });
    </script>
@endpush