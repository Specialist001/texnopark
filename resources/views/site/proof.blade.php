@extends('site.wrapper')

@section('title')
    @lang('site.proof_reading')
@endsection

@section('banner')
    <h1>@lang('site.proof_reading')</h1>
@endsection

@section('center_content')
    <section class="page_wrapper pt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    @if (session()->has('success'))
                        @component('component.alert', ['type' => 'success'])
                            {{ session('success') }}
                        @endcomponent
                    @endif

                    <div class="table-responsive">

                        <table class="table-striped table">
                            <tr>
                                <td>
                                    <strong>@lang('site.proof_reading_attributes.name')</strong>
                                </td>
                                <td>
                                    {{ $reading->name }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>@lang('site.proof_reading_attributes.email')</strong>
                                </td>
                                <td>
                                    {{ $reading->email }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>@lang('site.proof_reading_attributes.phone')</strong>
                                </td>
                                <td>
                                    {{ $reading->phone ?? '--' }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>@lang('site.proof_reading_attributes.type')</strong>
                                </td>
                                <td>
                                    {{ trans('site.proof_reading_types.'.$reading->type) }}
                                </td>
                            </tr>
                            @if($reading->type == 'publish')
                                <tr>
                                    <td>
                                        <strong>@lang('site.proof_reading_attributes.wallpaper')</strong>
                                    </td>
                                    <td>
                                        {{ $reading->wallpaper ?? '--' }}
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <td>
                                    <strong>@lang('site.proof_reading_attributes.file')</strong>
                                </td>
                                <td>
                                    <a href="{{ $reading->fileUrl() }}">
                                        @lang('site.download')
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>@lang('site.proof_reading_attributes.status')</strong>
                                </td>
                                <td>
                                    {{ trans('site.proof_reading_statuses.'.$reading->status) }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
