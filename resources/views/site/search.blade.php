@extends('site.wrapper')

@section('title')
    @lang('site.search')
@endsection

@section('banner')
    <h1>@lang('site.search')</h1>
@endsection

@section('center_content')
    <section class="page_wrapper pt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if($news->isNotEmpty())
                        <h4 class="my-5">@lang('site.news_search'): <strong>{{$news->count()}}</strong></h4>
                        <div class="row mb-5">
                            @foreach($news as $item)
                                <div class="col-lg-4 col-md-6">
                                    <a href="{{ route('site.news.show', $item->id) }}">
                                        <div class="sp-card news-card p-5">
                                            <p class="date">
                                                {{ $item->created_at->format('H:i, d.m.Y') }}
                                            </p>
                                            <img src="{{ $item->thumbUrl() }}" alt="" class="img-fluid mb-3">
                                            <p class="title">
                                                {{ $item->name }}
                                            </p>
                                            <p class="description">
                                                {{ $item->short }}
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                            <div class="clearfix"></div>
                        </div>
                    @endif
                    @if($pages->isNotEmpty())
                        <h4 class="my-5">@lang('site.pages_search'): <strong>{{$pages->count()}}</strong></h4>
                            <div class="row mb-5">
                                @foreach($pages as $item)
                                    <div class="col-lg-4 col-md-6">
                                        <a href="{{ route('site.page', $item->id) }}">
                                            <div class="sp-card news-card p-5">
                                                {{--<p class="date">--}}
                                                    {{--{{ $item->created_at->format('H:i, d.m.Y') }}--}}
                                                {{--</p>--}}
                                                <img src="{{ $item->thumbUrl() }}" alt="" class="img-fluid mb-3">
                                                <p class="title">
                                                    {{ $item->name }}
                                                </p>
                                                <p class="description">
                                                    {{ $item->short }}
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                                <div class="clearfix"></div>
                            </div>
                    @endif
                    @if($labs->isNotEmpty())
                        <h4 class="my-5">@lang('site.labs_search'): <strong>{{$labs->count()}}</strong></h4>
                        <div class="row mb-5">
                            @foreach($labs as $item)

                                <div class="col-lg-4 col-md-6">
                                    <a href="{{ route('site.labs', $item->id) }}">
                                        <div class="sp-card labs-card  p-4 ">
                                            <img src="{{ $item->thumbUrl() }}" alt="" class="img-fluid mb-3">
                                            <p class="title">
                                                {{ $item->name }}
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                            <div class="clearfix"></div>
                        </div>
                    @endif
                    @if($videos->isNotEmpty())
                        <h4 class="my-5">@lang('site.videos_search'): <strong>{{$videos->count()}}</strong></h4>
                        <div class="row">
                            @foreach($videos as $item)
                                <div class="col-lg-4 col-md-6">
                                    <div class="sp-card news-card p-5">
                                        <iframe class="animated" width="100%" height="315" src="https://www.youtube.com/embed/{{$item->link}}?rel=0" frameborder="0"
                                                allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen></iframe>
                                    </div>
                                </div>
                            @endforeach
                            <div class="clearfix"></div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
