@extends('site.wrapper')

@section('title')
    @lang('site.all_vacs')
@endsection

@section('banner')
    <h1>@lang('site.all_vacs')</h1>
@endsection

@section('center_content')
    @if($vacs->isNotEmpty())
        <div class="section-intro flex-shrink-1 w-100">
            <div class="page-header py-5 text-white" style="background-image: url(../images/page-header.jpg)">
                <div class="container text-bg-wrap">
                    <div class="text-bg text-bg-light" style="left:-150px">@lang('site.our_vacancy')</div>
                    <div class="font-weight-light">@lang('site.our')</div>
                    <h4 class="my-0 font-weight-normal">@lang('site.all_vacs')</h4>
                    <h2 class="my-0"><strong>@lang('site.our_vacancy')</strong></h2>
                </div>
            </div>
        </div>
        <div class="flex-shrink-0 flex-grow-1 d-flex">
            <div class="site-container w-100 px-0">
                <div class="vacansies-section py-5">
                    <div class="container">
                            <div class="row pt-2">
                        @foreach($vacs as $item)
                            <div class="col-md-30 pb-4">
                                <div class="p-2 d-inline-block float-left" style="width: 9%">
                                    <img src="{{$item->iconUrl()}}" class="img-fluid">
                                </div>
                                <div class="d-inline-block float-left" style="width:90%">
                                    <div class="mb-2">
                                        <span class="h5 my-0">{{$item->name}}</span>
                                        <button class="btn p-0 text-muted ml-3 collapsed" data-toggle="collapse" data-target="#d-{{$item->id}}">
                                            @lang('site.requirements')
                                            <i class="fal fa-angle-down caret"></i>
                                        </button>
                                    </div>
                                    <div id="d-{{$item->id}}" class="collapse">
                                        {!! $item->full !!}
                                        <hr class="mb-0 pb-3 mt-3">
                                    </div>
                                    <div class="text-muted">
                                        <p class="mb-0">
                                            {{$item->short}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                            </div>
                        <br>
                        @component('component.modal-vac', ['id' => 'vacancy_r', 'class' => 'btn btn-primary ml-2'])
                            @slot('label')
                                    @lang('site.vacancy_respond')
                            @endslot
                            @slot('title')
                                @lang('site.vacancy_respond')
                            @endslot
                            <form action="/" id="vacancyForm" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="vacancy_id">@lang('site.all_vacs')</label>
                                    <select id="vacancy_id" name="vacancy_id" class="form-control">
                                        @foreach($vacs as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="first_name">@lang('site.proof_reading_attributes.name')</label>
                                    <input class="form-control" type="text" id="first_name" name="first_name">
                                </div>
                                <div class="form-group">
                                    <label for="first_name">@lang('site.proof_reading_attributes.phone')</label>
                                    <input class="form-control phone-input" type="text" id="phone" name="phone">
                                </div>
                                <div class="form-group">
                                    <label for="resume_file">@lang('site.proof_reading_attributes.resume') (.pdf)</label><br>
                                    <input class="resume-input" type="file" id="resume_file" name="resume_file" accept="application/pdf">
                                </div>
                                <button  class="btn btn-primary float-right">@lang('site.respond_button')</button>

                            </form>
                        @endcomponent
                        <div class="d-block text-center mt-4">
                            <span>@lang('site.phone_info') <strong>+99895 515-00-72</strong></span>
                        </div>
                    </div>
                </div>
                @if($news->isNotEmpty())
                <div class="news-section">
                    <div style="background-color: #f2f3f5;">
                        <div class="pt-4">
                            <div class="container position-relative">
                                <h4 class="my-0 font-weight-normal">@lang('site.last_news')</h4>
                            </div>
                        </div>
                        <div class="container">
                            <div class="articles-list py-5">
                                <div class="row">
                                    @foreach($news as $item)
                                        <div class="col-lg-15 col-md-30 my-3">
                                            <article class="article-item">
                                                <div class="article-item-image">
                                                    <a href="{{ route('site.news.show', $item->uri) }}">
                                                        <img src="{{ $item->thumbUrl() }}" alt="">
                                                    </a>
                                                </div>
                                                <div class="article-item-content">
                                                    <header>
                                                        <h3 class="article-item-title">
                                                            <a href="{{ route('site.news.show', $item->uri) }}">{{ $item->name }}</a>
                                                        </h3>
                                                    </header>
                                                    <p>
                                                        {{ $item->short }}
                                                    </p>
                                                    <footer class="d-flex justify-content-between">
                                                        <time datetime="{{ $item->created_at->format('Y-m-d') }}">
                                                            <i class="fal fa-clock fa-lg"></i>
                                                            {{ $item->created_at->format('d.m.Y') }}
                                                        </time>
                                                        <span>
                                                    <i class="fas fa-eye fa-lg"></i>
                                                    {{ $item->views }}
                                                </span>
                                                    </footer>
                                                </div>
                                            </article>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>

    @endif
@endsection
@component('component.phone-stacks')@endcomponent
@push('scripts')
    <script>
        $(function () {

            $('#vacancyForm').submit(function(event) {
                event.preventDefault();
                var form = $(this);
                var formData = new FormData(form[0]);

                form.find('button').prop('disabled', true);
                $.ajax({
                    url: "{{ route('site.vacancy.respond') }}",
                    type: "POST",
                    method: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function(response)
                    {
                        if(response.result !== undefined) {
                            alert(response.result);
                            // form.find('button').prop('disabled', false);
                            return false;
                        } else {
                            form.find('button').prop('disabled', false);
                            alert("{{trans('admin.ajax_error')}}");
                        }                    },
                    error: function(response)
                    {
                        alert("{{trans('admin.ajax_error')}}");
                        form.find('button').prop('disabled', false);
                    }
                });
                return false;
            });
        });
    </script>
@endpush
