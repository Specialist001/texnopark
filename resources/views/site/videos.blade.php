@extends('site.wrapper')

@section('title')
    @lang('site.our_video')
@endsection

@section('banner')
    <h1>@lang('site.our_video')</h1>
@endsection

@section('center_content')
    @if($videos->isNotEmpty())
        <section class="news_wrapper all_news_wrapper pt-5" id="newsWrapper">
            <div class="container">
                <p class="section-heading p-relative py-5">
                <span class="main-text">
                @lang('site.our_video')
                </span>
                    <span class="d-none d-lg-block back-shadow">
                    @lang('site.our_video')
                </span>
                </p>
                <div class=" p-relative">
                    <div class="row">
                        @foreach($videos as $item)
                                <div class="col-lg-4 col-md-6">
                                    <div class="sp-card news-card p-5">
                                        <iframe class="animated" width="100%" height="315" src="https://www.youtube.com/embed/{{$item->link}}?rel=0" frameborder="0"
                                                allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen></iframe>
                                        <p class="title">
                                            {{ $item->title }}
                                        </p>
                                    </div>
                                </div>
                        @endforeach
                    </div>



                    <section class="my-5">
                        @include('site.pagination', ['data' => $videos])
                    </section>
                </div>
            </div>

        </section>

    @endif

@endsection
