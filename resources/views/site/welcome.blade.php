@extends('site.wrapper')

@section('title')
    @lang('site.title_main')
@endsection

@section('bg')
    1
@endsection

@section('banner')
    @if($banner)
        <div class="row">
            <div class="col-lg-6">
                <div class="page_title_date">
                    {{ $banner->created_at->format('d.m.Y') }}
                </div>
                <h1>{{ $banner->name }}</h1>
                <a href="{{ route('site.news.show', $banner->id) }}" class="decored">
                    @lang('site.more') <img src="{{ asset('images/site/i_more.png') }}" alt="@lang('site.more')"
                                            class="d-inline-block ml-2">
                </a>
            </div>
            <div class="col-lg-6 text-center">
                <img src="{{ asset('images/site/i_play.png') }}" class="img-fluid cur-pointer d-inline-block" data-toggle="modal" data-target="#mbVideoModal">
            </div>
        </div>

    @else
        <div class="row">
            <div class="col-lg-12 text-right">
                <img src="{{ asset('images/site/i_play.png') }}" class="img-fluid cur-pointer d-inline-block" data-toggle="modal" data-target="#mbVideoModal">
            </div>
        </div>
    @endif
@endsection

@section('center_content')
    @if($news->isNotEmpty())
    <div class="section-intro flex-shrink-1 w-100">
        <div class="swiper-container intro-slider">
            <div class="swiper-wrapper">
                @foreach($news as $item)
                    <div class="swiper-slide" style="background-image: url({{ $item->posterUrl() }})">
                        <div class="container text-bg-wrap">
                            <div class="intro-container">
                                <div class="text-bg text-bg-light" style="left:-144px">{{$item->name}} </div>
                                <h4 class="my-0 font-weight-normal">{{$item->name}}</h4>
                            </div>
                        </div>
                        <!-- <button class="play-button fa-5x">
                            <i class="fas fa-play-circle"></i>
                        </button> -->
                    </div>
                @endforeach

            </div>
        </div>
        <div class="swiper-navigation d-flex">
            <button class="swiper-prev swiper-button">
                <i class="fas fa-chevron-left"></i>
            </button>
            <button class="swiper-next swiper-button">
                <i class="fas fa-chevron-right"></i>
            </button>
        </div>
        <div class="swiper-container bg-primary text-white intro-slider-2">
            <div class="swiper-wrapper">
                @foreach($news as $item)
                <div class="swiper-slide">
                    <h4 class="mb-4">{{$item->name}}</h4>
                    <div style="max-width:370px;">
                        <p>{{$item->short}}</p>
                    </div>
                    <div class="pt-5">
                        <a href="{{ route('site.news.show', $item->uri) }}" class="text-white">
                            <b>@lang('site.more')</b>
                            <i class="fal fa-long-arrow-right"></i>
                        </a>
                    </div>
                </div>
                @endforeach

            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
    @endif
    <div class="flex-shrink-0 flex-grow-1 d-flex">
        <div class="site-container w-100 px-0">
            <div class="section-about py-5">
                <div class="py-5">
                    <div class="container position-relative">
                        <div class="text-bg" style="left:-62px">@lang('site.about_us')</div>
                        <div class="font-weight-light">@lang('site.who_are_we')?</div>
                        <!-- <h4 class="my-0 font-weight-normal">Наши проекты</h4> -->
                        <h2 class="text-primary my-0"><strong>@lang('site.about_us')</strong></h2>
                        <hr class="decoration-line bg-primary">
                    </div>
                </div>

                <div class="pb-5">
                    <div class="container">
                        <div class="row">
                            <div class="col order-1 order-lg-0 mb-3">
                                <div class="row">
                                    <div class="col">
                                        <p>
                                            {!! nl2br($system_page->full) !!}

										</p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="products-section">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-30">
                            <div class="tablet-block">
                                <div class="swiper-container tablet-slider">
                                    <div class="swiper-wrapper">
                                        @for ($i = 1; $i <= 6; $i++)
                                            <div class="swiper-slide">
                                                <div class="tablet-image" style="background-image: url(./images/projects/pr_{{$i}}.jpg)">
                                                </div>
                                            </div>
                                        @endfor                                        
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-xl-30 order-1 order-xl-0">
                            <div style="min-height:272px;padding-top:126px">
                                <div class="position-relative">

                                    <h2 class="text-primary my-3">
                                        <strong>@lang('site.what_we_produce')</strong>
                                    </h2>
                                </div>
                            </div>
                            <div class="py-3">
                                <p class="lead">
                                    @lang('site.about_text')
                                </p>
                            </div>

                        </div>
                        <div class="col-xl-30 mt-5">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="tablet-navigaiton d-flex">
                                    <button class="tablet-prev w-6">
                                        <i class="fas fa-chevron-left fa-lg"></i>
                                    </button>
                                    <button class="tablet-next">
                                        <i class="fas fa-chevron-right fa-lg"></i>
                                    </button>
                                </div>
                                <div class="tablet-pagination"></div>

                            </div>
                        </div>
{{--                        <div class="col-xl-30 mt-5 order-1 order-xl-0">--}}
{{--                            <div>--}}
{{--                                <a href="#" class="btn btn-lg btn-outline-primary px-5">Подробнее</a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>

                </div>
            </div>

            <div class="gallery-section py-5 bg-primary text-white">
                <div class="py-5">
                    <div class="container text-bg-wrap">
                        <div class="text-bg text-bg-light" style="left:-230px">@lang('site.in_central_asia') </div>
                        <h4 class="my-0 font-weight-normal">@lang('site.biggest_production') </h4>
                        <h2 class="my-0"><strong>@lang('site.in_central_asia') </strong></h2>
                        <hr class="decoration-line bg-white">
                    </div>
                </div>
                <div>
                    @if($projectCategories->isNotEmpty())
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-24 d-flex flex-column justify-content-between">
                                <div class="order-lg-1 my-2">

                                </div>
                                <div>

                                </div>
                                <div>
                                    <ul class="nav my-3 d-block my-tabs txt-size-lg font-weight-light">
                                        @php
                                            $shown = 0;
                                        @endphp
                                        @foreach($projectCategories as $projectCategory)
                                            @if($projects[$projectCategory->id]->isNotEmpty())
                                                <li>
                                                    <a href="#gal-{{$projectCategory->id}}" class="{{ $shown === 0 ? 'active': '' }}" data-toggle="tab">{{ $projectCategory->name }}</a>
                                                </li>
                                                @php
                                                    $shown++;
                                                @endphp
                                            @endif
                                        @endforeach

                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-28">
                                <div class="tab-content">
                                    @php
                                        $child_shown = 0;
                                    @endphp
                                    @foreach($projectCategories as $projectCategory)
                                        @if($projects[$projectCategory->id]->isNotEmpty())
                                            <div class="tab-pane fade {{ $child_shown === 0 ? 'active show': '' }}" id="gal-{{$projectCategory->id}}">
                                                <div class="form-row">
                                                     @foreach($projects[$projectCategory->id] as $project)
                                                        <div class="col-30 my-1">
                                                            <img src="{{$project->imageUrl()}}" class="img-fluid" alt="">
                                                        </div>
                                                    @endforeach

                                                </div>
                                            </div>
                                            @php
                                                $child_shown++;
                                            @endphp
                                        @endif
                                    @endforeach


                                </div>
                            </div>
                            <div class="col-md-8 flex-column justify-content-between text-center d-none d-lg-flex">

                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>

            <div class="partners-section py-5">
                <div class="py-5">
                    <div class="container position-relative">
                        <div class="text-bg" style="left:-62px">@lang('site.we_are_first')</div>
                        <div class="font-weight-light">@lang('site.partners')</div>
                        <h4 class="my-0 font-weight-normal">@lang('site.everyone_works_with_us')</h4>
                        <h2 class="text-dark my-0"><strong>@lang('site.we_are_first_in_uzb')</strong></h2>
                        <hr class="decoration-line bg-primary">
                    </div>
                </div>
                <div class="pt-5">
                    <div class="container">
                        <div class="row">
                            @foreach($partners as $partner)
								<div class="col-md-12 col-sm-30 mb-3">
									<div class="text-center">
										<div class="d-inline-block align-items-center justify-content-center w-50 m-auto">
											<img src="{{ $partner->logoUrl() }}" alt="{{ $partner->name }}" class="img-fluid">
										</div>
										<hr class="decoration-line mx-auto bg-primary shadow" style="width:52px;">
										<div class="txt-size-sm">
											{{ $partner->name }}
										</div>
									</div>
								</div>
							@endforeach
                        </div>
                        <hr class="mt-5">
                    </div>
                </div>
            </div>
            <div class="news-section">
                <div class="py-5">
                    <div class="container position-relative">
                        <h4 class="my-0 font-weight-normal">@lang('site.last_news')</h4>
                    </div>
                </div>
                <div style="background-color: #f2f3f5;">
                    <div class="container">
                        <div class="articles-list py-5">
                            <div class="row">
                                @foreach($news as $item)
                                <div class="col-lg-15 col-md-30 my-3">
                                    <article class="article-item">
                                        <div class="article-item-image">
                                            <a href="{{ route('site.news.show', $item->uri) }}">
                                                <img src="{{ $item->thumbUrl() }}" alt="">
                                            </a>
                                        </div>
                                        <div class="article-item-content">
                                            <header>
                                                <h3 class="article-item-title">
                                                    <a href="{{ route('site.news.show', $item->uri) }}">{{ $item->name }}</a>
                                                </h3>
                                            </header>
                                            <p>
                                                {{ $item->short }}
                                            </p>
                                            <footer class="d-flex justify-content-between">
                                                <time datetime="{{ $item->created_at->format('Y-m-d') }}">
                                                    <i class="fal fa-clock fa-lg"></i>
                                                    {{ $item->created_at->format('d.m.Y') }}
                                                </time>
                                                <span>
                                                    <i class="fas fa-eye fa-lg"></i>
                                                    {{ $item->views }}
                                                </span>
                                            </footer>
                                        </div>
                                    </article>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    @if($news->isNotEmpty())
        @if($news->count() < 4)
            @push('scripts')
                <style>
                    a.all-news {
                        position: unset;
                        margin-top: 50px;
                        display: inline-block;
                    }
                </style>
            @endpush
        @endif

    @endif
@endsection
@push('styles')
    <style>
        .page-title {
            padding-top: 100px !important;
            padding-bottom: 100px !important;
        }

        .bg_page_title {
            bottom: 230px !important;
        }

        .bg_header_circle {
            left: unset !important;
            right: 200px !important;
            top: 60px !important;
        }
        .section-about div.images {
            display: none;
        }
    </style>
@endpush
