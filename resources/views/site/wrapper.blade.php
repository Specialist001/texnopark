@extends('site.layout')

{{--@section('menu-left')--}}
    {{--@include('partner.menu-left')--}}
{{--@endsection--}}


@section('container')
    full-screen
@endsection

@section('header')
    @include('site.header')
@endsection

@section('content')
    @yield('center_content')

{{--    <div class="">--}}
{{--        <img src="{{ asset('images/site/I_up.png') }}" alt="@lang('site.up')" id="back-to-top" style="display: none;" class="up_button cur-pointer ">--}}
{{--    </div>--}}
@endsection

@section('footer')
    @include('site.footer')
@endsection
@push('styles')
{{--    <link href="{{ asset('css/style_vacancy.css') }}" rel="stylesheet">--}}
    <link href="{{ asset('css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
{{--    <link href="{{ asset('css/style_fixes.css') }}" rel="stylesheet">--}}
@endpush

@push('scripts')
    <script>
        if ($('#back-to-top').length) {
            var scrollTrigger = 800, // px
                backToTop = function () {
                    var scrollTop = $(window).scrollTop();
                    if (scrollTop > scrollTrigger) {
                        $('#back-to-top').show();
                    } else {
                        $('#back-to-top').hide();
                    }
                };
            backToTop();
            $(window).on('scroll', function () {
                backToTop();
            });
            $('#back-to-top').on('click', function (e) {
                e.preventDefault();
                $('html,body').animate({
                    scrollTop: 0
                }, 200);
            });
        }
    </script>
@endpush
