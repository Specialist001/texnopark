<?php

Route::group([
    'domain' => 'admin.' . config('app.domain'),
    'namespace' => 'Admin',
    'as' => 'admin.'
], function () {
    Route::fallback('ErrorController@notFound')->name('404');

    Route::group([
        'namespace' => 'Auth',
        'as' => 'auth.'
    ], function () {
        Route::get('login', 'LoginController@showLoginForm')->name('loginForm');
        Route::post('login', 'LoginController@login')->name('login');
        Route::get('logout', 'LoginController@logout')->name('logout');
    });

    Route::group([
        'middleware' => ['auth', 'active']
    ], function () {
        Route::group([
            'middleware' => ['role:admin,proof,manager']
        ], function () {
            Route::get('/', 'HomeController@index')->name('home');

            Route::get('profile', 'HomeController@profileForm')->name('profile');
            Route::put('profile', 'HomeController@profileUpdate')->name('profile.update');
        });

        Route::group([
            'middleware' => ['role:admin']
        ], function () {
            Route::group([
                'prefix' => 'admins',
                'as' => 'admins.',
            ], function () {
                Route::get('', 'AdminsController@index')->name('index');
                Route::get('create', 'AdminsController@create')->name('create');
                Route::post('store', 'AdminsController@store')->name('store');
                Route::get('edit/{user}', 'AdminsController@edit')->name('edit');
                Route::put('update/{user}', 'AdminsController@update')->name('update');
                Route::delete('destroy/{user}', 'AdminsController@destroy')->name('destroy');
            });

            Route::group([
                'prefix' => 'managers',
                'as' => 'managers.',
            ], function () {
                Route::get('', 'ManagersController@index')->name('index');
                Route::get('create', 'ManagersController@create')->name('create');
                Route::post('store', 'ManagersController@store')->name('store');
                Route::get('edit/{user}', 'ManagersController@edit')->name('edit');
                Route::put('update/{user}', 'ManagersController@update')->name('update');
                Route::delete('destroy/{user}', 'ManagersController@destroy')->name('destroy');
            });

            Route::group([
                'prefix' => 'proofs',
                'as' => 'proofs.',
            ], function () {
                Route::get('', 'ProofsController@index')->name('index');
                Route::get('create', 'ProofsController@create')->name('create');
                Route::post('store', 'ProofsController@store')->name('store');
                Route::get('edit/{user}', 'ProofsController@edit')->name('edit');
                Route::put('update/{user}', 'ProofsController@update')->name('update');
                Route::delete('destroy/{user}', 'ProofsController@destroy')->name('destroy');
            });

            Route::group([
                'prefix' => 'investors',
                'as' => 'investors.',
            ], function () {
                Route::get('', 'InvestorsController@index')->name('index');
                Route::get('create', 'InvestorsController@create')->name('create');
                Route::post('store', 'InvestorsController@store')->name('store');
                Route::get('edit/{user}', 'InvestorsController@edit')->name('edit');
                Route::put('update/{user}', 'InvestorsController@update')->name('update');
                Route::delete('destroy/{user}', 'InvestorsController@destroy')->name('destroy');
            });

            Route::group([
                'prefix' => 'startups',
                'as' => 'startups.',
            ], function () {
                Route::get('', 'StartupsController@index')->name('index');
                Route::get('create', 'StartupsController@create')->name('create');
                Route::post('store', 'StartupsController@store')->name('store');
                Route::get('edit/{user}', 'StartupsController@edit')->name('edit');
                Route::put('update/{user}', 'StartupsController@update')->name('update');
                Route::delete('destroy/{user}', 'StartupsController@destroy')->name('destroy');
            });

            Route::group([
                'prefix' => 'videos',
                'as' => 'videos.',
            ], function () {
                Route::get('', 'VideosController@index')->name('index');
                Route::get('create', 'VideosController@create')->name('create');
                Route::post('store', 'VideosController@store')->name('store');
                Route::get('edit/{video}', 'VideosController@edit')->name('edit');
                Route::put('update/{video}', 'VideosController@update')->name('update');
                Route::delete('destroy/{video}', 'VideosController@destroy')->name('destroy');
            });

            Route::group([
                'prefix' => 'contacts',
                'as' => 'contacts.',
            ], function () {
                Route::get('', 'ContactsController@index')->name('index');
                Route::put('update', 'ContactsController@update')->name('update');
            });

            Route::group([
                'prefix' => 'units',
                'as' => 'units.',
            ], function () {
                Route::get('', 'UnitsController@index')->name('index');
                Route::get('create', 'UnitsController@create')->name('create');
                Route::post('store', 'UnitsController@store')->name('store');
                Route::get('edit/{unit}', 'UnitsController@edit')->name('edit');
                Route::put('update/{unit}', 'UnitsController@update')->name('update');
                Route::delete('destroy/{unit}', 'UnitsController@destroy')->name('destroy');
//                Route::delete('image/{unit?}', 'UnitsController@deleteImage')->name('destroy.icon');
            });

            Route::group([
                'prefix' => 'project-categories',
                'as' => 'project-categories.',
            ], function () {
                Route::get('', 'ProjectCategoriesController@index')->name('index');
                Route::get('create', 'ProjectCategoriesController@create')->name('create');
                Route::post('store', 'ProjectCategoriesController@store')->name('store');
                Route::get('edit/{projectCategory}', 'ProjectCategoriesController@edit')->name('edit');
                Route::put('update/{projectCategory}', 'ProjectCategoriesController@update')->name('update');
                Route::delete('destroy/{projectCategory}', 'ProjectCategoriesController@destroy')->name('destroy');
                Route::delete('image/{projectCategory?}', 'ProjectCategoriesController@deleteImage')->name('destroy.icon');
            });

            Route::group([
                'prefix' => 'project-types',
                'as' => 'project-types.',
            ], function () {
                Route::get('', 'ProjectTypesController@index')->name('index');
                Route::get('create', 'ProjectTypesController@create')->name('create');
                Route::post('store', 'ProjectTypesController@store')->name('store');
                Route::get('edit/{projectType}', 'ProjectTypesController@edit')->name('edit');
                Route::put('update/{projectType}', 'ProjectTypesController@update')->name('update');
                Route::delete('destroy/{projectType}', 'ProjectTypesController@destroy')->name('destroy');
            });

            Route::group([
                'prefix' => 'page-categories',
                'as' => 'page-categories.',
            ], function () {
                Route::get('', 'PageCategoriesController@index')->name('index');
                Route::get('create', 'PageCategoriesController@create')->name('create');
                Route::post('store', 'PageCategoriesController@store')->name('store');
                Route::get('edit/{category}', 'PageCategoriesController@edit')->name('edit');
                Route::put('update/{category}', 'PageCategoriesController@update')->name('update');
                Route::delete('destroy/{category}', 'PageCategoriesController@destroy')->name('destroy');
            });

            Route::group([
                'prefix' => 'pages',
                'as' => 'pages.',
            ], function () {
                Route::get('', 'PagesController@index')->name('index');
                Route::get('create', 'PagesController@create')->name('create');
                Route::post('store', 'PagesController@store')->name('store');
                Route::get('edit/{page}', 'PagesController@edit')->name('edit');
                Route::put('update/{page}', 'PagesController@update')->name('update');
                Route::delete('destroy/{page}', 'PagesController@destroy')->name('destroy');
                Route::delete('image/{page?}', 'PagesController@deleteImage')->name('destroy.poster');
            });

            Route::group([
                'prefix' => 'news',
                'as' => 'news.',
            ], function () {
                Route::get('', 'NewsController@index')->name('index');
                Route::get('create', 'NewsController@create')->name('create');
                Route::post('store', 'NewsController@store')->name('store');
                Route::get('edit/{page}', 'NewsController@edit')->name('edit');
                Route::put('update/{page}', 'NewsController@update')->name('update');
                Route::delete('destroy/{page}', 'NewsController@destroy')->name('destroy');
                Route::delete('image/{page?}', 'NewsController@deleteImage')->name('destroy.poster');
            });
            Route::group([
                'prefix' => 'labs',
                'as' => 'labs.',
            ], function () {
                Route::get('', 'LabsController@index')->name('index');
                Route::get('create', 'LabsController@create')->name('create');
                Route::post('store', 'LabsController@store')->name('store');
                Route::get('edit/{page}', 'LabsController@edit')->name('edit');
                Route::put('update/{page}', 'LabsController@update')->name('update');
                Route::delete('destroy/{page}', 'LabsController@destroy')->name('destroy');
                Route::delete('image/{page?}', 'LabsController@deleteImage')->name('destroy.poster');
            });

            Route::group([
                'prefix' => 'vacancy',
                'as' => 'vacancy.',
            ], function () {
                Route::get('', 'VacancyController@index')->name('index');
                Route::get('create', 'VacancyController@create')->name('create');
                Route::post('store', 'VacancyController@store')->name('store');
                Route::get('edit/{page}', 'VacancyController@edit')->name('edit');
                Route::put('update/{page}', 'VacancyController@update')->name('update');
                Route::delete('destroy/{page}', 'VacancyController@destroy')->name('destroy');
                Route::delete('image/{page?}', 'VacancyController@deleteImage')->name('destroy.poster');
				Route::delete('icon/{page?}', 'VacancyController@deleteIcon')->name('destroy.icon');
            });

            Route::group([
                'prefix' => 'vacancy-responds',
                'as' => 'vacancy-responds.',
            ], function () {
                Route::get('', 'VacancyRespondController@index')->name('index');
                Route::delete('destroy/{vacancyRespond}', 'VacancyRespondController@destroy')->name('destroy');
            });

            Route::group([
                'prefix' => 'national-sites',
                'as' => 'national-sites.',
            ], function () {
                Route::get('', 'NationalSitesController@index')->name('index');
                Route::get('create', 'NationalSitesController@create')->name('create');
                Route::post('store', 'NationalSitesController@store')->name('store');
                Route::get('edit/{nationalSite}', 'NationalSitesController@edit')->name('edit');
                Route::put('update/{nationalSite}', 'NationalSitesController@update')->name('update');
                Route::delete('destroy/{nationalSite}', 'NationalSitesController@destroy')->name('destroy');
                Route::delete('image/{nationalSite?}', 'NationalSitesController@deleteImage')->name('destroy.poster');
            });

            Route::group([
                'prefix' => 'partners',
                'as' => 'partners.',
            ], function () {
                Route::get('', 'PartnersController@index')->name('index');
                Route::get('create', 'PartnersController@create')->name('create');
                Route::post('store', 'PartnersController@store')->name('store');
                Route::get('edit/{partner}', 'PartnersController@edit')->name('edit');
                Route::put('update/{partner}', 'PartnersController@update')->name('update');
                Route::delete('destroy/{partner}', 'PartnersController@destroy')->name('destroy');
                Route::delete('image/{partner?}', 'PartnersController@deleteImage')->name('destroy.logo');
            });

            Route::group([
                'prefix' => 'cities',
                'as' => 'cities.',
            ], function () {
                Route::get('', 'CitiesController@index')->name('index');
                Route::get('create', 'CitiesController@create')->name('create');
                Route::post('store', 'CitiesController@store')->name('store');
                Route::get('edit/{city}', 'CitiesController@edit')->name('edit');
                Route::put('update/{city}', 'CitiesController@update')->name('update');
                Route::delete('destroy/{city}', 'CitiesController@destroy')->name('destroy');
            });

            Route::group([
                'prefix' => 'subscribers',
                'as' => 'subscribers.',
            ], function () {
                Route::get('', 'SubscribersController@index')->name('index');
                Route::get('create', 'SubscribersController@create')->name('create');
                Route::post('store', 'SubscribersController@store')->name('store');
                Route::get('edit/{subscriber}', 'SubscribersController@edit')->name('edit');
                Route::put('update/{subscriber}', 'SubscribersController@update')->name('update');
                Route::delete('destroy/{subscriber}', 'SubscribersController@destroy')->name('destroy');

                Route::get('mailing-form', 'SubscribersController@mailingForm')->name('mailing.form');
                Route::post('mailing', 'SubscribersController@mailing')->name('mailing.send');
            });

            Route::group([
                'prefix' => 'forum',
                'as' => 'forum.',
            ], function () {
                Route::get('', 'ForumThemesController@index')->name('index');
                Route::get('create', 'ForumThemesController@create')->name('create');
                Route::post('store', 'ForumThemesController@store')->name('store');
                Route::get('edit/{theme}', 'ForumThemesController@edit')->name('edit');
                Route::put('update/{theme}', 'ForumThemesController@update')->name('update');
                Route::delete('destroy/{theme}', 'ForumThemesController@destroy')->name('destroy');
                Route::delete('file/{theme?}', 'ForumThemesController@deleteFile')->name('destroy.file');
            });

            Route::group([
                'as' => 'forum.',
            ], function () {
                Route::post('answer/{theme}', 'ForumAnswersController@store')->name('answer');
                Route::get('answer/edit/{answer}', 'ForumAnswersController@edit')->name('answer.edit');
                Route::put('answer/update/{answer}', 'ForumAnswersController@update')->name('answer.update');
                Route::get('answer/destroy/{answer}', 'ForumAnswersController@destroy')->name('answer.destroy');
                Route::delete('answer/file/{answer?}', 'ForumAnswersController@deleteFile')->name('answer.destroy.file');
            });

            Route::group([
                'prefix' => 'responds',
                'as' => 'responds.',
            ], function () {
                Route::get('', 'ForumRespondsController@index')->name('index');
                Route::get('show/{respond}', 'ForumRespondsController@show')->name('show');
                Route::delete('delete/{respond}', 'ForumRespondsController@destroy')->name('destroy');
            });

            Route::group([
                'prefix' => 'proof-readings',
                'as' => 'proof-readings.',
            ], function () {
                Route::delete('destroy/{reading}', 'ProofReadingController@destroy')->name('destroy');
            });


        });

        Route::group([
            'middleware' => ['role:admin,manager']
        ], function () {

            Route::group([
                'prefix' => 'projects',
                'as' => 'projects.',
            ], function () {
                Route::get('', 'ProjectsController@index')->name('index');
                Route::get('create', 'ProjectsController@create')->name('create');
                Route::post('store', 'ProjectsController@store')->name('store');
                Route::get('edit/{project}', 'ProjectsController@edit')->name('edit');
                Route::put('update/{project}', 'ProjectsController@update')->name('update');
                Route::delete('destroy/{project}', 'ProjectsController@destroy')->name('destroy');

                Route::delete('image/{project?}', 'ProjectsController@deleteImage')->name('destroy.image');
                Route::delete('logo/{project?}', 'ProjectsController@deleteLogo')->name('destroy.logo');
                Route::delete('presentation/{project?}', 'ProjectsController@deletePresentation')->name('destroy.presentation');
            });

            Route::group([
                'prefix' => 'investments',
                'as' => 'investments.',
            ], function () {
                Route::get('', 'InvestmentsController@index')->name('index');
                Route::get('create', 'InvestmentsController@create')->name('create');
                Route::post('store', 'InvestmentsController@store')->name('store');
                Route::get('edit/{investment}', 'InvestmentsController@edit')->name('edit');
                Route::put('update/{investment}', 'InvestmentsController@update')->name('update');
                Route::delete('destroy/{investment}', 'InvestmentsController@destroy')->name('destroy');
            });
        });

        Route::group([
            'middleware' => ['role:admin,proof']
        ], function () {

            Route::group([
                'prefix' => 'proof-readings',
                'as' => 'proof-readings.',
            ], function () {
                Route::get('', 'ProofReadingController@index')->name('index');
                Route::get('edit/{reading}', 'ProofReadingController@edit')->name('edit');
                Route::put('update/{reading}', 'ProofReadingController@update')->name('update');
            });
        });
    });
});
