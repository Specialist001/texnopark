<?php
/**
 * Created by PhpStorm.
 * User: irock
 * Date: 26.02.2019
 * Time: 16:30
 */
Route::group([
    'domain' => 'portal.' . config('app.domain'),
    'namespace' => 'Portal',
    'as' => 'portal.'
], function () {
    Route::fallback('ErrorController@notFound')->name('404');

    Route::group([
        'namespace' => 'Auth',
        'as' => 'auth.'
    ], function () {
        Route::get('login', 'LoginController@showLoginForm')->name('loginForm');
        Route::post('login', 'LoginController@login')->name('login');
        Route::get('logout', 'LoginController@logout')->name('logout');

        Route::get('registration', 'RegisterController@showRegistrationForm')->name('registerForm');
        Route::post('registration', 'RegisterController@register')->name('register');
        Route::get('verify/{id}', 'VerificationController@verify')->name('verify');
        Route::get('verify-resend', 'VerificationController@resend')->name('verify.resend');
        Route::get('not-verified', 'VerificationController@show')->name('not-verified');

        Route::get('forgot', 'ForgotPasswordController@showLinkRequestForm')->name('forgotForm');
        Route::post('forgot', 'ForgotPasswordController@sendResetLinkEmail')->name('forgot');

        Route::get('reset/{token}', 'ResetPasswordController@showResetForm')->name('resetForm');
        Route::post('reset', 'ResetPasswordController@reset')->name('reset');
    });

    Route::group([
        'prefix' => 'forum',
        'as' => 'forum.',
    ], function () {
        Route::get('', 'ForumThemesController@index')->name('index');
        Route::get('show/{theme}', 'ForumThemesController@show')->name('show');
    });

    Route::group([
        'as' => 'forum.',
    ], function () {
        Route::post('answer/{theme}', 'ForumAnswersController@store')->name('answer');
        Route::post('respond/{theme}', 'ForumRespondController@store')->name('respond');
    });

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('search', 'SearchController@index')->name('search');
    Route::get('project/{project}', 'ProjectController@index')->name('project');

    Route::group([
        'middleware' => ['auth', 'active', 'sp_verified']
    ], function () {

        Route::group([
            'middleware' => ['role:startup,investor']
        ], function () {
            Route::get('profile', 'ProfileController@edit')->name('profile');
            Route::put('profile', 'ProfileController@update')->name('profile.update');
            Route::get('relogin', 'ReloginController@index')->name('relogin');
            Route::get('notifications', 'NotificationsController@index')->name('notifications');

            Route::group([
                'prefix' => 'forum',
                'as' => 'forum.',
            ], function () {
//                Route::get('create', 'ForumThemesController@create')->name('create');
                Route::post('store', 'ForumThemesController@store')->name('store');
                Route::get('edit/{theme}', 'ForumThemesController@edit')->name('edit');
                Route::put('update/{theme}', 'ForumThemesController@update')->name('update');
                Route::delete('destroy/{theme}', 'ForumThemesController@destroy')->name('destroy');
                Route::delete('file/{theme?}', 'ForumThemesController@deleteFile')->name('destroy.file');
            });

            Route::group([
                'as' => 'forum.',
            ], function () {
//                Route::get('answer/edit/{answer}', 'ForumAnswersController@edit')->name('answer.edit');
//                Route::put('answer/update/{answer}', 'ForumAnswersController@update')->name('answer.update');
                Route::get('answer/destroy/{answer}', 'ForumAnswersController@destroy')->name('answer.destroy');
                Route::get('answer/file/{answer?}', 'ForumAnswersController@deleteFile')->name('answer.destroy.file');
            });

            Route::group([
                'prefix' => 'projects',
                'as' => 'projects.',
            ], function () {
                Route::get('', 'ProjectsController@index')->name('index');
                Route::get('create', 'ProjectsController@create')->name('create');
                Route::post('store', 'ProjectsController@store')->name('store');
                Route::get('show/{project}', 'ProjectsController@show')->name('show');
                Route::get('edit/{project}', 'ProjectsController@edit')->name('edit');
                Route::put('update/{project}', 'ProjectsController@update')->name('update');
                Route::delete('destroy/{project}', 'ProjectsController@destroy')->name('destroy');

                Route::delete('image/{project?}', 'ProjectsController@deleteImage')->name('destroy.image');
                Route::delete('logo/{project?}', 'ProjectsController@deleteLogo')->name('destroy.logo');
                Route::delete('presentation/{project?}', 'ProjectsController@deletePresentation')->name('destroy.presentation');
            });

            Route::group([
                'prefix' => 'investments',
                'as' => 'investments.',
            ], function () {
                Route::get('', 'InvestmentsController@index')->name('index');
                Route::post('invest/{project}', 'InvestmentsController@invest')->name('invest');
                Route::get('show/{investment}', 'InvestmentsController@show')->name('show');
                Route::delete('destroy/{investment}', 'InvestmentsController@destroy')->name('destroy');
                Route::get('edit/{investment}', 'InvestmentsController@edit')->name('edit');
                Route::put('update/{investment}', 'InvestmentsController@update')->name('update');
            });
        });

        Route::group([
            'middleware' => ['role:investor']
        ], function () {
            Route::group([
                'prefix' => 'favorites',
                'as' => 'favorites.',
            ], function () {
                Route::get('', 'FavouritesController@index')->name('index');
                Route::patch('add/{project?}', 'FavouritesController@add')->name('add');
            });
        });

    });

});
