<?php

Route::group([
    'domain' => config('app.domain'),
    'namespace' => 'Site',
    'as' => 'site.'
], function () {
    Route::fallback('ErrorController@notFound')->name('404');

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('page/{page}', 'PageController@index')->name('page');
//    Route::get('labs/{page}', 'LabsController@show')->name('labs');
    Route::get('vacancy', 'VacancyController@index')->name('vacancy.index');
    Route::post('vacancy/respond', 'VacancyController@respond')->name('vacancy.respond');
    Route::get('vacancy/{page}', 'VacancyController@show')->name('vacancy');
//    Route::get('videos', 'VideosController@index')->name('video.index');
    Route::get('search', 'HomeController@search')->name('search.index');
//    Route::get('partners', 'PartnersController@index')->name('partners');
    Route::get('news', 'NewsController@index')->name('news.index');
    Route::get('news/{page}', 'NewsController@show')->name('news.show');
    Route::get('project', 'ProjectController@index')->name('project.index');
    Route::get('project/{project}', 'ProjectController@show')->name('project.show');


//    Route::get('proof/{reading}', 'ProofReadingController@show')->name('proof.show');
//    Route::post('proof', 'ProofReadingController@store')->name('proof.store');

    Route::post('contact-us', 'HomeController@contactUs')->name('contact-us');
    Route::post('subscribe', 'HomeController@subscribe')->name('subscribe');
});
